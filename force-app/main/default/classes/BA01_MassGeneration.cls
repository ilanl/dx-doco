/*
*NAME: BA01_MassGeneration
*DESCRIPTION: GENERATION BATCH
*MAKE SURE ONLY TO CALL IT WITH BATCH SIZE = 1 to avoid callout problems.
*/
global class BA01_MassGeneration implements Database.Batchable<Id>, Database.Stateful, Database.AllowsCallouts{
	public string mailBodySubject;
	private map<id,string> recordsFileName;
	private static final string END_POINT;
	private static final Decimal mass_Generation_Expiration_Period;
	private static final id MassGenerationRTId, ImageGenerationRTId;
	private List<Id> ids;
	private Map<Id, String> nameMap;
	private Map<Id, Id> lookupsMap;
	private integer signal;
	private boolean isApiCall;
	private Id jobId;
	private string format;
	private Ctrl18_MassGenerate ctrl18;
	private Form__c form;
	private Mass_Generation_Execution__c Report;
	private Integer generatedItems = 0;
	private boolean combine;
	private string lookupString;
	private string sessionId;
	private Integer i;
	/** CHANNELS **/
	private boolean download;
	private boolean document;
	private boolean attachment;
	private boolean mail;
	private boolean link;
	private boolean chatter;
	/** END OF CHANNELS **/
	private Id templateId;

	//Static initiation
	static{
	    Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
	    END_POINT = myConf.Server_URL__c+'/api/render';
	    mass_Generation_Expiration_Period = myConf.Mass_Generation_Expiration_Period__c;
		String massGenerationType = ''+Mass_Generation_Execution__c.SobjectType;
		for(RecordType rt : [Select Id, developerName From RecordType Where sobjectType=:MassGenerationType]){
			if(rt.DeveloperName=='Generation_Asynchonous')
				ImageGenerationRTId = rt.Id;
			else if(rt.DeveloperName=='Mass_Generation')
				MassGenerationRTId = rt.Id;
		}
  	}

	/** CONSTRUCTORS **/
	public BA01_MassGeneration(Set<Id> sobjectIds, Id formId, string format, boolean isApiCall, string sessionId, boolean download, boolean document, boolean attachment, boolean mail, boolean link, boolean chatter, boolean combine, string lookupStr, Id templateId){
		ids = new List<Id>(sobjectIds);
		this.sessionId = sessionId;
		this.isApiCall = isApiCall;
		this.format = format;
		this.download = download;
		this.document = document;
		this.attachment = attachment;
		this.link = link;
		this.mail = mail;
		this.chatter = chatter;
		this.combine = combine;
		this.form = [Select Form_Id__c, name__c ,Main_Object__c ,Default_File_Name__c From Form__c Where id=:formId limit 1];
		recordsFileName = new map<id,string>();
		String queryStr ='select '+this.form.Default_File_Name__c+' ,id from ' + this.form.Main_Object__c +' Where id in :sobjectIds';
		for(SObject  sobj: Database.query(queryStr)){
			recordsFileName.put((id)sobj.get('id'),(String)sobj.get(this.form.Default_File_Name__c));
		}
		
		this.lookupString = lookupStr;
		this.templateId = templateId;
		nameMap = new Map<Id, String>();
		lookupsMap = new Map<Id, Id>();
		fillMaps(sobjectIds, ids.get(0).getSobjectType(),findNameField(ids.get(0).getSobjectType().getDescribe()),lookupString);
		
	}
	
	public BA01_MassGeneration(Ctrl18_MassGenerate ctrl18, string sessionId){
		this(ctrl18.ids, ctrl18.formId, ctrl18.format, false, sessionId, ctrl18.download, ctrl18.document, ctrl18.attachment, ctrl18.mail, ctrl18.link, ctrl18.chatter, ctrl18.combine, ctrl18.lookupStr, ctrl18.selectedTemplateId);
		this.ctrl18 = ctrl18;
	}

	public BA01_MassGeneration(Set<Id> sobjectIds, Id formId, string format, boolean isApiCall, string sessionId, RenderAPI.Mode aMode, boolean combine, string lookupStr, Id templateId){
		this( sobjectIds,  formId,  format,  isApiCall,  sessionId,  ((''+aMode)=='Download') , ((''+aMode)=='document')  , ((''+aMode)=='attachment') , ((''+aMode)=='mail') , ((''+aMode)=='link') , ((''+aMode)=='chatter') ,  combine,  lookupStr,  templateId);
	
	}
	/*BATCH GLOBAL METHODS */
	global Iterable<Id> start(Database.BatchableContext bc){
		signal = 1;
		i=0;
		jobId = bc.getJobId();
		report = createReport(ids.size());
		insert report;
		return ids;
	}

	global void execute(Database.BatchableContext bc, List<Id> scope){
		List<Mass_Execution_Error__c> errors = new List<Mass_Execution_Error__c>();
		for(Id sobjectId : scope){
			if(++i == ids.size())//last item
				signal = 3;
			generate(sobjectId, errors);
			if(signal == 1)//continue item
				signal = 2;
		}
		report.Number_Generated_Items__c = ++generatedItems;
		update report;
		if(!errors.isEmpty()){
			insert errors;
		}

	}


	global void finish(Database.BatchableContext bc){
		if(!needExecuteFinishFunction()){
			report.Completed__c = true;
			update report;
		}
	}

	//To crete links
	public void generateLink(){
		List<Signature__c> sugnatureList = new List<Signature__c>();
		if(lookupsMap != null && !lookupsMap.isEmpty()){
			for(Id sObjectID:lookupsMap.keySet()){
				sugnatureList.add(RenderAPI.createLink((integer)form.form_Id__c, sObjectID, lookupsMap.get(sObjectID), chatter, false));
			}
			insert sugnatureList;
		}
	}


	/**PRIVATE METHODS **/
	//To build the relevent maps
	private void fillMaps(Set<Id> sobjectIds, Schema.sobjectType stype, string fieldName, string lookupField){
		List<String> fields = new List<String>{fieldName};
		boolean hasLookupField = (lookupField != null && lookupField.length() > 0);
		if(hasLookupField){
			fields.add(lookupField);
		}
		string soql = 'Select '+String.join(fields, ', ')+' From '+sType+' Where id in :sobjectIds';
		for(Sobject sobj : Database.query(soql)){
			nameMap.put((Id)sobj.get('Id'), ''+sobj.get(fieldName));
			if(hasLookupField){
				lookupsMap.put((Id)sobj.get('Id'),getFieldValue(sobj, lookupField));
			}
		}
	}

	//to check if we need to use the async batch process
	private boolean needExecuteFinishFunction(){
		return (combine || (!combine && (download || document)));
	}

	//To get the id of the given field name
	private Id getFieldValue(Sobject sobj, string field){
		List<String> fields = field.split('\\.');
		Sobject o = sobj;
		Integer i = 0;
		for(String f : fields){
			if(o == null)
				break;
			if(i++ == fields.size()-1)
				return (Id)(o.get(f));
			else
				o = o.getSobject(f);
		}
		return null;
	}

	//To find the name field of the given object
	private string findNameField(Schema.DescribeSObjectResult sobjectDescribe){
		for (Schema.SObjectField field : sobjectDescribe.fields.getMap().values()){
			if(field.getDescribe().isNameField())
				return field.getDescribe().getName();
		}
		return null;
	}

	//To create a render request
	private SchemaFactory.RenderRequest createRenderRequest(Id SobjectId){
		SchemaFactory.Form f = new SchemaFactory.Form(SobjectId);
		f.setForm(form.Id);
		f.setFormat(''+format);
		f.setFileName(generateFileName(sobjectId));
		f.setIsApiCall(isApiCall);
		SchemaFactory.RenderRequest ret = f.createRenderRequestWithSessionId(sessionId);
		if(needExecuteFinishFunction())
			if(signal != 3){
				ret.setRenderBatchData(jobId, signal, (combine)?2:1);
			} 
			else {
				ret.setRenderBatchData(jobId, signal, (combine)?2:1, mass_Generation_Expiration_Period);
			}
		return ret;
	}

	//to generate
	private void generate(Id sobjectId, List<Mass_Execution_Error__c> errors){
		Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(END_POINT);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        string body = JSON.serialize(createRenderRequest(sobjectId));
        Utils.log('BODY',body);
        req.setBody(body);
        req.setTimeout(60000);
		try{
			HttpResponse res = h.send(req);
			Utils.log('REPONSE',res.getBody());
			if(res.getStatusCode() != 200){
				throw new SchemaFactory.RenderException('4003', res.getStatus()+' ('+res.getStatusCode()+')', (Integer)form.Form_Id__c);
			}
			SchemaFactory.RenderResponse response = (SchemaFactory.RenderResponse)JSON.deserialize(res.getBody(), SchemaFactory.RenderResponse.class);
			if(response.ErrorID == null || response.ErrorId == '0'){//success
				if(mail||chatter||attachment){
					saveAndMailExecution(sobjectId, response.FileURL, generateFileName(sobjectId));
				}
			}
			else{//error
				throw new SchemaFactory.RenderException(response.ErrorID, response.MessageId, (Integer)form.Form_Id__c);
			}
		}
		catch(System.EmailException e){
			system.debug(Logginglevel.ERROR,'***exception message: ' +e);
			errors.add(addError(e.getMessage(), sobjectId));
		}
		catch(SchemaFactory.RenderException re){
			string errorLink ='<a href=\"https://www.docomotion.com/support/complete-docomotion-guide/error-messages/#Error'+re.errorNumber+'\">for more info click here.</a>';
			//errors.add(addError(re.getMessage(), sobjectId));
			errors.add(addError( re.errorNumber+' '+errorLink,sobjectId));
    	}
    	catch(Exception e) {
    		system.debug('***EXCEPTION: '+e);
		    errors.add(addError(e.getMessage(), sobjectId));    
		}
	}
	//to build the file name of generated records
	private string generateFileName(Id sobjectId){
		string name = '';
		if(nameMap.containsKey(sobjectId)){
			name = nameMap.get(sobjectId);
		}
		name += '.'+format;
		name = name.replaceAll('\\s|\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
		name = name.replaceAll('\\s\\s+','_');
		return name;
	}

	//To save attachments and chatter and sending mail
	private void saveAndMailExecution(Id sobjectId, string fileUrl, string fileName){
		Blob b = getDocument(fileUrl);
		if(attachment){
			RenderAPI.saveAttachment(b, (Integer)form.Form_Id__c, format, SobjectID, fileName);
		}
		if(chatter){
			RenderAPI.saveChatter(b, (Integer)form.Form_Id__c, format, SobjectID, fileName);
		}
		if(mail && lookupsMap.containsKey(sobjectId)){
			if(format == 'HTML4S'){
				//send mail with the body
				//1.a need to check if it's possible to send a mail (< limit)
				if(Limits.getEmailInvocations() >= Limits.getLimitEmailInvocations())
					throw new Utils.CustomException('You\'ve exceed your email sending limit.');
				//1.b check if there is a recipient
				if(lookupsMap.get(sobjectId) == null)
					throw new Utils.CustomException('There isn\'t any recipient for the desired lookup.');
				//2. build the mail
				Messaging.SingleEmailMessage mail = createEmail(sobjectId, b, true );
				//3. send the mail
				List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {mail});
				if(!emailResults.IsEmpty() && !emailResults.get(0).isSuccess()){
					throw new Utils.CustomException('Email fails: '+emailResults.get(0).getErrors());
				}
			}
			else{
				//send mail with attached file
				//1.a need to check if it's possible to send a mail (< limit)
				if(Limits.getEmailInvocations() >= Limits.getLimitEmailInvocations())
					throw new Utils.CustomException('You\'ve exceed your email sending limit.');
				//1.b check if there is a recipient
				if(lookupsMap.get(sobjectId) == null)
					throw new Utils.CustomException('There isn\'t any recipient for the desired lookup.');
				//2. build the mail
				Messaging.SingleEmailMessage mail = createEmail(sobjectId, b, false);
				//3. send the mail
				List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {mail});
				if(!emailResults.IsEmpty() && !emailResults.get(0).isSuccess()){
					throw new Utils.CustomException('Email fails: '+emailResults.get(0).getErrors());
				}
			}
		}
	}

	//to create a SingleEmailMessage
	private Messaging.SingleEmailMessage createEmail(Id SobjectId, Blob b, boolean HTML4S){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(lookupsMap.get(sobjectId));
		if(!HTML4S){
			string fileName = recordsFileName.get(SobjectId);
			string fileExtention ;
			Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
			email.setTemplateID(templateId);
			efa.setBody(b);
			string ContentTypeFormat = null;
			if(format == 'DOCX'){
				ContentTypeFormat = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
				fileExtention ='docx';
			}
			else if(format == 'HTML'){
				ContentTypeFormat = 'text/html';
				fileExtention = 'html';
			}
			else if(format == 'PDF'){
				ContentTypeFormat = 'application/pdf';
				fileExtention = 'pdf';
			}
			efa.setContentType(ContentTypeFormat);
			fileName+='.'+fileExtention;
			fileName = fileName.replaceAll('\\s|\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
			fileName = fileName.replaceAll('\\s\\s+','_');
			efa.setFileName(fileName);
			email.setFileAttachments(new list<Messaging.Emailfileattachment>{efa});
			if(canSetWhatId(sobjectId.getSobjectType()))
				email.setWhatId(sobjectId);
		}
		else{
			//email.setSubject('Docomotion '+form.name__c+' ('+form.form_Id__c+') generated mail');
			email.setSubject(mailBodySubject);
			email.setHtmlBody(b.toString().unescapeJava());
			email.setCharset('UTF-8');
		}
		email.setSaveAsActivity(false);
		return email;
	}

	/*private Messaging.SingleEmailMessage createEmail(Id SobjectId){
		return createEmail(sobjectId, null);
	}*/

	//To get the blob of the given document (url)
	private Blob getDocument(string url){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
		if(url==null)
			return null;
        string first = url.substringBeforeLast('/');
		string fileName = url.substringAfterLast('/');
		string urlDoc2 = first + '/'+EncodingUtil.urlEncode(fileName,'UTF-8');
        req.setEndpoint(urlDoc2);
        req.setMethod('GET');
        req.setTimeout(60000);
        try{
            HttpResponse res = h.send(req);
            if(res.getStatusCode() == 200)
                return res.getBodyAsBlob();
            else
                return null;
        }
        catch(Exception e){
        	Utils.log('***Ex: ',e.getMessage());
        	if (Test.isRunningTest()) 
            	return Blob.valueof('test');
        	return null;
    	}  
    }

	//To creeate the Mass_Generation_Execution object
	private Mass_Generation_Execution__c createReport(Integer numberToGenerate){
		Mass_Generation_Execution__c ret = new Mass_Generation_Execution__c(Total_Number_of_Items__c=numberToGenerate, Job_Id__c=JobId, Form__c=form.Id, Object_Name__c = ''+Ids.get(0).getSobjectType());
		ret.document__c = document;
		ret.download__c = download;
		ret.RecordTypeId = MassGenerationRTId;//TO DO: Add the logics
		return ret;
	}

	//To add the execution errors
	private Mass_Execution_Error__c addError(string Error, Id objId){
		Mass_Execution_Error__c ret = new Mass_Execution_Error__c(Mass_Generation_Execution__c = report.Id, Related_To__c=objId, Error__c=error);
		if(nameMap.containsKey(objId))
			ret.Record_Name__c = nameMap.get(objId);
		return ret;
	}

	//to check if's possible to set what id in a email
	private boolean canSetWhatId(Schema.SobjectType sobjectType){
		return ((''+sobjectType) == 'Account' || (''+sobjectType) == 'Asset' || (''+sobjectType) == 'Campaign' || (''+sobjectType) == 'Case' || (''+sobjectType) == 'Contract' || (''+sobjectType) == 'Opportunity' || (''+sobjectType) == 'Order' || (''+sobjectType) == 'Product' || (''+sobjectType) == 'Solution' || sobjectType.getDescribe().isCustom());
	}

}