public with sharing class CTRL00_Config {

	public Boolean editUrlMode{get;set;}
  	public config__c config{get;set;}
  	public string clientId{get;private set;}
  	public string callBackURL{get;private set;}
  	public Map<string,object> usageMap{get;private set;}
	
	//Google OAuth Status
	public string gStatus{get;private Set;}
	public Boolean authorizeVisible{
		get{
			return !(gCall == null || gCall.gSettings == null || gCall.gSettings.Refresh_Token__c==null||myConf.Google_Drive_Mode__c=='User');
		}
		private Set;
	}
  
  	public Config__c myConf{get;set;}

	//For the link
	public Integer hours{get;set;}
	public Integer minutes{get;set;}
	public Integer hoursMassGeneration{get;set;}
	public Integer minutesMassGeneration{get;set;}
	
	private GoogleCall gCall;
	private Config__c myConfCopy;
	private string NameSpace;

  	public CTRL00_Config(){
    	initConf();
		NameSpace = Utils.getNameSpace();
      	clientId = '3MVG9A_f29uWoVQugzvcyMph8CjdbNOpBDvwaGZWgv1MYOpkzncoHc8oLJ0vdb4Ujlakzchvf.qCUh8TzilfA';
      	callBackURL='https://oapi6.docomotioninc.com/verification';
  		editUrlMode = false;
		gStatus = 'Pending';
		gCall = new GoogleCall(true);
  	} 

  	private void initConf(){
    	myConf = Config__c.getInstance('Docomotion Default Settings');
    	if(myConf == null)
      		myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://salesforce56.docomotioninc.com', google_drive__c=false, Signature_URL__c='https://oapi56.docomotioninc.com');
		myConfCopy = myConf.clone();
		Double expirationTime = (myconf.Signature_Expiration_Time__c !=null) ? myconf.Signature_Expiration_Time__c:168;
		hours = expirationTime.intValue();
		minutes = (60 * (expirationTime - hours)).intValue();
        Double GenerationExpirationTime =(myconf.Mass_Generation_Expiration_Period__c != null ) ? myconf.Mass_Generation_Expiration_Period__c:96;
        hoursMassGeneration = GenerationExpirationTime.intValue();
        minutesMassGeneration = (60 * (GenerationExpirationTime - hoursMassGeneration)).intValue();
        
        
        
  	}
   

	public pagereference init(){
    	if(ApexPages.currentPage().getParameters().get('authorize') == String.valueOf(1) ){}
  
      	if(ApexPages.currentPage().getParameters().get('authorize') == String.valueOf(0) ){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed'));
      	}
		saveCredentials();
      	return null;
	}
  
  	public Pagereference editUrl(){
    	editUrlMode = true;
        return null;
  	}
  
  	public Pagereference cancelUrl(){
    	editUrlMode = false;
		return null;
  	} 
   
  
  	public pagereference auth(){  
        string url = 'https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=' + ClientID + '&redirect_uri=' + CallBackURL;         
        if(utils.isSandbox())
            url = 'https://test.salesforce.com/services/oauth2/authorize?response_type=code&client_id=' + ClientID + '&redirect_uri=' + CallBackURL + '&state=1';
        else if (!Utils.isManagedPackage())
            url = 'https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=' + ClientID + '&redirect_uri=' + CallBackURL;
          
          Pagereference p = new pagereference(url);
        return p;
        
		return null;
  	}
  
	public void save(){
  		try{
      		DML_CustomExtension.upsertObj(myConf);
      		Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.Correctly_saved));
    	}
    	catch(Exception e){
      	Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Error_saving));
    	}
  	}
  
	public void showUsage(){
		Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
    	string END_POINT = myConf.Server_URL__c+'/api/stat/org/';
    	Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(END_POINT+UserInfo.getOrganizationId());
        req.setMethod('GET');
        req.setTimeout(60000);
        Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        try{
            HttpResponse res = h.send(req);
            Utils.log('REPONSE',res.getBody());
            String jsonStr = res.getBody();
            Map<String,Object> jsonMap = (Map<String,Object>)JSON.deserializeUntyped(jsonStr);
			Boolean isExistingOrg = (Boolean)jsonMap.get('IsExistingOrg');
            if(!isExistingOrg){
            	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please contact Docomotion support.'));
            	return;
            }
            usageMap = new Map<string,object>();
            usageMap.put('MonthlyRenderCount', jsonMap.get('MonthlyRenderCount'));
        }
        catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        	return;
		}
	}

	//Google Authorize. To init the OAuth flow with google.
	public PageReference googleCredentials(){
		return gCall.googleCredentials('https://'+(Utils.isSandbox()?'test':'login')+'.salesforce.com/apex/'+NameSpace+'VF00_Config');
	}

	//To check the status of the OAuth
	public void checkStatus(){
		if (gCall == null || gCall.gSettings == null || gCall.gSettings.Refresh_Token__c==null)
			gStatus = 'Failed';
		else{
			gStatus = (gCall.refreshAndSaveAccessToken()?'Success':'Failed');
		}
	}

	public PageReference saveGoogle(){
		PageReference ret = null;
		//Init google OAuth flow
		System.debug(myConf);
		System.debug(myConfCopy);
		if (myConf.google_drive__c != myConfCopy.google_drive__c || myConf.Google_Drive_Mode__c != myConfCopy.google_drive_Mode__c)
		{
			if(myConfCopy.google_drive_mode__c == 'Organization'){//There were organization settings so we need to revoke
				gCall.deleteSettings();//revoke and delte current ORG settings
			}
			if(myConfCopy.google_drive__c)//Google was activated, so we need to delete all settings
				DML_CustomExtension.deleteObj([Select Id From GoogleAPI_Settings__c limit 50000]);
			if(!myConf.google_drive__c)
				myConf.Google_Drive_Mode__c= null;
			System.debug(myConf);
			if (myConf.google_drive__c && myConf.Google_Drive_Mode__c == 'Organization'){
				ret = googleCredentials();
			}
			//save the settings
			DML_CustomExtension.upsertObj(myConf);
			initConf();
			gCall = new GoogleCall(true);
		}
		return ret;
	}

	public PageReference googleRevoke(){
		myConf.google_drive__c = false;
		gCall.deleteSettings();
		gCall = null;
		DML_CustomExtension.upsertObj(myConf);
		return null;
	}

	public void saveExpiration(){
		try{
			Decimal h = hours;
			Integer m = minutes;
			h += (minutes / 60.0);
			myConf.Signature_Expiration_Time__c = h;
			update myConf;
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}

	}
	public void saveMassGenerationExpiration(){
		try{
			Decimal h = hoursMassGeneration;
			Integer m = minutesMassGeneration;
			h += (minutesMassGeneration / 60.0);
			myConf.Mass_Generation_Expiration_Period__c = h;
            System.debug('@@@@@@@@@@ myConf.Mass_Generation_Expiration_Period__c '+myConf.Mass_Generation_Expiration_Period__c);
			update myConf;
		}
		catch(Exception e){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}

	}

	public void saveCredentials(){
		if(ApexPages.currentPage().getParameters().containsKey('error')){
			string error = ApexPages.currentPage().getParameters().get('error');
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Google Drive Error: '+error));
		}
		else if(ApexPages.currentPage().getParameters().containsKey('code')){
			string code = ApexPages.currentPage().getParameters().get('code');
			if (gcall != null){
				gCall.saveCredentials(code, 'https://'+(Utils.isSandbox()?'test':'login')+'.salesforce.com/apex/'+NameSpace+'VF00_Config');
				gCall.saveSettings();
			}
		}
	}
 
 }