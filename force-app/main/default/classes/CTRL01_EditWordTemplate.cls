public with sharing class CTRL01_EditWordTemplate {

    public Word_Template__c WordTemplate{get;set;}
    public String FileName{get;set;}
    public String cType{get;set;}
    public transient blob body{get;set;}
    public boolean isCreate{get; private Set;}
    
    public CTRL01_EditWordTemplate(ApexPages.StandardController con){
    	if(!test.isRunningTest())
        	con.addFields(new List<String>{'Name','Document_Id__c','description__c'});
        wordTemplate = (Word_Template__c)con.getRecord();
        isCreate = (wordTemplate.Id == null);
    }
    
    public PageReference save(){
    	set<String> acceptedType = new Set<String>{'application/vnd.ms-word.template.macroEnabled.12'};
        try{
        	if(FileName==null || body == null)
    			throw new Utils.CustomException(Label.Required_File);
    		if(!acceptedType.contains(cType))	
    			throw new Utils.CustomException(Label.VF01_Invalid_Format);
            DML_CustomExtension.upsertObj(wordTemplate);
            boolean updateTemplate = saveAttachment();
            if(updateTemplate)
                DML_CustomExtension.upsertObj(wordTemplate);
            return new PageReference('/'+wordTemplate.id);
        }
        catch(Exception e){
        	ApexPages.addMessages(e);
        }
        return null;
    }
    
    public PageReference cancel(){
    	if(ApexPages.CurrentPage().getParameters().containsKey('retURL'))
    		return new PageReference(ApexPages.CurrentPage().getParameters().get('retURL'));
        return new PageReference('/');
    }
    
    public boolean saveAttachment(){
        if(wordTemplate.Document_Id__c==null){
            //we have to create a feedItem
           	Attachment att = new Attachment();
            att.Body = body;
            att.ParentId = wordTemplate.id;
            att.Name = FileName;
            DML_CustomExtension.insertObj(att);
            wordTemplate.Document_Id__c = att.Id;
            return true;
        }
        return false;
    }
    
}