public with sharing class CTRL02_GenerateDocument {
	private static final String ACTIVATED_STATUS = 'Activated' ; // label.VF02_Activated;
	
	/** PROPERTIES **/
	public SchemaFactory.Form form{get; private set;}
	public String format{get;set;}
	public boolean mail{get;set;}
	public boolean chatter{get;set;}
	public boolean link{get;set;}//for signature
	public Id signatureId{get;private set;}
	public boolean document{get;set;}
	public boolean attachment{get;set;}
	public boolean download{get;set;}
	public boolean gDrive{get;Set;}
	public string instanceURL{get;private set;}
	public Id id{get;private set;}//The Id of the current object
	public String view{get;set;}
	public String urlDoc{get;private set;}
	public Id docId{get;private set;}
	public Boolean generationFailed{get;private set;}
	public String errorNumber{get; private set;}
	public String errorTitle{get;private set;}
	public String errorField{get; private set;}
	public String errorSobject{get;private set;}
	public Id formId{get;set;}
	public String selectedFormVersionStatus	{get;set;}
	public string lookupId{get;set;}
	public String fileName{get;set;}
	public String googleFileName{get;set;}
	public String googlePath{get;set;}
	public Boolean isIC {get ; Set;}
	public String NameSpace{
		get{
			if(NameSpace==null){
				NameSpace = Utils.getNameSpace();
			}
			return NameSpace;
		}
		private set;
	}
	public List<SelectOption> LookupSelectOptions {get ; private Set;}
	public string LookupFieldSelected {get ; Set;}

	//Return true if chatter is enable
	public Boolean chatterEnable{
		get{
			if (chatterEnable==null)
				chatterEnable = Utils.isChatterEnable();
			return chatterEnable;
		}
		private set;
	}

	//Return true if google drive
	public Boolean gDriveEnable{
		get{
			if (gDriveEnable==null){
				Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
				if (myConf == null || myConf.google_drive__c == null)
					gDriveEnable=false;
				else
					gDriveEnable = myConf.google_drive__c;
			}
			return gDriveEnable;
		}
		private set;
	}

	public Boolean gDriveUserEnable{
		get{
			if (gDriveEnable){
				Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
				return (myConf.Google_Drive_Mode__c == 'User');
			}
			return false;
		}
		private set;
	}

	public Boolean needUserDriveAuthorize{
		get{
			Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
			if (gDriveEnable && myConf.Google_Drive_Mode__c == 'User'){
				return gCall.isNew();
			}
			return false;
		}
		private Set;
	}
	
	public boolean isDesignerUser {
		get {
			if (isDesignerUser == null)
				isDesignerUser = Utils.isDesignerUser();
			return isDesignerUser;
		}
		private set;
	}

	public boolean hasForms{
		get {
			if (hasForms == null){
				if (isDesignerUser)
					hasForms = !formLst.isEmpty();
				else{
					hasForms = false;
					for (Form__c form : formLst){
						if (form.current_published_version__c != null){
							hasForms = true;
						}
					}
				}
				
			}
			return hasForms;
		}
		private Set;
	}

	public boolean canWhatId{
		get {
			if(canWhatId == null){
				canWhatId = (sobjectType == 'Account' || sobjectType == 'Asset' || sobjectType == 'Campaign' || sobjectType == 'Case' || sobjectType == 'Contract' || sobjectType == 'Opportunity' || sobjectType == 'Order' || sobjectType == 'Product' || sobjectType == 'Solution' || Id.getSobjectType().getDescribe().isCustom());
			}
			return canWhatId;
		}
		set;
	}

	public string filePath{
		get{
			if (gDriveEnable){
				return gCall.gSettings.path__c;
			}
			return '';
		}
		private Set;
	}

	//For the template
	public List<SelectOption> templatesList{
		get{
			if (templatesList == null)
				templatesList = initTemplatesList();
			return templatesList;
		}
		private Set;
	}
	public String selectedTemplateId{get;Set;}

	public Id defaultForm{get;private Set;}
	public boolean hideInGeneration{get;private Set;}

	public string parentFolderId{get;Set;}
	public string overridingFileId{get;Set;}
	//Google OAuth Status
	public string gStatus{get;private Set;}
	public String sConfigJson{get;private set;}

	/** VARIABLES **/
	private string sobjectType;//The sobjectType
	private List<Form__c> formLst;
	private string formName;
	private Map<String, Utils.LookupPickListValue> lookupPicklistMap;
	private Utils.LookupData lookupdata;
	private GoogleCall gCall;
	private Map<String,String> formDefaultFileName {get;set;}
	
	public String getJsonFormDefaultFileName(){
		return JSON.serialize(formDefaultFileName);
	}
	public void getFormDefaultFileName(){
		if(formDefaultFileName==null){
			formDefaultFileName = new Map<String,String>();
		}
		
		Set<String> defaultFileNameSet = new Set<String>();
		for(Form__c form :formLst){	
			if(form.Default_File_Name__c != null){		
				defaultFileNameSet.add((form.Default_File_Name__c).toLowerCase());
			}
		}
		if(!defaultFileNameSet.isEmpty()){
			buildMap(defaultFileNameSet,formDefaultFileName);
		}
		
	}
	
	private void buildMap(Set<String> defaultFileNameSet, Map<String,String> formDefaultFileName){
		String query = 'Select '+String.join(new List<String>(defaultFileNameSet), ', ');
		
		query += ' From ' +sobjectType +' ' + 'Where Id = \'' + id +'\'' ; 
		System.debug('query:' + query);
		List<Sobject> sobjectFields = (List<Sobject>)(Database.query(query));
		if(!sobjectFields.isEmpty()){
			for(String defaultFileName : defaultFileNameSet){
				if(defaultFileName != null){
					string fileName = String.valueOf(sobjectFields[0].get(defaultFileName));
					system.debug('***fileName: '+fileName);
					if(fileName != null){
						fileName = fileName.replaceAll('\\s|\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
						system.debug(fileName);
						fileName = fileName.replaceAll('\\s\\s+','_');
					}
					formDefaultFileName.put(defaultFileName, fileName);
				}
			}
		}		
	}
	
	/** PUBLIC METHODS **/
	public CTRL02_GenerateDocument(){
		document = false;
		mail = false;
		link = false;
		gDrive = false;
		chatter = false;
		attachment = false;
		//format='PDF';//The default format
        instanceURL = URL.getSalesforceBaseUrl().getHost();//the instance URL
        view='recent';
		if (ApexPages.currentPage().getParameters().containsKey('state')){
			convertStateToId();
		}
		if(ApexPages.currentPage().getParameters().containsKey('id')){
            id = ApexPages.currentPage().getParameters().get('id');
            Schema.SobjectType sobjectType2 = id.getSobjectType();
           // setDefaultName(sobjectType2.getDescribe());
            sobjectType = ''+sobjectType2;
            form = new SchemaFactory.Form(id);
            setFormLst();
            getFormDefaultFileName();
			initPicklistValues(Utils.getContactLookups(sobjectType, id));
			lookupdata = Utils.buildLookupData(sobjectType, id);
			initFormsSettings();
        }
		if (gDriveEnable)
		{
			gStatus = Label.VF02_Status_pending;
			gCall = new GoogleCall();
		}
	}
	
    public string getFormLstJSON(){
    	return JSON.serialize(formLst);
    }
    
    public string getLookupDataJSON(){
    	return JSON.serialize(lookupdata);
    }
	
	public void setFormLst() {
        //grouptemplateList = new List<Group__c>();
        //DML_CustomExtension.isAccessible(Schema.sObjectType.Form__c, new Set<String>{'Id', 'name', 'Description__c', 'Group__c', 'name__c', 'form_Id__c'});
        String soql = 'select Status__c, Default_File_Name__c, Name__c, Id, form_Id__c, description__c, preview_PDF_Att_Id__c, ' + 
						'Allow_Changing_Channel__c, Allow_Changing_File_Name__c, Allow_Changing_Format__c, Default_Channel__c, 	Default_Format__c, Hide_Channel_in_Generation__c, Hide_File_Name_in_Generation__c, Hide_Format_in_Generation__c, Use_Record_Name__c, ' +
						'Allow_Changing_Template__c, 	Default_Template__c, Hide_Template_In_Generation__c, '+
        				'current_published_Version__r.HasPictureSetting__c,current_published_Version__r.contains_interactive_fields__c, current_published_Version__r.contains_interactive_collection__c, current_published_version__r.Preview_PDF_Id__c, Current_Published_Version__r.Version__c,' + 
        				'Current_Activated_Version__r.HasPictureSetting__c,Current_Activated_Version__r.contains_interactive_fields__c, Current_Activated_Version__r.contains_interactive_collection__c, Current_Activated_Version__r.Preview_PDF_Id__c, Current_Activated_Version__r.Version__c, ' +
        				'(Select Subform_version__r.contains_interactive_fields__c, Subform_Version__r.contains_interactive_collection__c From Form_Subform_Associations__r) from Form__c';
    	soql += ' where ((active__c=true and current_published_Version__c != null) or Current_Activated_Version__c != null) and main_object__c=:sobjectType and RecordType.DeveloperName=\'Form\'';
    	if(view.equalsIgnoreCase('recent'))
    		soql += ' Order by LastViewedDate DESC NULLS LAST Limit 10';
    	else if(view.equalsIgnoreCase('All'))
    		soql+= ' Order by Name__c Asc';
    	else if(view.equalsIgnoreCase('last_modified'))
    		soql+= ' Order by LastModifiedDate Desc  Limit 10';
    	formLst = (List<Form__c>)(Database.query(soql));
    	
    	Utils.log('formLst:', formLst);
    }
    
    public void getPreviewUrl(){
    	form.setFormat('PDF');
    	try{
    		getDocumentUrl();
    	}
    	catch(SchemaFactory.RenderException re){
    		generationFailed = true;
    		errorNumber = re.errorNumber;
    		errorTitle = re.errorTitle;
			ExceptionWS eWs = new ExceptionWS(re);
			eWs.setSessionId(UserInfo.getSessionId());
			eWs.send();
    	}
		catch (Utils.DocomotionQueryFieldException qe){
			generationFailed = true;
			errorField = qe.getFieldName();
			errorSobject = qe.getSobject();
			ExceptionWS eWs = new ExceptionWS(qe,form.getFormId());
			eWs.setSessionId(UserInfo.getSessionId());
			eWs.send();
		}
    }
    public void setAsyncProperties(){
    	form.setChanel( attachment, gDrive, download, document, link, chatter, mail, (''+format=='HTML4S'));
    	if(gDrive){
    		if(googlePath != null && googlePath !='')
    			form.googlePath = googlePath ;
	    	else form.googlePath = gCall.gSettings.path__c;
	    	system.debug('@@@@@@@@@@@ form.googlePath = '+form.googlePath);
    	}
    	
    }
    private  Boolean isItAsync(){
    	Form__c cform;
    	for(Form__c f:formLst){
    		if(f.id == formId)
    			cform = f ;	
    	}
		id vId ;
		system.debug(logginglevel.info,'@@@@@@@@@@@  cform '+cform);
		if(cform == null)return false;
		if(cform.Status__C =='Published' )
			vId = cform.Current_Published_Version__C;
		else if(cform.Status__C =='Active')
			vId = cform.Current_Activated_Version__C;
		Integer Pcount = [select COUNT() from Pictures_Setting__c where Version__c = :vId];
		if(Pcount>0)
			return true;
		return false;	
		
	}
    public void generateDocument(){
    	
		if(link){
			Map<String,String> pageParameters = apexPages.currentPage().getParameters();
			Id templateId = (pageParameters.containsKey('templateId') ? (Id)(pageParameters.get('templateId')) : formId);
			String versionStatus = (pageParameters.containsKey('versionStatus') ? pageParameters.get('versionStatus') : selectedFormVersionStatus);
    		Boolean isActivated = (versionStatus == ACTIVATED_STATUS);
			Signature__c s = new Signature__c();
			s.Form__c = templateId;
			s.Record_ID__c = Form.getSobjectId();
			s.chatter__c = chatter;
			s.published__c = !isActivated;
			if(lookupId != null && lookupId != '' && Utils_Type.isId(lookupId)){
				Id lId = (Id)lookupId;
				if(lId.getSobjectType() == Contact.sobjectType)
					s.Signer__c = lId;
				else if(lId.getSobjectType() == Lead.sobjectType)
					s.Signer_Lead__c = lId;
			}
			DML_CustomExtension.insertObj(s);
			signatureId = s.Id;
		}
		else{
    		errorNumber=null;
    		errorTitle=null;
    		if(document && (!Schema.sObjectType.Document.isCreateable())){
    			generationFailed = true;
    			errorNumber='4000';
    			errorTitle='No Creation right on Document';
    			return;
    		}
    		form.setFormat(format);
    		form.setFileName(fileName);
    		if(lookupId != null && lookupId != '' && Utils_Type.isId(lookupId))
    			form.setWhoId(lookupId);
    		try{
    			if(!getDocumentUrl())return;
    		}
    		catch(SchemaFactory.RenderException re){
    			generationFailed = true;
    			errorNumber = re.errorNumber;
    			errorTitle = re.errorTitle;
				ExceptionWS eWs = new ExceptionWS(re);
				eWs.setSessionId(UserInfo.getSessionId());
				eWs.send();
    		}
			catch (Utils.DocomotionQueryFieldException qe){
				generationFailed = true;
				errorField = qe.getFieldName();
				errorSobject = qe.getSobject();
				ExceptionWS eWs = new ExceptionWS(qe,form.getFormId());
				eWs.setSessionId(UserInfo.getSessionId());
				eWs.send();
			}
    		if(generationFailed)
    			return;
    		if(document || attachment || chatter || gDrive){
    			Blob b = getBlobFromPdfUrl(!download);
				GoogleUtils.GoogleFile gFile = null;
	    		try{
					if (gDrive)
						gFile = saveDrive(b);	
		    		if(document)
		    			saveDocument(b);
		    		if(attachment)
		    			saveAttachment(b);
		    		if(chatter){
						if (gFile != null)
							saveChatter(b, gFile);
						else
		    				saveChatter(b);
					}
	    		}
	    		catch(DMLException de){
	    			generationFailed = true;
	    			if(de.getDmlType(0) == StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY){
	    				errorNumber='4001';
	    				errorTitle = 'No edit right on sobject';
	    			}
	    			else{
	    				errorNumber='4002';
	    				errorTitle=''+de.getDmlType(0);	
	    			}
					ExceptionWS eWs = new ExceptionWS(form.getFormId(),errorNumber+ ' - '+errorTitle);
					eWs.setSessionId(UserInfo.getSessionId());
					eWs.send();
	    			return;
	    		}	
    		}
			
		}
		string successMessage = Label.VF02_Correctly_Saved;
		if (isIC != null && isIc && !form.hasWhoId() && !link)
			successMessage+= ' '+Label.VF02_received_feedback_anonymous;
		Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, successMessage));
    }

	public void updateSelectedLookup(){
		Utils.LookupPickListValue selected = lookupPicklistMap.get(LookupFieldSelected);
        if (selected != null)
		  lookupdata = Utils.buildLookupData(selected.sobjecttype, selected.sobjectId);
	}

	//Google Authorize. To init the OAuth flow with google.
	public PageReference googleCredentials(){
		system.debug('formId: '+formId);
		Map<String, id> stateMap = new Map<String,id>();
		SaveConfig sConfig = new SaveConfig();
		sConfig.id = id;
		sConfig.view = view;
		sConfig.LookupFieldSelected = LookupFieldSelected;
		sConfig.defaultForm = formId;
		sConfig.contactId = Utils_Type.isId(lookupId)?lookupId:null;
		sConfig.fileName = fileName;
		sConfig.format = format;
		sConfig.selectedTemplateId = selectedTemplateId;
		sConfig.channel.put('document',document);
		sConfig.channel.put('mail',mail);
		sConfig.channel.put('link', link);
		sConfig.channel.put('google-drive', gdrive);
		sConfig.channel.put('chatter', chatter);
		sConfig.channel.put('attachment', attachment);
		string jsonStr = JSON.serialize(sConfig);
		Utils.log('jsonStr', jsonStr);
		return gCall.googleCredentials('https://'+(Utils.isSandbox()?'test':'login')+'.salesforce.com/apex/'+NameSpace+'VF02_GenerateDocument',Utils.encodeBase64(jsonStr));
	}

	//To save credentials when the page returns from google oauth process
	public void saveCredentials(){
		if(ApexPages.currentPage().getParameters().containsKey('error')){
			string error = ApexPages.currentPage().getParameters().get('error');
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Google Drive Error: '+error));
		}
		else if(ApexPages.currentPage().getParameters().containsKey('code')){
			string code = ApexPages.currentPage().getParameters().get('code');
			gCall.saveCredentials(code, 'https://'+(Utils.isSandbox()?'test':'login')+'.salesforce.com/apex/'+NameSpace+'VF02_GenerateDocument');
			gCall.saveSettings();
		}
	}

	public pagereference init(){
    	if(ApexPages.currentPage().getParameters().get('authorize') == String.valueOf(1) ){}
  
      	if(ApexPages.currentPage().getParameters().get('authorize') == String.valueOf(0) ){
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failed'));
      	}
		saveCredentials();
      	return null;
	}

	//To check the status of the OAuth
	public void checkStatus(){
		if (gCall == null || gCall.gSettings == null || gCall.gSettings.Refresh_Token__c==null)
			gStatus = Label.VF02_Status_Failed;
		else{
			gStatus = (gCall.refreshAndSaveAccessToken()?Label.VF02_Status_Success:Label.VF02_Status_Failed);
		}
	}

	//Revoke Google Oauth
	public PageReference googleRevoke(){
		gCall.deleteSettings();
		gCall = null;
		return null;
	}

	
	/** PRIVATE METHODS **/
	private Blob getBlobFromPdfUrl(){
		return getBlobFromPdfUrl(true);
	}	
	private Blob getBlobFromPdfUrl(boolean toDelete){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
		if(urlDoc==null)
			return null;
		string first = urlDoc.substringBeforeLast('/');
		string fileName = urlDoc.substringAfterLast('/');
		string urlDoc2 = first + '/'+EncodingUtil.urlEncode(fileName,'UTF-8');
		system.debug('***fileURL: '+urlDoc2);
        req.setEndpoint(urlDoc2+(toDelete?'':'/0'));
        req.setMethod('GET');
        req.setTimeout(60000);
        try{
            HttpResponse res = h.send(req);
			system.debug('***RESPONSE: '+res);
            if(res.getStatusCode() == 200)
                return res.getBodyAsBlob();
            else
                return null;
        }
        catch(Exception e){
        	if (Test.isRunningTest()) 
            	return Blob.valueof('test');
        	return null;
    	}
	}	
	
    private Boolean getDocumentUrl(){
    	
    	generationFailed = false;
    	Map<String,String> pageParameters = apexPages.currentPage().getParameters();
    	Id templateId = (pageParameters.containsKey('templateId') ? (Id)(pageParameters.get('templateId')) : formId);
    	Utils.log('templateId or formId: ', templateId);
    	
    	String versionStatus = (pageParameters.containsKey('versionStatus') ? pageParameters.get('versionStatus') : selectedFormVersionStatus);
    	Boolean isActivated = (versionStatus == ACTIVATED_STATUS);
    	Utils.log('getDocumentUrl', 'versionStatus: ' + versionStatus + ', isActivated: ' + isActivated);
        form.setForm(templateId, isActivated);
        //form.setWhoId(lookupId);
        Boolean isItAsync = isItAsync();
        if(isItAsync){
        	setAsyncProperties();
        	string ffi = form.getFormUrl(isItAsync);
        	SchemaFactory.RenderRequest rr = form.createRenderRequest(ffi, UserInfo.getSessionId(),true);
        	/*ID jobID  = System.EnqueueJob(new AsyncGenerationRequest(JSON.serialize(rr)));*/
        	ID jobID  = System.EnqueueJob(new AsyncGenerationRequest(rr));
        	
        	return false;
        }
        urlDoc = form.getFormUrl(isItAsync);
        if(urlDoc == null){
        	generationFailed = true;
        }
        return true;
    } 
    
    private string getFormatExtension(){
    	if(format=='HTML4S')
    		return 'html';	
    	else
    		return format.toLowerCase();			
    }
    
    private string saveAttachment(Blob b) {
        Attachment att=new Attachment();
        att.Body = b;
        att.Name = fileName + '.'+getFormatExtension();
        att.parentId = form.getSobjectId();
        //To DO: add contentType
        DML_CustomExtension.insertObj(att);
        return att.Id;
    }

    private string saveDocument(Blob b) {
        Document doc=new Document();
        doc.Body = b;
        doc.folderId = UserInfo.getUserId();
        doc.Name = fileName + '.'+getFormatExtension();
        DML_CustomExtension.insertObj(doc);
        docId = doc.id;
        return doc.Id;
    }

	//To generate simple chatter file
    private void saveChatter(Blob b) {
        FeedItem fi = new FeedItem();
		fi.ContentData = b;
        fi.ParentId = form.getSobjectId();
        fi.Type = 'ContentPost';
        fi.ContentFileName = fileName + '.'+getFormatExtension();
        DML_CustomExtension.insertObj(fi);
    }

	//to generate chatter file with google drive
	private void saveChatter(Blob b, GoogleUtils.GoogleFile gFile) {
        FeedItem fi = new FeedItem();
        fi.ContentData = b;
        fi.ParentId = form.getSobjectId();
		fi.linkUrl = gFile.webViewLink;
		fi.Body = gFile.webViewLink;
		fi.ContentFileName = fileName + '.'+getFormatExtension();
        DML_CustomExtension.insertObj(fi);
    }

	private GoogleUtils.GoogleFile saveDrive(Blob b){
		system.debug('@@@@@@@@@@@ googlePath = '+googlePath);
		GoogleUtils.GoogleFile ret = null;
		if (googlePath == null || googlePath == '')
			googlePath = gCall.gSettings.path__c;
		system.debug('@@@@@@@@@@@ googlePath = '+googlePath);	
		parentFolderId = gCall.getFolderId(googlePath);
		System.debug('***folderId: '+parentFolderId);
		System.debug('***pfilePath: '+gCall.gSettings.path__c);
		if(googleFileName==null || googleFileName=='')
			googleFileName = fileName;
		System.debug('***fileName: '+googleFileName);
		if (overridingFileId == null || overridingFileId == ''){
			overridingFileId = gCall.haveFileInFolder(parentFolderId, googleFileName+'.'+getFormatExtension());
		}
		if (overridingFileId != null && overridingFileId.length() > 3)//We want to update an existing file
			ret = gCall.updateExistingFile(overridingFileId,getFormatExtension(),b);
		else //Create a new File
			ret = gCall.createNewFile(googleFileName+'.'+getFormatExtension(),getFormatExtension(),parentFolderId,b);
		gCall.saveFilePath(parentFolderId, googlePath);
		gCall.saveSettings();
		return ret;
	}
    
   /* private void setDefaultName(Schema.DescribeSObjectResult sobjectDescribe){
    	string nameField = findNameField(sobjectDescribe);
		if (nameField==null) nameField = 'name';
    	string soql = 'Select '+nameField+' From '+sobjectDescribe.getName()+' Where id =\''+id+'\'';
    	try{
    		Sobject sobj = Database.query(soql).get(0);
    		fileName = (String)sobj.get(nameField);
    		fileName = fileName.replaceAll('\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
			fileName = fileName.replaceAll('\\s\\s+',' ');
    	}
    	catch(Exception e){fileName = '';}
    }

	 private void setDefaultFileName(Schema.DescribeSObjectResult sobjectDescribe, String fieldName){    	
    	string soql = 'Select '+fieldName+' From '+sobjectDescribe.getName()+' Where id =\''+id+'\'';
    	try{
    		Sobject sobj = Database.query(soql).get(0);
    		fileName = (String)sobj.get(fieldName);
    		fileName = fileName.replaceAll('\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
			fileName = fileName.replaceAll('\\s\\s+',' ');
    	}
    	catch(Exception e){fieldName = '';}
    }
    
    
	private string findNameField(Schema.DescribeSObjectResult sobjectDescribe){
		for (Schema.SObjectField field : sobjectDescribe.fields.getMap().values()){
			if(field.getDescribe().isNameField())
				return field.getDescribe().getName();
		}
		return null;
	}*/
	
	private void initPicklistValues(List<Utils.LookupPickListValue> valLst){
		LookupSelectOptions = new List<SelectOption>();
		lookupPicklistMap = new Map<String,Utils.LookupPickListValue>();
		for (Utils.LookupPickListValue val: valLst){
			LookupSelectOptions.add(new SelectOption(val.name, val.label));
			lookupPicklistMap.put(val.name,val);
		}
	}

	private void initFormsSettings(){
		List<Default_Form_Settings__c> settingsLst = [SELECT Form_Id__c, Hide_in_Generation__c, Main_Object__c FROM Default_Form_Settings__c WHERE Main_Object__c =: sobjectType];
		if (!settingsLst.isEmpty()){
			defaultForm = (Id)settingsLst.get(0).Form_Id__c;
			hideInGeneration = settingsLst.get(0).Hide_in_generation__c;
		}
	}

	private void convertStateToId(){
		String base64State = ApexPages.currentPage().getParameters().get('state');
		string jsonStr = Utils.decodeBase64(base64State);
		SaveConfig sConfig = (SaveConfig)JSON.deserialize(jsonStr, SaveConfig.class);
		id = sConfig.id;
		view = sConfig.view;
		//LookupFieldSelected = sConfig.LookupFieldSelected;
		formId = sConfig.defaultForm;
		lookupId = sConfig.contactId;
		fileName = sConfig.fileName;
		format = sConfig.format;
		document = sConfig.channel.get('document');
		mail = sConfig.channel.get('mail');
		link = sConfig.channel.get('link');
		gDrive = sConfig.channel.get('google-drive');
		chatter = sConfig.channel.get('chatter');
		attachment = sConfig.channel.get('attachment');
		selectedTemplateId = sConfig.selectedTemplateId;
		this.sConfigJson = JSON.serialize(sConfig);
		ApexPages.currentPage().getParameters().put('Id', id);
	}

	//To return the list of templates
	private List<SelectOption> initTemplatesList(){
		List<SelectOption> ret = new List<SelectOption>();
		if (Schema.sObjectType.EmailTemplate.IsAccessible() && Schema.sObjectType.EmailTemplate.fields.name.IsAccessible())
		{
			ret.add(new SelectOption('0',label.None));
			Set<String> toIgnore = new Set<String>{'document_expired','New_Link','Mass_Generation_Template'};
			for (EmailTemplate et : [Select id, Name, DeveloperName From EmailTemplate Where isActive=true and folder.namespaceprefix=:NameSpace.replace('__','') and folder.DeveloperName='Docomotion_Emails'])
			{
				if(!toIgnore.contains(et.DeveloperName))
					ret.add(new SelectOption(et.Id, et.Name));
			}
		}
		return ret;
	}

	class SaveConfig{
		Id id;
		Id defaultForm;
		Id contactId;
		string fileName;
		string format;
		string view;
		string LookupFieldSelected;
		string selectedTemplateId;

		public Map<string, boolean> channel;
		//add google settings?

		public SaveConfig(){
			channel = new Map<String, boolean>();
		}
	}

}