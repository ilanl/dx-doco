public with sharing class CTRL04_DataSchema {

    //The constructor
    public CTRL04_DataSchema(CTRL03_DefineDataSchema ctrl){}
    
    /**
    *
    * Adapter for accessing this method not from remote action 
    *
    */
    //Used
    public static Sobject getDataSchemaById(String id){
        DataSchema ds = new DataSchema(id);
        try{
            Sobject dataSchema = ds.getDataSchema();
            if(dataSchema.get('Edit_Fields__c') != null){
            	string editFields = ''+dataSchema.get('Edit_Fields__c');
                dataSchema.put('Edit_Fields__c',editFields.replaceAll('\\([^\\)]+\\)',''));
            }
            return dataSchema;
        }
        catch(Exception e){
            return null;
        }
    }
    
	
    //used
    @remoteAction
    public static void saveFieldsDelta(string schemaId, List<FieldSaveAction> fieldsSaveActions){
        DataSchema ds = new DataSchema(schemaId);
        List<FieldSaveAction> faction = createSortedList(fieldsSaveActions);
        for (FieldSaveAction action : fieldsSaveActions){
            Utils.FieldUpdate fu;
            if(action.action=='edit')
                fu = Utils.generateFieldUpdate(action.sobjectName, action.path);  
            if(action.childrenPath!=null && action.childrenPath!=''){
            	ds.selectActionChild(action.action, action.path, action.childrenPath, action.lookupField, action.sobjectName, action.onChild);
            }
            else
            	ds.selectActions(action.action, action.path, fu, action.lookupField);  
        }  
        ds.save();
    }
    
    //used
    //This methods returns the children relationship
    @remoteAction
    public static String getObjectChildren(string id){
      List<Utils.Child> ret = new List<Utils.Child>();
        DataSchema ds = new DataSchema(id);
        return getObjectChildrenFromObjectName(ds.getFirstRelation());
    }
    
    @remoteAction
    public static String getObjectChildrenFromObjectName(string oName){
      List<Utils.Child> ret = new List<Utils.Child>();
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Map<Schema.Sobjecttype,Schema.Describesobjectresult> objectDescribeMap = new Map<Schema.Sobjecttype,Schema.Describesobjectresult>();
        if(globalDescription.containsKey(oName)){
          Schema.DescribeSobjectResult R = globalDescription.get(oName).getDescribe();
          for(Schema.ChildRelationship child:R.getChildRelationships()){
            if(child.getRelationshipName() != null && child.getChildSobject() != OpenActivity.getSobjectType() && child.getChildSobject() != ActivityHistory.getSobjectType()){
              Schema.Describesobjectresult childObject;
              string lookup = ''+child.getField();
              if(objectDescribeMap.containsKey(child.getChildSObject())){
                childObject = objectDescribeMap.get(child.getChildSObject());
              }
              else{
                childObject = child.getChildSObject().getDescribe();
                objectDescribeMap.put(child.getChildSObject(),childObject);
                
              }
              ret.add(new Utils.Child(childObject.getName(),childObject.getLabel(),child.getRelationshipName(),lookup));
            }
          }
        }
        ret.sort();
        return Json.serialize(ret);
    }
    
    //used
    @remoteAction
    public static string getAllFields(string objectName){
      List<Utils.Field> ret = new List<Utils.Field>();
      Map<Schema.Sobjecttype,Schema.Describesobjectresult> objectDescribeMap = new Map<Schema.Sobjecttype,Schema.Describesobjectresult>();
      for(Schema.Describesobjectresult objD:Schema.describeSObjects(new List<String>{objectName})){
        Map<String, Schema.SObjectField> fMap = objD.fields.getMap();
        for(string key:fMap.keySet()){
          Schema.Describefieldresult fD = fMap.get(key).getDescribe();
          if(fD.isAccessible()){
	          if(fD.getType() == Schema.Displaytype.REFERENCE){
	          	if(fD.getReferenceTo().size()==1 && (fD.getRelationshipName()!=null && !fD.isNamePointing())){
		            if(!objectDescribeMap.containsKey(fD.getReferenceTo().get(0)))
		              objectDescribeMap.put(fD.getReferenceTo().get(0),fD.getReferenceTo().get(0).getDescribe());
		            Schema.Describesobjectresult oD = objectDescribeMap.get(fD.getReferenceTo().get(0));
		            string lLabel = fD.getLabel().replaceFirst(' ID$','');
		            ret.add(new Utils.Lookup(fD.getName(),lLabel,oD.getName(),oD.getLabel())); 
	          	}      
	          }
	          else if(fD.getType() != Schema.DisplayType.ADDRESS && fD.getType() != Schema.DisplayType.BASE64){
	            ret.add(new Utils.Field(fD.getName(),fD.getLabel(),isFieldUpdatable(fD)));
	          }
          }
        }
      }
      ret.sort();
      return Json.serialize(ret);
    }
    
    public static Map<String,String> getLabelsMap(Id dsId){
      DataSchema ds = new DataSchema(dsId);
      Map<string,String> labels = ds.getLabelsMap();
      for(String key:labels.keySet()){
        labels.put(key,labels.get(key).replaceAll('\\(.+\\)\\s{0,1}',''));
      }
      return labels;
    }
    
    //used
    @remoteAction
    public static boolean generateXML(Id dsId){
      DataSchema ds = new DataSchema(dsId);
      //try{
        ds.generateXML();
      /*}
      catch(Exception e){
        Utils.log('***error: '+e.getMessage());
        return false;
      }*/
      return true;
    }

    
    private static boolean isFieldUpdatable(Schema.Describefieldresult fD){
      Set<Schema.DisplayType> updatableType = new Set<Schema.DisplayType>{
        Schema.DisplayType.BOOLEAN,
        Schema.DisplayType.EMAIL,
        Schema.DisplayType.INTEGER,
        Schema.DisplayType.PICKLIST,
        Schema.DisplayType.MULTIPICKLIST,
        Schema.DisplayType.COMBOBOX,
        Schema.DisplayType.PHONE,
        Schema.DisplayType.STRING,
        Schema.DisplayType.TEXTAREA,
        Schema.DisplayType.URL,
		Schema.DisplayType.DOUBLE,
		Schema.DisplayType.CURRENCY,
		Schema.DisplayType.PERCENT
      };
      
      return (fD.isUpdateable() && updatableType.contains(fD.getType()));
    }
    
    private static List<FieldSaveAction>createSortedList(List<FieldSaveAction> fLst){
    	Map<Integer,List<FieldSaveAction>> m = new Map<Integer,List<FieldSaveAction>>();
    	for(FieldSaveAction f:fLst){
    		Integer c = f.path.countMatches('.');
    		if(!m.containsKey(c))
    			m.put(c, new List<FieldSaveAction>());
    		m.get(c).add(f);
    	}
    	List<FieldSaveAction> ret = new List<FieldSaveAction>();
    	List<Integer> kLst = new List<Integer>(m.keySet());
    	kLst.sort();
    	for(Integer k: kLst){
    		ret.addAll(m.get(k));
    	}
    	return ret;
    }

    public class FieldSaveAction{
        public string action {set; get;}
        public string path {set; get;}
        public boolean onChild {set; get;}//have to be deleted
        public string childrenPath{set;get;}
        public string sobjectName {set; get;}
        public string lookupField{set;get;}
    }

}