public with sharing class CTRL05_Form_Extensions {
    
    public List<SelectOption> recipientOptions { get;set; }

    public String errorMsg{get;private set;}
    public Default_Form_Settings__c defaultFormSettings {get;set;}
    public Boolean isDefaultForm {get;set;}
	public Boolean hasDefaultForm {get;set;}
	public Boolean hasError{get; private set;}

    private Form__c myForm;
    private ApexPages.Standardcontroller stdC;
    public String SelectedDsId{get;set;}
    public boolean existingDS{get;set;}
    public boolean noDS{get;set;}
    public boolean loadFromV4{get;set;}//to be deleted when all user in V5
    public String dataschemaName{get;set;}
    public boolean isClone{get;private set;}
    public boolean oldForm{get;private set;}
	public string defaultFormId{get;private set;}
	//public String settingsMessage{get; private set;}
    private Form__c clonedForm;
    private Boolean isUpdate;
	public String NameSpace{
		get{
			if(NameSpace==null){
				NameSpace = Utils.getNameSpace();
			}
			return NameSpace;
		}
		private set;
	}

	public Boolean trialForce{
		get{
			if(trialForce==null){
				Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
				trialForce = (myConf.trialforce__c != null && myConf.trialforce__c);
			}
			return trialForce;
		}
		private Set;
	}

	public Integer openingDesignerTimeout{
		get{
			if (openingDesignerTimeout == null){
				Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
				if (myConf.Opening_Designer_Timeout__c == null){
					myConf.Opening_Designer_Timeout__c = 60;
				}
				openingDesignerTimeout = (Integer)myConf.Opening_Designer_Timeout__c;
			}
			return openingDesignerTimeout;
		}
		private Set;
	}

	//For the template
	public List<SelectOption> templatesList{
		get{
			if (templatesList == null)
				templatesList = initTemplatesList();
			return templatesList;
		}
		private Set;
	}
	
	public List<SelectOption> defaultFileNameList{
		get{
			if (defaultFileNameList == null)
				defaultFileNameList = initDefaultFileNameList();
			return defaultFileNameList;
		}
		private Set;
	}
    
    //Constructor
    public CTRL05_Form_Extensions(ApexPages.standardController con){
        hasError=false;
		hasDefaultForm = false;
        stdC = con;
    	isClone = false;
    	existingDs = false;
    	noDs = false;
    	isUpdate = false;
    	loadFromV4=false;
    	if(!test.isRunningTest())
        	con.addFields(new List<String> {'Internal_Error_Code__c','Status__c','form_ID__c','New_Dataschema__c', 'Main_Object__c','Preview_PDF_Document_ID__c','fields__c', 'edit_fields__c', 'Dataschema__c', 'Regenerate_XML__c','RecordType.DeveloperName','Data_Schema_XML_ID__c','Word_Document_Version_Id__c','Metadata_Version_Id__c','Preview_PDF_Document_ID__c','current_version__c'});
        myForm = (Form__c)con.getRecord();
        if(myForm.id==null)
        	initNewForm();
        else{
        	existingDS = (myForm.Dataschema__c!=null);
        	loadFromV4 = (myForm.Dataschema__c == null);
        	isUpdate=true;
        }
        oldForm = isOldForm();
		if (myForm.Id != null)
			initDefaultSettings();
		System.debug('default form defaultFormSettings: '+defaultFormSettings);
        
        // custom picklist for page
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        SobjectType sType = schemaMap.get(myForm.Main_object__c);
        if(sType!=null){
            recipientOptions = getRecipientOptions(sType,'','',true);
        }
    }


    //new form initialisation
    public void initNewForm(){
		try{
			if(apexPages.currentPage().getParameters().containsKey('clone_id')){//it's a clone
				Id cloneId = (Id)(apexPages.currentPage().getParameters().get('clone_id'));
				initClonedForm(cloneId);
			}	
		}
		catch(Exception e){}
    }
    
    //The Activate function
    public PageReference activate(){
        if(myForm.New_Dataschema__c){
            addError(Label.Dataschema_changed);
            return null;
        }
		//to immediatly publish if it's a subform
		if (myForm.RecordType.developerName == 'Subform')
			myForm.Immediate_Publish__c = true;
        
		Boolean isSuccess = true;
        if(myForm.Status__c=='Activating')
            isSuccess = reEdit();
        if(isSuccess){  
            myForm.status__c='Activating';
            try{
                DML_CustomExtension.updateObj(myForm);
				isSuccess = true;
            }
            catch(Exception e){
                isSuccess=false;
            }
        }
        if(isSuccess)
            return null;
        errorMsg = Label.Error_publish_process;
        return null;            
    }
    
    //To reedit a form
    public Boolean reEdit(){
        myForm.status__c = 'Edit';
        try{
            DML_CustomExtension.updateObj(myForm);
			return true;
        }
        catch (Exception e) {    return false;   }
    }
    
    //back function
    public PageReference back(){
        return new PageReference('/'+myForm.id);
    }
    
    //clone function
    public PageReference cloneForm(){
    	PageReference p = Page.VF07_CreateForm;
		p.setRedirect(true);
		p.getParameters().put('clone_id',myForm.Id);
		return p;
    }
    
    
    //To get the list of objects
    public static List<SelectOption> getAllObjectsList(){
        List<SelectOption> ret2 = new List<SelectOPtion>();
        List<Utils.Option> ret = new List<Utils.Option>();
        Set<String> objectNames = new Set<String>();
        
        Set<String> toExclude = new Set<String>{'openactivity', 'activityhistory', 'announcement', 'apexclass', 'apexlog','asyncapexjob','apextestqueueitem','apextestresult','apextrigger','chatterconversation','chatterconversationmember','chattermessage','datacloudcompany','datacloudcontact','datacloudownedentity','datacloudpurchaseusage','workfeedback','workfeedbackhistory','workfeedbackquestion','workfeedbackquestionhistory','workfeedbackquestionsethistory','workfeedbackquestionset','workfeedbackquestionsetshare','workfeedbackquestionshare','workfeedbackrequest','workfeedbackrequestfeed','workfeedbackrequesthistory','workfeedbackrequestshare','workfeedbackshare','workgoal','workgoalcollaborator','workgoalcollaboratorhistory','workgoalfeed','workgoalhistory','workgoallink','workgoalshare','contentworkspace','contentworkspacedoc','workperformancecycle','workperformancecyclefeed','workperformancecycleshare','workperformancecyclehistory','secureagent','secureagentplugin','secureagentpluginproperty','apexcomponent','apexpage'};
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		for(String str: gd.keySet()){
			if(!toExclude.contains(str) && (str.endsWith('__c') || (!str.endsWith('__c') && !str.contains('__'))))
				objectNames.add(str);	
		}
          
        
        List<String> objectNamesLst = new List<String>(objectNames);    
        for(Schema.DescribeSobjectResult obj:Schema.describeSObjects(objectNamesLst)){
            if(obj.isAccessible() && obj.isQueryable()){
                Utils.Option option = new Utils.Option(obj.getName(), obj.getLabel());
                ret.add(option);
            }
        }   
        ret.sort();
        for(Utils.Option opt:ret)
            ret2.add(new SelectOption(opt.value, opt.label));
        return ret2;
    }
    
    //to get a list of compatible schemas
    public String getCompatibleDataschmas2(){
    	
    	List<Dataschema__c> ret = [Select Id, Name, Description__c, createdBy.Name, lastModifiedDate From Dataschema__c Where Main_Object__c=:myForm.Main_Object__c];
    	return JSON.serialize(ret);
    }

    public boolean upsertCustomSettings(){
		Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
		Boolean ret = true;
		//check for errors:
		if (myForm.Default_Format__c == null){
			if(!myForm.Allow_Changing_format__c)
				myForm.Allow_Changing_format__c.addError('You need to select a default format to unselect this option.');
			if(myForm.Hide_Format_in_Generation__c)
				myForm.Hide_Format_in_Generation__c.addError('You need to select a default format to select this option.');
		}
		else if(myForm.Hide_Format_in_Generation__c){
			if(myForm.Allow_Changing_Format__c)
				myForm.Allow_Changing_Format__c.addError('You can\'t allow changing format if the form selection is hidden.');
		}
		//Channel
		if (myForm.Default_Channel__c == null){
			if(!myForm.Allow_Changing_Channel__c)
				myForm.Allow_Changing_Channel__c.addError('You need to select a default channel to unselect this option.');
			if(myForm.Hide_Channel_in_Generation__c)
				myForm.Hide_Channel_in_Generation__c.addError('You need to select a default channel to select this option.');
		}
		else if(myForm.Hide_Channel_in_Generation__c){
			if(myForm.Allow_Changing_Channel__c)
				myForm.Allow_Changing_Channel__c.addError('You can\'t allow changing channel if the form selection is hidden.');
		}
		else{
			Set<string> channels = new Set<String>(myForm.Default_Channel__c.split(';'));
			if(channels.contains('Link') && channels.size()>1 && !channels.contains('Chatter')){
				myForm.Default_Channel__c.addError('Link channel can be used only with Chatter.');
				if(myForm.Default_Format__c != 'HTML')
					myForm.Default_Format__c.addError('Only HTML format can be use with Link channel');
			}
			
		}
		
		//File name
		/*if (!myForm.Use_Record_Name__c){
			if(!myForm.Allow_Changing_File_Name__c)
				myForm.Allow_Changing_File_Name__c.addError('You need to use the default file name to unselect this option.');
			if(myForm.Hide_File_Name_in_Generation__c)
				myForm.Hide_File_Name_in_Generation__c.addError('You need to use the default file name to select this option.');
		}*/
		if(myForm.Hide_File_Name_in_Generation__c){
			if(myForm.Allow_Changing_File_Name__c)
				myForm.Allow_Changing_File_Name__c.addError('You can\'t allow changing file name if the form selection is hidden.');
				
		}

		if (!(myForm.Default_Channel__c == 'Link;'||myForm.Default_Channel__c=='link' )&&(myForm.Default_Template__c==null  || myForm.Default_Template__c.length() < 15 ))
		{
			if(!myForm.Allow_Changing_Template__c)
				myForm.Allow_Changing_Template__c.addError('You need to select a default template to unselect this option.');
			if(myForm.Hide_Template_in_Generation__c)
				myForm.Hide_Template_in_Generation__c.addError('You need to use the default template to select this option.');
		}
		else if(myForm.Hide_Template_In_Generation__c){
			if(myForm.Allow_Changing_Template__c)
				myForm.Allow_Changing_Template__c.addError('You can\'t allow changing the template if the form selection is hidden.');
		}

		if (!isDefaultForm){
			if(defaultFormSettings.Hide_in_Generation__c)
				defaultFormSettings.Hide_in_Generation__c.addError('You need to select a default form to unselect this option.');
		}

        //PageReference p = stdC.save();
        if(isDefaultForm){
			Boolean toUpsert = false;
			System.debug('default form defaultFormSettings: '+defaultFormSettings);
			if (defaultFormSettings.Main_Object__c==null){//There wasn't any settings for the current main object
				defaultFormSettings.Main_Object__c = myForm.Main_object__c;
				toUpsert = true;
			}
			defaultFormSettings.Form_Id__c = myForm.Id;
			DML_CustomExtension.upsertObj(defaultFormSettings);	
        }
		else{
            if (defaultFormSettings.Form_Id__c == myForm.Id){//We need to remove the existing settings
                DML_CustomExtension.deleteObj(defaultFormSettings);
            }
        }
        //return p;
        System.debug(Logginglevel.ERROR,'@@@@@@ ApexPages.hasMessages(): '+ ApexPages.hasMessages());
		return !ApexPages.hasMessages();
		return ret;
    }
    
    //to save a form
    public PageReference saveForm(){
		hasError = false; 
    	Dataschema__c ds;
    	System.Savepoint savePoint;
    	if(!isClone && existingDS){
    		savePoint = Database.setSavepoint();
    		if(isUpdate && (selectedDsId=='' || selectedDsId==null))
    			selectedDsId = myForm.dataschema__c;
    		if(SelectedDsId != null && SelectedDsId !=''){//we use a dataschema
    			ds = [Select Id, Fields__c, Edit_Fields__c, (Select Interactive_Form__c From Interactive_Related_Data_Schemas__r) From Dataschema__c Where id = :SelectedDsId];
    			myForm.dataschema__c = selectedDsId;
    		}
    		else{
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.No_dataschem));
    			return null;
    		}
    	}
    	else if((!isUPdate||loadFromV4) && !isClone && !existingDS){
    		//insert Name field of main object
    		myForm.Default_File_Name__c = getObjectNameField(myForm.Main_Object__c);
    		//Create the DS
    		savePoint = Database.setSavepoint();
    		Dataschema__c ds2 = new Dataschema__c(Name=dataschemaName,Main_Object__c=myForm.Main_object__c);
    		if(loadFromV4){
    			ds2.fields__c = myForm.Fields__c;
    			ds2.Edit_Fields__c = myForm.Edit_Fields__c;
    		}
    		try{
    			DML_CustomExtension.insertObj(ds2);
    		}
    		catch(DMLException e){
    			for(Integer i=0; i<e.getNumDml(); ++i){
    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
    			}
    			return null;
    		}
    		myForm.Dataschema__c = ds2.id;
    	}
    	else if(!isUPdate && !isClone && noDS){
    		savePoint = Database.setSavepoint();
    		myForm.main_object__c = null;
    	}
    	if(!isUPdate &&!isUnique(myForm.name__c)){
    		if(savePoint != null)
    			Database.rollback(savePoint);
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Duplicate_Name));
    		return null;
    	}
		if(isUpdate){
			boolean generationSettingsFlag = upsertCustomSettings();
			hasError = !generationSettingsFlag;
			if(!generationSettingsFlag){
				Database.rollback(savePoint);
				return null;
			}
		}
    	PageReference p =  stdC.save();
    	if(p == null){
    		Database.rollback(savePoint);
    		return p;
    	}
    	myForm.Id = stdC.getId();
    	if(!isUpdate && ds != null){//If new from from existing dataschema
	    	List<Interactive_Collections_Association__c> aLst = new List<Interactive_Collections_Association__c>();
	    	for(Interactive_Dataschema_Association__c da :ds.Interactive_Related_Data_Schemas__r){
	    		aLst.add(new Interactive_Collections_Association__c(form__c = myForm.id,collection__c=da.Interactive_Form__c,From_Dataschema__c=true));
	    	}
	    	if(!aLst.isEmpty())
	    		
	    		DML_CustomExtension.insertObj(aLst);	
    	}
    	//Generate the XML for the new form
    	Dataschema das = new Dataschema(myForm.id);
	    das.generateXML();
    	
    	if(isClone){//if clone
    		List<Interactive_Collections_Association__c> aLst = new List<Interactive_Collections_Association__c>();
    		for(Interactive_Collections_Association__c da :clonedForm.Interactive_Collections_Associations__r){
	    		aLst.add(new Interactive_Collections_Association__c(form__c = myForm.id,collection__c=da.collection__c));
	    	}
    		if(!aLst.isEmpty())
    			DML_CustomExtension.insertObj(aLst);
    		List<Form_Subform_Association__c> sLst = new LIst<Form_Subform_Association__c>();
    		for(Form_Subform_Association__c sa:clonedForm.Form_subform_Associations__r){
    			sLst.add(new Form_Subform_Association__c(form__c = myForm.id, subform__c = sa.Subform__c));
    		}
    		if(!sLst.isEmpty())
    			DML_CustomExtension.insertObj(sLst);	
    		Dataschema d = new Dataschema(myForm.id);
	    	d.generateXML();
	    	cloneDocuments();		
    	}
    	return p;
    }
    
    //the save and open define dataschema
    public PageReference saveFormAndDefineDataSchema(){
    	if(saveForm() != null){
    		return goToDefineDataschema();
    	}
    	return null;
    }
    
    //to go to the define dataschema page
    public PageReference goToDefineDataschema(){
    	PageReference p = Page.VF10_DefineDataSchemaFromDataschema;
    	p.setRedirect(true);
		p.getParameters().put('id',myForm.dataschema__c);
		p.getParameters().put('retUrl',myForm.id);
		return p;
    }

	public void publish(){
		myForm.Status__c='Published';
		DML_CustomExtension.updateObj(myForm);
		updateStatus();
	}

	public PageReference unpublish(){
		myForm.Status__c='Active';
		System.debug('***current form Version: '+myForm.current_version__c);
		myForm.current_version__c = ''+(Integer.valueOf(myForm.current_version__c) - 1); 
		myForm.Active__c = false;
		//No published version when unpublish
		myForm.current_published_version__c = null;
		return activate();
	}
    
    //To refresh the status
    public void updateStatus(){
    	
    	Form__c f = [Select Internal_Error_Code__c,Active__c, Status__c, current_version__c, current_Published_Version__c, current_Activated_Version__c, Published_By__c, Published_Date__c, World_Document_FFX_ID__c, Publish_Error__c, New_Dataschema__c From Form__c Where id=:stdC.getId()];
		myForm.Status__c = f.status__c;
		myForm.current_version__c = f.current_version__c;
    	myForm.current_Published_Version__c = f.current_Published_Version__c;
		myForm.current_Activated_Version__c = f.current_Activated_Version__c;
    	myForm.World_Document_FFX_ID__c = f.World_Document_FFX_ID__c;
		myForm.Published_By__c = f.published_By__c;
		myForm.published_date__c = f.published_Date__c;
		myForm.Active__c = f.Active__c;
		myForm.New_Dataschema__c = f.New_Dataschema__c;
		myForm.Internal_Error_Code__c = f.Internal_Error_Code__c;
		MessagesToSHow();
    }

	public void failStatus(){
		ExceptionWS eWS = new ExceptionWS((Integer)myForm.Form_ID__c,'No Response From Publish WS');
		eWs.setSessionId(UserInfo.getSessionId());
		eWS.send();
		try{
			myForm.Status__c='Failed';	        
			DML_CustomExtension.updateObj(myForm);
		}
		catch (Exception e){}
	}
    
    public void MessagesToSHow(){
    	if(myForm.Main_Object__c != null && myForm.Dataschema__c == null){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.VF08_V4_form));
    	}
    	if(myForm.New_Dataschema__c){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.VF08_Reopen));
    	}
    	if(oldForm){
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, Label.VF08_Old_5_2_Form));
    	}
    }
    
    public string getCreatedByLabel(){
    	return Schema.SObjectType.Form__c.fields.CreatedById.getLabel().replace(' ID','');
    }
    
    public pageReference updateOldForm(){
    	//Need to copy all the chatter files to attachment ones (Dataschema XML, Word file, metadata, preview).
    	ContentVersion cvXml, cvWord, cvMd, cvPdf;
    	Attachment attXml, attWord, attMd, attPdf;
    	List<ContentDocument> toDelete = new List<ContentDocument>();
    	SavePoint savePointOldFormUpdate = Database.setSavepoint();
    	try{
	    	//for the dataschema XML
	    	cvXml = [Select Id, VersionData, Description, ContentDocumentId From ContentVersion where id= :myForm.Data_Schema_XML_ID__c];
	    	attXml = new Attachment();
	    	attXml.body = cvXml.VersionData;
	        attXml.ParentId = myForm.id;
	        attXml.ContentType = 'application/xml';
	    	attXml.Name = 'DataSchema.xml';
	    	attXml.Description = cvXml.description;
	    	//For the word file
	    	if(myForm.Word_Document_Version_Id__c != null){
		    	cvWord = [Select Id, VersionData, Description, ContentDocumentId From ContentVersion Where id=:myForm.Word_Document_Version_Id__c];
		    	attWord = new Attachment();
		    	attWord.body = cvWord.VersionData;
		    	attword.ParentId=myForm.id;
		    	attWord.Name = 'Document.ffx';
		    	attWord.description = cvWord.Description;
	    	}
	    	//for the metadata
	    	if(myForm.Metadata_Version_Id__c != null){
		    	cvMd = [Select Id, VersionData, Description, ContentDocumentId From ContentVersion Where id=:myForm.Metadata_Version_Id__c];
		    	attMd = new Attachment();
		    	attMd.body = cvMd.VersionData;
		    	attMd.ParentId = myForm.id;
		    	attMd.ContentType = 'application/xml';
		    	attMd.Name='Metadata.xml';
		    	attMd.Description = cvMd.Description;
	    	}
	    	//For the pdf
	    	if(myForm.Preview_PDF_Document_ID__c != null){
		    	cvPdf = [Select id, VersionData, Description, ContentDocumentId From ContentVersion Where ContentDocumentId=:myForm.Preview_PDF_Document_ID__c order by CreatedDate Desc limit 1];
		    	attPdf = new Attachment();
		    	attPdf.body = cvPdf.VersionData;
		    	attPdf.parentId = myForm.id;
		    	attPdf.ContentType = 'application/pdf';
		    	attPdf.Name='Preview.pdf';
		    	attPdf.Description = cvPdf.Description;
	    	}
			
			//Insert the attachments
			List<Attachment> toInsert = new List<Attachment>();
			toInsert.add(attXml);
			if(attWord != null)
				toInsert.add(attWord);
			if(attMd != null)
				toInsert.add(attMd);
			if(attPdf != null)
				toInsert.add(attPdf);
				
			DML_CustomExtension.insertObj(toInsert);
			
			//Update the form;
			myForm.Data_Schema_XML_ID__c = myForm.document_Content_Id__c = attXml.id;
			toDelete.add(new ContentDocument(id=cvXml.ContentDocumentId));
			if(attWord != null){
				myForm.World_Document_FFX_ID__c = myForm.Word_Document_Version_Id__c = attWord.id;
				toDelete.add(new ContentDocument(id=cvWord.ContentDocumentId));
			}
			if(attMd != null){
				myForm.MetaData_Document_Id__c = myForm.Metadata_Version_Id__c = attMd.id;
				toDelete.add(new ContentDocument(id=cvMd.ContentDocumentId));
			}
			if(attPdf != null){
				myForm.Preview_PDF_Document_ID__c = attPdf.id;
				toDelete.add(new ContentDocument(id=cvPdf.ContentDocumentId));
			}
			//increment the form current version
			if(myForm.current_version__c==null)
				myForm.Current_Version__c = '1';
			else	
				myForm.Current_Version__c = ''+(Integer.valueOf(myForm.current_version__c)+1);
			DML_CustomExtension.updateObj(myForm);
    	}
    	catch(Exception e){
    		Database.rollback(savePointOldFormUpdate);
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'There was a problem. Please Contact the Docomotion support'));
    		return null;
    	}
		//delete the old chatter files
		try{
			DML_CustomExtension.deleteObj(toDelete);
		}
		catch(Exception e){Database.rollback(savePointOldFormUpdate);}
		return new PageReference('/'+myForm.id);		
    	
    }

	//to restore the last published version
	public void restoreLastVersion(){
		//1. we are selecting the field from the published version
		String versionNumber = ''+ (Integer.valueOf(myForm.Current_Version__c) - 1 );
		Version__c publishedVersion = [Select Document_Id__c, Metadata_Id__c, Preview_PDF_Id__c, Contains_Interactive_Collection__c, Contains_Interactive_Fields__c, Version__c From Version__c Where Form__c=:myForm.Id and version__c=:versionNumber];
		Set<Id> attIds = new Set<Id>{publishedVersion.Document_Id__c, publishedVersion.Metadata_Id__c, publishedVersion.Preview_PDF_Id__c};
		//2. we are coping the files (Word, PDF and Metadata)
		List<Attachment> attToUpdate = new List<Attachment>();
		for (Attachment att : [Select body From Attachment Where id in :attIds]){
			if (att.Id == publishedVersion.Document_Id__c){
				Attachment wordAtt = new Attachment(
					Id=myForm.Word_Document_Version_Id__c,
					body = att.body
				);
				attToUpdate.add(wordAtt);
			}
			else if(att.Id == publishedVersion.Metadata_Id__c){
				Attachment metaAtt = new Attachment(
					Id=myForm.Metadata_Version_Id__c,
					body = att.body
				);
				attToUpdate.add(metaAtt);
			}
			else if(att.Id == publishedVersion.Preview_PDF_Id__c && myForm.Preview_PDF_Document_ID__c != null){
				Attachment pdfAtt = new Attachment(
					Id=myForm.Preview_PDF_Document_ID__c,
					body = att.body
				);
				attToUpdate.add(pdfAtt);
			}
		}
		update attToUpdate;
		myForm.status__c='Published';
		myForm.Active__c=true;
		myForm.Contains_Interactive_Collection__c = publishedVersion.Contains_Interactive_Collection__c;
		myForm.Contains_Interactive_Fields__c = publishedVersion.Contains_Interactive_Fields__c;
		myForm.Current_version__c = versionNumber;
		myForm.current_Activated_version__c = publishedVersion.Id;
		System.debug('myForm: '+myForm);
		DML_CustomExtension.updateObj(myForm);
		updateStatus();
	}







    public List<SelectOption> getRecipientOptions(SobjectType sType, string currentPath, string currentLabel, boolean toContinue){
        List<SelectOption> recOptLst = new List<SelectOption>();
        if(currentPath != ''){
            currentPath = Utils.getRelationshipName(currentPath);
            currentPath += '.';
        }
        if(currentLabel != ''){
            currentLabel = ' of '+currentLabel + ' lookup';
        }

        if(currentPath == '' && (sType == Contact.SobjectType || sType == Lead.SobjectType)){
            recOptLst.add(new SelectOption('Id','Record Itself')); // id,value
        }
        Map<String, Schema.SobjectField> fsMap = sType.getDescribe().fields.getMap();
        for(String fName : fsMap.keySet()){
            Schema.DescribeFieldResult field = FsMap.get(fName).getDescribe();
            if(field.getType() == Schema.DisplayType.REFERENCE && field.getReferenceTo().size()==1 && (!(Ctrl18_MassGenerate.FieldsToFilter.containsKey(sType)&&Ctrl18_MassGenerate.FieldsToFilter.get(sType).containsKey(field.getName())))&& (field.getReferenceTo().get(0) == Contact.SobjectType || field.getReferenceTo().get(0) == Lead.SobjectType)){
                system.debug('*****field: '+field.getName());
                recOptLst.add(new SelectOption(currentPath+field.getName(), field.getLabel()+currentLabel));
                if(toContinue){
                    recOptLst.addAll(getRecipientOptions(field.getReferenceTo().get(0), field.getName(), field.getLabel(), false));
                }
            }
        }
        return recOptLst;
    }

    
    //To check if is the field is unique
    private boolean isUnique(string name){
    	List<Form__c> lst;
    	if(myForm.Id == null)
			lst = [Select Id From Form__c Where name__c = :name];
		else
			lst = [Select Id From Form__c Where name__c = :name and id != :myForm.id];	
    	return lst.isEmpty();
    }
    
    private void initClonedForm(id clonedFormId){
    	isClone = true;
		clonedForm = [Select Main_Object__c, Default_File_Name__c, Status__c, dataschema__c, Description__c, Fields__c, edit_fields__c, group__c, recordTypeId, word_template__c, Metadata_Version_Id__c, Preview_PDF_Document_ID__c, Word_Document_Version_Id__c, Allow_Changing_Channel__c, Default_Channel__c, Hide_Channel_in_Generation__c, Contains_Interactive_Collection__c,	Contains_Interactive_Fields__c, Allow_Changing_Template__c, Default_Template__c, 	Hide_Template_In_Generation__c, Allow_Changing_File_Name__c, Hide_File_Name_in_Generation__c, 	Allow_Changing_Format__c, Default_Format__c, Hide_Format_in_Generation__c, (Select collection__c From Interactive_Collections_Associations__r), (Select Subform__c From Form_Subform_Associations__r) From Form__c Where id = :clonedFormId];
		myForm.Main_Object__c = clonedForm.Main_Object__c;
		myForm.Dataschema__c = clonedForm.Dataschema__c;
		myForm.Description__c = clonedForm.Description__c;
		myForm.Fields__c = clonedForm.Fields__c;
		myForm.Edit_Fields__c = clonedForm.Edit_Fields__c;
		myForm.Group__c = clonedForm.group__c;
		myForm.recordTypeId = clonedForm.recordTypeId;
		myForm.word_template__c = clonedForm.word_template__c;
		myForm.Allow_Changing_Channel__c = clonedForm.Allow_Changing_Channel__c;
		myForm.Default_Channel__c = clonedForm.Default_Channel__c;
		myForm.Hide_Channel_in_Generation__c = clonedForm.Hide_Channel_in_Generation__c;
		myForm.Contains_Interactive_Collection__c = clonedForm.Contains_Interactive_Collection__c;
		myForm.Contains_Interactive_Fields__c = clonedForm.Contains_Interactive_Fields__c;
		myForm.Allow_Changing_Template__c = clonedForm.Allow_Changing_Template__c;
		myForm.Default_Template__c = clonedForm.Default_Template__c;
		myForm.Hide_Template_In_Generation__c = clonedForm.Hide_Template_In_Generation__c;
		myForm.Allow_Changing_File_Name__c = clonedForm.Allow_Changing_File_Name__c;
		myForm.Hide_File_Name_in_Generation__c = clonedForm.Hide_File_Name_in_Generation__c;
		myForm.Allow_Changing_Format__c = clonedForm.Allow_Changing_Format__c;
		myForm.Default_Format__c = clonedForm.Default_Format__c;
		myForm.Hide_Format_in_Generation__c = clonedForm.Hide_Format_in_Generation__c;
		myForm.status__c = (clonedForm.Status__c=='Edit')?'Edit':'Activating';		
		myForm.Default_File_Name__c = clonedForm.Default_File_Name__c;
    }
    
    private void cloneDocuments(){
    	//1 select on the metadata and word version Ids
    	Map<Id, Attachment> attMap = new Map<Id,Attachment>([Select  Id, Body, Name, ContentType From Attachment where (id in :new Set<Id>{clonedForm.Metadata_Version_Id__c, clonedForm.Word_Document_Version_Id__c, clonedForm.Preview_PDF_Document_ID__c})]);//add PDF?
    	List<Attachment> attToAdd = new List<Attachment>();
    	Attachment attMetadata;
    	Attachment attWord;
    	Attachment attPdf;
    	//2 Working on the metadata file
    	if(clonedForm.Metadata_Version_Id__c != null){
	    	Form__c f = [Select form_id__c From Form__c Where id =:myForm.id];
	    	myForm.Form_Id__c = f.form_Id__c;
	    	attMetadata = cloneMetaData(attMap.get(clonedForm.Metadata_Version_Id__c));
	    	attToAdd.add(attMetadata);
    	}
    	
    	//3 working on the word file
    	if(clonedForm.Word_Document_Version_Id__c != null){
	    	attWord = cloneWordFile(attMap.get(clonedForm.Word_Document_Version_Id__c));
	    	attToAdd.add(attWord);
    	}
    	
    	//4 the preview stuff
    	//We'll get the preview from the attachment to limite the HeapSize
    	try{
    		//Attachment prevAttach = [Select Body From Attachment Where parentId=:clonedForm.id limit 1];
	    	if(clonedForm.Preview_PDF_Document_ID__c!= null && attMap.containsKey(clonedForm.Preview_PDF_Document_ID__c)){
	   			attPdf = clonePdfFile(attMap.get(clonedForm.Preview_PDF_Document_ID__c));
	    		attToAdd.add(attPdf);
	    	}
    	
    	}
    	catch(Exception e){}
		//5 add the Fis
		if(!attToAdd.isEmpty()){
			DML_CustomExtension.insertObj(attToAdd);
	    	
	    	//6 complete the form
	    	if(attMetadata.id != null){
		    	myForm.MetaData_Document_Id__c = attMetadata.id;
		    	myForm.Metadata_Version_Id__c = attMetadata.id;
	    	}
	    	if(attWord.Id != null){
		    	myForm.World_Document_FFX_ID__c = attWord.Id;
		    	myForm.Word_Document_Version_Id__c = attWord.Id;
	    	}
	    	myForm.New_Dataschema__c = false;
	    	if(attPdf != null && attPdf.Id != null){
	    		myForm.Preview_PDF_Document_ID__c = attPdf.Id;
	    	}
	    	DML_CustomExtension.updateObj(myForm);
	    	
		} 
    }
    
    //to clone the metadata
    private Attachment cloneMetaData(Attachment cvMetadata){
    	Attachment attMetadata = cvMetadata.clone();
    	attMetadata.ParentId = myForm.id;
    	DOM.Document dsrc = new Dom.Document();
		dsrc.load(attMetadata.Body.toString());
		DOM.XMLNode srcroot = dsrc.getRootElement();
		updateMetadata(srcroot);
		attMetadata.Body = Blob.valueOf(dSrc.toXmlString());
		return attMetadata;
    }
    
    //to clone the word file
    private Attachment cloneWordFile(Attachment cvWord){
    	Attachment attWord = new Attachment();
        attWord.body = cvWord.body;
        attWord.Name = cvWord.Name;
        attWord.ContentType = cvWord.ContentType;
    	attWord.parentId = myForm.id;
    	return attWord;
    }
    
    //to clone the PDF
    private Attachment clonePdfFile(Attachment aPdf){
    	Attachment attPdf = new Attachment();
        attPdf.Body = aPdf.Body;
        attPdf.Name = aPdf.Name;
        attPdf.ContentType = aPdf.ContentType;
    	attPdf.parentId = myForm.id;
    	return attPdf;
    }
    
    private void updateMetadata(Dom.XMLNode src){
    	if(src != null){
    		DOM.XMLNode propNode = src.getChildElement('FreeFormProperties',null);
    		if(propNode != null){
    			DOM.XMLNode IdNode = propNode.getChildElement('ID',null);
    			if(IdNode != null){
    				if(idNode.getChildren().size() == 1)
    					idNode.removeChild(idNode.getChildren().get(0));
    				idNode.addTextNode(''+myForm.form_id__c);
    			}
    			DOM.XMLNode versionNode = propNode.getChildElement('Version',null);
    			if(versionNode != null){
    				if(versionNode.getChildren().size() == 1)
    					versionNode.removeChild(versionNode.getChildren().get(0));
    				versionNode.addTextNode('1');
    			}
    			DOM.XMLNode	nameNode = propNode.getChildElement('Name',null);
    			if(nameNode != null){
    				if(nameNode.getChildren().size() == 1)
    					nameNode.removeChild(nameNode.getChildren().get(0));
    				nameNode.addTextNode(myForm.Name__c);
    			}	
    		}
    	}
    }
    
    private boolean isOldForm(){
    	//If the Form used the chatter files (v<=5.1)
    	return(myForm.Data_Schema_XML_ID__c != null && (''+((Id)myForm.Data_Schema_XML_ID__c).getSObjectType()).toLowerCase() != 'attachment');
    }

	private void initDefaultSettings(){
		isDefaultForm = false;
		if (myForm.Main_Object__c != null){
			defaultFormSettings = Default_Form_Settings__c.getInstance(''+myForm.Main_Object__c.hashCode());
			if (myForm.Id != null && defaultFormSettings != null){
				isDefaultForm = (myForm.Id == defaultFormSettings.Form_Id__c);
				hasDefaultForm = true;
				try{
					defaultFormId = ''+[Select Form_Id__c From Form__c Where id = :defaultFormSettings.Form_id__c].Form_Id__c;
				}
				catch(Exception e){}
			}
			if(defaultFormSettings == null)
				defaultFormSettings = new Default_Form_Settings__c(Name = ''+myForm.Main_Object__c.hashCode());
		}    
	}
    
    @remoteAction
    public static void regenerateXML(Id id){
    	Form__c f = [Select Id, Regenerate_XML__c From Form__c Where id=:id];
    	Dataschema d = new Dataschema(id);
    	d.generateXML();
    	f.Regenerate_XML__c = false;
		f.Opening_Designer__c = true;
    	try{
    		DML_CustomExtension.updateObj(f);
    	}catch(Exception e){}
    }

	@remoteAction
	public static boolean designerHasBeenOpened(Id id){
		try 
		{	        
			Form__c f = [Select Opening_Designer__c From Form__c Where id =: id];
			System.debug('***result'+(!f.Opening_Designer__c));
			return (!f.Opening_Designer__c);
		} catch (Exception e){return true;}
	}
    
    @testVisible
    private void addError(String errorStr){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,errorStr));
    }

	//To return the list of templates
	private List<SelectOption> initTemplatesList(){
		List<SelectOption> ret = new List<SelectOption>();
		if (Schema.sObjectType.EmailTemplate.IsAccessible() && Schema.sObjectType.EmailTemplate.fields.name.IsAccessible())
		{
			ret.add(new SelectOption('0','None'));
			Set<String> toIgnore = new Set<String>{'document_expired','New_Link','Mass_Generation_Template'};
			for (EmailTemplate et : [Select id, Name, DeveloperName From EmailTemplate Where isActive=true and folder.namespaceprefix=:NameSpace.replace('__','') and folder.DeveloperName='Docomotion_Emails'])
			{
				if(!toIgnore.contains(et.DeveloperName))
					ret.add(new SelectOption(et.Id, et.Name));
			}
		}
		return ret;
	}
   	
   	
   	private Map<String, Schema.SObjectField> getfieldMap(String mainObject){
   		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
   		if (mainObject != null && schemaMap.containsKey(mainObject)){	    	
	    	String objType= myForm.Main_Object__c;
			Schema.SObjectType objectSchema = schemaMap.get(objType);
			return objectSchema.getDescribe().fields.getMap();
   		}
   		return null;
   	}
   	
   	private String getObjectNameField(String mainObject){   			
		Map<String, Schema.SObjectField> fieldMap = getfieldMap(mainObject);
		if(fieldMap != null){
			for (String fieldName: fieldMap.keySet()) {
				if(fieldMap.get(fieldName).getDescribe().isNameField()){
					return fieldName;
				}
			}  	
		}			 
   		return null;
   	} 
   	
    private Map<String,String> getMainObjectTextFields(String mainObject){    	    	    	
		Map<String, Schema.SObjectField> fieldMap = getfieldMap(mainObject);
		if(fieldMap != null){
			Map<String,String> mainObjectTextFieldsMap = new Map<String,String>();
			for (String fieldName: fieldMap.keySet()) {
				Schema.DisplayType fielddataType = fieldMap.get(fieldName).getDescribe().getType();	
				if(fielddataType == Schema.DisplayType.String) {
					mainObjectTextFieldsMap.put(fieldName, fieldMap.get(fieldName).getDescribe().getLabel());
				}	
			}
			return mainObjectTextFieldsMap;	
		}    	
		return null;
    }
    
    private List<SelectOption> initDefaultFileNameList(){
    	List<SelectOption> optionsToReturn = new List<SelectOption>();
    	Map<String,String> mainObjectTextFields = new Map<String,String>();
		List<SelectOption> ret = new List<SelectOption>();
		mainObjectTextFields = getMainObjectTextFields(myForm.Main_Object__c);
		for(String textField: mainObjectTextFields.keySet()){
			optionsToReturn.add(new SelectOption(textField,mainObjectTextFields.get(textField)));
		}
		return optionsToReturn;
		
	}
}