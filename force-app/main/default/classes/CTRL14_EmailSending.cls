public with sharing class CTRL14_EmailSending {
	public String toId{get;set;}
	public String toName{get;set;}
	public String tos{get;set;}
	public String cc{get;set;}
	public String bcc{get;set;}
	public String subject{get;set;}
	public Boolean Success{get;private set;}
	public transient String body{get;set;}
	//public Config__c myConf{get;private set;}
	private Id docId;
	
	private String retURL;
	
	public CTRL14_EmailSending(){
		if(ApexPages.currentPage().getParameters().containsKey('template_id')){
			Document doc = [Select body From Document Where id = :ApexPages.currentPage().getParameters().get('template_id')];
			body = doc.body.toString();
			docId = doc.id;
		}
		if(ApexPages.currentPage().getParameters().containsKey('to_id')){
			if(toId == null)
				toId = ApexPages.currentPage().getParameters().get('to_id');
			if(toId != null){	
				Contact c = [Select Name From Contact Where id= :toId];
				toName = c.Name;
			}
		}
		else
			toId = '0000000000000000';
		if(ApexPages.currentPage().getParameters().containsKey('retURL')){
			retURL = ApexPages.currentPage().getParameters().get('retURL');
		}
		
		//myConf = Config__c.getInstance('Docomotion Default Settings');	
	}
	
	//The Method that send the Email
	public PageReference send(){
		boolean hasError = false;
		
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		List<String> toLst = new List<String>{};
		if(toId != null && toId != '' && toId != '0000000000000000'){
			Contact toContact = [Select Name, email From Contact Where id = :toId];
			if(toContact.email == null){
				hasError = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_no_email_field));
			}
			else{	
				toName = toContact.Name;
				toLst.add(toContact.email);
			}
		}
		/*else{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You have the fill a recipient.'));
		}*/
		if(cc != ''){
			cc = cc.replaceAll(',',';').replaceAll('\\s+',';');
			if(checkEmailAddresses(cc.split(';')))
				email.setCcAddresses(cc.split(';'));
			else{
				hasError = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_incorrect_cc));
			}
		}
		if(bcc != ''){
			bcc = bcc.replaceAll(',',';').replaceAll('\\s+',';');
			if(checkEmailAddresses(bcc.split(';')))
				email.setBccAddresses(bcc.split(';'));
			else{
				hasError=true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_incorrect_bcc));
			}	
		}
		
		if(tos != ''){
			tos = tos.replaceAll(',',';').replaceAll('\\s+',';');
			if(checkEmailAddresses(tos.split(';')))
				toLst.addAll(tos.split(';'));
			else{
				hasError = true;
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_incorrect_to));
			}	
		}
		if(toLst.isEmpty()){
			hasError=true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_no_recipient));
		}
		email.setToAddresses(toLst);
		if(subject==''){
			hasError = true;
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.VF14_no_subject));	
		}
		email.setsubject(subject);
		email.setHtmlBody(body);
		email.setCharset('UTF-8');
		if(!hasError){
			try{
				Messaging.SendEmailResult[] results = Messaging.SendEmail(new List<Messaging.Email>{email},true);
				if(results[0].isSuccess()){
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, Label.VF14_Mail_Sent));
					success=true;
				}
				else{
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, results[0].getErrors()[0].getMessage()));
					success=false;
				}	
			}
			catch(Exception e){
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
				success=false;
			}
		}
		Document d = new Document(id=docId,body=Blob.valueOf(body));
		update d;
		d = [Select body From Document Where id=:docId];
		body = d.body.toString().unescapeJava();	
		return null;
	}
	
	public PageReference cancel(){
		return new PageReference(retUrl);
	}
	
	private boolean checkEmailAddress(String email){
		String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; // source: <a href="http://www.regular-expressions.info/email.html" target="_blank">http://www.regular-expressions.info/email.html</a>
		Pattern MyPattern = Pattern.compile(emailRegex);
		Matcher MyMatcher = MyPattern.matcher(email);

    	return (MyMatcher.matches());
	}
	
	private boolean checkEmailAddresses(List<String> emails){
		boolean ret = true;
		for(String email:emails){
			ret &= checkEmailAddress(email);
		}
		return ret;
	}
}