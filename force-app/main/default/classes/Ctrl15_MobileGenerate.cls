/**
This class is for the remote function of the Mobile Generate Application
**/
public with sharing class Ctrl15_MobileGenerate {
	
	//For adpation dev / package
	public string nsField{
    	get{
    		if(nsField==null){
    			nsField = Utils.getNameSpace();
    		}
    		return nsField;
    	}
    	private set;
	}
    public string nsClass{
    	get{
    		if(nsClass==null){
    			nsClass = Utils.getNameSpaceForClass();
    		}
    		return nsClass;
    	}
    	private set;
	}
	public boolean isDesignerUser {
		get {
			if (isDesignerUser == null)
				isDesignerUser = Utils.isDesignerUser();
			return isDesignerUser;
		}
		private set;
	}
	
	
	//To return the forms for the main object of the selected sobjectId
	@RemoteAction
	public static String getForms(Id sobjectId, string view){
		string sobjectType = ''+sobjectId.getSobjectType();
		string soql = 'select Name__c,Status__c, Id, form_Id__c, description__c, Preview_PDF_Att_Id__c, ' + 
						'Current_Published_Version__r.HasPictureSetting__c,Current_Published_Version__r.contains_interactive_fields__c, Current_Published_Version__r.contains_interactive_collection__c, Current_Published_Version__r.Preview_PDF_Id__c, Current_Published_Version__r.Version__c, ' + 
						'Current_Activated_Version__r.HasPictureSetting__c, Current_Activated_Version__r.contains_interactive_collection__c, Current_Activated_Version__r.Preview_PDF_Id__c, Current_Activated_Version__r.Version__c, ' + 
						'(Select Subform_version__r.contains_interactive_fields__c, Subform_Version__r.contains_interactive_collection__c From Form_Subform_Associations__r) from Form__c';
    	soql += ' where ((active__c=true and current_published_Version__c != null) or Current_Activated_Version__c != null) and main_object__c=:sobjectType and RecordType.DeveloperName=\'Form\'';
    	if(view.equalsIgnoreCase('recent'))
    		soql += ' Order by LastViewedDate DESC NULLS LAST';
    	else if(view.equalsIgnoreCase('All'))
    		soql+= ' Order by Name__c Asc';
    	else if(view.equalsIgnoreCase('last_modified'))
    		soql+= ' Order by LastModifiedDate Desc';
    	return JSON.serialize(Database.query(soql));
	}
	
	//To return the sobject Name
	@remoteAction
	public static String getSobjectName(Id sobjectId){
		string fileName;
		String sobjectName = sobjectId.getSobjectType().getDescribe().getName();
		string nameField = (sobjectName=='Case')?'CaseNumber':'Name';
    	string soql = 'Select '+nameField+' From '+sobjectName+' Where id =\''+sobjectId+'\'';
    	try{
    		Sobject sobj = Database.query(soql).get(0);
    		fileName = (String)sobj.get(nameField);
			fileName = fileName.replaceAll('\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\?|\\?|\\?|\\?|\\"|\\\'', '_');
    		fileName = fileName.replaceAll('\\s', '_');
    	}
    	catch(Exception e){fileName='';}
    	return fileName;
	}
	
	//To return the lookup on contacts or leads
	@remoteAction
	public static String getLookups(Id sobjectId){
		string sobjectType = ''+sobjectId.getSobjectType();
		Utils.LookupData lookupdata = Utils.buildLookupData(sobjectType, sobjectId);
    	return JSON.serialize(lookupdata).unescapeHtml4();
	}

	@remoteAction
	public static String getContactLookupFields(Id sobjectId){
		string sobjectType = ''+sobjectId.getSobjectType();
		List<Utils.LookupPickListValue> contactLookups = Utils.getContactLookups(sobjectType, sobjectId);
    	return JSON.serialize(contactLookups);
	}
	
	//@remoteAction
	public static String saveGenerationWithFileName(Id sobjectId, Integer formId, string format, List<String> outputLst, Id lookupId, string fileName){
		return 	saveGenerationWithFileName(sobjectId, formId, format, outputLst, lookupId, fileName, false);
	}
	
	//To save the document
	@RemoteAction
	public static String saveGenerationWithFileName(Id sobjectId, Integer formId, string format, List<String> outputLst, Id lookupId, string fileName, Boolean isActivatedVersion){
		boolean isLink;//added by yuval 18/12/16
		Set<String> outputs = new Set<String>(outputLst);
		Map<String,String> ret = new Map<String,String>();
		//FOR TEST
		/*ret.put('errorNumber','4444');
		ret.put('errorTitle','TEST ERROR');
		return JSON.serialize(ret);*/
		//END FOR TEST
		//1. If the channel have a document, we have to check that the user have the right to create a new document
		if(outputs.contains('download') && (!Schema.sObjectType.Document.isCreateable())){
			ret.put('errorNumber','4000');
			return JSON.serialize(ret);
		}
		RenderAPI.Format f = getFormat(format);
		
		String url;
		try{
			if((!outputs.contains('link') && outputs.contains('chatter')) ||( outputs.contains('attachment') || outputs.contains('document')||outputs.contains('download')))
				url = RenderAPI.renderCreateFile(formId, sobjectId, RenderApi.Mode.Download, f, lookupId, fileName, isActivatedVersion);
			else isLink = true;
		}
		catch(SchemaFactory.RenderException re){
			ret.put('errorNumber',re.errorNumber);
			ret.put('errorTitle',re.errorTitle);
			ExceptionWS eWS = new ExceptionWS(re);
			eWs.setSessionId(UserInfo.getSessionId());
			eWS.send();
			return JSON.serialize(ret);
		}
		catch (utils.DocomotionQueryFieldException qe)
		{
			ret.put('errorField',qe.getFieldName());
			ret.put('errorSobject',qe.getsObject());
			ExceptionWS eWS = new ExceptionWS(qe, formId);
			eWs.setSessionId(UserInfo.getSessionId());
			eWS.send();
			return JSON.serialize(ret);
		}
		if(outputs.contains('download'))
			ret.put('download',url);
		
		Blob b;	
		if(!outputs.contains('download'))	
			 b = RenderAPI.getDocument(url);
		else if(outputs.size()>1){
			 b = RenderAPI.getDocument(url+'/0');
		}	
			
		try{
			if(outputs.contains('attachment'))
				ret.put('attachmentId',RenderAPI.saveAttachment(b, formId, ''+f, SobjectId, fileName));
			if(outputs.contains('document'))
				ret.put('documentId',RenderAPI.saveDocument(b, formId, ''+f, fileName));
			if(outputs.contains('chatter')&& !string.isBlank(url))
				ret.put('chatterId',RenderAPI.saveChatter(b, formId, ''+f, SobjectId, fileName));
			
			if(isLink == true){
				Signature__c Sresult;
				if(outputs.contains('chatter'))
					Sresult = RenderAPI.createLink( formId, SobjectId , lookupId, true, true);
				else 
					Sresult = RenderAPI.createLink( formId, SobjectId , lookupId, false, true);
				ret.put('link' , Sresult.id);
			}			
		}
		catch(DMLException de){
    		if(de.getDmlType(0) == StatusCode.INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY){
    			ret.put('errorNumber','4001');
    		}
    		else{
    			ret.put('errorNumber','4002');
    		}
    		ret.put('errorTitle',''+de.getDmlType(0));
			ExceptionWS eWS = new ExceptionWS(formId, ret.get('errorNumber')+' - '+de.getDmlType(0));
			eWs.setSessionId(UserInfo.getSessionId());
			eWS.send();		
    		return JSON.serialize(ret);
    	}	
		return JSON.serialize(ret);
	}
	
	//To save the document
	@remoteAction
	public static String saveGeneration(Id sobjectId, Integer formId, string format, List<String> outputLst, Id lookupId, Boolean isActivatedVersion){
		return saveGenerationWithFileName(sobjectId, formId, format, outputLst, lookupId, null, isActivatedVersion);
	}
	
	public static String saveGeneration(Id sobjectId, Integer formId, string format, List<String> outputLst, Id lookupId){
		return saveGenerationWithFileName(sobjectId, formId, format, outputLst, lookupId, null, false);
	}
	@RemoteAction
	public static Boolean hasPictureSettings(id Current_Activated_Version, id Current_Published_Version,string Status){
		string vId ;
		if(Status =='Published' )
			vId = Current_Published_Version;
		else if(Status =='Active')
			vId = Current_Activated_Version;
		Integer Pcount = [select COUNT() from Pictures_Setting__c where Version__c = :vId];
		if(Pcount>0)
			return true;
		return false;	
		
	}

	@remoteAction
	public static String asyncGeneration(Id sobjectId, Integer formId, string format, List<String> outputLst, Id lookupId,string fileName, Boolean isActivatedVersion){	
		boolean isLink;
		Set<String> outputs = new Set<String>(outputLst);
		Map<String,String> ret = new Map<String,String>();
		if(outputs.contains('download') && (!Schema.sObjectType.Document.isCreateable())){
			ret.put('errorNumber','4000');
			return JSON.serialize(ret);
		}
		map<string,string>Channels = new  map<string,string>();
		for(string Channel:outputs){
			Channel = Channel.toLowerCase();
			if(Channel == 'download')
				Channels.put('DOWNLOAD',null);
			if(Channel == 'attachment')
				Channels.put('ATTACHMENT',null);
			if(Channel == 'document')
				Channels.put('DOCUMENT',null);
			if(Channel == 'chatter')
				Channels.put('CHATTER',null);
			if(Channel == 'link')
				Channels.put('LINK',null);
			if(Channel == 'google')
				Channels.put('GOOGLE',null);	
		}
		RenderAPI.Format f = getFormat(format);
		RenderAPI.renderCreateFileAsyncIsPicture( formId,  sobjectId, null ,  f,  lookupId,  fileName,isActivatedVersion , Channels);
		return null;  
			
	}
	public static RenderAPI.Format getFormat(string format){
		RenderAPI.Format f;
		if(format=='pdf')
			f = RenderAPI.Format.PDF;
		else if(format=='html')
			f = renderAPI.Format.HTML;
		else if(format=='docx')
			f = renderAPI.Format.DOCX;
		return f;	
	
	}
}