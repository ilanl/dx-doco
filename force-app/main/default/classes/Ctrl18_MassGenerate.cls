public class Ctrl18_MassGenerate {
	public string mailBodySubject {get;set;}
	public static map<Schema.SObjectType ,map<string,string>> FieldsToFilter{
		get{
				if(FieldsToFilter == null){
					FieldsToFilter = new map<Schema.SObjectType,map<string,string>>();
					for(Fields_Filter__mdt ftf:  [SELECT Fields_Name__c,Sobject__c FROM Fields_Filter__mdt]){
						Schema.SObjectType stype = Schema.getGlobalDescribe().get(ftf.Sobject__c);
						if(FieldsToFilter.containsKey(stype)){
							FieldsToFilter.get(stype).put(ftf.Fields_Name__c,null);
						}
						else FieldsToFilter.put(stype,new map<string,string>{ftf.Fields_Name__c=>null} );	
					}
				}
				return FieldsToFilter;
		}
	} 
	//PRIVATE
	SobjectType sType;
	List<Form__c> formLst;
	List<Lookup> lookups;
	Id jobId;

	//PUBLIC VARIABLES
	public Set<Id> ids;


	//PROPERTIES
	public String view{get;set;}
	public String format{get;set;}
	public Id formId{get;set;}
	public string lookupStr{get;set;}
	public boolean combine{get;set;}
	public Id selectedTemplateId{get;set;}
	public boolean pollActive{get;private set;}
	public Mass_Generation_Execution__c report{get;private set;}

	//channels
	public boolean mail{get;set;}
	public boolean chatter{get;set;}
	public boolean link{get;set;}//for signature
	public boolean document{get;set;}
	public boolean attachment{get;set;}
	public boolean download{get;set;}

	// setting difault
	public string settingDefaultJson{get;set;}

    public String errorListJson { get; set; }


	//to check if the user has designer or admin permissions set
	public boolean isDesignerUser {
		get {
			if (isDesignerUser == null)
				isDesignerUser = Utils.isDesignerUser();
			return isDesignerUser;
		}
		private set;
	}

	//Return true if chatter is enable
	public Boolean chatterEnable{
		get{
			if (chatterEnable==null)
				chatterEnable = Utils.isChatterEnable();
			return chatterEnable;
		}
		private set;
	}

	//Return true if there are forms.
	public boolean hasForms{
		get {
			if (hasForms == null){
				if (isDesignerUser)
					hasForms = !formLst.isEmpty();
				else{
					hasForms = false;
					for (Form__c form : formLst){
						if (form.current_published_version__c != null){
							hasForms = true;
						}
					}
				}
				
			}
			return hasForms;
		}
		private Set;
	}

	//Namespace


	public String NameSpace{
		get{
			if(NameSpace==null){
				NameSpace = Utils.getNameSpace();
			}
			return NameSpace;
		}
		private set;
	}

	//For the template
	public List<SelectOption> templatesList{
		get{
			if (templatesList == null)
				templatesList = initTemplatesList();
			return templatesList;
		}
		private Set;
	}

	//CONSTRUCTOR
	public Ctrl18_MassGenerate(){
		initVariables();
		readParameters();
		if(!ids.isEmpty()){
			setType();
			setFormLst();
			setLookups();
			setDefaultChecked();
		}
	}

	public string getFormLstJSON(){
  		return JSON.serialize(formLst);
    }

	public void setFormLst(){
		setFormLst(''+sType);
	}

	public void setLookups(){
		lookups = setLookups(sType);
	}

	public string getLookupsJSON(){
		return JSON.serialize(lookups);
	}

	//To generate - will lunch a batch
	public void generateMass(){
		System.debug(Logginglevel.ERROR,'@@@@@@ attachment '+attachment);
		BA01_MassGeneration BA01obj = new BA01_MassGeneration(this, UserInfo.getSessionId());
		BA01obj.mailBodySubject = mailBodySubject;
		if(link){
			try{
				BA01obj.generateLink();
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The links have been created.'));
			}
			catch(DMLException dE){
				string errorMessage = dE.getDmlId(0)+' - '+dE.getMessage();
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
			}
			catch(Exception e){
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		}else{
			pollActive = true;
			jobId = Database.executeBatch(BA01obj,1);
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Generation process takes a few minutes. You can close this window. Once the generation ends, you will receive an email notification.'));
		}
	}


	public void rerenderReport(){
		System.debug('***jobId: '+jobId);
		if(jobId == null)
			return;
		try{
			report = [Select Id, Total_Number_of_Items__c, Number_Generated_Items__c, Number_of_errors__c, completed__c, Document_Id__c, Batch_URL__c, Error_Message__c,(Select Error__c, Related_To__c, Record_Name__c From Mass_Execution_Errors__r) From Mass_Generation_Execution__c Where job_Id__c =:jobId limit 1];
		    System.debug('rreport: '+report);
			List<Mass_Execution_Error__c> errorList = new List<Mass_Execution_Error__c>();
			for(Mass_Execution_Error__c error : report.Mass_Execution_Errors__r){
				errorList.add(error);
			}
			errorListJson = JSON.serialize(errorList);
			if(report.completed__C){
				pollActive = false;
			}
			System.debug('ppollActive: '+pollActive);
		}
		catch(exception e){
			system.debug(Logginglevel.ERROR,'@@@@@@@@@@@@@@@@@@@@@ EXCEPTION report failed '+e);
		}
	}


	//PRIVATE METHODS
	private void initVariables(){
		ids = new Set<Id>();
		view='recent';
		formLst = new List<Form__c>();
		lookups = new List<lookup>();
		mail = false;
		chatter = false;
		link = false;//for signature
		document = false;
		attachment = false;
		download = false;
		combine = false;
		pollActive = false;
	}

	private void readParameters(){
		if(ApexPages.currentPage().getParameters().containsKey('ids')){
			for(String idString : ApexPages.currentPage().getParameters().get('ids').split(',')){
				ids.add((Id)idString);
			}
		}
	}

	
	private void setType(){
		List<Id> idsLst = new List<Id>(Ids);
		sType = idsLst.get(0).getSobjectType();
	}

	//return the list of all forms for the current object
	private void setFormLst(string sobjectType) {
       String soql = 'select Name__c, Id, form_Id__c, description__c, preview_PDF_Att_Id__c, ' + 
						'Allow_Changing_Channel__c, Allow_Changing_File_Name__c, Allow_Changing_Format__c, Default_Channel__c, 	Default_Format__c, Hide_Channel_in_Generation__c, Hide_File_Name_in_Generation__c, Hide_Format_in_Generation__c, Use_Record_Name__c, ' +
						'Allow_Changing_Template__c, Default_Template__c, Hide_Template_In_Generation__c, Combine_Allow_Changing__c, Combine_Default__c, Recipient_Default__c, Recipient_Allow_Changing__c, Recipient_Hide__c, '+
        				'current_published_Version__r.contains_interactive_fields__c, current_published_Version__r.contains_interactive_collection__c, current_published_version__r.Preview_PDF_Id__c, Current_Published_Version__r.Version__c,' + 
        				'Current_Activated_Version__r.contains_interactive_fields__c, Current_Activated_Version__r.contains_interactive_collection__c, Current_Activated_Version__r.Preview_PDF_Id__c, Current_Activated_Version__r.Version__c, ' +
        				'(Select Subform_version__r.contains_interactive_fields__c, Subform_Version__r.contains_interactive_collection__c From Form_Subform_Associations__r) from Form__c';
    	soql += ' where current_published_Version__c != null and main_object__c=:sobjectType and RecordType.DeveloperName=\'Form\'';
    	if(view.equalsIgnoreCase('recent'))
    		soql += ' Order by LastViewedDate DESC NULLS LAST Limit 10';
    	else if(view.equalsIgnoreCase('All'))
    		soql+= ' Order by Name__c Asc';
    	else if(view.equalsIgnoreCase('last_modified'))
    		soql+= ' Order by LastModifiedDate Desc  Limit 10';
    	formLst = (List<Form__c>)(Database.query(soql));
    	Utils.log('formLst:', formLst);
    }

	private List<Lookup> setLookups(SobjectType sType){
		return setLookups(stype, '', '', true);
	}

	private List<Lookup> setLookups(SobjectType sType, string currentPath, string currentLabel, boolean toContinue){
		List<Lookup> ret = new List<Lookup>();
		if(currentPath != ''){
			currentPath = Utils.getRelationshipName(currentPath);
			currentPath += '.';
		}
		if(currentLabel != ''){
			currentLabel = ' of '+currentLabel + ' lookup';
		}

		if(currentPath == '' && (sType == Contact.SobjectType || sType == Lead.SobjectType)){
			ret.add(new Lookup('Id','Record Itself', ''+sType));
		}
		Map<String, Schema.SobjectField> FsMap = sType.getDescribe().fields.getMap();
		for(String fName : fsMap.keySet()){
			Schema.DescribeFieldResult field = FsMap.get(fName).getDescribe();
		
			if(field.getType() == Schema.DisplayType.REFERENCE && field.getReferenceTo().size()==1 && ( (!(FieldsToFilter.containsKey(sType)&&FieldsToFilter.get(sType).containsKey(field.getName()))) &&(field.getReferenceTo().get(0) == Contact.SobjectType || field.getReferenceTo().get(0) == Lead.SobjectType ))){
				system.debug('***field: '+field.getName());
				ret.add(new Lookup(currentPath+field.getName(), field.getLabel()+currentLabel,''+field.getReferenceTo().get(0)));
				if(toContinue){
					ret.addAll(setLookups(field.getReferenceTo().get(0), field.getName(), field.getLabel(), false));
				}
			}
		}
		return ret;
	}




	private void setDefaultChecked() {
		Map<Id,DefaultSetting> formItemMap = new Map<Id,DefaultSetting>();
		for(Form__c formItem : formLst){
				DefaultSetting ds = new DefaultSetting(formItem.Id, formItem.Default_Channel__c, formItem.Default_Format__c,
					formItem.Allow_Changing_Channel__c, formItem.Allow_Changing_Format__c,
					formItem.Hide_Format_in_Generation__c, formItem.Hide_Channel_in_Generation__c,
					formItem.Default_Template__c, formItem.Combine_Allow_Changing__c, formItem.Combine_Default__c,
					formItem.Recipient_Default__c, formItem.Recipient_Allow_Changing__c, formItem.Recipient_Hide__c
					);
				formItemMap.put(formItem.Id, ds);
		}
		settingDefaultJson = JSON.serialize(formItemMap);
	}

	//To return the list of templates
	private List<SelectOption> initTemplatesList(){
		List<SelectOption> ret = new List<SelectOption>();
		if (Schema.sObjectType.EmailTemplate.IsAccessible() && Schema.sObjectType.EmailTemplate.fields.name.IsAccessible())
		{
			Set<String> toIgnore = new Set<String>{'document_expired','New_Link'};
			for (EmailTemplate et : [Select id, Name, DeveloperName From EmailTemplate Where isActive=true and folder.namespaceprefix=:NameSpace.replace('__','') and folder.DeveloperName='Docomotion_Emails'])
			{
				if(!toIgnore.contains(et.DeveloperName))
					ret.add(new SelectOption(et.Id, et.Name));
			}
		}
		return ret;
	}



	class Lookup {
		public string path;
		public string label;
		public string type;

		public Lookup(string p, string l, string t){
			path = p;
			label = l;
			type = t;
		}
	}




	class DefaultSetting {
		public Id formId;
		public String channelDefault;
		public String formatDefault;
		public String emailTemplateDefault;
		public String recipientDefault;
		public boolean recipientAllowChanging;
		public boolean recipientHide;
    	public boolean channelAllowChanging;
		public boolean formatAllowChanging;
		public boolean formatHide;
		public boolean channelHide;
		public boolean combineAllowChanging;
		public boolean defCombineChecked;

		public DefaultSetting(Id id, String cd, String fd, boolean cac, boolean fac, boolean fh, boolean ch, String etd, boolean coac, boolean coh, String rd, boolean rac, boolean rh){
			formId = id;
			channelDefault = cd;
	   	    formatDefault = fd;
		    channelAllowChanging = cac;
		    formatAllowChanging = fac;
		    formatHide = fh;
		    channelHide = ch;
		    emailTemplateDefault = etd;
			combineAllowChanging = coac;
		    defCombineChecked = coh;	
		    recipientDefault = rd;
		 	recipientAllowChanging = rac;
			recipientHide = rh;	
		}
	}
}