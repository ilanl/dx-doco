/**
* @author ilanl@balink.net
* @description Class that handle DML operation in ISV packages.
* This class enforces CRUD and FLS while executing DML operations
*/
public class DML_CustomExtension {

	/* SETTINGS */
	public static Boolean deepCheck = false;

	/* THE PUBLIC STATIC Methods for executing DML */
	/* ON single Record */
	public static void insertObj(Sobject sobj){
		insertObj(sobj, null);
	}

	public static void insertObj(Sobject sobj, Set<Schema.sobjectField> fieldsToCheck){
		insertObj(new List<Sobject>{sobj}, fieldsToCheck);
	}

	public static void updateObj(Sobject sobj){
		updateObj(sobj, null);
	}

	public static void updateObj(Sobject sobj, Set<Schema.sobjectField> fieldsToCheck){
		updateObj(new List<Sobject>{sobj}, fieldsToCheck);
	}


	public static void upsertObj(Sobject sobj){
		upsertObj(sobj, null);
	}

	public static void upsertObj(Sobject sobj, Set<Schema.sobjectField> fieldsToCheck){
		upsertObj(new List<Sobject>{sobj}, fieldsToCheck);
	}

	public static void deleteObj(Sobject sobj){
		deleteObj(new List<Sobject>{sobj});
	}

	public static void undeleteObj(Sobject sobj){
		undeleteObj(new List<Sobject>{sobj});
	}

	/* ON List of records */
	public static void insertObj(List<Sobject> sobj){
		insertObj(sobj,null);
	}

	public static void insertObj(List<Sobject> sobj, Set<Schema.sobjectField> fieldsToCheck){
		if (!sobj.isEmpty() && check(sobj, DMLActions.a_insert, fieldsToCheck)){
			system.debug(logginglevel.error,sobj);
			insert sobj;
		}
	}

	public static void updateObj(List<Sobject> sobj){
		updateObj(sobj, null);
	}

	public static void updateObj(List<Sobject> sobj, Set<Schema.sobjectField> fieldsToCheck){
		if (!sobj.isEmpty() && check(sobj, DMLActions.a_update, fieldsToCheck)){
			update sobj;
		}
	}

	public static void upsertObj(List<Sobject> sobj){
		upsertObj(sobj, null);
	}

	public static void upsertObj(List<Sobject> sobj, Set<Schema.sobjectField> fieldsToCheck){
		Map<DMLActions, List<sobject>> recordsMap = buildActionsMapOnUpsert(sobj);
		if (recordsMap.containsKey(DMLActions.a_insert)){
			insertObj(recordsMap.get(DMLActions.a_insert),fieldsToCheck);
		}
		if (recordsMap.containsKey(DMLActions.a_update)){
			updateObj(recordsMap.get(DMLActions.a_update),fieldsToCheck);
		}
	}

	public static void deleteObj(List<Sobject> sobj){
		if (!sobj.isEmpty() && check(sobj, DMLActions.a_delete)){
			delete sobj;
		}
	}

	public static void undeleteObj(List<Sobject> sobj){
		if (!sobj.isEmpty() && check(sobj, DMLActions.a_undelete)){
			undelete sobj;
		}
	}

	private static Boolean check(List<sobject> records, DMLActions action){
		return check(records, action, null);
	}

	private static Boolean check(List<sobject> records, DMLActions action, Set<Schema.sobjectField> fieldsToCheck){
		PreparingDmlList pDMLList = new PreparingDmlList(records,action, fieldsToCheck);
		return pDMLList.check();
	}

	private static Map<DMLActions, List<sobject>> buildActionsMapOnUpsert(List<Sobject> records){
		Map<DMLActions, List<Sobject>> ret = new Map<DMLActions, List<Sobject>>();
		for (Sobject so : records){
			DMLActions a = (so.Id != null)?DMLActions.a_update:DMLActions.a_insert;
			if (!ret.containsKey(a))
					ret.put(a, new List<Sobject>());
				ret.get(a).add(so);
		}
		return ret;
	}

	/* CLASS FOR PREPARING THE DATA ON LIST*/
	class PreparingDmlList{
		private DMLActions action;
		private Map<Schema.SobjectType, List<Sobject>> recordsMap;//Map SobjectType => records
		private Set<Schema.sobjectField> fieldsToCheck;

		public PreparingDmlList(List<SObject> records, DMLActions anAction, Set<Schema.sobjectField> fToCheck){
			action = anAction;
			buildRecordsMap(records);
			fieldsToCheck = fToCheck;
		}

		public Boolean check(){
			Boolean ret = true;
			for (List<Sobject> records : recordsMap.values()){
				DmlList dmlOnList = new DmlList(records, action, fieldsToCheck);
				ret &= dmlOnList.check();
			}
			return ret;
		}

		/* Building the map of records when all key represents a type */
		private void buildRecordsMap(List<Sobject> records){
			recordsMap = new Map<Schema.SobjectType, List<Sobject>>();
			Schema.SobjectType listType = records.getSObjectType();
			if (listType != null)
				recordsMap.put(listType, records);
			else{
				for (Sobject record : records){
					if (!recordsMap.containsKey(record.getSObjectType()))
						recordsMap.put(record.getSObjectType(), new List<Sobject>());
					recordsMap.get(record.getSObjectType()).add(record);
				}
			}
		}
	}

	/* CLASS That check DML on List */
	Class DmlList{
		private DMLActions action;
		private List<Sobject> sobjectLst;//List of sobject of the SAME type
		private Schema.DescribeSobjectResult sobjectDescribe;
		private Set<Schema.sobjectField> fieldsToCheck;

		public DmlList(List<SObject> records, DMLActions anAction, Set<Schema.sobjectField> fToCheck){
			action = anAction;
			sobjectLst = records;
			sobjectDescribe = sobjectLst[0].getSObjectType().getDescribe();
			fieldsToCheck = fToCheck;
		}

		public boolean check(){
			if (!isCrudAllowed()){
				throw new DMLActionException(sobjectDescribe, action);
				return false;
			}
			else if(action != DMLActions.a_delete && action != DMLActions.a_undelete)//On Delete or Undelete there is no need to check FLS
				checkFLS();
			return true;
		}

		/* To check the crud */
		private boolean isCrudAllowed(){
			if (action == DMLActions.a_insert)
				return sobjectDescribe.isCreateable();
			else if(action == DMLActions.a_update)
				return sobjectDescribe.isUpdateable();
			else if(action == DMLActions.a_delete)
				return sobjectDescribe.isDeletable();
			else //undelete
				return sobjectDescribe.isUndeletable();
		}

		/* To Check the FLS */
		private boolean checkFLS(){
			FLSCheckOnSobject flsCheck = new FLSCheckOnSobject(action, sobjectLst, sobjectDescribe, fieldsToCheck);
			List<Schema.DescribeFieldResult> fieldsWithoutRights = flsCheck.checkFLS();
			if (fieldsWithoutRights.isEmpty()){
				return true;
			}
			else
			{
				throw new DMLFieldException(sobjectDescribe, action, fieldsWithoutRights);
				return false;
			}
		}

	}

	/* CLASS THAT CHECKS THE FLS */
	class FLSCheckOnSobject{
		private DMLActions action;
		private List<Sobject> records;
		private Schema.DescribeSobjectResult sobjectResult;
		private Map<String,Schema.SObjectField> fieldMap;
		private Set<Schema.SobjectField> fieldsToCheck;

		/* CONSTRUCTOR */
		public FLSCheckOnSobject(DMLActions a, List<Sobject> r, Schema.DescribeSobjectResult dr, Set<Schema.sobjectField> fToCheck){
			records = r;
			action = a;
			sobjectResult = dr;
			fieldMap = sobjectResult.fields.getMap();
			if (fToCheck != null)
				fieldsToCheck = fToCheck;
			else{
				fieldsToCheck = new Set<Schema.SobjectField>();
				initFieldsLst();
			}
		}

		public List<Schema.DescribeFieldResult> checkFLS(){
			return fieldsWithoutRights();
		}

		//To build the List of fields to check
		private void initFieldsLst(){
			if (deepCheck)
				fieldsLstAdd(records.get(0));
			else{
				for (Sobject so : records){
					fieldsLstAdd(so);
				}
			}
		}

		private void fieldsLstAdd(Sobject record){
			Map<String, Object> queriedFieldValues = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(record));
    		for(String fieldName:queriedFieldValues.keySet()){
    			if(fieldName != 'Id' && fieldName != 'attributes' && fieldMap.containsKey(fieldName) && !fieldsToCheck.contains(fieldMap.get(fieldName)))
    				fieldsToCheck.add(fieldMap.get(fieldName));
    		}
		}

		private Boolean hasRight(Schema.DescribeFieldResult fr){
			if (action == DMLActions.a_insert)
				return fr.isCreateable();
			else //update
				return fr.isUpdateable();
		}

		private boolean fieldToCheck(Schema.DescribeFieldResult fr){
			return !(
				!fr.isPermissionable() || 
        		fr.isCalculated() || 
        		fr.isQueryByDistance() || 
        		fr.isDisplayLocationInDecimal() || 
        		fr.isAutoNumber() || 
        		fr.getType().equals(Schema.DisplayType.Location));
		}

		private List<Schema.DescribeFieldResult> fieldsWithoutRights(){
			List<Schema.DescribeFieldResult> ret = new List<Schema.DescribeFieldResult>();
			for (Schema.sobjectField field : new List<Schema.SobjectFIeld>(fieldsToCheck)){
				Schema.DescribeFieldResult fr = field.getDescribe();
				if (fieldToCheck(fr) && !hasRight(fr))
					ret.add(fr);
			}
			return ret;
		}



	}

	/* ENUM FOR OPERATIONS */
	public enum DMLActions {
		a_insert,//INSERT MODE
		a_update,//UPDATE MODE
		a_upsert,//UPSERT MODE
		a_delete,//DELETE MODE
		a_undelete//UNDELETE MODE
	}

	/* THE EXCEPTIONS CLASS */
	/* Exception class for CRUD */
	public class DMLActionException extends Exception{
		private Schema.DescribeSobjectResult sobjectDescribe;
		private DMLActions action;

		public DMLActionException(Schema.DescribeSobjectResult sResult, DMLActions a){
			sobjectDescribe = sResult;
			action = a;
			this.setMessage(getMessageStr());
		}

		public string getMessageStr(){
			return 'User doesn\'t have the '+getActionName(action)+' right on the '+sobjectDescribe.getName()+' sObject';
		}
	}
	/* Exception class for FLS */
    public class DMLFieldException extends Exception{
		private Schema.DescribeSobjectResult sobjectDescribe;
		private DMLActions action;
		private List<Schema.DescribeFieldResult> fieldsDescribe;

		public DMLFieldException(Schema.DescribeSobjectResult sResult, DMLActions a, List<Schema.DescribeFieldResult> fieldsD){
			sobjectDescribe = sResult;
			action = a;
			fieldsDescribe = fieldsD;
			this.setMessage(getMessageStr());
		}

		public string getMessageStr(){
			List<String> fieldsName = new List<String>();
			for(Schema.DescribeFieldResult fr : fieldsDescribe){
				fieldsName.add(fr.getName());
			}
			return 'User doesn\'t have the '+getActionName(action)+' right on the {'+String.join(fieldsName,', ')+'} field' +((fieldsName.size() == 1)?'':'s')+ ' of the '+sobjectDescribe.getName()+' sObject';
		}
	}

	/** GLOBAL STATIC METHODS FOR THIS CLASS **/
	private static string getActionName(DMLActions action){
		if (action == DMLActions.a_insert)
			return 'insert';
		else if(action == DMLActions.a_update)
			return 'update';
		else if(action == DMLActions.a_upsert)
			return 'upsert';
		else if(action == DMLActions.a_delete)
			return 'delete';
		else //undelete
			return 'undelete';
	}
}