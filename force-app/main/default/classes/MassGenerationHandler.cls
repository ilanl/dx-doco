public class MassGenerationHandler {

	public static void generateFiles(List<Mass_Generation_Execution__c> execs, Map<Id,Mass_Generation_Execution__c> oldMap){
		for(Mass_Generation_Execution__c newExec : execs){
			if(newExec.Batch_URL__c != null && newExec.Batch_URL__c != oldMap.get(newExec.Id).Batch_URL__c){
				System.EnqueueJob(new QU01_PostGenerationProcess(newExec));
			}
		}
	}

}