global class PostInstallScriptDebugOrg implements InstallHandler{
	global void onInstall(InstallContext context){
		//the settings
		Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
		if(myConf == null)
			myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=true,Delete_chatter_files__c=false, server_URL__c='https://apiqa.docomotioninc.com');
		else
			myConf.server_URL__c='https://apiqa.docomotioninc.com';	
		upsert myConf;
		
		if(context.previousVersion() == null){//it's the first time we install the app
			//For the form ID begins with 1
			Form__c f = new Form__c(Name__c='todelete');
			insert f;
			delete f;
		}
	}
}