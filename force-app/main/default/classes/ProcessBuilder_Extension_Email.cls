global with sharing class ProcessBuilder_Extension_Email {
	@InvocableMethod
	global static void processBuilderEmail(list<PbeRequest> requests){//Id formId,Id recoredId,string To,set<id> ccList,id templateId
    	for(PbeRequest request: requests){
    		System.enqueueJob(new ProcessBuilder_Extension_Queueable(request.formId, request.sobjectId,request.whoId, 'EMAIL',request.format, request.isActivatedVersion,request.fileName,request.TemplateId));
    		//RenderAPI.renderCreateFile(request.formId, request.sobjectId,RenderAPI.getMode(request.Mode),RenderAPI.getFormat(request.format), request.whoId, request.fileName, request.isActivatedVersion,request.TemplateId);
    	}     
   	}
   	global with sharing class PbeRequest {
 	@InvocableVariable(required=true label='Id (Integer) of the Form')
    global Integer formId;

    @InvocableVariable(required=true label='Insert the record reference, for example -> Contant.Id')
    global ID sobjectId;
    
    @InvocableVariable(required=true label='Email Recipient Id')
    global ID whoId;
    
    @InvocableVariable(required=true LABEL='Choose format :{PDF,HTML,DOCX,HTML4S,TXT,BMP,JPEG,PNG,GIF,TIF}')
    global string format;
    
    @InvocableVariable(required=true)
    global Boolean isActivatedVersion;
    
    @InvocableVariable(required=true label='Insert file name')
    global string fileName;
    @InvocableVariable(required=true label='Insert Template Id')
    global id TemplateId;
    }   
}