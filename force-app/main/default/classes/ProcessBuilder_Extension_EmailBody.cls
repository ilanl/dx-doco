global with sharing class ProcessBuilder_Extension_EmailBody {
	@InvocableMethod
	global static void processBuilderEmailBody(list<PbeeRequest> requests){//Id formId,Id recoredId,string To,set<id> ccList
    	for(PbeeRequest request: requests){
    		System.enqueueJob(new ProcessBuilder_Extension_Queueable(request.formId,request.sobjectId,request.whoId,'EMAILBODY','HTML4S',request.isActivatedVersion,null,null));
    		//RenderAPI.renderCreateFile(request.formId, request.sobjectId,RenderAPI.getMode(request.Mode),RenderAPI.getFormat(request.format), request.whoId, request.fileName, request.isActivatedVersion,null);
    	}   
   	}
	global with sharing class PbeeRequest {
 	@InvocableVariable(required=true label='Id (Integer) of the Form')
    global Integer formId;

    @InvocableVariable(required=true label='Insert the record reference, for example -> Contant.Id')
    global ID sobjectId;
    
    @InvocableVariable(required=true label='Email Recipient Id')
    global ID whoId;
    
    @InvocableVariable(required=true)
    global Boolean isActivatedVersion;
    
    }   	     
}