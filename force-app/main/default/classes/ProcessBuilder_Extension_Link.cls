global with sharing class ProcessBuilder_Extension_Link {
	@InvocableMethod
	global static void processBuilderLink(list<PblRequest> requests){//Id formId,Id recoredId,Id whoId
		for(PblRequest request: requests)
			System.enqueueJob(new ProcessBuilder_Extension_Queueable(request.formId, request.sobjectId,request.whoId, 'LINK',null,false,null,null));
    		//RenderAPI.createLink(request.formId,request.sobjectId,request.whoId,false,true);    
   	}
   	global with sharing class PblRequest {
 	@InvocableVariable(required=true label='Id (Integer) of the Form')
    global Integer formId;

    @InvocableVariable(required=true label='Insert the record reference, for example -> Contant.Id')
    global ID sobjectId;
    
    @InvocableVariable(required=true label='Insert Signer Id')
    global id whoId;
    
    }   	        
}