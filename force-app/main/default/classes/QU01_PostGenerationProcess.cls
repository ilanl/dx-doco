public class QU01_PostGenerationProcess implements System.Queueable, Database.AllowsCallouts{
	
	private Mass_Generation_Execution__c eProcess;
	private Form__c form;
	private string fileName;
	public Blob b;
	public String format;
	public GoogleCall gCall;
	public GoogleUtils.GoogleFile gFile;
	public QU01_PostGenerationProcess(Mass_Generation_Execution__c execProcess){
		gCall = new GoogleCall();
		eProcess = execProcess.clone(true);
		system.debug(eProcess);
		form = [Select Id,form_Id__c,name__c  From Form__c Where Id =:eProcess.form__c];
	}

	public void execute(QueueableContext context){
		
		if(eProcess.Batch_URL__c != null && !eProcess.Completed__c){
			system.debug('***IN');
			
			//if(needGetBlob()){
				if(eProcess.Format__c != null)
					format = eProcess.Format__c;
				else format = getFormat(eProcess.Batch_URL__c);
				b = getDocument(eProcess.Batch_URL__c);
				if(eProcess.Document__c){
					saveDocument(b, format, eProcess.Object_Name__c, (Integer)form.Form_Id__c);
				}
				if(eProcess.Attachment__c){
					system.debug('***b '+b);
					saveAttachment(b,format ,eProcess.Sobject_Id__c);
				}
				if(eProcess.Google_Drive__c){
					gFile = RenderAPI.saveGoogleDoc( b,  format,eProcess.GooglePath__c ,  fileName.substringBeforeLast('.'));
				}
				if(eProcess.Chatter__c){
					saveChatter(b,format ,eProcess.Sobject_Id__c);
				}
				if(eProcess.Download__c){
					//no need
				}
				
				if(eProcess.Mail_Body__c || eProcess.Mail__c){
					if(Limits.getEmailInvocations() >= Limits.getLimitEmailInvocations())
					throw new Utils.CustomException('You\'ve exceed your email sending limit.');
					//1.b check if there is a recipient
					if(eProcess.AsincRecipientId__c == null)
						throw new Utils.CustomException('There isn\'t any recipient for the desired lookup.');
					//2. build the mail
					Messaging.SingleEmailMessage mail = createEmail(eProcess.AsincRecipientId__c,eProcess.Sobject_Id__c, b, eProcess.Mail_Body__c);
					//3. send the mail
					List<Messaging.SendEmailResult> emailResults = Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {mail});
					if(!emailResults.IsEmpty() && !emailResults.get(0).isSuccess()){
						throw new Utils.CustomException('Email fails: '+emailResults.get(0).getErrors());
					}
				}
				
			//}
			eProcess.Completed__c=true;
			DML_CustomExtension.updateObj(eProcess);
		}
	}

	private string getFormat(String fileName){
		return fileName.substringAfterLast('.');
	}

	private boolean needGetBlob(){
		return (eProcess.document__c);
	}

	private Blob getDocument(string url){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
		if(url==null)
			return null;
        string first = url.substringBeforeLast('/');
		fileName = url.substringAfterLast('/');
		string urlDoc2 = first + '/'+EncodingUtil.urlEncode(fileName,'UTF-8');
        req.setEndpoint(urlDoc2);
        req.setMethod('GET');
        req.setTimeout(60000);
        try{
            HttpResponse res = h.send(req);
            if(res.getStatusCode() == 200)
                return res.getBodyAsBlob();
            else
                return null;
        }
        catch(Exception e){
        	Utils.log('***Ex: ',e.getMessage());
        	if (Test.isRunningTest()) 
            	return Blob.valueof('test');
        	return null;
    	}  
    }

	private void saveDocument(Blob b, String format, string objectName, Integer formIdNumber) { 
        Document doc=new Document();
        doc.Body = b;
        string documentFolder = UserInfo.getUserId();
        doc.folderId = documentFolder;	
        doc.Name = objectName + ' - '+formIdNumber  + ' - ' + datetime.now() + '.'+format;
        DML_CustomExtension.insertObj(doc);
        eProcess.Document_Id__c = doc.Id;
    }
    
    private string saveAttachment(Blob b,string formatExtension , id SobjectId) {
        Attachment att=new Attachment();
        att.Body = b;
        att.Name =  fileName;
        att.parentId = SobjectId;
        att.ContentType = getContentType();
        DML_CustomExtension.insertObj(att);
        return att.Id;
    }
	
 

	//To generate simple chatter file
    private void saveChatter(Blob b,string formatExtension , id SobjectId) {
        FeedItem fi = new FeedItem();
		fi.ContentData = b;
        fi.ParentId = SobjectId;
        fi.Type = 'ContentPost';
        fi.ContentFileName = fileName;
        if(gFile != null)
        	fi.linkUrl = gFile.webViewLink;
        DML_CustomExtension.insertObj(fi);
    }
	private Messaging.SingleEmailMessage createEmail(Id Recipient,Id SobjectId, Blob b, boolean HTML4S){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(Recipient);
		if(!HTML4S){
			string fileExtention ;
			Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
			email.setTemplateID(eProcess.TemplateId__c);
			efa.setBody(b);
			efa.setContentType( getContentType());
			fileName = fileName.replaceAll('\\s|\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
			fileName = fileName.replaceAll('\\s\\s+','_');
			efa.setFileName(fileName);
			email.setFileAttachments(new list<Messaging.Emailfileattachment>{efa});
			if(canSetWhatId(SobjectId.getSobjectType()))
				email.setWhatId(SobjectId);
		}
		else{
			email.setSubject('Docomotion '+form.name__c+' ('+form.form_Id__c+') generated mail');
			system.debug('@@@@@@@@@@@@@ blob');
			email.setHtmlBody(b.toString().unescapeJava());
			email.setCharset('UTF-8');
		}
		email.setSaveAsActivity(false);
		return email;
	}
	private boolean canSetWhatId(Schema.SobjectType sobjectType){
		return ((''+sobjectType) == 'Account' || (''+sobjectType) == 'Asset' || (''+sobjectType) == 'Campaign' || (''+sobjectType) == 'Case' || (''+sobjectType) == 'Contract' || (''+sobjectType) == 'Opportunity' || (''+sobjectType) == 'Order' || (''+sobjectType) == 'Product' || (''+sobjectType) == 'Solution' || sobjectType.getDescribe().isCustom());
	}
	private string getContentType(){
		if(format == 'DOCX'){
			return  'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
		}
		else if(format == 'HTML'){
			return 'text/html';
		}
		else if(format == 'PDF'){
			return 'application/pdf';
		}
		return null;
	}
	

}