global with sharing class RenderAPI {
	
	/** GLOBAL METHODS**/
	global enum Mode {DOCUMENT,ATTACHMENT,CHATTER,DOWNLOAD,GOOGLE,LINK,EMAIL,EMAILBODY}
	global enum Format {PDF,HTML,DOCX,HTML4S,TXT,BMP,JPEG,PNG,GIF,TIF}
	
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format){
		return renderCreateFile(formId, sobjectId, aMode, format, null, null, false);
	}
	
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId){
		return renderCreateFile(formId, sobjectId, aMode, format, whoId, null, false);
	}
	
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, Boolean isActivatedVersion){
		return renderCreateFile(formId, sobjectId, aMode, format, whoId, null, isActivatedVersion);
	}
	
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, String fileName){
		return renderCreateFile(formId, sobjectId, aMode, format, null, fileName, false);
	}
	
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, string fileName){
		return renderCreateFile(formId, sobjectId, aMode, format, whoId, fileName, false);
	}
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, string fileName, Boolean isActivatedVersion){
		return renderCreateFile(formId, sobjectId, aMode, format, whoId, fileName, isActivatedVersion, null);
	}
	global static string renderCreateFile(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, string fileName, Boolean isActivatedVersion,id TemplateId){
		system.debug('***formId: '+formId);
		system.debug('***sobjectId: '+sobjectId);
		system.debug('***aMode: '+aMode);
		system.debug('***whoId: '+whoId);
		system.debug('***fileName: '+fileName);
		system.debug('***isActivatedVersion: '+isActivatedVersion);
		system.debug('***TemplateId: '+TemplateId);
		Form__c form = [Select Id ,Name__c ,form_Id__c From Form__c where form_Id__c=:formId];
		SchemaFactory.Form f = new SchemaFactory.Form(sobjectId);
		f.setForm(form.id, isActivatedVersion);
		f.setFormat(''+format);
		f.setIsApiCall(true);
		if (fileName != null)
			f.setFileName(fileName);
		if(whoId != null)
			f.setWhoId(whoId);
		if(aMode == Mode.LINK){
			return getSignatureURL(formId, sobjectId, whoId);
		}
		string url = f.getFormUrl();
		if(aMode == Mode.DOWNLOAD)
			return url;
		else if(aMode == Mode.EMAIL || aMode == Mode.EMAILBODY){
			Blob b = getDocument(url);
			createEmail( form, whoId, SobjectId,  b, TemplateId, aMode == Mode.EMAILBODY ,fileName, string.valueof(format));   	
		}	
		else{	
	        Blob b = getDocument(url);
	        Id retID;
	        if(aMode == Mode.CHATTER){
	            retId = saveChatter(b, formId, ''+format, sobjectID, fileName);
	        }
	        else if(aMode == Mode.ATTACHMENT){
	            retId = saveAttachment(b, formId, ''+format, sobjectId, fileName);
	        }
	        else if(aMode == Mode.DOCUMENT){
	            retId = saveDocument(b, formId, ''+format, fileName);
	        }
	        return retId;
		}
		return null;
	}
	
	global static void renderCreateFileAsync(Integer formId, Id sobjectId, Mode aMode, Format format){
		renderCreateFileAsync(formId, sobjectId, aMode, format, null, null);
	}
	
	global static void renderCreateFileAsync(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId){
		renderCreateFileAsync(formId, sobjectId, aMode, format, whoId, null);
	}
	
	global static void renderCreateFileAsync(Integer formId, Id sobjectId, Mode aMode, Format format, string fileName){
		renderCreateFileAsync(formId, sobjectId, aMode, format, null, fileName);
	}
	
	global static void renderCreateFileAsync(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, string fileName){
		if (fileName==null)
			fileName='Preview';
		Form__c form = [Select Id, Current_Published_Version__r.total_version__c From Form__c where form_Id__c=:formId]; 
		SchemaFactory.Form f = new SchemaFactory.Form(sobjectId);
		f.setForm(form.id);
		f.setFormat(''+format);
		f.setIsApiCall(true);
		if(whoId != null)
			f.setWhoId(whoId);
		string ffidata = f.getFFIData();
		string body  = f.generateJson(ffidata);
		getDocumentUrlAsync(body,''+aMode,''+format,sobjectID, formId, ''+(Integer)(form.Current_Published_Version__r.total_version__c), fileName);	
	}
	global static void renderCreateFileAsyncIsPicture(Integer formId, Id sobjectId, Mode aMode, Format format, Id whoId, string fileName,Boolean isActivatedVersion , map<string,string>Channels){
		if (fileName==null)
			fileName='Preview';
		Form__c form = [Select Id, Current_Published_Version__r.total_version__c From Form__c where form_Id__c=:formId]; 
		SchemaFactory.Form f = new SchemaFactory.Form(sobjectId);
		f.setForm(form.id);
		f.setFormat(''+format);
		f.setChanel( Channels.containskey('ATTACHMENT'), Channels.containskey('GOOGLE'), Channels.containskey('DOWNLOAD'),Channels.containskey('DOCUMENT') , Channels.containskey('LINK'),Channels.containskey('CHATTER') ,Channels.containskey('MAIL') , (''+format=='HTML4S'));
		f.setIsApiCall(true);
		if(whoId != null)
			f.setWhoId(whoId);
		string ffidata = f.getFFIData();
		SchemaFactory.RenderRequest rr;
		string body;
		rr = f.createRenderRequest( ffidata, UserInfo.getSessionId() ,true);
		ID jobID  = System.EnqueueJob(new AsyncGenerationRequest(rr)); 
		
	}
	
	global static Blob renderBlob(Integer formId, Id sobjectId, Format format){
		return renderBlob(formId, sobjectId, format, null);
	}
	
	global static Blob renderBlob(Integer formId, Id sobjectId, Format format, Id whoId){
		Form__c form = [Select Id From Form__c where form_Id__c=:formId]; 
		SchemaFactory.Form f = new SchemaFactory.Form(sobjectId);
		f.setForm(form.id);
		f.setFormat(''+format);
		f.setIsApiCall(true);
		if(whoId != null)
			f.setWhoId(whoId);
		string url = f.getFormUrl();
		Blob b = getDocument(url);
		return b;
	}

	//to create a link
	global static Signature__c createLink(Integer formId, Id sobjectId, Id whoId, boolean chatter, boolean toInsert){
		Signature__c signature;
		try{
			signature = createSignature(formId, sobjectId, whoId, chatter);
			if(toInsert)
				DML_CustomExtension.insertObj(signature);
		}
		catch(Exception e){
			Utils.log('EXCEPTION', e.getMessage()+'; '+e.getStackTraceString());
		}
		return signature;
	}

	//to create a link
	global static List<Signature__c> createLinks(Id sobjectId, Map<Integer, Id> whoIdsMap, boolean chatter, boolean toInsert){
		List<Signature__c> signatures = new List<Signature__c>();
		try{
			for(Integer formId : whoIdsMap.keySet()){
				signatures.add(createSignature(formId, sobjectId, whoIdsMap.get(formId), chatter));
			}
			if(toInsert)
				DML_CustomExtension.insertObj(signatures);
		}
		catch(Exception e){
			Utils.log('EXCEPTION', e.getMessage()+'; '+e.getStackTraceString());
		}
		return signatures;
	}

	//to insert link
	global static boolean insertLinks(List<Signature__c> signatures){
		try{
			DML_CustomExtension.insertObj(signatures);
			return true;
		}
		catch(Exception e){
			Utils.Log('Exception',e);
			return false;
		}
	}

	/**MASS GENERATION**/
	global static void massGenerate(Integer formId, Set<Id> sobjectIds, Mode aMode, Format format, boolean combine){
		massGenerate(formId, sobjectIds, aMode, format, combine, null, null);
	}

	global static void massGenerate(Integer formId, Set<Id> sobjectIds, Mode aMode, Format format, boolean combine, Id TemplateId){
		massGenerate(formId, sobjectIds, aMode, format, combine, null, templateId);
	}

	global static void massGenerate(Integer formId, Set<Id> sobjectIds, Mode aMode, Format format, boolean combine, string whoIdField){
		massGenerate(formId, sobjectIds, aMode, format, combine, whoIdField, null);
	}
	
	global static void massGenerate(Integer formId, Set<Id> sobjectIds, Mode aMode, Format format, boolean combine, String whoIdField, Id templateId){
		Form__c form = [Select Id From Form__c where form_Id__c=:formId];
		BA01_MassGeneration massGeneration = new BA01_MassGeneration(sobjectIds, form.Id, ''+format, true, UserInfo.getsessionId(), aMode, combine, whoIdField, templateId);
		Database.executeBatch(massGeneration,1);
	}

	/** PUBLIC METHODS**/
	//public because it use by the CTRL 15 too
    public static Id saveAttachment(Blob b, Integer formName, String format, id SobjectId, string fileName) {
    	System.debug(Logginglevel.ERROR,'@@@@@@ new att ');
        Attachment att = new Attachment();	
    	att=new Attachment();
    	if(fileName != null)
        	att.Name = fileName +'.'+getFormatExtension(format);
        else	
        	att.Name = formName  + ' - ' + datetime.now() + '.'+getFormatExtension(format);        	
    	att.parentId = SobjectId;
		if (format=='pdf')
			att.contentType = 'application/pdf';
		else if(format=='docx')
    		att.contentType = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
		else if(format=='html')
			att.contentType = 'text/html';
        att.Body = b;
        DML_CustomExtension.upsertObj(att);
        return att.Id;
    }
    
	//public because it use by the CTRL 15 too
    public static Id saveDocument(Blob b, Integer formName, String format, string fileName) { 
        Document doc=new Document();
        doc.Body = b;
        string documentFolder = UserInfo.getUserId();
        doc.folderId = documentFolder;
		if(fileName != null)
			doc.Name = fileName + '.'+getFormatExtension(format);
		else	
        	doc.Name = formName  + ' - ' + datetime.now() + '.'+getFormatExtension(format);
        DML_CustomExtension.insertObj(doc);
        return doc.Id;
    }

    
	//public because it use by the CTRL 15 too
    public static ID saveChatter(Blob b, Integer formName, String format, Id SobjectID, string fileName) {
        FeedItem fi = new FeedItem();
        fi.ContentData = b;
        fi.ParentId = SobjectId;
        fi.Type = 'ContentPost';
        if(fileName != null)
        	fi.ContentFileName = fileName + '.'+getFormatExtension(format);
        else	
        	fi.ContentFileName = formName + ' - ' + datetime.now() + '.'+getFormatExtension(format);
        DML_CustomExtension.insertObj(fi);
        return fi.id;
    }

	public static GoogleUtils.GoogleFile saveGoogleDoc(Blob b, String format, string googlePath, string fileName){
		GoogleUtils.GoogleFile ret = null;
		GoogleCall gCall = new GoogleCall();
		fileName = fileName +'.'+format;
		string pathId = gCall.getFolderId(googlePath);
		string overridingFileId = gCall.haveFileInFolder(pathId,fileName);
		if (overridingFileId != null && overridingFileId.length() > 3)//We want to update an existing file
			ret = gCall.updateExistingFile(overridingFileId,format,b);
		else //Create a new File
			ret = gCall.createNewFile(fileName,format,pathId,b);
		gCall.saveSettings();
		return ret;
	}
    
    public static Blob getDocument(string url){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
		if(url==null)
			return null;
        string first = url.substringBeforeLast('/');
		string fileName = url.substringAfterLast('/');
		string urlDoc2 = first + '/'+EncodingUtil.urlEncode(fileName,'UTF-8');
        req.setEndpoint(urlDoc2);
        req.setMethod('GET');
        req.setTimeout(60000);
        try{
            HttpResponse res = h.send(req);
            if(res.getStatusCode() == 200)
                return res.getBodyAsBlob();
            else
                return null;
        }
        catch(Exception e){
        	Utils.log('***Ex: ',e.getMessage());
        	if (Test.isRunningTest()) 
            	return Blob.valueof('test');
        	return null;
    	}  
    }
	
	/** PRIVATE METHODS**/
	@future(callout=true)
	public static void getDocumentUrlAsync(string body, string mode, string format, Id sobjectID, Integer formId, String Version, string fileName) {
        String fileURL;
		String filePath = '';   
		if (fileName.countMatches('\\') > 0)
		{
			filePath = fileName.substringBeforeLast('\\');
			fileName = fileName.substringAfterLast('\\');
		}
        Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
    	string END_POINT = myConf.Server_URL__c+'/api/render';
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(END_POINT);
        req.setMethod('POST');
        req.setHeader('Content-Type','application/json');
        Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        //string body = '{"VersionID" : '+Version+',"UserId" : "'+UserInfo.getUserId()+'","IsLicenseUser" : '+Utils.isUserDocoLicensed()+', "OrgID" : "'+UserInfo.getOrganizationId()+'","FFIData" : "' + FFIDataTxt + '" }';
        Utils.log('BODY',body);
        req.setBody(body);
        req.setTimeout(60000);
        if(Test.isRunningTest()) {
           fileURL =  'www.test.com';
        }
        else{
	        HttpResponse res = h.send(req);
	        Utils.log('REPONSE',res.getBody());
	        JSONParser parser = JSON.createParser(res.getBody());
	        
	        while (parser.nextToken() != null) {
	            if (parser.getCurrentToken() == JSONToken.FIELD_NAME && parser.getText() == 'FileURL'){
	                parser.nextToken();
	                fileURL = parser.GetText();
	            }
	        }
        }
        if(fileURL != null){
        	Blob b = getDocument(fileUrl);
	        if(mode == 'CHATTER'){
	            saveChatter(b, formId, ''+format, sobjectID,fileName);
	        }
	        else if(mode == 'ATTACHMENT'){
	            saveAttachment(b, formId, ''+format, sobjectId,fileName);
	        }
	        else if(mode == 'DOCUMENT'){
	            saveDocument(b, formId, ''+format,fileName);
	        }
			else if(mode == 'GOOGLE'){
				saveGoogleDoc(b, ''+format, filePath, fileName);
			}
        }
    }

    
    private static string getFormatExtension(string format){
    	if(format=='HTML4S')
    		return 'html';	
    	else
    		return format.toLowerCase();			
    }

	//To create the signature
	private static string getSignatureUrl(Integer formId, Id recordId, string lookupId){
		Signature__c s = createSignature(formId, recordId, lookupId, false);
		DML_CustomExtension.insertObj(s);
		return (new PageReference('/'+s.id)).getUrl();
	}

	private static Signature__c createSignature(Integer formId, Id recordId, string lookupId, boolean chatter){
		Signature__c s = new Signature__c();
		s.Form__r = new Form__c(Form_Id__c = formId);
		s.Record_ID__c = recordId;
		s.published__c = true;
		s.chatter__c = chatter;
		if(lookupId != null && lookupId != '' && Utils_Type.isId(lookupId)){
			Id lId = (Id)lookupId;
			if(lId.getSobjectType() == Contact.sobjectType)
				s.Signer__c = lId;
			else if(lId.getSobjectType() == Lead.sobjectType)
				s.Signer_Lead__c = lId;
		}
		return s;
	}
	public static Messaging.SingleEmailMessage createEmail(Form__c form,Id Recipient,Id SobjectId, Blob b,ID TemplateId, boolean HTML4S,string fileName , string format){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		email.setTargetObjectId(Recipient);
		if(!HTML4S){
			string fileExtention ; 
			Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
			email.setTemplateID(TemplateId);
			efa.setBody(b);
			efa.setContentType( getContentType(format));
			fileName = fileName.replaceAll('\\s|\\+|\\{|\\}|\\\\|\\^|\\%|\\`|\\[|\\]|\\>|\\<|\\~|\\||\\#|\\�|\\�|\\�|\\�|\\"|\\\'', '_');
			fileName = fileName.replaceAll('\\s\\s+','_');
			efa.setFileName(fileName+'.'+format.tolowercase());
			email.setFileAttachments(new list<Messaging.Emailfileattachment>{efa});
			if(canSetWhatId(SobjectId.getSobjectType()))
				email.setWhatId(SobjectId);
		}
		else{
			email.setSubject('Docomotion '+form.name__c+' ('+form.form_Id__c+') generated mail');
			system.debug('@@@@@@@@@@@@@ blob');
			system.debug(b.size());
			email.setHtmlBody(b.toString().unescapeJava());
			email.setCharset('UTF-8');
		}
		email.setSaveAsActivity(false);
		
		System.debug('@@@@@@@@@@ SendEmailResult = '+Messaging.sendEmail(new list<Messaging.SingleEmailMessage>{email}));
		return email;
	}
	public static string getContentType(string format){
		if(format == 'DOCX'){
			return  'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
		}
		else if(format == 'HTML'){
			return 'text/html';
		}
		else if(format == 'PDF'){
			return 'application/pdf';
		}
		return null;
	}
	public static boolean canSetWhatId(Schema.SobjectType sobjectType){
		return ((''+sobjectType) == 'Account' || (''+sobjectType) == 'Asset' || (''+sobjectType) == 'Campaign' || (''+sobjectType) == 'Case' || (''+sobjectType) == 'Contract' || (''+sobjectType) == 'Opportunity' || (''+sobjectType) == 'Order' || (''+sobjectType) == 'Product' || (''+sobjectType) == 'Solution' || sobjectType.getDescribe().isCustom());
	}
	global static RenderAPI.Mode  getMode(string mode){
    	mode = mode.toUpperCase();
    	if(string.valueOf(RenderAPI.Mode.CHATTER)==mode)
    		return RenderAPI.Mode.CHATTER;
    	if(string.valueOf(RenderAPI.Mode.DOCUMENT)==mode)
    		return RenderAPI.Mode.DOCUMENT;
    	if(string.valueOf(RenderAPI.Mode.ATTACHMENT)==mode)
    		return RenderAPI.Mode.ATTACHMENT;
		if(string.valueOf(RenderAPI.Mode.EMAIL)==mode)
    		return RenderAPI.Mode.EMAIL;
    	if(string.valueOf(RenderAPI.Mode.EMAILBODY)==mode)
    		return RenderAPI.Mode.EMAILBODY;
    	if(string.valueOf(RenderAPI.Mode.LINK)==mode)
    		return RenderAPI.Mode.LINK;	
    	return null;
    }
    global static RenderAPI.Format  getFormat(string format){
    	format = format.toUpperCase();
    	if(string.valueOf(RenderAPI.Format.PDF)==format)
    		return RenderAPI.Format.PDF;
    	if(string.valueOf(RenderAPI.Format.HTML)==format)
    		return RenderAPI.Format.HTML;
    	if(string.valueOf(RenderAPI.Format.DOCX)==format)
    		return RenderAPI.Format.DOCX;
		if(string.valueOf(RenderAPI.Format.TXT)==format)
    		return RenderAPI.Format.TXT;
		if(string.valueOf(RenderAPI.Format.BMP)==format)
    		return RenderAPI.Format.BMP;
		if(string.valueOf(RenderAPI.Format.JPEG)==format)
    		return RenderAPI.Format.JPEG;
		if(string.valueOf(RenderAPI.Format.PNG)==format)
    		return RenderAPI.Format.PNG;
		if(string.valueOf(RenderAPI.Format.GIF)==format)
    		return RenderAPI.Format.GIF;
		if(string.valueOf(RenderAPI.Format.TIF)==format)
    		return RenderAPI.Format.TIF;	
    	return null;
    }
	

}