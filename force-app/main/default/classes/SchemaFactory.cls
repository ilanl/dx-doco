public with sharing class SchemaFactory {
    
    private static final string END_POINT;

    private static Map<string, string> keyPrefixMap;

  	static{
	    Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
	    END_POINT = myConf.Server_URL__c+'/api/render';
  	}

	private static String generateRandomString(Integer len) {
		final String chars = 'ABCDEFtemplateIdGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		String randStr = '';
		while (randStr.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randStr += chars.substring(idx, idx+1);
		}
		return randStr; 
	}

    public class Form{
    	public id TemplateId;
    	public map<string,Boolean> Chanel;
    	//Public Boolean IsInsertPicture ;
        public string Name { get; private set; }
        private string VersionID;
        public Id sobjectId;   
        private DataSchema ds;
        private Id whoId;
        private string format = 'PDF';
        private string fileName;
		private boolean IsApiCall = false;
		public string googlePath ;
        public RenderRequest rr ;
        
        public Form(Id sobjectId){
            this.sobjectId = sobjectId;
        }
        public void setChanel(Boolean  attachment,Boolean gDrive,Boolean download,Boolean document,Boolean link,Boolean chatter,Boolean mail,Boolean MailBody){
        	Chanel = new map<string,Boolean>{
        		'attachment'=>attachment,
        		'gDrive'=>gDrive,
        		'download'=>download,
        		'document'=>document,
        		'link'=>link,
        		'chatter'=>chatter,
        		'mail'=>mail,
        		'MailBody'=>MailBody
        		};
        }
        public void setForm(Id formId){
        	setForm(formId, false);
        }
        public void setForm(Id formId, boolean isActivated){
        	TemplateId = formId;
            ds = new DataSchema(formId, true, isActivated);
            VersionID = ds.getPublishVersionId();
        }

		public void setIsAPICall(boolean apiCall){
			isApiCall = apiCall;
		}

		public Integer getFormId(){
			Form__c f = (Form__c) ds.getDataSchema();
			return (Integer)(f.form_Id__c);
		}
        
        public void setFileName(string fName){
        	this.fileName = fName;
        }
        
        public void setFormat(String format){
        	this.format = format;
        }
        
        public void setWhoId(Id id){
            whoId = id;
        }


		public boolean hasWhoId(){
			return (whoId != null);
		}
        
        public Id getSobjectId(){
            return this.sobjectId;
        }
        
        public string getName(){
            return ''+ds.ds.form_Id__c;
        }       
        
        //to build the xml we send to the WS
        public string getFormUrl(){
        	return getFormUrl(false); 
        }
        public string getFormUrl(Boolean isItAsync){
            string dataStr = buildFFiDataTxt(); 
            if (dataStr.IndexOf('?>') > -1)
                dataStr = dataStr.SubstringAfter('?>');
			//dataStr = dataStr.escapeJava();
            Utils.log('XML',dataStr);
            if(isItAsync)
            	return dataStr;
            return getDocumentUrl(dataStr);
        }
        
        
        public string getFFIData(){
        	string dataStr = buildFFiDataTxt();
            if (dataStr.IndexOf('?>') > -1)
                dataStr = dataStr.SubstringAfter('?>');
			//dataStr = dataStr.escapeJava();
			return dataStr;
        }
		public string generateJson(string FFIDataTxt ){
			rr = createRenderRequest(FFIDataTxt);
			return JSON.serialize(rr);
			//return '{"VersionID" : '+ds.getPublishVersionNumber()+', "SfSessionId" : "'+UserInfo.getSessionId()+'", "UserId" : "'+UserInfo.getUserId()+'","IsLicenseUser" : '+Utils.isUserDocoLicensed()+',"OrgID" : "'+UserInfo.getOrganizationId()+'","FFIData" : "' + FFIDataTxt + '","IsApiCall" :'+IsApiCall+'}';
		}


		public RenderRequest createRenderRequest(string FFIDataText, string sessionId){
			return  createRenderRequest( FFIDataText,  sessionId , false);
		
		}
		public RenderRequest createRenderRequest(string FFIDataText, string sessionId ,Boolean IsInsertPicture){
		
		
		
			System.debug('@@@@@@@@@@@@@@ inside createRenderRequest');
			RenderRequest ret = new RenderRequest();
			ret.IsInsertPicture = IsInsertPicture;
			ret.VersionID = ds.getPublishVersionNumber();
			ret.SfSessionId = sessionId;
			ret.UserId = UserInfo.getUserId();
			ret.IsLicenseUser = Utils.isUserDocoLicensed();
			ret.OrgID = UserInfo.getOrganizationId();
			ret.FFIData = FFIDataText;
			ret.IsApiCall = IsApiCall;
			ret.isInteractive = !(whoId == null);
			if(IsInsertPicture){
				
				String sobjectName = sobjectId.getSObjectType().getDescribe().getName();
				RecordType rty = [SELECT Id, Name, DeveloperName, sObjectType FROM RecordType WHERE DeveloperName ='Generation_Asynchonous'];
				Mass_Generation_Execution__c mge = new Mass_Generation_Execution__c(
					RecordTypeId = rty.id,
					Form__c = ds.ds.id,
					Attachment__c = Chanel.get('attachment') ,
					Chatter__c = Chanel.get('chatter'),
					Document__c = Chanel.get('document'),
					Download__c = Chanel.get('download'),
					Mail__c = Chanel.get('mail') ,
					Mail_Body__c = Chanel.get('MailBody'),
					Google_Drive__c = Chanel.get('gDrive'),
					Object_Name__c = sobjectName ,
					Sobject_Id__c = sobjectId,
					Total_Number_of_Items__c = 1,
					AsincRecipientId__c = whoId,
					TemplateId__c = TemplateId,
					Format__c = format,
					GooglePath__c = googlePath);
				insert 	mge;
				ret.AsyncId = mge.id;
				System.debug('@@@@@@@@@@@@@ mge '+mge);
			}
				 
			return ret;
		}

		public RenderRequest createRenderRequest(string FFIDataText){
			return createRenderRequest(FFIDataText, UserInfo.getSessionId());
		}

		public RenderRequest createRenderRequest(){
			return createRenderRequest(getFFIData());
		}
        
		public RenderRequest createRenderRequestWithSessionId(string sessionId){
			return createRenderRequest(getFFIData(), sessionId); 
		}

        public string getDocumentUrl(string FFIDataTxt) {
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(END_POINT);
            req.setMethod('POST');
            req.setHeader('Content-Type','application/json');
            Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);   
            string body = generateJson(FFIDataTxt);
            Utils.log('BODY',body);
            req.setBody(body);
            req.setTimeout(60000);
            try{
	            HttpResponse res = h.send(req);
	            Utils.log('REPONSE',res.getBody());
	            if(res.getStatusCode() != 200){
	            	throw new RenderException('4003', res.getStatus()+' ('+res.getStatusCode()+')', getFormId());
	            }
	            RenderResponse response = (RenderResponse)JSON.deserialize(res.getBody(), RenderResponse.class);
	            if(response.ErrorID == null || response.ErrorId == '0'){
	            	return response.FileURL;
	            }
	            else{//error
	            	throw new RenderException(response.ErrorId, response.MessageId, getFormId(),response.InternalErrorCode);
                    //throw new RenderException(response.ErrorId, response.MessageId, getFormId());
	            }
            }
            catch(RenderException re){
            	throw re;
            }
            catch(Exception e){
				if (Test.isRunningTest()) 
			    	return '{"FileURL":"www.test.com\\test.pdf"}';
			    else{throw new RenderException('4003', e.getMessage(), getFormId());}	
			}
            
            return null;
        }
        
        private String buildFFiDataTxt(){
            DOM.Document doc = new DOM.Document();
            dom.Xmlnode rootNode = doc.createRootElement('AutoFontTempFFI',null,null);
            addSettingNode(rootNode);
            addFieldsNode(rootNode);
            addInteractiveFormNode(rootnode);
            return doc.toXmlString();
        }
        
        private void addChildElementWithText(Dom.XmlNode node, string nodeName, string txt) {
            node.addChildElement(nodeName, null, null).addTextNode(txt);
        }
        
        private void addSettingNode(Dom.XmlNode node){
            DOM.XmlNode ffNode = node.addChildElement('freeform',null,null);
            addChildElementWithText(ffnode, 'project_symbol', 'GRP');
            addChildElementWithtext(ffnode, 'form_id',''+ds.ds.form_id__c);
            addChildElementWithtext(ffnode, 'forms_path','D:\\\\Autofont_tfs\\\\Designer Launcher\\\\Designer Launcher\\\\bin\\\\Debug\\\\Temp');
            addChildElementWithtext(ffnode,'technology','VIEW');
            addChildElementWithtext(ffnode,'analyzer_id','0');
            addChildElementWithtext(ffnode,'code_page','65001');
            DOM.XmlNode viewNode = ffNode.addChildElement('view',null,null);
            DOM.XmlNode fileNode = viewNode.addChildElement('file',null,null);
            if(fileName != null)
            	addChildElementWithtext(fileNode,'file_name','D:\\\\Autofont_tfs\\\\Designer Launcher\\\\Designer Launcher\\\\bin\\\\Debug\\\\'+fileName);
        	else
            	addChildElementWithtext(fileNode,'file_name','D:\\\\Autofont_tfs\\\\Designer Launcher\\\\Designer Launcher\\\\bin\\\\Debug\\\\preview'+generateRandomString(16));
            addChildElementWithtext(fileNode,'auto_launch','0');
            addChildElementWithtext(fileNode,'overwrite','1');
            fileNode.addChildElement('run_after',null,null);
            addChildElementWithtext(viewNode,'destination','buffer');
            addChildElementWithtext(viewNode,'format',format);
            viewNode.addChildElement('format_parameters',null,null).addChildElement('pdf',null,null).addChildElement('interactive',null,null).addChildElement('action',null,null);
            DOM.XmlNode printNode = ffNode.addChildElement('print',null,null);
            DOM.XmlNode fileNode2 = printNode.addChildElement('file',null,null);
            addChildElementWithtext(fileNode2,'file_name','D:\\\\Autofont_tfs\\\\Designer Launcher\\\\Designer Launcher\\\\bin\\\\Debug\\\\Temp%projectsymbol%_%time%.prn');
            addChildElementWithtext(fileNode2,'auto_launch','0');
            addChildElementWithtext(fileNode2,'overwrite','1');
            fileNode2.addChildElement('run_after',null,null);
            addChildElementWithText(printNode,'copies','1');
            addChildElementWithText(printNode,'destination','PORT');
            printNode.addChildElement('queue_name',null,null);
            addChildElementWithText(printNode,'spool_job_name','Autofont');
            addChildElementWithText(printNode,'spool_timeout','0');
            addChildElementWithText(printNode,'input_tray','0');
            addChildElementWithText(printNode,'duplex','1');
            DOM.XmlNode operationNode = ffNode.addChildElement('operation_mode',null,null);
            operationNode.addChildElement('act_on_error',null,null);
            addChildElementWithText(operationNode,'use_lc','0');
            operationNode.addChildElement('writing_mode',null,null);
            operationNode.addChildElement('data_tags_warnings',null,null).addChildElement('tag_replacement_string',null,null);
        }
        
        private void addFieldsNode(DOM.XmlNode node){
            DOM.XmlNode rootNode = node.addChildElement('ROOT',null,null);
            SYSTEM.Debug('***test sobjectId: '+sobjectId);
            system.debug('***test versionId: '+VersionId);
            list<Boolean> IsInsertPictureList = new list<Boolean>();
            system.debug(logginglevel.info,'@@@@@@@@@@@ addFieldsNode sobjectId '+sobjectId);
            ds.generateRenderXmlNode(rootNode, sobjectId ,VersionID,IsInsertPictureList);
            /*if(IsInsertPictureList.size()>0)
            	IsInsertPicture = true;
        	else 
        		IsInsertPicture = false;*/
			addInteractiveFieldsInFieldsNode(rootNode);
        }

		//To add an interactive field in the fields node
		private void addInteractiveFieldsInFieldsNode(Dom.XmlNode rootNode){
			Set<Id> xmlIdsAttachment = new Set<Id>();
			Set<Id> xmlIdsChatter = new Set<Id>();
			Map<Id,InteractiveIdsStructure> idsMap = new Map<Id, InteractiveIdsStructure>();
			Form__c f = [Select  current_published_Version__r.XML_ID__c, (Select Subform_version__r.XML_ID__c From Form_Subform_Associations__r) From Form__c Where Id = :ds.getDataSchema().Id];
			if (f.current_published_Version__r.XML_ID__c != null && isAttachmentFile(f.current_published_Version__r.XML_ID__c))
				xmlIdsAttachment.add(f.current_published_Version__r.XML_ID__c);
			else if(f.current_published_Version__r.XML_ID__c != null && isChatterFile(f.current_published_Version__r.XML_ID__c))
				xmlIdsChatter.add(f.current_published_Version__r.XML_ID__c);
			for (Form_Subform_Association__c association : f.Form_Subform_Associations__r){
				if (isAttachmentFile(association.Subform_version__r.XML_ID__c))
					xmlIdsAttachment.add(association.Subform_version__r.XML_ID__c);
				else if(isChatterFile(association.Subform_version__r.XML_ID__c))
					xmlIdsChatter.add(association.Subform_version__r.XML_ID__c);
			}
			if (!xmlIdsAttachment.isEmpty()){
				for (Attachment xmlAttachment : [Select body From Attachment Where id in :xmlIdsAttachment]){
					addXmlInteractiveData(xmlAttachment.body, idsMap);
				}
			}
			if (!xmlIdsChatter.isEmpty()){
				for (ContentVersion xmlVersion: [Select VersionData From ContentVersion Where id in :xmlIdsChatter]){
					addXmlInteractiveData(xmlVersion.VersionData, idsMap);
				}
			}
			buildXmlInteractiveNode(idsMap, rootNode);
		}

		private void buildXmlInteractiveNode(Map<Id,InteractiveIdsStructure> idsMap, Dom.XmlNode rootNode){
			Dom.XmlNode iNode = rootNode.addChildElement('Interactive_fields', null, null);
			for (Id id:idsMap.keySet()){
				Dom.XmlNode node = iNode.addChildElement(id+'', null, null);
				idsMap.get(id).addXmlNode(node);
			}
		}

		private void addXmlInteractiveData(Blob b , Map<Id,InteractiveIdsStructure> idsMap){
			Dom.Document doc = new Dom.Document();
			doc.load(b.toString());
			Dom.XmlNode root = doc.getRootElement();
			Dom.XmlNode interactiveNode = root.getChildElement('InteractiveRootTag',null);
			if (interactiveNode != null){
				for (Dom.XmlNode surveyNode:interactiveNode.getChildElements()){
					if (surveyNode.getName() == 'IBranch' && surveyNode.getAttribute('isSurvey', null)=='true'){
						String tagName = surveyNode.getAttribute('tag_name', null);
						if (!idsMap.containsKey(tagName))
							idsMap.put(tagName, new InteractiveIdsStructure());
						for (Dom.XmlNode sNode : surveyNode.getChildElements())
							addSurveyNode(sNode ,  idsMap.get(tagName));
					}
				}
			}
		}

		private void addSurveyNode(Dom.XmlNode sNode, InteractiveIdsStructure parentIdsStructure){
			if (sNode.getAttribute('tag_name',null) != null){
				InteractiveIdsStructure idsStructure = parentIdsStructure.add(sNode.getAttribute('tag_name',null));
				for (Dom.XmlNode cNode : sNode.getChildElements())
							addSurveyNode(cNode ,  idsStructure);
			}
		}

		private boolean isAttachmentFile(Id fileId){
			return (fileId.getSobjectType() == Schema.Attachment.SObjectType);
		}

		private boolean isChatterFile(Id fileId){
			return (fileId.getSobjectType() == Schema.ContentVersion.SobjectType);
		}

        private void addInteractiveFormNode(DOM.XmlNode node){
            DOM.XmlNode sfdcNode = node.addChildElement('sfdc',null,null);
            addChildElementWithtext(sfdcNode,'whoid', (whoId==null)?'':whoid);
            addChildElementWithtext(sfdcNode,'formid',''+ds.ds.form_id__c);
            addChildElementWithtext(sfdcNode,'whatid',''+sobjectId);
        }
     
    } 

    
    public class RenderResponse{
    	public Id OrgId;
    	public String FileURL;
    	public String S3Key;
    	public String MessageID;
    	public String ErrorID;
    	public string InternalErrorCode;// added by yuval 12/12/16
    }
    
    public class RenderException extends Exception{
        public String InternalErrorNumber{get; private set;}	
    	public String errorNumber{get; private set;}
    	public String errorTitle{get; private set;}
		public Integer formId{get; private Set;}
    	
    	public RenderException(String en, String et, Integer fId){
    		errorNumber = en;
    		errorTitle = et;
			formId = fId;
			this.setMessage(getMessageError());
    	}
        public RenderException(String en, String et, Integer fId, String InternalErrorCode ){
            errorNumber = en;
    		errorTitle = et;
			formId = fId;
    		InternalErrorNumber = InternalErrorCode;
			this.setMessage(getMessageError());
    	}
    	
    	public string getMessageError(){
    		if(!string.isBlank(InternalErrorNumber) && InternalErrorNumber!='0')
                return ''+errorNumber+': '+errorTitle+'\n'+' InternalErrorCode : '+InternalErrorNumber;
            return ''+errorNumber+': '+errorTitle;
    	}
    }

	public class InteractiveIdsStructure{
		private Map<String,InteractiveIdsStructure> ids;

		public InteractiveIdsStructure(){
			this.ids = new Map<String,InteractiveIdsStructure>();
		}

		public InteractiveIdsStructure add(String id){
			if (!ids.containsKey(id))
				ids.put(id, new InteractiveIdsStructure());
			return ids.get(id);
		}

		public void addXmlNode(Dom.XmlNode parentNode){
			for (string id: ids.keySet()){
				Dom.xmlNode currentNode = parentNode.addChildElement(id,null,null);
				ids.get(id).addXmlNode(currentNode);
			}
		}
	}

	public class RenderRequest{
		public string OrgId;
		public string FFIData;
		public string VersionID;
		public boolean IsLicenseUser;
		public string UserId;
		public string SfSessionId;
		public Boolean isInteractive;
		public boolean isApiCall;
		public BatchData RenderBatchData;
		public Boolean IsInsertPicture;
		public Id AsyncId;

		public void setRenderBatchData(string jobId, Integer signal, Integer type){
			setRenderBatchData(jobId, signal, type, null);
		}
		
		public void setRenderBatchData(string jobId, Integer signal, Integer type, Decimal ExpHours){
			RenderBatchData = new BatchData();
			RenderBatchData.SfBatchId = jobId;
			RenderBatchData.signal = signal;
			RenderBatchData.type = type;
			RenderBatchData.ExpHours = ExpHours;
		}
	}

	public class BatchData{
		public string SfBatchId;
		public Integer Signal;
		public Integer type;
		public Decimal ExpHours;
	}
	

}