public class SignaturesRestCall {

	private final static string retURL = 'api/interactive/';
	private static boolean isTest = false;

	@future(callout=true)
	public static void SendSignatures(Set<Id> signaturesIds, Decimal expirationTime){
		SendCall sCall = new SendCall(signaturesIds, expirationTime);
		if(isTest)
			return;
		CallResponse response = httpCall('POST', 'save', sCall.getJsonStr());
		if(!response.Success)
			Utils.log('ERROR',response.Error);
		else
		{
			List<Signature__c> toUpdate = new List<Signature__c>();
			for (Id sId : new List<Id>(signaturesIds)){
				toUpdate.add(new Signature__c(Id=sId, Created__c=true));
			}
			try{
				update toUpdate;
			}
			catch(Exception e){}
		}
	}

	public static void setIsTest(boolean t){
		isTest = Test.isRunningTest() && t;
	}

	
	public static void deleteFilesOnDocomotion(Set<Id> signaturesIds){
		SendCall sCall = new SendCall(signaturesIds, 0.0);
		if(isTest)
			return;
        try{
            CallResponse response = httpCall('POST', 'delete', sCall.getJsonStr());
            if(!response.Success)
                Utils.log('ERROR',response.Error);
        }
        catch(Exception e){Utils.log('EXCEPTION', e.getMessage()+'\n\r'+e.getStackTraceString());}
	}

	private static CallResponse httpCall(String Method, String relURL, string JSONstr){
		Config__c conf = Config__c.getInstance('Docomotion Default Settings');
		string baseURL = conf.server_URL__c;
		string endPoint = baseURL + '/' + retURL + relURL;
		Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		HttpRequest req = new HttpRequest();
		req.setEndpoint(endPoint);
		req.setMethod(Method);
		req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json'); 
		req.setBody(JSONstr);
		Utils.log('REQUEST', req.getBody());
		Http http = new Http();
		HttpResponse response = http.send(req);
		Utils.log('RESPONSE', response.getBody());
		return (CallResponse) JSON.deserialize(response.getBody(), CallResponse.class);
	}

	class SendCall {
		private Set<Id> SignatureId;
		private Decimal ExpirationHours;
		private Id OrgId;

		public SendCall(Set<Id> signatures, Decimal timeP){
			SignatureId = signatures;
			ExpirationHours = timeP;
			OrgId = UserInfo.getOrganizationId();
		}

		public string getJsonStr(){
			return JSON.serialize(this);
		}

	}

	class CallResponse {
		public Boolean Success;
		public String Error;
	}

}