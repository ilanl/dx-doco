/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_WS_Interactive {

    static testMethod void myUnitTest() {
		TEST_Helper.insertSettings();
    	Contact c = new Contact(FirstName='Ilan', LastName='Levi', email='test@test.com');
    	insert c;
    	Case papaCa = new Case();
    	insert papaCa;
    	Case ca = new Case(ContactId = c.id, parentId=papaCa.Id);
    	insert ca;
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.addFeedItem(form);
        TEST_Helper.publish(form);
		Signature__c signature = new Signature__c(Signer__c=c.Id, form__c=form.Id, record_Id__c=ca.Id);
		SignaturesRestCall.setIsTest(true);
		insert signature;
        Interactive_Fields_Collection__c ic = Test_Helper.createInteractiveCollection();
        Interactive_Field__c f = new Interactive_Field__c();
        f.Interactive_Fields_Collection__c = ic.id;
        f.Question__c = 'test';
        f.Type__c = 'Text';
        Interactive_Field__c f2 = new Interactive_Field__c();
        f2.Interactive_Fields_Collection__c = ic.id;
        f2.Question__c = 'test';
        f2.Type__c = 'Multi Picklist';
        f2.choices2__c = 'Hamburger\r\nSalmon\r\nPizza';
        Interactive_Field__c f3 = new Interactive_Field__c();
        f3.Interactive_Fields_Collection__c = ic.id;
        f3.Question__c = 'test';
        f3.Type__c = 'checkbox';
        Interactive_Field__c f4 = new Interactive_Field__c();
        f4.Interactive_Fields_Collection__c = ic.id;
        f4.Question__c = 'test';
        f4.Type__c = 'Picklist';
        f4.choices2__c = 'Hamburger\r\nSalmon\r\nPizza';
        Interactive_Field__c f5 = new Interactive_Field__c();
        f5.Interactive_Fields_Collection__c = ic.id;
        f5.Question__c = 'test';
        f5.Type__c = 'Radio';
        f5.choices2__c = 'Hamburger\r\nSalmon\r\nPizza';
        insert new List<Interactive_Field__c> {f,f2,f3,f4,f5};
        Interactive_Collections_Association__c a = new Interactive_Collections_Association__c();
        a.form__c = form.id;
        a.collection__c = ic.id;
        insert a;
        CTRL05_Form_Extensions.regenerateXML(form.id);
        string req = '<root><fields><'+f.id+'>test</'+f.id+'><'+f2.id+'.value>Pizza</'+f2.id+'.value><'+f2.id+'.value>Salmon</'+f2.id+'.value><Origin>Website</Origin><Parent.Origin>Phone</Parent.Origin></fields><sfdc><formid>'+form.form_Id__c+'</formid><whoid>'+c.id+'</whoid><whatid>'+ca.id+'</whatid></sfdc><cello><signatureId>'+signature.Id+'</signatureId><ip>3543454</ip><browser>dfdgfgf</browser><time>dgfgfg</time><file_url>http://google.com</file_url><attachments></attachments></cello></root>';
        Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(true));
		WS_Interactive.doPost(req);
		Test.stopTest();
    }
}