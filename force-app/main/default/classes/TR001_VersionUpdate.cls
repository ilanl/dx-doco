public with sharing class TR001_VersionUpdate {
	
	//To update the version of subform in an association
	//Call by trigger FormAfterUpdate
	public static void UpdateAssociationVersionsAndPublishForms(List<Form__c> publishedForms){
		Map<Id, Form__c> publishedFormsMap = new Map<Id,Form__c>(publishedForms);
		List<Form_Subform_Association__c> subformAssociations = new List<Form_Subform_Association__c>();
		List<Form_Subform_Association__c> formAssociations = new List<Form_Subform_Association__c>();
		
		for(Form_Subform_Association__c association:[Select Id,version__c,subform__c, form__c, form__r.status__c, subform__r.Current_Published_Version__c, Subform_Version_Soql__c, Subform_Version__c From Form_Subform_Association__c Where subform__c in :publishedFormsMap.keySet() or form__c in :publishedFormsMap.keySet()]){
			if(publishedFormsMap.containsKey(association.subform__c))
				subformAssociations.add(association);
			if(publishedFormsMap.containsKey(association.form__c))
				formAssociations.add(association);	
		}
		if(!subformAssociations.isEmpty()){
			updateAssociationVersions(publishedFormsMap, subformAssociations);
			publishForms(subformAssociations);
		}
		if(!formAssociations.isEmpty()){
			formPublished(formAssociations);
		}
	}
	
	//to init the version and metadata version
	public static void initVersions(List<Form__c> forms){
		for(Form__c form:forms){
			form.current_Version__c = '1';
			form.total_version__c = 1;
			form.current_Metadata_Version__c = '1';
		}
	}

	
	//To update the association with the last published version of a subform
	//Update 11/1/2015: We only update the lookup on the version.
	private static void updateAssociationVersions(Map<Id, Form__c> subformsMap, List<Form_Subform_Association__c> associations){
		
		
		for(Form_Subform_Association__c a:associations){
			if(subformsMap.containsKey(a.subform__c)){
				a.subform_version__c = a.subform__r.Current_Published_Version__c;
				if(a.form__r.status__c=='Published')
					a.subform_version_soql__c = a.subform__r.Current_published_Version__c;
			}
		}
		
		if(!associations.isEmpty())
			DML_CustomExtension.updateObj(associations);
	}
	
	//To publish the form after one subform has been published
	private static void publishForms(List<Form_Subform_Association__c> associations){
		Map<Id,Form__c> formsMap = new Map<Id,Form__c>();
		for(Form_Subform_Association__c association:associations){
			if(association.form__r.status__c=='Published' && !formsMap.containsKey(association.form__c))
				formsMap.put(association.form__c,new Form__c(id=association.form__c,status__c='Activating', immediate_publish__c=true));
		}
		if(!formsMap.isEmpty()){
			DML_CustomExtension.updateObj(formsMap.values());	
		}
	}
	
	//To update the subform_Version_Soql when the form is updated
	private static void formPublished(List<Form_Subform_Association__c> associations){
		for(Form_Subform_Association__c association:associations){
			association.Subform_Version_Soql__c = association.Subform_Version__c;
		}
		DML_CustomExtension.updateObj(associations);
	}

}