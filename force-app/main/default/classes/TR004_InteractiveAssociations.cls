public with sharing class TR004_InteractiveAssociations {
	
	private static boolean createXML = true;
	
	public static void createAssociations(List<Form_Subform_Association__c> fAssociations){
		List<Interactive_Collections_Association__c> toInsert = new List<Interactive_Collections_Association__c>();
		Map<Id, Set<Id>> existingRelationship = new Map<Id,Set<Id>>();
		
		for(Form_Subform_Association__c a: fAssociations){
			if(!existingRelationship.containsKey(a.form__c))
				existingRelationship.put(a.form__c, new Set<id>());
			if(!existingRelationship.containsKey(a.subform__c))	
				existingRelationship.put(a.subform__c, new Set<id>());
		}
		
		for(Interactive_Collections_Association__c a: [Select form__c, collection__c From Interactive_Collections_Association__c Where form__c in :existingRelationship.keySet()]){
			existingRelationship.get(a.form__c).add(a.collection__c);
		}
		
		for(Form_Subform_Association__c a: fAssociations){
			for(Id collectionId :existingRelationship.get(a.subform__c)){
				if(!existingRelationship.get(a.form__c).contains(collectionId))
					toInsert.add(new Interactive_Collections_Association__c(form__c=a.form__c,collection__c=collectionId));
			}
		}
		
		if(!toInsert.isEmpty())
			DML_CustomExtension.insertObj(toInsert);
	}
	
	public static void createAssociations(List<Interactive_Dataschema_Association__c> iLSt){
		Map<Id,List<Form__c>> dMap = new Map<Id,List<Form__c>>();
		List<Interactive_Collections_Association__c> toInsert = new List<Interactive_Collections_Association__c>();
		for(Interactive_Dataschema_Association__c i:iLst){
			dMap.put(i.dataschema__c, new List<Form__c>());
		}
		List<Form__c> fLst = [Select Id, dataschema__c, (Select Id, From_Dataschema__c, collection__c From Interactive_Collections_Associations__r) From Form__c Where Dataschema__c in :dMap.keySet()];
		for(Form__c f:fLst){
			dMap.get(f.dataschema__c).add(f);
		}
		for(Interactive_Dataschema_Association__c i:iLst){
			for(Form__c f: dMap.get(i.dataschema__c)){
				boolean toAdd = true;
				for(Interactive_Collections_Association__c a: f.Interactive_Collections_Associations__r){
					if(a.collection__c == i.Interactive_Form__c){
						toAdd = false;
						break;
					}
				}
				if(toAdd){
					toInsert.add(new Interactive_Collections_Association__c(form__c=f.id,collection__c=i.Interactive_Form__c, from_dataschema__c=true));
				}
			}
		}
		if(!toInsert.isEmpty()){
			try{
				DML_CustomExtension.insertObj(toInsert);
			}catch(Exception e){}
		}
	}
	
	public static void deleteAssociations(List<Interactive_Dataschema_Association__c> iLSt){
		Map<Id,List<Form__c>> dMap = new Map<Id,List<Form__c>>();
		List<Interactive_Collections_Association__c> toDelete = new List<Interactive_Collections_Association__c>();
		for(Interactive_Dataschema_Association__c i:iLst){
			dMap.put(i.dataschema__c, new List<Form__c>());
		}
		List<Form__c> fLst = [Select Id, dataschema__c, (Select Id, From_Dataschema__c, collection__c From Interactive_Collections_Associations__r where from_dataschema__c=true) From Form__c Where Dataschema__c in :dMap.keySet()];
		for(Form__c f:fLst){
			dMap.get(f.dataschema__c).add(f);
		}
		for(Interactive_Dataschema_Association__c i:iLst){
			for(Form__c f: dMap.get(i.dataschema__c)){
				for(Interactive_Collections_Association__c a: f.Interactive_Collections_Associations__r){
					if(a.collection__c == i.Interactive_Form__c){
						toDelete.add(a);
						break;
					}
				}
			}
		}
		if(!toDelete.isEmpty()){
			try{
				DML_CustomExtension.deleteObj(toDelete);
			}catch(Exception e){}
		}
	}
}