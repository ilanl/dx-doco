/***
Executed to re-generate the dataschema XML when an interactive field has been inserted or deleteted from a interactive collection.
Author: ilan LEVI-MAZLOUM @ balink
Date: 29/4/2015
**/
public with sharing class TR010_InteractiveFieldsUpdate {

	public static void regenerateDataschemaXML(List<Interactive_Field__c> ifLst, boolean deleted){
		Set<Id> collectionIds = new Set<Id>();
		for(Interactive_Field__c iField: ifLst){
			if(iField.Interactive_Fields_Collection__c != null)
				collectionIds.add(iField.Interactive_Fields_Collection__c);
		}
		if(!collectionIds.isEmpty()){
			List<Interactive_Collections_Association__c> cLst = [Select Form__c From Interactive_Collections_Association__c Where collection__c in :collectionIds];
			Set<Id> formIds = new Set<id>();
			for(Interactive_Collections_Association__c a:cLst){
				if(a.form__c != null)
					formIds.add(a.form__c);
			}
			if(!formIds.isEmpty()){
				if(deleted)
					TR013_RegenerateXML.needToOpenInDesigner(formIds);
				else	
					TR013_RegenerateXML.regenerateFormXML(formIds);
			}
		}
	}

}