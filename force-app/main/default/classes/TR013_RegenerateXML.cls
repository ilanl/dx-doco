public with sharing class TR013_RegenerateXML {

	private static Set<Id> alreadyDone = new Set<Id>();
	
	public static void regenerateXML(Set<id> dsIds){
		Set<Id> dIDs = new Set<Id>();
		for(Id id :dsIds){
			if(!alreadyDone.contains(id)){
				dIds.add(id);
				alreadyDone.add(id);
			}
		}
		if(!dIds.isEmpty()){
			List<Form__c> fLst = [Select Id From Form__c Where Dataschema__c in :dIds and Regenerate_XML__c = false];
			for(Form__c f:fLst){
				f.Regenerate_XML__c = true;
			}
			try{
				DML_CustomExtension.UpdateObj(fLst);
		}catch(Exception e){}
		}
	}
	
	public static void regenerateFormXML(Set<id> fIds){
		List<Form__c> formsToUpdate = new List<Form__c>();
		for(Form__c f:[Select id From Form__c Where id in :fIds]){
			if(!alreadyDone.contains(f.id)){
				f.Regenerate_XML__c = true;
				formsToUpdate.add(f);
				alreadyDone.add(f.id);
			}
		}
		if(!formsToUpdate.isEmpty()){
			try{
				DML_CustomExtension.UpdateObj(formsToUpdate);
			}catch(Exception e){}
		}
		
	}

	public static void regenerateXML(List<Dataschema__c> dLst){
		Set<Id> dIDs = new Set<Id>();
		for(Dataschema__c d:dLst){
			dIds.add(d.id);
		}
		regenerateXML(dids);
	}
	
	public static void needToOpenInDesigner(Set<id> fIDs){
		List<Form__c> fLst = new List<Form__c>();
		for(Id Id:fIds){
			if(!alreadyDone.contains(Id)){
				Form__c f = new Form__c(id=Id);
				f.New_Dataschema__c=true;
				f.Regenerate_XML__c=true;
				fLst.add(f);
				alreadyDone.add(Id);
			}
		}
		if(!fLst.isEmpty()){
			DML_CustomExtension.updateObj(fLst);
		}
	}

}