@isTest
private class Test_CTRL00_Config {
	static testMethod void myUnitTest() {
        CTRL00_Config c = new CTRL00_Config();
        PageReference pageRef = Page.VF00_Config;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('authorize', '1');
        c.init();
        
        ApexPages.currentPage().getParameters().put('authorize', '0');
        c.init();
        
        c.editUrl();
        system.assertEquals(c.editUrlMode,true);
        
        c.cancelUrl();
        system.assertEquals(c.editUrlMode,false);
        
        c.auth();
        
        c.save();
		c.hours=12;
		c.minutes=4;
		c.saveExpiration();
        system.assertEquals(c.myConf.debug_mode__c,Config__c.getInstance('Docomotion Default Settings').debug_mode__c);
        
        Config__c co = c.config;
        Test.setMock(HttpCalloutMock.class, new Test_UsageMock(false));
        c.showUsage();
	}
	
	static testMethod void myUnitTestShowUsageOk() {
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		CTRL00_Config c = new CTRL00_Config();
        Test.setMock(HttpCalloutMock.class, new Test_UsageMock(false));
        Test.startTest();
        c.showUsage();
        Test.stopTest();
	}
	
	static testMethod void myUnitTestShowUsageError() {
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		CTRL00_Config c = new CTRL00_Config();
        Test.setMock(HttpCalloutMock.class, new Test_UsageMock(true));
        Test.startTest();
        c.showUsage();
        Test.stopTest();
	}

	static testMethod void myUnitTestGoogleInit() {
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		CTRL00_Config c = new CTRL00_Config();
        Test.startTest();
		c.myConf.google_drive__c=true;
		c.myConf.google_drive_mode__c = 'Organization';
        c.saveGoogle();
		c.checkStatus();
		CTRL00_Config c2 = new CTRL00_Config();
		c2.myConf.google_drive_mode__c = 'User';
		c2.saveGoogle();
		c2.googleRevoke();
        Test.stopTest();
	}

	static testMethod void myUnitTestGoogleCode(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		Test.setCurrentPageReference(Page.VF00_Config);
		System.currentPageReference().getParameters().put('code', '1234');
		CTRL00_Config c = new CTRL00_Config();
		c.saveCredentials();
	}

	static testMethod void myUnitTestGoogleError(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		Test.setCurrentPageReference(Page.VF00_Config);
		System.currentPageReference().getParameters().put('error', '1234');
		CTRL00_Config c = new CTRL00_Config();
		c.saveCredentials();
	}
}