/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CTRL01_EditWordTemplate {

    static testMethod void myUnitTest() {
        Ctrl01_EditWordTemplate ctrl = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
        ctrl.fileName = 'WordTemplate.dot';
        ctrl.body = Blob.valueOf('test');
		ctrl.cType = 'application/vnd.ms-word.template.macroEnabled.12';
        ctrl.save();
    }
    
	static testMethod void myUnitTestWrongFormat() {
        Ctrl01_EditWordTemplate ctrl = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
        ctrl.fileName = 'WordTemplate.dot';
        ctrl.body = Blob.valueOf('test');
		ctrl.cType = 'application/pdf';
        ctrl.save();
    }

    static testMethod void myUnitTestError() {
        Ctrl01_EditWordTemplate ctrl = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
        ctrl.fileName = null;
        ctrl.body = Blob.valueOf('test');
        ctrl.save();
    }
    
    static testMethod void cancel(){
    	Contact c = new Contact(LastName='Levi', firstName='Ilan');
    	insert c;
    	PageReference pageRef = Page.VF01_EditWordTemplate;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('retURL',c.id);
    	Ctrl01_EditWordTemplate ctrl = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
    	ctrl.cancel();
    }
    
    static testMethod void cancel2(){
    	Ctrl01_EditWordTemplate ctrl = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
    	ctrl.cancel();
    }
}