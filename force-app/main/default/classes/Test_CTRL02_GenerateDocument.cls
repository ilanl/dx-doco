/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CTRL02_GenerateDocument {

    static testMethod void myUnitTestPreview() {
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = TEST_Helper.newFormWithDs();
        form.Default_File_Name__c ='name';
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
		try 
		{	        
			boolean hasChatter = ctrl.chatterEnable;
		}
		catch (Exception  e){}
		
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.getPreviewUrl();
        Test.stopTest();
    }

    static testMethod void myUnitTestQueryError() {
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Form__c f2 = [Select Current_Published_version__c From Form__c Where Id=:form.id];
        Version__c version = [Select fields__c from Version__c Where Id=:f2.Current_Published_version__c];
        version.fields__c = 'testing_non_existings_fields__c';
		update version;
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
		try 
		{	        
			boolean hasChatter = ctrl.chatterEnable;
		}
		catch (Exception  e){}
		
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.getPreviewUrl();
        Test.stopTest();
    }
    
    static testMethod void myUnitTestPreviewError() {
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = new Form__c(Name__c='test', Main_object__c='Account', fields__c='Name');
        insert form;
        
        Version__c v = new Version__c(version__c='1', fields__c=form.fields__c, form__c=form.id);
        insert v;
        
        form.Current_Published_Version__c = v.id;
        update form;

        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(true));
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.getPreviewUrl();
        Test.stopTest();
    }
    
    
    static testMethod void myUnitTestGenerate() {
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        //ctrl.lookupId = c.id;
        Test.startTest();
		ctrl.format = 'PDF';
        ctrl.download=true;
        ctrl.Chatter=true;
        ctrl.Attachment=true;
        ctrl.Document=true;
        ctrl.mail=true;
		ctrl.gDrive = false;
		ctrl.link = false;
		ctrl.link = false;
        ctrl.generateDocument();
		Id d = ctrl.defaultForm;
		Boolean b = ctrl.hideInGeneration;
		ctrl.parentFolderId='123';
		ctrl.overridingFileId='143';
        Test.stopTest();
    }
    
    static testMethod void myUnitTestGenerateError() {
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
       
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};
		
		//Create a form and published it
        Form__c form = new Form__c(Name__c='test', Main_object__c='Account', fields__c='Name');
        insert form;
        
        
        Version__c v = new Version__c(version__c='1', fields__c=form.fields__c, form__c=form.id);
        insert v;
        
        form.Current_Published_Version__c = v.id;
        update form;
        system.debug('@@@@@@@@@@@@@ form field testing = '+form);
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(true));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
		boolean b = ctrl.hasForms;
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.download=true;
        ctrl.Chatter=true;
        ctrl.Attachment=true;
        ctrl.Document=true;
        ctrl.mail=true;
		ctrl.gDrive = false;
		ctrl.link = false;
		ctrl.format = 'PDF';
        ctrl.generateDocument();
        Test.stopTest();
    }
    
    static testMethod void testlookups() {
    	Contact c = new Contact(LastName='Test', FirstName='Rsss');
    	insert c;
    	Lead l = new Lead(LastName='sfd', company='Test');
    	insert l;
    	Interactive_Fields_Collection__c co = Test_Helper.createInteractiveCollection();
    	Interactive_Submission__c i = new Interactive_Submission__c(Contact__c = c.id, Lead__c = l.id, interactive_fields_collection__c=co.id);
    	insert i;
    	ApexPages.currentPage().getParameters().put('id', i.id);
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.LookupFieldSelected = 'Id';
        ctrl.updateSelectedLookup();
    }

    private static testMethod void test_Leftovers() {
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        String ns = ctrl.NameSpace;
        Boolean isDesignerUser = ctrl.isDesignerUser;
    }

	static testmethod void testGoogleDriveCredentials(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='User');
		insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = new Form__c(Name__c='test', Main_object__c='Account', fields__c='Name');
        insert form;
        
        Version__c v = new Version__c(version__c='1', fields__c=form.fields__c, form__c=form.id);
        insert v;
        
        form.Current_Published_Version__c = v.id;
        update form;
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
		ApexPages.currentPage().getParameters().put('authorize', '1');
		ApexPages.currentPage().getParameters().put('code', '1345');
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(true));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
		Boolean b = ctrl.gDriveUserEnable;
		b = ctrl.needUserDriveAuthorize;
		string filepath = ctrl.filePath;
		ctrl.googleCredentials();
		ctrl.init();
		ctrl.checkStatus();
	}

	static testmethod void testGoogleDriveErrorAndRevoke(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='User');
		insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = new Form__c(Name__c='test', Main_object__c='Account', fields__c='Name');
        insert form;
        
        Version__c v = new Version__c(version__c='1', fields__c=form.fields__c, form__c=form.id);
        insert v;
        
        form.Current_Published_Version__c = v.id;
        update form;
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
		ApexPages.currentPage().getParameters().put('authorize', '0');
		ApexPages.currentPage().getParameters().put('error', '1345');
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(true));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
		
		ctrl.init();
		ctrl.googleRevoke();
	}

	static testMethod void myUnitTestGenerateGoogle() {
        //To add the settings
    	Config__c c = TEST_Helper.insertSettings();
        c.Google_Drive__c=true;
		c.Google_Drive_Mode__c='User';
		update c;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.download=false;
        ctrl.Chatter=true;
        ctrl.Attachment=false;
        ctrl.Document=false;
        ctrl.mail=false;
		ctrl.gDrive = true;
		ctrl.link = false;
		ctrl.format = 'PDF';
        ctrl.generateDocument();
		Id d = ctrl.defaultForm;
		Boolean b = ctrl.hideInGeneration;
		ctrl.parentFolderId='123';
		ctrl.overridingFileId='143';
        Test.stopTest();
    }

	static testMethod void myUnitTestGenerateLink() {
        //To add the settings
    	Config__c c = TEST_Helper.insertSettings();
        c.Google_Drive__c=true;
		c.Google_Drive_Mode__c='User';
		update c;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(true));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        //ctrl.lookupId = c.id;
        Test.startTest();
        ctrl.download=false;
        ctrl.Chatter=true;
        ctrl.Attachment=false;
        ctrl.Document=false;
        ctrl.mail=false;
		ctrl.gDrive = false;
		ctrl.link = true;
        ctrl.generateDocument();
		Id d = ctrl.defaultForm;
		Boolean b = ctrl.hideInGeneration;
        Test.stopTest();
    }
}