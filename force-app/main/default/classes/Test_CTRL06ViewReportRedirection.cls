/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class Test_CTRL06ViewReportRedirection {
    static testMethod void myUnitTest() {
        // For this example, we assume that there is
        // at least one Report in our org WITH a namespace
 
        // Get a report to work with
        List<Report> reports = [
            select Id, DeveloperName, NamespacePrefix
            from Report
            where NamespacePrefix != null
            limit 1
        ];
 
        // Assuming that we have reports...
        if (!reports.isEmpty()) {
            // Get the first one in our list
            Report r = reports[0];
 
            //
            // CASE 1: Passing in both namespace, developername,
            // and a parameter value
            //
 
            // Load up our Visualforce Page
            PageReference p = System.Page.VF06_ViewReportRedirection;
            p.getParameters().put('ns',r.NamespacePrefix);
            p.getParameters().put('dn',r.DeveloperName);
            p.getParameters().put('pv0','llamas');
            p.getParameters().put('pv2','alpacas');
            Test.setCurrentPage(p);
 
            // Load up our Controller
            CTRL06_ViewReportRedirection ctl = new CTRL06_ViewReportRedirection();
 
            // Manually call the redirect() action,
            // and store the page that we are returned
            PageReference ret = ctl.redirect();
 
            // We should be sent to the View page for our Report
            System.assert(ret.getURL().contains('/'+r.Id));
            // Also, make sure that our Filter Criterion values
            // got passed along
            System.assert(ret.getURL().contains('pv0=llamas'));
            System.assert(ret.getURL().contains('pv2=alpacas'));
 
            //
            // CASE 2: Passing in both just developername
            //
 
            // Load up our Visualforce Page
            p = System.Page.VF06_ViewReportRedirection;
            p.getParameters().put('dn',r.DeveloperName);
            Test.setCurrentPage(p);
 
            // Load up our Controller
            ctl = new CTRL06_ViewReportRedirection();
 
            // Manually call the redirect() action,
            // and store the page that we are returned
            ret = ctl.redirect();
 
            // We should be sent to the View page for our Report
            System.assert(ret.getURL().contains('/'+r.Id));
 
            //
            // CASE 3: Passing in a nonexistent Report name
            //
 
            // Load up our Visualforce Page
            p = System.Page.VF06_ViewReportRedirection;
            p.getParameters().put('dn','BlahBLahBlahBlahBlahBlah');
            Test.setCurrentPage(p);
 
            // Load up our Controller
            ctl = new CTRL06_ViewReportRedirection();
 
            // Manually call the redirect() action,
            // and store the page that we are returned
            ret = ctl.redirect();
 
            // We should be sent to the Reports tab
            System.assert(ret.getURL().contains(
                '/'+Report.SObjectType.getDescribe().getKeyPrefix()+'/o'
            ));
 
        }
    }
}