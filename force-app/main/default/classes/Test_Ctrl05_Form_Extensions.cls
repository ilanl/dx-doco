/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Ctrl05_Form_Extensions {

    static testMethod void myUnitTestPublishForm() {
    	//To add the settings
    	TEST_Helper.insertSettings();
        //We hade to create a form (a complex one) and to publish it
        Test.setMock(HttpCalloutMock.class, new Test_PublishMockup());
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.addFeedItem(form);
        Test_Helper.saveFromDesigner(form.id);
        Form__c subForm = Test_Helper.newSubFormWithDs();
        Test_Helper.saveFromDesigner(subform.id);
        subform.status__c = 'edit';
        CTRL05_Form_Extensions ctrl = new Ctrl05_Form_Extensions(new ApexPages.Standardcontroller(subform));
        ctrl.activate();
        ctrl.publish();
        ctrl.reEdit();
        ctrl.back();
        form.new_dataschema__c = true;
        CTRL05_Form_Extensions ctrl2 = new Ctrl05_Form_Extensions(new ApexPages.Standardcontroller(form));
        ctrl.publish();
        ctrl.unpublish();
        ctrl.addError('test error');
        ctrl.failStatus();
    }
    
    
    static testMethod void myUnitTestPublishAssociated() {
    	//To add the settings
    	TEST_Helper.insertSettings();
        //We hade to create a form (a complex one) and to publish it
        Test.setMock(HttpCalloutMock.class, new Test_PublishMockup());
        Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.addFeedItem(form);
        Test_Helper.saveFromDesigner(form.id);
        Form__c subForm = Test_Helper.newSubFormWithDs();
        Test_Helper.saveFromDesigner(subform.id);
        subform.status__c = 'Active';
        update subform;
        subform.status__c = 'published';
        update subform;
        Form_Subform_Association__c a = new Form_Subform_Association__c(form__c=form.id,subform__c=subform.id);
        insert a;
        CTRL05_Form_Extensions.regenerateXML(form.id);
        TEST_HElper.publish(form);
        TEST_HElper.republish(subform);
        CTRL04_DataSchema.FieldSaveAction fsa1 = new CTRL04_DataSchema.FieldSaveAction();
        fsa1.action='select';
        fsa1.path='createdDate';
        CTRL04_DataSchema.saveFieldsDelta(subform.id, new List<CTRL04_DataSchema.FieldSaveAction> {fsa1});
        CTRL04_DataSchema.generateXML(subform.id);
        TEST_Helper.republish(subform);
    }
    
    static testMethod void createNewSubForm() {
    	Word_Template__c wc = TEST_Helper.createWordTemplate();
    	string frt = Utils.getNameSpace()+'Form__c';
        RecordType rt = [Select Id From RecordType Where sobjectType=:frt and developerName='Subform'];
    	Dataschema__c ds = new Dataschema__c(main_object__c='Account', Name='Test', fields__c='Name');
    	insert ds;
    	PageReference pageRef = Page.VF07_CreateForm;
    	Test.setCurrentPage(pageRef);
    	Form__c myForm = new Form__c(Main_Object__c=ds.Main_Object__c, Name__c='Test', Word_Template__c = wc.id, RecordTypeId=rt.id, Description__c='Test');
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPages.StandardController(myForm));
    	ctrl.existingDs = true;
    	ctrl.selectedDsId = ds.id;
    	ctrl.saveFormAndDefineDataSchema();
    	ctrl.updateStatus();
    }
    
    static testMethod void createNewForm() {
    	Word_Template__c wc = TEST_Helper.createWordTemplate();
    	string frt = Utils.getNameSpace()+'Form__c';
        RecordType rt = [Select Id From RecordType Where sobjectType=:frt and developerName='Form'];
    	Dataschema__c ds = new Dataschema__c(main_object__c='Account', Name='Test', fields__c='Name');
    	insert ds;
    	PageReference pageRef = Page.VF07_CreateForm;
    	Test.setCurrentPage(pageRef);
    	Form__c myForm = new Form__c(Main_Object__c=ds.Main_Object__c, Name__c='Test', Word_Template__c = wc.id, RecordTypeId=rt.id, Description__c='Test');
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPages.StandardController(myForm));
    	CTRL05_Form_Extensions.getAllObjectsList();
    	ctrl.getCompatibleDataschmas2();
    	ctrl.existingDs = true;
    	ctrl.selectedDsId = ds.id;
    	ctrl.saveFormAndDefineDataSchema();
    }
    
    static testMethod void cloneForm(){
	    Form__c myForm = TEST_Helper.newFormWithDs();
	    myForm.Status__c = 'Edit';
        PageReference pageRef = Page.VF07_CreateForm;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('clone_id',myForm.id);
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(new Form__c(Name__c='test_coucou')));
    	ctrl.cloneForm();
    	ctrl.saveForm();
    	CTRL05_Form_Extensions.regenerateXML(myForm.id);
    }
    
    static testMethod void cloneForm2(){
	    Form__c myForm = TEST_Helper.newFormWithDs();
	    Test_Helper.saveFromDesigner(myform.Id);
	    myForm.Status__c = 'Edit';
	    TEST_Helper.saveFromDesigner(myForm.id);
        PageReference pageRef = Page.VF07_CreateForm;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('clone_id',myForm.id);
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(new Form__c(Name__c='test_coucou')));
    	ctrl.cloneForm();
    	ctrl.saveForm();
    	
    }
    
    static testMethod void deleteForm(){
    	Form__c myForm = TEST_Helper.newFormWithDs();
	    Test_Helper.saveFromDesigner(myform.Id);
        delete myForm;
    }
    
    static testMethod void messagesToShow(){
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(new Form__c(Name__c='test_coucou', main_object__c='Account', new_Dataschema__c=true)));
    	ctrl.MessagesToSHow();
    }
    
    static testMethod void newDS(){
    	Dataschema__c d = new Dataschema__c(name='ilanDs', Main_Object__c='Account', fields__c='Name');
    	insert d;
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(new Form__c(Name__c='test_coucou', main_object__c='Account', new_Dataschema__c=true)));
    	ctrl.existingDs = false;
    	ctrl.dataschemaName='ilanDs';
    	ctrl.saveForm();
    	ctrl.dataschemaName='assd ds';
    	ctrl.saveForm();
    }
    
    static testMethod void publishFormWithoutDataschema(){
    	Form__c f = new Form__c(name__c='tetr', main_object__c='Account', New_Dataschema__c=true);	
    	insert f;
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(f));
    	ctrl.activate();
    }
    
    static testMethod void saveFormWithouDS(){
    	Form__c f = new Form__c(name__c='tetr', main_object__c='Account');	
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(f));
    	ctrl.existingDS=true;
    	ctrl.saveForm();
    }
    
    /*static testMethod void deleteFeed(){
    	Form__c f = TEST_Helper.newFormWithDs();
    	Test_Helper.saveFromDesigner(f.Id);
    	Map<Id,Set<Id>> formCDId = new Map<Id,Set<Id>>();
    	Id metaId = [Select MetaData_Document_Id__c From Form__c Where id=:f.id].MetaData_Document_Id__c;
    	formCDId.put(f.id,new Set<Id>{metaId});
    	TR008_DeleteFeedItem.deleteFeedItem(formCDId);
    }*/
    
    static testMethod void containsInteractive(){
    	Form__c form = TEST_Helper.newFormWithDs();
    	Interactive_Fields_Collection__c c = TEST_Helper.createInteractiveCollection();
    	FeedItem fi = new FeedItem();
		fi.parentId = form.id;
		fi.type='ContentPost';
		fi.ContentFileName='Metadata.xml';
		fi.ContentData = Blob.valueOf('<FFHeader><FreeFormProperties><ID>'+form.form_id__c+'</ID><Name>'+form.Name__c+'</Name><Version>'+form.current_version__c+'</Version></FreeFormProperties><FreeFormTags><NewDataTag><ROOT><ROOT_Interactive_Fields><'+c.Id+'><tatata><Tag></Tag></tatata></'+c.Id+'></ROOT_Interactive_Fields></ROOT></NewDataTag></FreeFormTags></FFHeader>');
		insert fi;
		fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
		ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
		form.MetaData_Document_Id__c = cv.ContentDocumentId;
		form.MetaData_Version_Id__c = cv.id;
		update form;
    }
    
     static testMethod void containsInteractive2(){
    	Form__c form = TEST_Helper.newFormWithDs();
    	Interactive_Fields_Collection__c c = TEST_Helper.createInteractiveCollection();
    	FeedItem fi = new FeedItem();
		fi.parentId = form.id;
		fi.type='ContentPost';
		fi.ContentFileName='Metadata.xml';
		fi.ContentData = Blob.valueOf('<FFHeader><FreeFormProperties><ID>'+form.form_id__c+'</ID><Name>'+form.Name__c+'</Name><Version>'+form.current_version__c+'</Version></FreeFormProperties><FreeFormTags><NewDataTag><ROOT><ROOT_Interactive_Fields><'+c.Id+'><t><tatata><Tag></Tag></tatata></t></'+c.Id+'></ROOT_Interactive_Fields></ROOT></NewDataTag></FreeFormTags></FFHeader>');
		insert fi;
		fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
		ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
		form.MetaData_Document_Id__c = cv.ContentDocumentId;
		form.MetaData_Version_Id__c = cv.id;
		update form;
    }
    
    static testMethod void deleteAssociation(){
    	Form__c s = TEST_Helper.newSubFormWithDs();
    	Test_Helper.saveFromDesigner(s.Id);
    	Test_Helper.publish(s);
    	Form__c f = Test_Helper.newFormWithDs();
    	Form_Subform_Association__c a = new Form_Subform_Association__c(form__c = f.id, subform__c=s.id);
    	insert a;
    	delete a;
    }
    
    static testMethod void deleteDataschemaInteractiveAssociation(){
    	Form__c f = Test_Helper.newFormWithDs();
   		Interactive_Fields_Collection__c c = Test_Helper.createInteractiveCollection();
   		Interactive_Dataschema_Association__c a = new Interactive_Dataschema_Association__c(Interactive_Form__c=c.id, Dataschema__c=f.dataschema__c);
   		insert a;
   		delete a;
   		//TR015_CheckInteractiveAssociations.checkObject('Order');
    }
    
    static testMethod void convertOldV5Form(){
    	Form__c form = new Form__c(name__c='Test', main_Object__c='Account',fields__c = 'Name');
    	insert form;
    	FeedItem fi = new FeedItem();
		fi.parentId = form.id;
		fi.type='ContentPost';
		fi.ContentFileName='Dataschema.xml';
		fi.ContentData = Blob.valueOf('<root>blablabla</root>');
		insert fi;
		fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
		ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
		form.document_Content_Id__c = cv.ContentDocumentId;
		form.Data_Schema_XML_ID__c = cv.id;
		update form;
		
		form = TEST_Helper.saveFromDesignerOld(form.Id);
		CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPages.StandardController(form));
		ctrl.messagesToShow();
		ctrl.updateOldForm();
    }

    private static testMethod void test_restoreLastVersion() {
        Form__c form = TEST_Helper.createPublishedForm();
		form.main_object__c = 'Account';
        CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPages.StandardController(form));
        ctrl.restoreLastVersion();
    }
    
    static testMethod void initDefaultFileNameListTest(){
    	Form__c f = new Form__c(name__c='tetr', main_object__c='Account');	
    	CTRL05_Form_Extensions ctrl = new CTRL05_Form_Extensions(new ApexPAges.standardController(f));
    	List<SelectOption> defaultFileNameList = new List<SelectOption>();
    	defaultFileNameList= ctrl.defaultFileNameList;
    }
    
    
}