/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_InteractiveAssociation {

    static testMethod void myUnitTest() {
        Form__c form = TEST_Helper.newFormWithDs();
        
        TEST_Helper.addFeedItem(form);
        TEST_Helper.publish(form);
        Interactive_Fields_Collection__c ic = Test_Helper.createInteractiveCollection();
        Interactive_Collections_Association__c a = new Interactive_Collections_Association__c();
        a.form__c = form.id;
        a.collection__c = ic.id;
        insert a;
        Interactive_Field__c ifi = new Interactive_Field__c();
		ifi.question__c ='test2';
		ifi.type__c='checkbox';
		ifi.Interactive_Fields_Collection__c = ic.id;
		insert ifi;
		delete ifi;
        delete a;
    }
}