@isTest
private class Test_MassGeneration { 

	public static testMethod void generateLinks(){
		TEST_Helper.insertSettings();
		Contact c = new Contact(LastName='test', email='test@test.com');
		Contact c2 = new Contact(LastName='test2', email='test2@test.com');
		insert new List<Contact>{c,c2};
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(true));
		Test.setCurrentPageReference(Page.VF18_MassGenerate);
		System.currentPageReference().getParameters().put('ids', String.join(new List<String>{c.Id, c2.Id},','));
		Ctrl18_MassGenerate ctrl = new Ctrl18_MassGenerate();
		ctrl.FormId = form.Id;
		ctrl.Format = 'HTML';
		ctrl.link = true;
		ctrl.lookupStr = 'Id';
		ctrl.generateMass();
		Test.stopTest();
	}

	public static testMethod void generateDownloadError(){
		TEST_Helper.insertSettings();
		
		Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setCurrentPageReference(Page.VF18_MassGenerate);
		System.currentPageReference().getParameters().put('ids', String.join(new List<String>{a1.Id},','));
		Ctrl18_MassGenerate ctrl = new Ctrl18_MassGenerate();
		ctrl.FormId = form.Id;
		ctrl.Format = 'PDF';
		ctrl.download = true;
		ctrl.lookupStr = 'Id';
		ctrl.generateMass();
		ctrl.rerenderReport();
		Test.stopTest();
	}

	public static testMethod void generateDownload(){
		TEST_Helper.insertSettings();
		
		Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
        
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setCurrentPageReference(Page.VF18_MassGenerate);
		Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
		System.currentPageReference().getParameters().put('ids', String.join(new List<String>{a1.Id},','));
		Ctrl18_MassGenerate ctrl = new Ctrl18_MassGenerate();
		ctrl.FormId = form.Id;
		ctrl.Format = 'PDF';
		ctrl.download = true;
		ctrl.lookupStr = 'Id';
		ctrl.generateMass();
		ctrl.rerenderReport();
		Test.stopTest();
	}

	public static testMethod void generateMaiLBody(){
		TEST_Helper.insertSettings();
		Contact c = new Contact(LastName='sdsds', FirstName='ssss', email='ass@ass.cs');
		insert c;
		Form__c form = TEST_Helper.newFormOnContactWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setCurrentPageReference(Page.VF18_MassGenerate);
		Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
		System.currentPageReference().getParameters().put('ids', String.join(new List<String>{c.Id},','));
		Ctrl18_MassGenerate ctrl = new Ctrl18_MassGenerate();
		ctrl.FormId = form.Id;
		ctrl.Format = 'HTML4S';
		ctrl.mail = true;
		ctrl.attachment = true;
		ctrl.chatter = true;
		ctrl.lookupStr = 'Id';
		ctrl.generateMass();
		ctrl.rerenderReport();
		Test.stopTest();
	}

	public static testMethod void generateMaiLTemplate(){
		TEST_Helper.insertSettings();
		EmailTemplate validEmailTemplate = new EmailTemplate();
		System.runAs ( new User(Id = UserInfo.getUserId()) ) {
			validEmailTemplate.isActive = true;
			validEmailTemplate.Name = 'name';
			validEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere';
			validEmailTemplate.TemplateType = 'text';
			validEmailTemplate.FolderId = UserInfo.getUserId();
			validEmailTemplate.Subject = 'Your Subject Here';
			insert validEmailTemplate;
		}
		Contact c = new Contact(FirstName='cc', LastName='sdsds', email='ass@ass.cs');
		insert c;
        
		Form__c form = TEST_Helper.newFormOnContactWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setCurrentPageReference(Page.VF18_MassGenerate);
		Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
		System.currentPageReference().getParameters().put('ids', String.join(new List<String>{c.Id},','));
		Ctrl18_MassGenerate ctrl = new Ctrl18_MassGenerate();
		List<SelectOption> sl = ctrl.templatesList;
		string n = ctrl.namespace;
		ctrl.FormId = form.Id;
		ctrl.Format = 'PDF';
		ctrl.mail = true;
		ctrl.attachment = true;
		ctrl.chatter = true;
		ctrl.lookupStr = 'Id';
		ctrl.selectedTemplateId = validEmailTemplate.Id;
		ctrl.generateMass();
		ctrl.rerenderReport();
		string eStr = ctrl.errorListJson;
		boolean isdu = ctrl.isDesignerUser;
		//boolean ce = ctrl.chatterEnable;
		boolean hf = ctrl.hasForms;
		ctrl.getFormLstJSON();
		ctrl.getLookupsJSON();
		Test.stopTest();
	}

}