@isTest
public with sharing class Test_PictureGeneration {
	static testMethod void addPsLookupToVersion() {
		Form__c form = new Form__c(Name__c='test', Main_object__c='Account', fields__c='Name');
        insert form;
        
       
        Pictures_Setting__c ps = new Pictures_Setting__c(Extensions__c='pdf',Max_Pictures_Nb__c = 130,Maximum_Size__c=5000000,Path__c='Attachment.id' ,Form__c = form.id);
        insert ps;
        Version__c v = new Version__c(version__c='1', fields__c=form.fields__c, form__c=form.id,Total_Version__c=1);
        insert v;
        v.Total_Version__c =2;
        update v;
        
	}
	static testMethod void myUnitTestAsyncGenerate() {
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='User');
		insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
        //To add the settings
    	TEST_Helper.insertSettings();
        
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a1 = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a1;
       	Attachment attach=new Attachment(); 
        attach.Name='Unit Test Attachment'; 
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); 
        attach.body=bodyBlob; 
        attach.parentId=a1.Id; 
        attach.ContentType = 'application/msword'; 
        attach.IsPrivate = false; 
        attach.Description = 'Test'; 
        insert attach; 
        Case c1 = new Case(AccountId=a1.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};

		//Create a form and published it
		Boolean isPicture = true; 
        Form__c form = TEST_Helper.newFormWithDs(isPicture);
        TEST_Helper.saveFromDesigner(form.Id);
        // simulate ps  form related
        Pictures_Setting__c ps = new Pictures_Setting__c(Extensions__c='pdf',Max_Pictures_Nb__c = 130,Maximum_Size__c=5000000,Path__c='Attachments.Id' ,Form__c = form.id);
       	insert ps;
        Test_Helper.publish(form); 
        
        
        PageReference pageRef = Page.VF02_GenerateDocument;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', a1.id);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        CTRL02_GenerateDocument ctrl = new CTRL02_GenerateDocument();
        system.debug(logginglevel.info,'@@@@@@@@@@@ sobjid '+ctrl.form.sobjectId); 
        ctrl.getFormLstJSON();
        String str = ctrl.getLookupDataJSON();
        ctrl.view='last_modified';
        ctrl.setFormLst();
        ctrl.view='All';
        ctrl.setFormLst();
        ctrl.formId = form.id;
        //ctrl.lookupId = c.id;
        Test.startTest();
		ctrl.format = 'PDF';
        ctrl.download=true;
        ctrl.Chatter=true;
        ctrl.Attachment=true;
        ctrl.Document=true;
        ctrl.mail=false;
		ctrl.gDrive = false;
		ctrl.link = false;
		ctrl.link = false;
        ctrl.generateDocument();
		Id d = ctrl.defaultForm;
		Boolean b = ctrl.hideInGeneration;
		ctrl.parentFolderId='123';
		ctrl.overridingFileId='143';
		Mass_Generation_Execution__c mge =[select id from Mass_Generation_Execution__c limit 1];
		mge.Batch_URL__c='https://googel.com';
		update mge;
        Test.stopTest();
    }
    
}