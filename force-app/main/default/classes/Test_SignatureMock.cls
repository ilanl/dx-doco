@isTest
global class Test_SignatureMock implements HttpCalloutMock{

	private CallResponse response;

	public Test_SignatureMock(boolean s){
		response = new CallResponse();
		setSuccess(s);
	}

	public void setSuccess(boolean s){
		if(s)
			response.Success=true;
		else{
			response.Success=false;
			response.Error='error';
		}
	}

	global HTTPResponse respond(HTTPRequest req){
		HttpResponse res = new HttpResponse();
		res.setBody(JSON.serialize(response));
		if(response.Success)
			res.setStatusCode(200);
		return res;
	}

	class CallResponse {
		public Boolean Success;
		public String Error;
	}

}