@isTest
global class Test_UsageMock implements HttpCalloutMock {

	private boolean errorMode;

	global Test_UsageMock(boolean error){
		errorMode = error;
	}

    global HTTPResponse respond(HTTPRequest req){
    	HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        if(errorMode)
        	res.setBody('{"IsExistingOrg":true,"MonthlyRenderCount":67}');
        else
        	res.setBody('{"IsExistingOrg":false}');	
        res.setStatusCode(200);
        return res;
    	
    }
}