/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_WSCustomBulkAPI {
	static testmethod void unitTest1(){
		Contact c = new Contact(FirstName='John', LastName='Do');
		WS_CustomRestBulkApi.RestApiAction action = new WS_CustomRestBulkApi.RestApiAction();
		action.sobjectName = 'Contact';
		action.action='insert';
		action.jsonStr = JSON.serialize(c);
		RestRequest req = new RestRequest();
		req.requestURI = 'https://cs9.salesforce.com/services/apexrest/custom_bulk';  
		req.httpMethod = 'PATCH'; 
		RestResponse res = new RestResponse();
		RestContext.request = req;
		RestContext.response = res;
		WS_CustomRestBulkApi.doPatch(new List<WS_CustomRestBulkApi.RestApiAction>{action});
		c = [select Id From Contact where FirstName='John' and LastName='Do'];
		c.FirstName='Jo';
		WS_CustomRestBulkApi.RestApiAction action2 = new WS_CustomRestBulkApi.RestApiAction();
		action2.sobjectName = 'Contact';
		action2.action='update';
		action2.jsonStr = JSON.serialize(c);
		WS_CustomRestBulkApi.doPatch(new List<WS_CustomRestBulkApi.RestApiAction>{action2});
		WS_CustomRestBulkApi.RestApiAction action3 = new WS_CustomRestBulkApi.RestApiAction();
		action3.sobjectName = 'Contact';
		action3.action='delete';
		action3.jsonStr = JSON.serialize(c);
		WS_CustomRestBulkApi.RestApiAction action4 = new WS_CustomRestBulkApi.RestApiAction();
		action4.sobjectName = 'Contact';
		action4.action='undelete';
		action4.jsonStr = JSON.serialize(c);
		WS_CustomRestBulkApi.doPatch(new List<WS_CustomRestBulkApi.RestApiAction>{action3, action4});
	}
}