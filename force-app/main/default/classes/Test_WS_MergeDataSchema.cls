/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_WS_MergeDataSchema {

    static testMethod void myUnitTestOld() {
    	//FORM
        Form__c form = Test_Helper.newFormWithDs();
        //SUBFORM
        Form__c subform = Test_Helper.newSubFormWithDs();
        Integer formId = (Integer)form.form_Id__c;
        Integer subformId = (Integer)subform.form_id__c;
        WS_MergeDataSchema.doPost(formId, new List<Integer>{subformId});
    }
    
    static testMethod void myUnitTest() {
    	//FORM
        Form__c form = Test_Helper.newFormWithDs();
        //SUBFORM
        Form__c subform = Test_Helper.newSubFormWithDs();
        Integer formId = (Integer)form.form_Id__c;
        Integer subformId = (Integer)subform.form_id__c;
        WS_MergeDataSchema2.doPost(formId, new List<Integer>{subformId}, new List<String>{'Cases.CaseNumber','Parent.Parent.Cases.CaseNumber','Opportunities.OpportunitiesLineItems.Amount','Opportunities.OpportunityLineItems.ListPrice','Contact__r.Cases.CaseNumber'}, new List<String>{'Fax','Phone'});
    }
}