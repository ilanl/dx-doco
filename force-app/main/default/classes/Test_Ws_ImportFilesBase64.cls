@isTest
public class Test_Ws_ImportFilesBase64 {
	public static Boolean noPermission;
	static list<Attachment> atttList;
	public static void prepperData(){
		atttList = new list<Attachment>();
		Case cse=new Case();
        insert cse;
 		Attachment item = new Attachment();   	
    	item.Name='Unit Test Attachment';
    	item.body = Blob.valueOf('Unit Test Attachment Body');
        item.parentId=cse.id;
        insert item;
        
        Attachment bigItem = (Attachment)JSON.deserialize('{"BodyLength": 25000000}', Attachment.class);
    	bigItem.Name='Unit Test Attachment';
    	bigItem.body=Blob.valueOf('Unit Test Attachment Body');
        bigItem.parentId=cse.id;
        insert bigItem;
        atttList.add(item); 
        atttList.add(bigItem);
        	
	}
	
	static testMethod void testDoPost() {
	prepperData();
    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.addHeader('Content-Type', 'application/json');
    WS_importFilesBase64.RequestJesonFormat rData = new WS_importFilesBase64.RequestJesonFormat();
    rData.Ids = new list<id>(); 
    for(Attachment a : atttList){
    	rData.Ids.add(a.Id);
    }
    
    req.requestBody = Blob.valueOf(JSON.serialize(rData)); 
    

    req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/importFilesBase64'; 
    req.httpMethod = 'POST';
	RestContext.request = req;
	RestContext.response = res;
	WS_ImportFilesBase64.RequestJSONformat();
	//RestContext.response = res;

  }
  static testMethod void emptyJson(){
  	RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.addHeader('Content-Type', 'application/json');
    req.requestBody = Blob.valueOf('{"Ids": []}'); 
    req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/importFilesBase64'; 
    req.httpMethod = 'POST';
	RestContext.request = req;
	RestContext.response = res;
	WS_ImportFilesBase64.RequestJSONformat();
    
  }
  static testMethod void invalidReq(){
  	RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();
    req.addHeader('Content-Type', 'application/json');
    req.requestBody = Blob.valueOf('{Ids: []}'); 
    req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/importFilesBase64'; 
    req.httpMethod = 'POST';
	RestContext.request = req;
	RestContext.response = res;
	WS_ImportFilesBase64.RequestJSONformat();
    
  }
  static testMethod void noAccessPermissions(){
  	 	noPermission = true;
	  	RestRequest req = new RestRequest();
	    RestResponse res = new RestResponse();
	    req.addHeader('Content-Type', 'application/json');
	    req.requestBody = Blob.valueOf('{Ids: []}'); 
	    req.requestURI = URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/importFilesBase64'; 
	    req.httpMethod = 'POST';
		RestContext.request = req;
		RestContext.response = res;
		WS_ImportFilesBase64.RequestJSONformat();
    
  }
}