public class Utils_Type{

	public static string getType(Object o) {
		if(o==null) return '';              // we can't say much about null with our current techniques
	    if(o instanceof SObject)            return ((SObject)o).getSObjectType().getDescribe().getName()+''; 
	    if(o instanceof Boolean)            return 'Boolean';
	    if(o instanceof Id)                 return 'Id';
	    if(o instanceof String)             return 'String';
	    if(o instanceof Blob)               return 'Blob';
	    if(o instanceof Date)               return 'Date';
	    if(o instanceof Datetime)           return 'Datetime';
	    if(o instanceof Time)               return 'Time';
	    if(o instanceof Integer)            return 'Integer';
	    if(o instanceof Long)               return 'Long';
	    if(o instanceof Decimal)            return 'Decimal';  // we can't distinguish between decimal and double
	    if(o instanceof Double)             return 'Double';   // we can't distinguish between decimal and double
	    if(o instanceof List<object>)       return 'List';
	    return 'Object';                    // actually we can't detect maps and sets and maps
	}
  
  	public static Object createObject(String str, string typeStr){
  		if(typeStr=='Boolean')
  			return Boolean.valueOf(str);
  		else if(typeStr=='Date'){
  			String[] tab = str.split('/');
  			if(tab.size() == 3){
  				return Date.newInstance(Integer.valueOf(tab[2]), Integer.valueOf(tab[1]), Integer.valueOf(tab[0]));
  			}
  			else{
  				tab = str.split('-');
  				if(tab.size() == 3){
	  				return Date.newInstance(Integer.valueOf(tab[0]), Integer.valueOf(tab[1]), Integer.valueOf(tab[2]));
	  			}
  			}
  			return Date.valueOf(str);
  		}
  		else if(typeStr=='DateTime')
  			return DateTime.valueOf(str);
  		else if(typeStr=='Double')
  			return Double.valueOf(str);
  		else if(typeStr == 'Id')
  			return Id.valueOf(str);
  		else if(typeStr=='Integer')
  			return Integer.valueOf(str);
		else if(typeStr=='Decimal')
  			return Integer.valueOf(str);
		else if(typeStr=='Percent')
  			return Integer.valueOf(str);
		else if(typeStr=='Currency')
  			return Integer.valueOf(str);
  		return str;				
  	}
  
  	public static boolean isId(string sId){
		try{
			Id.valueOf(sId);
		}
		catch(Exception e){return false;}
		return true;
	}

}