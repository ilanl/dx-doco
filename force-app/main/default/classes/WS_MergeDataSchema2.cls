@RestResource(urlMapping='/merge2')
global with sharing class WS_MergeDataSchema2 {

    @HttpPost
    global static string doPost(Integer formId, List<Integer> subIds, List<String> paths, List<String> editPaths){
    	Set<Integer> ids = new Set<Integer>();
        ids.addAll(subIds);
        ids.add(formId);
        //Map of the form
        Map<Integer,Form__c> forms = new Map<Integer,Form__c>();
        //Map of dataschema__c
        Map<Id,Dataschema__c> dsMap = new Map<Id,Dataschema__c>();
		//To delete inexisting fields
		CTRL03_DefineDataSchema ctrl03 = new CTRL03_DefineDataSchema([Select DataSchema__c From Form__c Where form_Id__c=:formId].dataschema__c);
		List<String> deleteFields = ctrl03.fieldsToDelete;
		ctrl03.removeFields();
        for(Form__c f: [Select id,Main_Object__c, dataschema__r.Fields__c, dataschema__r.edit_fields__c, description__c,Name,data_Schema_XML_Id__c, RecordType.DeveloperName, form_ID__c, (select Id, subform__c, subform__r.Form_ID__c, subform__r.Document_Content_Id__c From Form_Subform_Associations__r), (Select collection__c From Interactive_Collections_Associations__r) From Form__c Where form_ID__c in :ids]){
            forms.put((Integer)f.form_Id__c,f);
            if(!dsMap.containsKey(f.dataschema__c))
					dsMap.put(f.dataschema__c,null);
        }
		dsMap = new Map<Id,Dataschema__c>([Select id,Main_Object__c, Fields__c, edit_fields__c, description__c,Name, (Select Name, On__c, Path__c, Children_Name__c, lookup_field__c, sobject__c, fields__c From SOQL_Extensions__r)  From Dataschema__c where id in:dsMap.keySet()]);
        //The Main dataschema of the form
        Dataschema mainDataschema = new Dataschema(dsMap.get(forms.get(formId).dataschema__c));
        //The list of dataschema
        List<Dataschema> dsLst = new List<Dataschema>();
        for(Dataschema__c ds:dsMap.values()){
        	if(ds.id != forms.get(formId).dataschema__c){
        		dsLst.add(new Dataschema(ds));
        	}
        }
        //to add the fields
        addNewFields(paths, editPaths, mainDataschema, dsLst, forms.get(formId));
        
        mainDataschema.save();
        mainDataschema = new Dataschema(forms.get(formId).Id);
        mainDataschema.generateXML(true);
    	return 'OK';
    }
    
    private static List<String> buildPathsLst(Set<String> pathsSet){
    	List<String> ret = new List<String>();
    	Map<Integer,List<String>> m = new Map<Integer,List<String>>();
    	for(String path:pathsSet){
    		Integer c = path.countMatches('.');
    		if(!m.containsKey(c))
    			m.put(c,new List<String>());
    		m.get(c).add(path);	
    	}
    	List<Integer> cLst = new List<Integer>(m.keySet());
    	cLst.sort();
    	for(Integer i:cLst){
    		ret.addAll(m.get(i));
    	}
    	return ret;
    }
    
    private static void addNewFields(List<String> paths, List<String> editPaths, Dataschema mainDataschema, List<Dataschema> dsLst, Form__c mainForm){
    	//For the applicative fields
    	addNewSelectedFields(paths, mainDataschema, dsLst);
    	//For the interactive fields
    	addNewInteractiveFields(editPaths, mainDataschema, dsLst, mainForm);
    }
    
    private static void addNewSelectedFields(List<String> paths, Dataschema mainDataschema, List<Dataschema> dsLst){
    	//For the selected fields
    	Set<String> pathsSet = new Set<String>();
    	for(String path:paths){
        	if(!mainDataschema.isSelectedPath(path))
        		pathsSet.add(path);
        }
        List<String> pathsLst = buildPathsLst(pathsSet);
    	if(!pathsLst.isEmpty()){
        	Set<String> alreadyDone = new Set<String>();	
    		for(Dataschema d:dsLst){
        		if(d.getDataSchema().get('Id') != mainDataschema.getDataSchema().get('Id')){
        			for(String path:pathsLst){
        				if(!alreadyDone.contains(path)){
	        				Set<String> info = new Set<String>();
	        				if(d.isSelectedPath(path,info)){
	        					alreadyDone.add(path);
	        					String inf = (new List<String>(info)).get(0);
	        					if(inf=='field')
	        						mainDataschema.selectActions('select', path, null, null);
	        					else if(inf=='children'){
	        						string childrenPath = path.substringbefore('.');
	        						mainDataschema.selectActionChild('select', path, childrenPath, null, null,null);
	        					}
	        					else if(inf!=null){
	        						if(inf.countMatches(':')==4){
	        							List<String> tab = inf.split(':');
	        							string parent = tab.get(0);
	        							string rName = tab.get(1);
	        							string sobjectName = tab.get(2);
	        							string lookup = tab.get(3);
	        							string cType = tab.get(4);
	        							mainDataschema.selectActionChild('select', path, parent+'.'+rName, lookup, sobjectName,(cType=='Children'));
	        						}
	        					}		
	        				}
        				}
        			}
        		}
        	}
        }
    }
    
    private static void addNewInteractiveFields(List<String> editPaths, Dataschema mainDataschema, List<Dataschema> dsLst, Form__c mainForm){
    	List<String> editFields = new List<String>();
    	Set<Id> Ids = new Set<Id>();
    	for(String s:editPaths){
    		if(s InstanceOf Id){
    			ids.add(s);
    		}
    		else{
    			editFields.add(s);
    		}
    	}
    	//1. add the edit fields
    	for(String path:editPaths){
        	if(!mainDataschema.isEditSelected(path)){
        		for(Dataschema d:dsLst){
        			if(d.isEditSelected(path)){
        				Utils.FieldUpdate fUpdate = d.getFieldUpdate(path);
        				mainDataschema.selectActions('edit', path, fUpdate, null);
        				break;
        			}
        		}
        	}
        }
    	//2. add the survey
    	Set<Id> collectionIds = new Set<Id>();
    	for(Interactive_Field__c iField: [Select Id, Interactive_Fields_collection__c From Interactive_Field__c Where id in :ids]){
    		collectionIds.add(iField.Interactive_Fields_collection__c);
    	}

    	for(Interactive_Collections_Association__c iCollection : mainForm.Interactive_Collections_Associations__r){
    		if(!collectionIds.isEmpty()){
    			if(collectionIds.contains(iCollection.collection__c)){
    				collectionIds.remove(iCollection.collection__c);
    			}
    		}
    	}

		//2.1 adding the interactive collections from the datamodel - only when they aren't connected to the form
		Set<Id> collectionConnectedIds = new Set<Id>();
		for (Interactive_Collections_Association__c ic : [select collection__c From Interactive_Collections_Association__c Where Form__c =:mainForm.Id]){
			collectionConnectedIds.add(ic.collection__c);
		}
		if (mainDataschema.dss != null && mainDataschema.dss.Id != null)
		{
			for (Interactive_Dataschema_Association__c icollection : [Select Id, Interactive_Form__c From Interactive_Dataschema_Association__c Where DataSchema__c =:mainDataschema.dss.Id and Interactive_Form__c not in :collectionConnectedIds]){
				if (!collectionIds.contains(icollection.Interactive_Form__c))
					collectionIds.add(icollection.Interactive_Form__c);
			}
		}
    	
    	List<Interactive_Collections_Association__c> surveyToAdd = new List<Interactive_Collections_Association__c>();
    	for(Id id:collectionIds){
    		surveyToAdd.add(new Interactive_Collections_Association__c(Form__c=mainForm.id, collection__c=id));
    	}
    	if(!surveyToAdd.isEmpty()){
    		DML_CustomExtension.insertObj(surveyToAdd);
    	}
    }
    
}