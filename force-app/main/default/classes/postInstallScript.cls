global class postInstallScript implements InstallHandler{
	global void onInstall(InstallContext context){
		//the settings
		Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
		if(myConf == null)
			myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://salesforce6.docomotioninc.com', Opening_Designer_Timeout__c = 60, Signature_Expiration_Time__c =168, Mass_Generation_Expiration_Period__c = 96);
		if(myConf.Mass_Generation_Expiration_Period__c == null){
			myConf.Mass_Generation_Expiration_Period__c = 96;
		}
		myConf.server_URL__c='https://salesforce6.docomotioninc.com';
		myConf.Signature_URL__c='https://oapi6.docomotioninc.com';
		upsert myConf;
		
		if(context.previousVersion() == null){//it's the first time we install the app
			
		}
		else{
			//When updating to 6 Then copy the curr
			if (context.previousVersion().major() < 6 ){
				try{	        
					/**
					* FOR THE FORMS
					* Combine options
					*/
					List<Form__c> forms = [Select Main_Object__c, total_version__c, current_Version__c, current_published_version__c, current_activated_version__c From Form__c];
					
					updateFormsNewFields(forms);					
					update forms;

				}
				catch (Exception  e){ }
			}
		}
	}
	
	
	private void updateFormsNewFields(List<Form__c> forms){
		for (Form__c form : forms){
			if(form.Main_Object__c == 'Case'){
				form.Default_File_Name__c = 'caseNumber';
			}else{
				form.Default_File_Name__c = 'name';
			}
			form.Recipient_Allow_Changing__c = true;
			form.Combine_Allow_Changing__c = true;
			form.Combine_Default__c = false;
		}
	}
}