trigger DataschemaAfterUpdate on Dataschema__c (after update) {
	//All the dataschema with changements on fields or updates fields and needs to regenerate the linked form
	List<Dataschema__c> dLst = new List<Dataschema__c>();
	//For the old Values
	Map<Id, Dataschema__c> dOldMap = new Map<Id,Dataschema__c>();
	
	for(Dataschema__c d: Trigger.new){
		Dataschema__c oldD = Trigger.OldMap.get(d.id);
		if(d.fields__c != oldD.fields__c || d.edit_fields__c != oldD.edit_fields__c){
			dLst.add(d);
			dOldMap.put(d.id,oldD);
		}
	}
	
	if(!dLst.isEmpty()){
		//To check the regenarete XML checkbox on the form
		TR013_RegenerateXML.regenerateXML(dLst);
		//To check the new Dataschema checkbox on the form
		TR014_DataschemaFieldsUpdate.needToOpenInDesigner(dLst, dOldMap);
	}	
}