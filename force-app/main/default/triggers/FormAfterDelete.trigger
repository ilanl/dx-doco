trigger FormAfterDelete on Form__c (after delete) {
	/**To delete the content files when deleting a form**/
	try{
		if(!TR011_DeleteFormFiles.inactive)
			TR011_DeleteFormFiles.deleteFiles(Trigger.old);
	}
	catch(Exception e){}
}