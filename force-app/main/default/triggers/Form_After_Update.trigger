trigger Form_After_Update on Form__c (after update) {

    //To update the association with the last published version of a subform
    List<Form__c> subformsPublished = new List<Form__c>();
    List<Form__c> activatingForms = new List<Form__c>();
    
    //to delete the FeedItem
    Map<Id,Set<Id>> formCdMap = new Map<Id,Set<Id>>();
    
    for(Form__c form:Trigger.new){
        Form__c oForm = Trigger.OldMap.get(form.id);
        
        if(form.status__c=='Published' && form.status__c != oForm.status__c)
            subformsPublished.add(form);
        
        if(form.status__c=='Activating' && form.status__c != oForm.status__c)
            activatingForms.add(form);                             
    }
    
    if(!subformsPublished.isEmpty()){
        TR001_VersionUpdate.UpdateAssociationVersionsAndPublishForms(subformsPublished);
    }
    
    if(!activatingForms.isEmpty()){
        TR006_PublishForms.activate(activatingForms);
    }

}