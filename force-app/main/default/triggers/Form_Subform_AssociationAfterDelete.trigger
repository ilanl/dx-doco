trigger Form_Subform_AssociationAfterDelete on Form_Subform_Association__c (after delete) {
	Set<Id> formIds = new Set<Id>();
	for(Form_Subform_Association__c a: Trigger.old){
		formIds.add(a.form__c);
	}
	if(!formIds.isEmpty()){
		TR013_RegenerateXML.needToOpenInDesigner(formIds);
	}
}