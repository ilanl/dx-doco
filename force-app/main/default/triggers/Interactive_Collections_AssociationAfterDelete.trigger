trigger Interactive_Collections_AssociationAfterDelete on Interactive_Collections_Association__c (after delete) {
	Set<Id> formsIds = new Set<Id>();
	for(Interactive_Collections_Association__c a:trigger.old){
		formsIds.add(a.form__c);
	}
	if(!formsIds.isEmpty()){
		TR013_RegenerateXML.needToOpenInDesigner(formsIds);
	}
}