trigger SignatureTrigger on Signature__c (before insert, after insert, after delete, after update, before update)  {
	if (Trigger.isBefore){
		if(Trigger.isInsert){
			SignatureHandler.prepopulateData(Trigger.new);
			SignatureHandler.populateEmailFields(Trigger.new);
		}
		
	}
	else if(Trigger.isAfter){
		if(Trigger.isInsert){
			SignatureHandler.sendSignatures(Trigger.new);
		}
		else if(Trigger.IsUpdate){
			SignatureHandler.addChatter(Trigger.new, Trigger.oldMap);
		}
	}
}