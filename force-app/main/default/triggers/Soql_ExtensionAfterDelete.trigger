trigger Soql_ExtensionAfterDelete on SOQL_Extension__c (after delete) {
	Set<Id> dIds = new Set<Id>();
	for(SOQL_Extension__c s:Trigger.old){
		dIds.add(s.dataschema__c);
	}
	if(!dIds.isEmpty()){
		TR013_RegenerateXML.regenerateXML(dIds);
		TR014_DataschemaFieldsUpdate.needToOpenInDesigner(Trigger.old);
	}
}