trigger Soql_ExtensionAfterInsert on SOQL_Extension__c (before insert) {
	Set<Id> dIds = new Set<Id>();
	for(SOQL_Extension__c s:Trigger.new){
		dIds.add(s.dataschema__c);
	}
	if(!dIds.isEmpty())
		TR013_RegenerateXML.regenerateXML(dIds);
}