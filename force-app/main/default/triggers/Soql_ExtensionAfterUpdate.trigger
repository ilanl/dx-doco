trigger Soql_ExtensionAfterUpdate on SOQL_Extension__c (after update) {
	Set<Id> dIds = new Set<Id>();
	List<SOQL_Extension__c> updatedFields = new List<SOQL_Extension__c>();
	for(SOQL_Extension__c s:Trigger.new){
		if(s.fields__c != Trigger.oldMap.get(s.id).fields__c){
			dIds.add(s.dataschema__c);
			updatedFields.add(s);
		}
	}
	if(!dIds.isEmpty())
		TR013_RegenerateXML.regenerateXML(dIds);
	if(!updatedFields.isEmpty())
		TR014_DataschemaFieldsUpdate.needToOpenInDesigner(updatedFields, Trigger.oldMap);	
}