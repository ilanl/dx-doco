public with sharing class AsyncGenerationRequest  implements System.Queueable,Database.AllowsCallouts{
	public SchemaFactory.RenderRequest requestBody;
	public AsyncGenerationRequest(SchemaFactory.RenderRequest requestBody){
		this.requestBody = requestBody;
	}
	public void execute(QueueableContext context) {
			//requestBody.SfSessionId = UserInfo.getSessionId();
			Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
	    	string END_POINT = myConf.Server_URL__c+'/api/render';
		 	Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(END_POINT);
            req.setMethod('POST');
            req.setHeader('Content-Type','application/json');
            Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            req.setHeader('Authorization', authorizationHeader);   
            string body = JSON.serialize(requestBody);
            Utils.log('BODY',body);
            req.setBody(body);
            req.setTimeout(60000);
            try{
	            HttpResponse res = h.send(req);
	            Utils.log('REPONSE',res.getBody());
	            if(res.getStatusCode() != 200){
	            	throw new SchemaFactory.RenderException('4003', res.getStatus()+' ('+res.getStatusCode()+')',null);
	            }
	            SchemaFactory.RenderResponse response =(SchemaFactory.RenderResponse)JSON.deserialize(res.getBody(), SchemaFactory.RenderResponse.class); 
	            if(response.ErrorID == null || response.ErrorId == '0'){
	            	return ;
	            }
	            else{//error
	            	throw new SchemaFactory.RenderException(response.ErrorId, response.MessageId, null,response.InternalErrorCode);
                  
	            }
            }
            catch(SchemaFactory.RenderException re){
            	throw re;
            }
            catch(Exception e){
			    throw new SchemaFactory.RenderException('4003', e.getMessage(), null);	
			}
            
          
               
    }
    
}