public with sharing class CTRL03_DefineDataSchema {

    public Integer currentStep { get; set; }
    //public List<SelectOption> objects { get; set; }
    //public string selectedObject {get; set; }
    //public string selectedObjectFields {get; set;}
    public string dataSchema {get; set;}
    public string children {get; set;}
    //public string objectsSchemas {get; set;}
    public String id;
    private Boolean isNewDataschema = false;
    public ApexPages.StandardController stdController;
    public string objectType {get;private set;}
    public String dsLst{get;private set;}
    public string nsField{
    	get{
    		if(nsField==null){
    			nsField = Utils.getNameSpace();
    		}
    		return nsField;
    	}
    	private set;
	}
    public string nsClass{
    	get{
    		if(nsClass==null){
    			nsClass = Utils.getNameSpaceForClass();
    		}
    		return nsClass;
    	}
    	private set;
	}
	//For the JSON tree
	public string jSONData{get;private Set;}
	//the set of fields to delete
	public List<String> fieldsToDelete{get;private Set;}

    public Boolean isRtl {get {return (new Set<String>{'iw'}).contains(UserInfo.getLanguage().subString(0, 2));} private set;}
	
	private DataSchema d;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CTRL03_DefineDataSchema() {
    	if(ApexPages.currentPage().getParameters().containsKey('id')){
	        id = ApexPages.currentPage().getParameters().get('id');
    		setObjectType();
			initData();
			initDataSchema((id)id);
		}
    }

	public CTRL03_DefineDataSchema(Id dId){
		id = dId;
    	setObjectType();
		initData();
		initDataSchema(id);
	}

	public PageReference removeFields(){
		d.save();
		PageReference pageRef = page.VF10_DefineDataSchemaFromDataschema;
		pageRef.getParameters().put('id',id);
		pageRef.setRedirect(true);
		return pageRef;
	}
    
    private void setObjectType(){
    	objectType = ''+((Id)id).getSobjectType();
    }

    private void initData(){
    	if(id != null){
	        Sobject object_form = CTRL04_DataSchema.getDataSchemaById(id);
	        //dataSchema = String.escapeSingleQuotes(JSON.serialize(object_form));//fix of the bug with single quote in the object name
	        dataSchema = JSON.serialize(object_form);
	        if (objectType.containsIgnoreCase('Form__c') && object_form.get('Fields__c')==null && !ApexPages.currentPage().getParameters().containsKey('schema')){
	            currentStep = 0;
	        } else {
                currentStep = 1;
	        }
            system.debug('currentStep: '+currentStep);
			system.debug(object_form);
	        children = CTRL04_DataSchema.getObjectChildren(id);
    	}
    }

	private void initDataschema(Id datamodelId){
		d = new dataSchema(datamodelId);
		Set<Utils.FieldInfo> toDelete = new Set<Utils.FieldInfo>();
		jSONData = d.generateJsonTree(toDelete);
		fieldsToDelete = new List<String>();
		for (Utils.FieldInfo fi:new List<Utils.FieldInfo>(toDelete)){
			fieldsToDelete.add(fi.toString());
		}
		Utils.log('fieldsToDelete',fieldsToDelete);
	}

    public PageReference cancelChanges(){
    	if(ApexPages.currentPage().getParameters().containsKey('retUrl')){
	        String retUrl = ApexPages.currentPage().getParameters().get('retUrl');
	        return new PageReference('/'+retUrl);
    	}
        return new PageReference('/'+id);
    }
    
    public string getLabelsMap(){
        try {
            system.debug('ID: '+id);
            return JSON.serialize(CTRL04_DataSchema.getLabelsMap(id));
        } catch (Exception e){
            return '';
        }
    }

}