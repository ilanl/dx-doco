public with sharing class CTRL12_Dataschema_Extensions {

	private boolean isClone;
	private ApexPages.StandardController stdC;

    public CTRL12_Dataschema_Extensions(ApexPages.StandardController ctrl){
    	if(!Test.isRunningTest())
    		ctrl.addFields(new List<String>{'Fields__c', 'Edit_Fields__c', 'Description__c'});
    	stdC = ctrl;
    	isClone = (ApexPages.currentPage().getParameters().containsKey('clone') && (ApexPages.currentPage().getParameters().get('clone')=='1'));
    }
    
    //To get the list of objects
    public static List<SelectOption> getAllObjectsList(){
        List<SelectOption> ret2 = new List<SelectOPtion>();
        List<Utils.Option> ret = new List<Utils.Option>();
        Set<String> objectNames = new Set<String>();
        
        Set<String> toExclude = new Set<String>{'openactivity', 'activityhistory' ,'announcement', 'apexclass', 'apexlog','asyncapexjob','apextestqueueitem','apextestresult','apextrigger','chatterconversation','chatterconversationmember','chattermessage','datacloudcompany','datacloudcontact','datacloudownedentity','datacloudpurchaseusage','workfeedback','workfeedbackhistory','workfeedbackquestion','workfeedbackquestionhistory','workfeedbackquestionsethistory','workfeedbackquestionset','workfeedbackquestionsetshare','workfeedbackquestionshare','workfeedbackrequest','workfeedbackrequestfeed','workfeedbackrequesthistory','workfeedbackrequestshare','workfeedbackshare','workgoal','workgoalcollaborator','workgoalcollaboratorhistory','workgoalfeed','workgoalhistory','workgoallink','workgoalshare','contentworkspace','contentworkspacedoc','workperformancecycle','workperformancecyclefeed','workperformancecycleshare','workperformancecyclehistory','secureagent','secureagentplugin','secureagentpluginproperty','apexcomponent','apexpage'};
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
		for(String str: gd.keySet()){
			if(!toExclude.contains(str) && (str.endsWith('__c') || (!str.endsWith('__c') && !str.contains('__'))))
				objectNames.add(str);	
		}
          
        
        List<String> objectNamesLst = new List<String>(objectNames);    
        for(Schema.DescribeSobjectResult obj:Schema.describeSObjects(objectNamesLst)){
            if(obj.isAccessible() && obj.isQueryable()){
                Utils.Option option = new Utils.Option(obj.getName(), obj.getLabel());
                ret.add(option);
            }
        }   
        ret.sort();
        for(Utils.Option opt:ret)
            ret2.add(new SelectOption(opt.value, opt.label));
        return ret2;
    }
    
    public PageReference saveDs(){
    	Id clonedId = stdC.getId();
    	if(!isClone){
    		PageReference p = stdC.save();
    		return p;
    	}
    	else{
    		//Save new Dataschema
    		Dataschema__c d = ((Dataschema__c)stdC.getRecord()).clone();
    		ApexPages.StandardController std2 = new ApexPages.StandardController(d);
			PageReference p2 = std2.save();
			if(p2==null)
				return null;
			Id newId = std2.getId();
			system.debug('***new id: '+newId);
    		List<Interactive_Dataschema_Association__c> aLst = new List<Interactive_Dataschema_Association__c>();
    		for(Interactive_Dataschema_Association__c a:[Select Interactive_Form__c From Interactive_Dataschema_Association__c Where Dataschema__c = :clonedId]){
    			aLst.add(new Interactive_Dataschema_Association__c(Interactive_Form__c = a.Interactive_Form__c, Dataschema__c = newId));
    		}
    		List<SOQL_Extension__c> sLst = new List<SOQL_Extension__c>();
    		for(SOQL_Extension__c s:[Select Children_Name__c, Fields__c, Lookup_Field__c, On__c, Path__c, Sobject__c From SOQL_Extension__c Where dataschema__c = :clonedId]){
    			sLst.add(new SOQL_Extension__c(dataschema__c = newId, Children_Name__c = s.Children_Name__c, Fields__c=s.Fields__c, Lookup_Field__c=s.Lookup_Field__c, On__c=s.On__c, Path__c=s.Path__c, Sobject__c=s.Sobject__c));
    		}
    		if(!aLst.isEmpty()){
    			try{
    				DML_CustomExtension.insertObj(aLst);
    			}
    			catch(Exception e){
    				return null;
    			}
    		}
    		if(!sLst.isEmpty()){
    			try{
    				DML_CustomExtension.insertObj(sLst);
    			}
    			catch(Exception e){
    				return null;
    			}
    		}
    		return new PageReference('/'+newId);
    	}
    	
    }
    
    public PageReference saveAndDefineDs(){
    	if(saveDS()!=null){
    		PageReference p = Page.VF10_DefineDataSchemaFromDataschema;
    		p.setRedirect(true);
    		Id dsId = stdC.getId();
			p.getParameters().put('id',dsId);
			return p;
    	}
    	return null;
    }

}