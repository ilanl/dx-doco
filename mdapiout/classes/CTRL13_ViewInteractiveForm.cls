public with sharing class CTRL13_ViewInteractiveForm {
    public Map<Integer,Interactive_Field__c> interactiveFieldMap{set;get;}
    public Map<Integer,boolean> viewMap{set;get;}
    public Integer index{set;get;}
    private apexpages.standardcontroller con;
    //private list<Interactive_Field__c> deleteList;
    public integer cancelSize{set;get;}
    public string objectId{set;get;}
    
    public CTRL13_ViewInteractiveForm(apexpages.standardcontroller con){
        this.con = con;
        objectId = con.getId();
        index = 0;
        //deleteList = new list<Interactive_Field__c>();
        interactiveFieldMap = new Map<integer,Interactive_Field__c>();
        viewMap = new Map<Integer,boolean>();
        for(Interactive_Field__c tmpIntField : [select Id,Type__c,Required__c,Question__c,Choices2__c,Report_Link__c,Interactive_Fields_Collection__c
                                                From Interactive_Field__c 
                                                where Interactive_Fields_Collection__c =: objectId
                                                order by createdDate]){
            interactiveFieldMap.put(index, tmpIntField);
            viewMap.put(index, true);
            index++;                                            
        }
        interactiveFieldMap.put(index, new Interactive_Field__c(Interactive_Fields_Collection__c = objectId));
        viewMap.put(index, false);
        cancelSize = index;
    }
    
    public void changeView(){
        integer changeIndex = integer.valueof(ApexPages.currentPage().getParameters().get('changeIndex'));
        string status = ApexPages.currentPage().getParameters().get('status');
        if(status == Label.Edit)
            viewMap.put(changeIndex,false);
        if(status == Label.Cancel){
            interactiveFieldMap.put(changeIndex, [select Id,Type__c,Required__c,Question__c,Choices2__c,Report_Link__c
                                                    From Interactive_Field__c 
                                                    where Id =: interactiveFieldMap.get(changeIndex).Id][0]);
            viewMap.put(changeIndex,true);
        }
        return;
    }
    
    public void addLine(){
        if(correctInput()){
            interactiveFieldMap.get(index).Interactive_Fields_Collection__c = objectId;
            viewMap.put(index, true);
            index++;
            interactiveFieldMap.put(index, new Interactive_Field__c(Interactive_Fields_Collection__c = objectId));
            viewMap.put(index, false);
        }
        return;
    }
    
    public void deleteLine(){
        integer deleteIndex = integer.valueof(ApexPages.currentPage().getParameters().get('deleteIndex'));
        if(interactiveFieldMap.get(deleteIndex).Id != null){
            cancelSize--;
            DML_CustomExtension.deleteObj(interactiveFieldMap.get(deleteIndex));
        }
        interactiveFieldMap.remove(deleteIndex);
        viewMap.remove(deleteIndex);
        return;
    }
        
    public PageReference saveFields() {
        if(interactiveFieldMap.get(Index).Question__c == null){
            interactiveFieldMap.remove(Index);
        }
        if(correctInput()){
            system.debug('---------------values: '+interactiveFieldMap.values());
            DML_CustomExtension.upsertObj(interactiveFieldMap.values());
            /*if(deleteList.size() > 0){
                DML_CustomExtension.deleteObj(deleteList);
            }*/
            con.save();
            return ToListPage();
        }
        else{
            if(interactiveFieldMap.size() == Index){
                interactiveFieldMap.put(index, new Interactive_Field__c(Interactive_Fields_Collection__c = objectId));
            }
            return null;
        }
    }
    
    public PageReference cancel(){
        con.cancel();
        return ToListPage();
    }
    
    private boolean correctInput(){
        set<string> questionSet = new set<string>();
        for(Interactive_Field__c intFieldtmp : interactiveFieldMap.values()){
            questionSet.add(intFieldtmp.Question__c);
            // error if empty
            if(string.isempty(intFieldtmp.Choices2__c) && (intFieldtmp.Type__c == Label.Picklist || intFieldtmp.Type__c == Label.Multi_Picklist || intFieldtmp.Type__c == Label.Radio)){
                intFieldtmp.Choices2__c.addError(Label.Required_Value);
            } 
            // error if Duplicate Values
            else if (intFieldtmp.Type__c == Label.Picklist || intFieldtmp.Type__c == Label.Multi_Picklist || intFieldtmp.Type__c == Label.Radio){
                Integer count = 0;
                List<String> listOfNewLineValues = intFieldtmp.Choices2__c.split('\n');
                for (String str1 : listOfNewLineValues){
                    for (String str2 : listOfNewLineValues){
                        if(str1.equals(str2)){
                            count++;
                            if(count>listOfNewLineValues.size()){
                              intFieldtmp.Choices2__c.addError(Label.Duplicate_Value);
                            }
                        }
                    }
                }
            }

            if(string.isempty(intFieldtmp.Question__c)){
                intFieldtmp.Question__c.addError(Label.Required_Value);
            }

        }
        if(questionSet.size() < interactiveFieldMap.size()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.Duplicate_Name));
        }
        
        if(ApexPages.hasMessages()){
            return false;
        }
        else{
            return true;
        }
    }
    
    private PageReference ToListPage(){
        return new PageReference('/'+con.getId().substring(0,3));
    }
}