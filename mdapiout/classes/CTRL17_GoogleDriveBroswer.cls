public with sharing class CTRL17_GoogleDriveBroswer {

	private String path;
	private String filedId;

	//For adpation dev / package
	public string nsField{
    	get{
    		if(nsField==null){
    			nsField = Utils.getNameSpace();
    		}
    		return nsField;
    	}
    	private set;
	}
    public string nsClass{
    	get{
    		if(nsClass==null){
    			nsClass = Utils.getNameSpaceForClass();
    		}
    		return nsClass;
    	}
    	private set;
	}
	
	public CTRL17_GoogleDriveBroswer() {	
	}

	public CTRL17_GoogleDriveBroswer(CTRL02_GenerateDocument genCtrl){}

	@remoteAction
	public static Boolean isConnectionStatusOk(){	
		GoogleCall google=  new GoogleCall();
		Boolean returnStr = google.isConnectionStatusOk();
		google.saveSettings();
		return returnStr;
	}

	@remoteAction
	public static String getfilesListFromGSettingsFolder(){
		GoogleCall google=  new GoogleCall();
		String returnStr =  google.getFilesList('');
		google.saveSettings();
		return returnStr;
	}

	@RemoteAction
	public static String getFolderId(String path){
		GoogleCall google = new GoogleCall();
		String returnStr =  google.getFolderId(path);
		//google.saveSettings();
		return returnStr;
	}

	@RemoteAction
	public static String getFilesList(String folderID){
		GoogleCall google = new GoogleCall();
		String returnStr=  google.getfilesList(folderID);
		google.saveSettings();
		return returnStr;
	}

	@RemoteAction
	public static String moveBackByFolderId(String folderId){
		GoogleCall google=  new GoogleCall();
		String returnStr =  google.moveBackByFolderId(folderId);
		google.saveSettings();
		return returnStr;
	}

	@RemoteAction 
	public static String createNewFolder (String folderName,String path){
		GoogleCall google = new GoogleCall();
		String newFolderId = google.googleCreateFolder(folderName,path);
		System.debug(newFolderId);
		String returnStr = google.getfilesList(newFolderId);
		google.saveSettings();
		return returnStr;

	}


}