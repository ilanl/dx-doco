public class ExceptionWS { 
	private Id OrgId;
	private Integer formId;
	private string ErrorText;
	private string SfSessionId;
	private string userName;

	public ExceptionWS(Integer fId, String m){
		formId = fId;
		ErrorText = m;
		OrgId = UserInfo.getOrganizationId();
		//SfSessionId = UserInfo.getSessionId();
		userName = UserInfo.getUserName();
	}

	public ExceptionWS(String m){
		this(null, m);
	}

	public ExceptionWS(SchemaFactory.RenderException re){
		this(re.formId,re.getMessage());
	}

	public void setSessionId(String sessionId){
		this.SfSessionId = sessionId;
	}

	public ExceptionWS(Utils.DocomotionQueryFieldException qe, Integer fId){
		this(fId, qe.getMessage());
	}

	public void send(){
		string jsonStr = JSON.serialize(this);
		send(jsonStr);
	}

	@future(callout=true)
	public static void send(string jsonStr){
		Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
        string epUrl = myConf.Server_URL__c+'/api/sflog';

        
        Http h = new Http();
        
        Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(epUrl);
        req.setMethod('POST');
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setBody(jsonStr);
        req.setTimeout(120000);
        Utils.log('req',req);
        Utils.log('BODY',req.getBody());
        if (Test.isRunningTest())
            return;
        // Send the request, and return a response
        HttpResponse res = h.send(req);
	}
}