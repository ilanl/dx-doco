public class GoogleCall {
	
	//GOOGLE AUTH ENDPOINT
	final static string GOOGLE_AUTH_END_POINT = 'https://www.googleapis.com/oauth2/v3/token';
	final static string GOOGLE_DRIVE_END_POINT = 'https://www.googleapis.com/drive/v3/files';
	final static string GOOGLE_DRIVE_UPLOAD_END_POINT = 'https://www.googleapis.com/upload/drive/v3/files';
	final static String GOOGLE_APIKEY = '682422048047-dpl7ikbnm8cgafkhc60v68mjpiufecj7.apps.googleusercontent.com';
	final static String DEFAULT_FOLDER_NAME = 'Docomotion Documents';

	public boolean IsOAuthFailedbefore;
	
	/**PUBLIC INSTANCE VARIABLES **/
	public boolean hasCredentials{
		get{
			hasCredentials = (gSettings != null);
			return hasCredentials;
		}
		private set;
	}
	
	public GoogleAPI_Settings__c gSettings{get;private Set;}
	private boolean settingsChanged;
	private boolean settingsInit;
	
	/** CONSTRUCTOR **/
	public GoogleCall(Boolean settingsInit){
		settingsChanged=false;
		this.settingsInit = settingsInit;
		IsOAuthFailedbefore = false;
		if (googleOnUser()){
			gSettings = getUsersSettings();
		}
		else{
			gSettings = GoogleAPI_Settings__c.getOrgDefaults();
			if (!settingsInit){
				GoogleAPI_Settings__c gSettings2 = getUsersSettings();
				if (gSettings2.Refresh_Token__c != gSettings.Refresh_Token__c){
					gSettings2.Path__c = gSettings.Path__c;
					gSettings2.Docomotion_Folder_Id__c = gSettings.Docomotion_Folder_Id__c;
				}
				gSettings2.Refresh_Token__c = gSettings.Refresh_Token__c;
				gSettings2.Access_Token__c = gSettings.Access_Token__c;
				gSettings = gSettings2;
				settingsChanged = true;
			}
		}
	}

	public GoogleCall(){
		this(false);
	}

	/* PUBLIC METHODS */
	//Return true if the form is new
	public Boolean isNew(){
		return (gSettings.Refresh_Token__c==null);
	}

	//Method to refresh and save the google access token
	public Boolean refreshAndSaveAccessToken(){
		Boolean ret = refreshGoogleAccessToken();
		if (ret)
		{
			DML_CustomExtension.upsertObj(gSettings);
		}
		return ret;
	}

	//To delete the related settings
	public void deleteSettings(){
		if (revoke() && gSettings.Id != null)
			DML_CustomExtension.deleteObj(gSettings);
	}

	//To save the credentials (when receiving code from google)
	public void saveCredentials(string code, string callbackURL){
	    Map<String,String> params = new Map<String,String>();
	    params.put('code',code);
	    params.put('redirect_uri',callBackURL);
		params.put('client_id','682422048047-dpl7ikbnm8cgafkhc60v68mjpiufecj7.apps.googleusercontent.com');
		params.put('client_secret','xZN_-pYzRUhqB6E0keE26gEu');
		params.put('grant_type','authorization_code');
		string body='';
		for(String key:params.keySet()){
			body+=key+'='+params.remove(key);
			if(!params.isEmpty())
				body +='&';
		}
		Map<String, String> headers = new Map<String,String>();
		headers.put('Content-Type','application/x-www-form-urlencoded');
	    try{
	        HttpResponse res = googleHttpCall(GOOGLE_AUTH_END_POINT, body, headers);
			if (res != null){
				system.debug('***RESPONSE: '+res.getBody());
				Map<string,Object> repObj = (Map<string,Object>) JSON.deserializeUntyped(res.getBody());
				if(repObj.containsKey('refresh_token')){
					gSettings.Refresh_Token__c = ''+repObj.get('refresh_token');
					gSettings.Access_Token__c = ''+repObj.get('access_token');
					string fId = haveDocomotionFolder();
					if (fId == null)
						GoogleCreateInitFolder();
					else{
						gSettings.Path__c = DEFAULT_FOLDER_NAME;
						gSettings.Docomotion_Folder_Id__c = fId;
					}
					if (googleOnUser())
						gSettings.SetupOwnerId = UserInfo.getUserId();
					settingsChanged = true;
				}
			}
	    }
	    catch(Exception e){}
	}

	//Check if there is a docomotion folder
	private string haveDocomotionFolder(){
		string r = getfilesList(null, DEFAULT_FOLDER_NAME);
		Map<String,Object> body = (Map<String,Object>)JSON.deserializeUntyped(r);
		string responseStr = (String)body.get('response');
		Map<String, List<GoogleUtils.GoogleFile>> filesMap = (Map<String, List<GoogleUtils.GoogleFile>>)JSON.deserialize(responseStr,Map<String, List<GoogleUtils.GoogleFile>>.class);
		if (!filesMap.containsKey('files') || filesMap.get('files').size() == 0)
			return null;
		else{
			List<GoogleUtils.GoogleFile> files = filesMap.get('files');
			return files[0].id;
		}
	}

	public string haveFileInFolder(string folderId, string fileName){
		string r = getfilesList(folderId, fileName);
		if (r != null)
		{
			Map<String,Object> body = (Map<String,Object>)JSON.deserializeUntyped(r);
			string responseStr = (String)body.get('response');
			Map<String, List<GoogleUtils.GoogleFile>> filesMap = (Map<String, List<GoogleUtils.GoogleFile>>)JSON.deserialize(responseStr,Map<String, List<GoogleUtils.GoogleFile>>.class);
			if (!filesMap.containsKey('files') || filesMap.get('files').size() == 0)
				return null;
			else{
				List<GoogleUtils.GoogleFile> files = filesMap.get('files');
				return files[0].id;
			}
		}
		return null;
	}

	//to initalize the oauth flow
	public PageReference googleCredentials(string callbackURL){
		return googleCredentials(callbackURL, null);
	}

	public PageReference googleCredentials(string callbackURL, string state){
		string url = 'https://accounts.google.com/o/oauth2/auth';
		Map<String, string> params = new Map<String,String>();
		params.put('scope','https://www.googleapis.com/auth/drive');
		params.put('response_type','code');
		params.put('redirect_uri', callbackURL);
		params.put('client_id','682422048047-dpl7ikbnm8cgafkhc60v68mjpiufecj7.apps.googleusercontent.com');
		params.put('access_type','offline');
		//params.put('approval_prompt','force');
		params.put('prompt','select_account consent');
		if (state != null)
			params.put('state',state);
		url += '?';
		for(string key:params.keySet()){
			url += key +'='+params.remove(key);
			if(!params.isEmpty())
				url +='&';
		}
		PageReference p = new PageReference(url);
		p.setRedirect(true);
		return p;
	}

	//To create the init folder
	public void GoogleCreateInitFolder(){
		String folderId = googleCreateFolder(DEFAULT_FOLDER_NAME,null);
		gSettings.Docomotion_Folder_Id__c = folderId;
		gSettings.Path__c = DEFAULT_FOLDER_NAME;
	}

	public String googleCreateFolder(String path, String parentId){
		string url = GOOGLE_DRIVE_END_POINT;
		Map<String,Object> m = new map<String,Object>();
		String[] tab = path.split('\\\\');
		m.put('name', tab[tab.size()-1]);
		m.put('mimeType','application/vnd.google-apps.folder');
		if (parentId != null)
		{
			m.put('parents',new List<String>{parentId});
		}
		string jsonStr = JSON.serialize(m);
		Map<String,String> headers = new Map<String,String>();
        headers.put('Content-Type','application/JSON');
        headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
        try{
            HttpResponse res = googleHttpCall(url, jsonStr, headers);
			if (res != null){
				system.debug('***RESPONSE: '+res.getBody());
				Map<String, Object> resMap = (Map<String,Object>) (JSON.deserializeUntyped(res.getBody()));
				string folderId = (String) resMap.get('id');
				return folderId;
			}
        }
        catch(Exception e){
        	system.debug('***EXCEPTION: '+e.getMessage());
        }
		return null;
	}

	//To save the google settings
	public void saveSettings(){
		if(settingsChanged){
			List<GoogleAPI_Settings__c> settingslst = new List<GoogleAPI_Settings__c>();
			settingslst.add(gSettings);
			system.debug(gSettings);
			if(!googleOnUser() && !settingsInit){
				GoogleAPI_Settings__c gSettings3 = GoogleAPI_Settings__c.getOrgDefaults();
				gSettings3.Access_Token__c = gSettings.Access_Token__c;
				settingslst.add(gSettings3);
				system.debug(gSettings3);
			}
			DML_CustomExtension.upsertObj(settingslst);
			System.debug('***Saved settings: '+settingsLst);
		}
	}

	// A function that checks if the connetion to googg
	public Boolean isConnectionStatusOk (){
		refreshGoogleAccessToken();
		String url = GOOGLE_DRIVE_END_POINT;
		Map <String,String> headers = new Map<String,String>();
		headers.put('Content-Type','application/JSON');
        headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
        try{
			HttpResponse resp = googleHttpCall(url,'',headers);
			return (resp != null);      
        }catch(Exception e){
        	System.debug('Exception at GoogleCall.isConnectionStatusOk. Msg : '+ e.getMessage());
        	return false;
       	}
     }
     
	 //To get the files list
	 //folder Id: the id of the folder to search for
	 //string filename: the name of the file to search for
	 public String getfilesList (String folderId, string fileName, boolean onlyFolder){
     	if (folderId == ''){
     		//folderId = gSettings.Docomotion_Folder_Id__c;
			folderId = getFolderId(gSettings.Path__c);
     	}
		string q = 'not trashed';
		if (folderId != null)
			q += ' and \''+folderId+'\' in parents';
		if (fileName != null)
			q += ' and name=\''+fileName+'\'';
		if (onlyFolder)
			q += ' and mimeType=\'application/vnd.google-apps.folder\'';
     	String url = GOOGLE_DRIVE_END_POINT+'?q='+EncodingUtil.urlEncode(q,'UTF-8')+'&fields=files(id,parents,name,capabilities(canEdit),mimeType,modifiedTime,createdTime)';
		Map <String,String> headers = new Map<String,String>();
		headers.put('Content-Type','application/JSON');
        headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
        try{
        	HttpResponse resp = googleHttpCall(url,null,headers,'GET');
			System.debug('resp: '+resp);
        	if(resp != null){
				Map<String,Object> resultMap = new Map<String,Object>();
			 	resultMap.put('response', resp.getBody());
			 	resultMap.put('currentFolderId',folderId);
			 	resultMap.put('currentPath',gSettings.Path__c);
			 	return JSON.serialize(resultMap);
			}
        }catch(Exception e){
        	System.debug('Exception at GoogleCall.getfilesList. Msg : '+ e.getMessage());
        	return null;
       	}
       	return null;
     }

	  public String getfilesList (String folderId, string fileName){
		return getfilesList(folderId, fileName, false);
	  }

	 public String getfilesList (String folderId){
		return getfilesList(folderId, null);
	 }

     public String moveBackByFolderId (String folderId){
     	String parentId = getParentFolderIdByFolderId(folderId);
     	system.debug('**' + parentId);
     	if (parentId != null){
     		return getfilesList(parentId);
     	}
     	//return null - Means this is the root folder.
     	return null;
     }

     public String getParentFolderIdByFolderId (String folderId){
     	String url = GOOGLE_DRIVE_END_POINT+'/'+folderId+'?fields=parents';
     	Map <String,String> headers = new Map<String,String>();
     	headers.put('Content-Type','application/JSON');
        headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
         try{
	      	HttpResponse resp = googleHttpCall(url,null,headers,'GET');
	        if(resp != null){
				Map<String,List<String>> bodyMap = (Map<String,List<String>>)JSON.deserialize(resp.getBody(),Map<String,List<String>>.Class);
			    List<String> parents = bodyMap.get('parents');
			    if (parents.size() >0){
			        return parents[0];
			    }
			}
    	}catch(Exception e){
    		System.debug('Exception at GoogleCall.getParentFolderIdByFolderId. Msg : '+ e.getMessage());
        	return null;
    	}
    	return null;
     }
	 
	 public GoogleUtils.GoogleFile updateExistingFile(String fileId, String extension, Blob bodyB){
		return googleUploadFile(fileId, null, extension, null, bodyB);
	 }

	 public GoogleUtils.GoogleFile createNewFile(String fileName, String extension, String parentId, Blob bodyB){
		if (parentId == null || parentId=='')
			parentId = gSettings.Docomotion_Folder_Id__c;
		return googleUploadFile(null, fileName, extension, parentId, bodyB);
	 }

	 public GoogleUtils.GoogleFile googleUploadFile(String fileId, String fileName, String extension, String parentId, blob bodyB){
		GoogleUtils.GoogleFile ret = null;
		string url = GOOGLE_DRIVE_UPLOAD_END_POINT;
		if (fileId != null)
			url += '/'+fileId;
		url += '?uploadType=multipart';
		url += '&fields='+EncodingUtil.urlEncode('kind,id,name,mimeType,webViewLink','UTF-8');
		Map<String,String> headers = new Map<String,String>();
		headers.put('Content-Type','multipart/related; boundary="foo_bar_baz"');
		headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
		if (fileId != null)
			headers.put('X-HTTP-Method-Override','PATCH');
        string body='';
        body+='--foo_bar_baz\r\n';
        body+='Content-Type: application/json; charset=UTF-8\r\n\r\n';
		Map<String,Object> m = new map<String,Object>();
		if(fileName != null){
			m.put('name',fileName);
			m.put('parents', new List<String>{parentId});
		}
		body += JSON.serialize(m)+'\r\n';
        body+='--foo_bar_baz\r\n';
		if (extension=='PDF')
			body+='Content-Type: application/pdf\r\n';
		else if(extension=='DOCX')
			body+='Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document\r\n';
		else//HTML
			body+='Content-Type: application/html\r\n';
        body+='Content-Transfer-Encoding: base64\r\n\r\n';
        body+=EncodingUtil.base64Encode(bodyB)+'\r\n';
        body+='--foo_bar_baz\r\n';
        try{
            HttpResponse res = googleHttpCall(url, body, headers);
			if(res  != null){
				ret = (GoogleUtils.GoogleFile)JSON.deserialize(res.getBody(),GoogleUtils.GoogleFile.class);
			}
        }
        catch(Exception e){
        	system.debug('***EXCEPTION: '+e.getMessage());
        }
        if(settingsChanged)
        	saveSettings();
		return ret;
	}

	public String getFolderId(String path){
		String rootId = getRootId();
		if (path=='')
			return rootId;
		return getFolderId(rootId,path);
	}

	public String getExistingFolderId(String folderId, String folderName){
		string jSonStr = getfilesList(folderId, folderName, true);
		system.debug('***jsonSTR: '+jSonStr);
		if(jsonStr != null){
			Map<String, Object> rMap = (Map<String, Object>)JSON.deserializeUntyped(jSonStr);
			String responseStr = (String)rMap.get('response');
			Map<String,List<GoogleUtils.GoogleFile>> m = (Map<String,List<GoogleUtils.GoogleFile>>) JSON.deserialize(responseStr,Map<String,List<GoogleUtils.GoogleFile>>.class);
			if (m.containsKey('files') && !m.get('files').isEmpty()){
				List<GoogleUtils.GoogleFile> files = m.get('files');
				return files[0].id;
			}
		}
		return null;
	}

	public void saveFilePath(String pathId, String googlePath){
		gSettings.Docomotion_Folder_Id__c = pathId;
		gSettings.Path__c = googlePath;
		System.debug('***Save File : '+gSettings);
	}


	/* PRIVATE METHODS */
	private string getFolderId(String folderId, String path){
		string folderToSearch = path.substringBefore('\\');
		string retFolderId = getExistingFolderId(folderId, folderToSearch);
		if (retFolderId == null)//create Folder
			retFolderId = googleCreateFolder(folderToSearch, folderId);
		if (path.countMatches('\\') == 0) //end of the iteraction
			return retFolderId;
		string path2 = path.substringAfter('\\');
		return getFolderId(retFolderId, path2);
	}

	private GoogleAPI_Settings__c getUsersSettings(){
		GoogleAPI_Settings__c ret = GoogleAPI_Settings__c.getInstance(UserInfo.getUserId());
		if (ret == null)
			ret =  new GoogleAPI_Settings__c();
		return ret;
	}

	private boolean revoke(){
		if (gSettings.refresh_Token__c == null)
			return true;
		string url = 'https://accounts.google.com/o/oauth2/revoke';
    	Map<String,String> params = new Map<String,String>();
        params.put('token',gSettings.refresh_Token__c);
        string body='';
		for(String key:params.keySet()){
			body+=key+'='+params.remove(key);
			if(!params.isEmpty())
				body +='&';
		}
		string endpoint = url+'?'+body;
		Map<String,String> headers = new Map<String,String>();
		//headers.put('Content-Length','0');
        try{
			HttpResponse res = googleHttpCall(endPoint, null, headers,'GET');
            system.debug('***RESPONSE: '+res.getBody());
            return true;
        }
        catch(Exception e){
        	system.debug('***EXCEPTION: '+e.getMessage());
        }
        return false;
	}

	//To refresh the access token
	private boolean refreshGoogleAccessToken(){
		string url = 'https://www.googleapis.com/oauth2/v3/token';
    	Map<String,String> params = new Map<String,String>();
		params.put('client_id',GOOGLE_APIKEY);
		params.put('client_secret','xZN_-pYzRUhqB6E0keE26gEu');
        params.put('refresh_token',gSettings.refresh_Token__c);
		params.put('grant_type','refresh_token');
        string body='';
		for(String key:params.keySet()){
			body+=key+'='+params.remove(key);
			if(!params.isEmpty())
				body +='&';
		}
		string endpoint = url+'?'+body;
		Map<String,String> headers = new Map<String,String>();
		headers.put('Content-Length','0');
        try{
			HttpResponse res = googleHttpCall(endPoint, null, headers);
            system.debug('***RESPONSE: '+res.getBody());
            if(res != null){
				Map<String,Object> resMap = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
				if(resMap.containsKey('access_token')){
            		gSettings.access_token__c = (String)resMap.get('access_token');
            		settingsChanged=true;
            		return true;
				}
			}
        }
        catch(Exception e){
        	system.debug('***EXCEPTION: '+e.getMessage());
        }
        return false;
	}

	//the base method for requests
	private HttpResponse googleHttpCall(string endPoint, string body, Map<String,String> headers, String methodType){
		HttpRequest req = new HttpRequest();
		Http h = new Http();
        req.setEndpoint(endPoint);
        req.setMethod(methodType);
		for (string head : headers.keySet())
			req.setHeader(head, headers.get(head));
		system.debug('header: '+headers);
		if (body != null)
			req.setBody(body);
        req.setTimeout(60000);
		system.debug('***REQ: '+req);
		HttpResponse res = h.send(req);
		if (res.getStatusCode() == 401){
        	if (!IsOAuthFailedbefore){
	        	IsOAuthFailedbefore = true;
	        	refreshGoogleAccessToken();
				if(headers.containsKey('Authorization'))
					headers.put('Authorization','Bearer '+gSettings.access_token__c);
	        	return googleHttpCall(endPoint, body, headers, methodType);
        	}else{
        		deleteGoogleAccess();
				googleError(res);
				return null;
        	}
        }
		else if(res.getStatusCode() != 200){
			googleError(res);
			return null;
		}
		else{
			system.debug('***RESPONSE: '+res.getBody());
			return res;
		}
	}


	private HttpResponse googleHttpCall(string endPoint, string body, Map<String,String> headers){
		return googleHttpCall(endPoint, body, headers, 'POST');
	}

	//To check if the google is on organization or on user
	private boolean googleOnUser(){
		config__c config = Config__c.getInstance('Docomotion Default Settings');
		return (config != null && config.Google_Drive_Mode__c == 'User');
	}
	
	

	private String getRootId(){
     	String url = GOOGLE_DRIVE_END_POINT+'/root?fields=id';
		Map <String,String> headers = new Map<String,String>();
		headers.put('Content-Type','application/JSON');
        headers.put('Authorization','Bearer '+gSettings.Access_Token__c);
		try{
        	HttpResponse resp = googleHttpCall(url,null,headers,'GET');
        	if (resp != null){
        		Map<String,Object> resultMap = (Map<String,Object>)JSON.deserializeUntyped(resp.getBody());
			 	string rootId = (String)resultMap.get('id');
			 	return rootId;
		    }
			else{
				return null;
			}
        }catch(Exception e){
        	System.debug('Exception at GoogleCall.getfilesList. Msg : '+ e.getMessage());
        	return null;
       	}
	}

     private void deleteGoogleAccess(){
		gSettings.Refresh_Token__c = null;
		gSettings.Access_Token__c = null;
     }

	 private void googleError(HttpResponse res){
		Utils.log('GOOGLE DRIVE ERROR ',generateGoogleDriveErrorCode(res));
		ExceptionWS eWS = new ExceptionWS(generateGoogleDriveErrorCode(res));
		eWs.setSessionId(UserInfo.getSessionId());
		eWS.send();
	 }

     private String generateGoogleDriveErrorCode(HttpResponse res){
		return 'Http: '+res.getStatusCode()+' , message: '+res.getBody();
	 }

}