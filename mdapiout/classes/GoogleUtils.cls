global with sharing class GoogleUtils {


	public class GoogleFile{
		public String name;
		public String mimeType;
		public String id;
		public string webViewLink;
		public List<String> parents;
		public Map<String,Boolean> capabilities;
	}

	
}