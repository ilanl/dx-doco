global with sharing class ProcessBuilder_Extension_Doc {
	@InvocableMethod
	global static void processBuilderDoc(list<PbdRequest> requests){//Id formId,Id recoredId,Mode aMode, Format format
    	for(PbdRequest request: requests){
    		System.enqueueJob(new ProcessBuilder_Extension_Queueable(request.formId, request.sobjectId,null, request.Mode,request.format,false,null,null));
	    	//RenderAPI.renderCreateFile( request.formId, request.sobjectId,  RenderAPI.getMode(request.Mode),  RenderAPI.getFormat(request.format));    
    	}
	}
   	global with sharing class PbdRequest {
 	@InvocableVariable(required=true label='Id (Integer) of the Form')
    global Integer formId;

    @InvocableVariable(required=true label='Insert the record reference, for example -> Contant.Id')
    global ID sobjectId;
    
    @InvocableVariable(required=true LABEL='Choose Mode:{DOCUMENT,ATTACHMENT,CHATTER}')
    global string mode;
    
    @InvocableVariable(required=true LABEL='Choose Format :{PDF,HTML,DOCX,HTML4S,TXT,BMP,JPEG,PNG,GIF,TIF}')
    global string format;
    }   	        
}