global with sharing class ProcessBuilder_Extension_Queueable implements System.Queueable, Database.AllowsCallouts{
    global Integer formId;
    global ID sobjectId;
    global ID whoId;
    global string mode;
    global string format;
    global Boolean isActivatedVersion;
    global string fileName;
    global id TemplateId;
    global ProcessBuilder_Extension_Queueable(Integer formId, ID sobjectId,ID whoId, string mode,string format,Boolean isActivatedVersion,string fileName,id TemplateId){
    	this.formId = formId;
    	this.sobjectId = sobjectId;
    	this.whoId = whoId;
    	this.mode = mode;
    	this.format = format;
    	this.isActivatedVersion = isActivatedVersion;
    	this.fileName = fileName;
    	this.TemplateId = TemplateId;
    }
	public void execute(QueueableContext context){
		system.debug('***formId: '+formId);
		system.debug('***sobjectId: '+sobjectId);
		system.debug('***mode: '+mode);
		system.debug('***whoId: '+whoId);
		system.debug('***fileName: '+fileName);
		system.debug('***isActivatedVersion: '+isActivatedVersion);
		system.debug('***TemplateId: '+TemplateId);
		system.debug('***RenderAPI.getMode(mode) =: '+RenderAPI.getMode(mode) );
		if(RenderAPI.getMode(mode) == RenderAPI.Mode.ATTACHMENT || RenderAPI.getMode(mode) == RenderAPI.Mode.DOCUMENT || RenderAPI.getMode(mode) == RenderAPI.Mode.CHATTER){
			system.debug('***we here');
			RenderAPI.renderCreateFile( formId, sobjectId,  RenderAPI.getMode(mode),  RenderAPI.getFormat(format));    
		}
		else if(RenderAPI.getMode(mode) == RenderAPI.Mode.LINK){
			RenderAPI.createLink(formId,sobjectId,whoId,false,true); 
		}
		else if(RenderAPI.getMode(mode) == RenderAPI.Mode.EMAIL){
			RenderAPI.renderCreateFile(formId, sobjectId,RenderAPI.getMode(mode),RenderAPI.getFormat(format), whoId, fileName, isActivatedVersion,TemplateId);
		}
		else if(RenderAPI.getMode(mode) == RenderAPI.Mode.EMAILBODY){
			RenderAPI.renderCreateFile(formId, sobjectId,RenderAPI.getMode(mode),RenderAPI.getFormat(format), whoId, fileName, isActivatedVersion,null);
		}
	}
	
	    
}