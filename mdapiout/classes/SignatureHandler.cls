public class SignatureHandler {

	//To prepopulate data on insert
	public static void prepopulateData(List<Signature__c> Signatures){
		Config__c conf = Config__c.getInstance('Docomotion Default Settings');
		Double expirationTime = (conf != null && conf.Signature_Expiration_Time__c !=null) ? conf.Signature_Expiration_Time__c:168;
		Integer hour = expirationTime.intValue();
		Integer minutes = (60 * (expirationTime - hour)).intValue();
		for (Signature__c signature : Signatures){
			signature.expiration_date__c = Datetime.now().addHours(hour).addMinutes(minutes);
			signature.base_Url__c = conf.signature_Url__c;
			prepopulateLookup(signature);
		}
	}

	public static void populateEmailFields(List<Signature__c> signatures){
		Map<Id, Contact> contactsMap = new Map<Id,Contact>();
		Map<Id, Lead> leadsMap = new Map<Id,Lead>();
		for(Signature__c signature : signatures){
			if(signature.signer__c != null)
				contactsMap.put(signature.signer__c, null);
			else if(signature.Signer_lead__c != null)
				leadsMap.put(signature.signer_Lead__c, null);
		}
		if(!contactsMap.isEmpty())
			contactsMap = new Map<Id,Contact>([Select Id, Email From Contact Where Id in : contactsMap.keySet()]);
		if(!leadsMap.isEmpty())
			leadsMap = new Map<Id, Lead>([Select Id, Email From Lead Where Id in :leadsMap.keySet()]);
		for(Signature__c signature : signatures){
			if(signature.signer__c != null){
				Signature.email_address__c = contactsMap.get(signature.signer__c).email;
				if(signature.email_address__c == null)
					signature.email_address__c.addError('The Contact doesn\'t have any Email Address in its email field');
			}
			else if(signature.Signer_lead__c != null){
				Signature.email_address__c = leadsMap.get(signature.signer_Lead__c).email;
				if(signature.email_address__c == null)
					signature.email_address__c.addError('The Lead doesn\'t have any Email Address in its email field');
			}
		}
	}

	//to send signature via WS after insert
	public static void sendSignatures(List<Signature__c> Signatures){
		Set<Id> signaturesIds = new Set<Id>();
		for (Signature__c signature : Signatures){
			signaturesIds.add(signature.Id);
		}
		Config__c conf = Config__c.getInstance('Docomotion Default Settings');
		Double expirationTime = (conf.Signature_Expiration_Time__c !=null) ? conf.Signature_Expiration_Time__c:168;
		SignaturesRestCall.SendSignatures(signaturesIds, expirationTime);
	}


	//to add chatter when activated
	public static void addChatter(List<Signature__c> Signatures, Map<Id, Signature__c> oldMap){
		List<FeedItem> fiList = new List<FeedItem>();
		for(Signature__c signature : Signatures){
			if(signature.chatter__c && signature.created__c && !oldMap.get(signature.Id).created__c){
				FeedItem fi = new FeedItem();
				//fi.ContentData = Blob.valueOf('New Document for you');
				fi.ParentId = signature.Signer__c;
				fi.linkUrl = signature.Client_URL__c;
				fi.Body = 'New Document for you';
				//fi.ContentFileName = fileName + '.'+getFormatExtension();
				fi.title='View your assigned document!';
				fiList.add(fi);
			}
		}
		if(!fiList.isEmpty())
			insert fiList;
	}

	private static void prepopulateLookup(Signature__c signature){
		Set<String> lookupToExclude = new Set<String>{'Signer__c', 'Form__c'};
		SobjectType recordSType = ((Id)signature.Record_ID__c).getSobjectType();
		Map<String, Schema.SObjectField> fields = Schema.SObjectType.Signature__c.fields.getMap();
		for (String fieldName : fields.keySet()){
			Schema.DescribeFieldResult field = fields.get(fieldName).getDescribe();
			if (field.getType() == Schema.DisplayType.REFERENCE && field.getReferenceTo().size() == 1 && field.getReferenceTo()[0] == recordSType && !lookupToExclude.contains(field.getName())){
				signature.put(field.getSObjectField(),signature.Record_ID__c);
				break;
			}
		}
	}
}