/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TEST_CTRL04_DataSchema {

    static testMethod void DataSchemaFlow() {
        //List<Utils.Option> allObjectsLst = CTRL04_DataSchema.getAllObjectsList();
        Dataschema__c d = new Dataschema__c(name='test', Main_object__c='Account');
        insert d; 
        Id dsId = d.id;
        string allCaseFields = CTRL04_DataSchema.getAllFields(d.main_object__c);
        string allChildrenOfCase = CTRL04_DataSchema.getObjectChildren(dsId);
        
        CTRL04_DataSchema.FieldSaveAction fsa1 = new CTRL04_DataSchema.FieldSaveAction();
        fsa1.action='select';
        fsa1.path='Name';
        fsa1.sobjectname=null;
        
        CTRL04_DataSchema.FieldSaveAction fsa2 = new CTRL04_DataSchema.FieldSaveAction();
        fsa2.action='edit';
        fsa2.path='Type';
        fsa2.sobjectname='Case';
        
        CTRL04_DataSchema.FieldSaveAction fsa3 = new CTRL04_DataSchema.FieldSaveAction();
        fsa3.action='select';
        fsa3.path='Parent.Name';
        fsa3.sobjectname=null;
        
        CTRL04_DataSchema.FieldSaveAction fsa4 = new CTRL04_DataSchema.FieldSaveAction();
        fsa4.action='select';
        fsa4.path='Opportunities.Name';
        fsa4.childrenPath='Opportunities';
        fsa4.sobjectname='Opportunity';
        
        CTRL04_DataSchema.FieldSaveAction fsa5 = new CTRL04_DataSchema.FieldSaveAction();
        fsa5.action='select';
        fsa5.path='Opportunities.OpportunityLineItems.Product.Name';
        fsa5.childrenPath='Opportunities.OpportunityLineItems';
        fsa5.sobjectname='OpportunityLineItem';
        fsa5.onChild=true;
        
        CTRL04_DataSchema.FieldSaveAction fsa6 = new CTRL04_DataSchema.FieldSaveAction();
        fsa6.action='edit';
        fsa6.path='Industry';
        fsa6.sobjectname='Account';
        
        List<CTRL04_DataSchema.FieldSaveAction> fsaLst = new List<CTRL04_DataSchema.FieldSaveAction>{fsa1,fsa2,fsa3,fsa4,fsa5,fsa6};
        CTRL04_DataSchema.saveFieldsDelta(dsId, fsaLst);
        
        fsa1.action='none';
        CTRL04_DataSchema.saveFieldsDelta(dsId, new List<CTRL04_DataSchema.FieldSaveAction>{fsa1});
        
        fsa1.action='select';
        CTRL04_DataSchema.saveFieldsDelta(dsId, new List<CTRL04_DataSchema.FieldSaveAction>{fsa1});
        CTRL04_DataSchema.generateXML(dsid);
        
        DataSchema ds = new DataSchema(dsId);
        ds.getLabelsMap();
        ds.getFirstRelation();
        
        List<Utils.Field> fLst = (List<Utils.Field>)JSON.deserialize(allCaseFields, List<Utils.Field>.Class);
        
        PageReference pageRef = Page.VF03_DefineDataSchema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',dsid);
        CTRL03_DefineDataSchema ctrl3 = new CTRL03_DefineDataSchema();
        ctrl3.getLabelsMap();
        string jsonStr = ctrl3.JSONData;
        ctrl3.cancelChanges();
    }

	static testmethod void dataschemageDeleteFlow(){
		DataSchema__c d = new DataSchema__c(name='Test', main_object__c='Contact', fields__c = 'FirstName, test__c, ilan__r.test__c');
		insert d;
		PageReference pageRef = Page.VF03_DefineDataSchema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',d.id);
        CTRL03_DefineDataSchema ctrl3 = new CTRL03_DefineDataSchema();
        ctrl3.getLabelsMap();
		boolean b = ctrl3.isRtl;
		string s = ctrl3.dsLst;
		s = ctrl3.nsClass;
		s = ctrl3.nsField;
        string jsonStr = ctrl3.JSONData;
		ctrl3.removeFields();
	}
}