@isTest
public with sharing class TEST_Helper {
	
	public static Config__c insertSettings(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=false,Delete_chatter_files__c=false, server_URL__c='https://application.docomotioninc.com');
		insert myConf;
		return myConf;
	}
	
	public static Word_Template__c createWordTemplate(){
		Ctrl01_EditWordTemplate ctrl1 = new Ctrl01_EditWordTemplate(new ApexPages.standardController(new Word_Template__c()));
        ctrl1.fileName = 'WordTemplate.dot';
        ctrl1.body = Blob.valueOf('test');
        ctrl1.save();
		return ctrl1.WordTemplate;
	}
	
	public static Form__c newFormWithDs(){
		return TEST_Helper.newFormWithDs(false);
	}
		
	public static Form__c newFormWithDs(Boolean isPicture){
		Dataschema__c d = new Dataschema__c(name='test', Main_Object__c='Account');
		insert d;
		Word_Template__c wt = createWordTemplate();
		Form__c form = new Form__c(Default_File_Name__c='Name',name__c='test', Main_Object__c=d.Main_object__c, dataschema__c=d.id, word_template__c =wt.id);
        insert form; 
		
        Id dsId = d.id;
        string allCaseFields = CTRL04_DataSchema.getAllFields(d.main_object__c);
        string allChildrenOfCase = CTRL04_DataSchema.getObjectChildren(dsId);
        
        CTRL04_DataSchema.FieldSaveAction fsa1 = new CTRL04_DataSchema.FieldSaveAction();
        fsa1.action='select';
        fsa1.path='Name';
        fsa1.sobjectname=null;
		
		CTRL04_DataSchema.FieldSaveAction fsa2 = new CTRL04_DataSchema.FieldSaveAction();
        fsa2.action='edit';
        fsa2.path='Phone';
        fsa2.sobjectname='Account';
        
        CTRL04_DataSchema.FieldSaveAction fsa3 = new CTRL04_DataSchema.FieldSaveAction();
        fsa3.action='select';
        fsa3.path='Parent.Name';
        fsa3.sobjectname=null;
        
        CTRL04_DataSchema.FieldSaveAction fsa4 = new CTRL04_DataSchema.FieldSaveAction();
        fsa4.action='select';
        fsa4.path='Cases.CaseNumber';
        fsa4.onChild=false;
        fsa4.childrenPath='Cases';
        fsa4.sobjectname='Case';
        
        CTRL04_DataSchema.FieldSaveAction fsa5 = new CTRL04_DataSchema.FieldSaveAction();
        fsa5.action='select';
        fsa5.path='Parent.Description';
        
        CTRL04_DataSchema.FieldSaveAction fsa6 = new CTRL04_DataSchema.FieldSaveAction();
        fsa6.action='edit';
        fsa6.path='Parent.Description';
        fsa6.sobjectname='Account';
        
        CTRL04_DataSchema.FieldSaveAction fsa7 = new CTRL04_DataSchema.FieldSaveAction();
        fsa7.action='select';
        fsa7.path='Opportunities.Amount';
        fsa7.childrenPath = 'Opportunities';
        
        /**Adding children of children**/
		CTRL04_DataSchema.FieldSaveAction fsa8 = new CTRL04_DataSchema.FieldSaveAction();
        fsa8.action='select';
        fsa8.path='Opportunities.OpportunitiesLineItems.Amount';
        fsa8.childrenPath = 'Opportunities.OpportunitiesLineItems';
        fsa8.onChild=true;
        fsa8.sobjectName='OpportunityLineItem';
		
		CTRL04_DataSchema.FieldSaveAction fsa9 = new CTRL04_DataSchema.FieldSaveAction();
        fsa9.action='select';
        fsa9.path='Assets.Cases.CaseNumber';
        fsa9.childrenPath = 'Assets.Cases';
        fsa9.onChild=true;
        fsa9.sobjectName='Case';
        
        /**Adding children of lookups**/
		CTRL04_DataSchema.FieldSaveAction fsa10 = new CTRL04_DataSchema.FieldSaveAction();
        fsa10.action='select';
        fsa10.path='Parent.Cases.CaseNumber';
        fsa10.childrenPath = 'Parent.Cases';
        fsa10.onChild=false;
        fsa10.sobjectName='Case';
		
		CTRL04_DataSchema.FieldSaveAction fsa11 = new CTRL04_DataSchema.FieldSaveAction();
        fsa11.action='select';
        fsa11.path='Parent.Parent.Cases.CaseNumber';
        fsa11.childrenPath = 'Parent.Parent.Cases';
        fsa11.onChild=false;
        fsa11.sobjectName='Case';
        
		
		List<CTRL04_DataSchema.FieldSaveAction> fsaLst = new List<CTRL04_DataSchema.FieldSaveAction>{fsa1,fsa2,fsa3,fsa4,fsa6, fsa7, fsa8, fsa9, fsa10, fsa11};
        
        if(isPicture){
        	CTRL04_DataSchema.FieldSaveAction fsa12 = new CTRL04_DataSchema.FieldSaveAction();
	        fsa12.action='select';
	        fsa12.path='Attachments.Id';
	        fsa12.childrenPath='Attachments';
	        fsa12.onChild=false;
	        fsa12.sobjectname='Attachment';
	        fsaLst.add(fsa12);
        	
        }
        CTRL04_DataSchema.saveFieldsDelta(dsId, fsaLst);
        
        CTRL04_DataSchema.generateXML(form.id);
        form = [Select Id, Form_id__c, fields__c, edit_fields__c, main_object__c,MetaData_Document_Id__c, dataschema__c,Name__c, current_Version__c From Form__c Where id=:form.id];
        return form;
	}

	public static Form__c newFormOnContactWithDs(){
		Dataschema__c d = new Dataschema__c(name='test', Main_Object__c='Contact');
		insert d;
		Word_Template__c wt = createWordTemplate();
		Form__c form = new Form__c(Default_File_Name__c='Name',name__c='test', Main_Object__c=d.Main_object__c, dataschema__c=d.id, word_template__c =wt.id);
        insert form; 
		
        Id dsId = d.id;
        string allCaseFields = CTRL04_DataSchema.getAllFields(d.main_object__c);
        string allChildrenOfCase = CTRL04_DataSchema.getObjectChildren(dsId);
        
        CTRL04_DataSchema.FieldSaveAction fsa1 = new CTRL04_DataSchema.FieldSaveAction();
        fsa1.action='select';
        fsa1.path='FirstName';
        fsa1.sobjectname=null;
		
		CTRL04_DataSchema.FieldSaveAction fsa2 = new CTRL04_DataSchema.FieldSaveAction();
        fsa2.action='edit';
        fsa2.path='Phone';
        fsa2.sobjectname='Account';
		
		CTRL04_DataSchema.FieldSaveAction fsa11 = new CTRL04_DataSchema.FieldSaveAction();
        fsa11.action='select';
        fsa11.path='Parent.Parent.Cases.CaseNumber';
        fsa11.childrenPath = 'Parent.Parent.Cases';
        fsa11.onChild=false;
        fsa11.sobjectName='Case';
		
		List<CTRL04_DataSchema.FieldSaveAction> fsaLst = new List<CTRL04_DataSchema.FieldSaveAction>{fsa1,fsa2};
        CTRL04_DataSchema.saveFieldsDelta(dsId, fsaLst);
        
        CTRL04_DataSchema.generateXML(form.id);
        form = [Select Id, Form_id__c, fields__c, edit_fields__c, main_object__c,MetaData_Document_Id__c, dataschema__c,Name__c, current_Version__c From Form__c Where id=:form.id];
        return form;
	}

	
	public static Form__c newSubFormWithDs(){
		string frt = Utils.getNameSpace()+'Form__c';
		RecordType rt = [select id,DeveloperName,sobjecttype from RecordType Where sobjectType=:frt and developerName='Subform'];
		Dataschema__c d = new Dataschema__c(name='test20', Main_Object__c='Account');
		insert d;
		Word_Template__c wt = createWordTemplate();
		Form__c form = new Form__c(name__c='test2', RecordTypeId=rt.id, main_object__c=d.main_object__c, word_template__c=wt.id, dataschema__c=d.id);
        insert form;
        Id dsId = d.id;
        
        CTRL04_DataSchema.FieldSaveAction fsa1 = new CTRL04_DataSchema.FieldSaveAction();
        fsa1.action='select';
        fsa1.path='Name';
        fsa1.sobjectname=null;
		
		CTRL04_DataSchema.FieldSaveAction fsa2 = new CTRL04_DataSchema.FieldSaveAction();
        fsa2.action='edit';
        fsa2.path='Fax';
        fsa2.sobjectname='Account';
        
        CTRL04_DataSchema.FieldSaveAction fsa3 = new CTRL04_DataSchema.FieldSaveAction();
        fsa3.action='select';
        fsa3.path='Parent.Phone';
        fsa3.sobjectname=null;
        
        CTRL04_DataSchema.FieldSaveAction fsa4 = new CTRL04_DataSchema.FieldSaveAction();
        fsa4.action='select';
        fsa4.path='Opportunities.Amount';
        fsa4.childrenpath='Opportunities';
        fsa4.sobjectname='Opportunity';
        
        CTRL04_DataSchema.FieldSaveAction fsa5 = new CTRL04_DataSchema.FieldSaveAction();
        fsa5.action='select';
        fsa5.path='Opportunities.OpportunityLineItems.ListPrice';
        fsa5.childrenpath='Opportunities.OpportunityLineItems';
        fsa5.sobjectname='OpportunityLineItem';
        fsa5.onChild = true;
        
        CTRL04_DataSchema.FieldSaveAction fsa6 = new CTRL04_DataSchema.FieldSaveAction();
        fsa6.action='select';
        fsa6.path='Parent.Cases.CaseNumber';
        fsa6.childrenpath='Parent.Cases';
        fsa6.sobjectname='Case';
        fsa6.onChild = false;
		
		List<CTRL04_DataSchema.FieldSaveAction> fsaLst = new List<CTRL04_DataSchema.FieldSaveAction>{fsa1,fsa2,fsa3,fsa4, fsa5, fsa6};
        CTRL04_DataSchema.saveFieldsDelta(dsId, fsaLst);
        CTRL04_DataSchema.generateXML(form.id);
        form = [Select Id, Form_id__c, fields__c, edit_fields__c, main_object__c,MetaData_Document_Id__c From Form__c Where id=:form.id];
        return form;
	}
	
	public static void addFeedItem(Form__c f){
		FeedItem fiD = new FeedItem();
        fiD.ContentData = Blob.valueOf('doc data');
        fiD.ParentId = f.id;
        fiD.Type = 'ContentPost';
    	fiD.ContentFileName = 'Document.ffx';
    	FeedItem fiM = new FeedItem();
        fiM.ContentData = Blob.valueOf('meta data');
        fiM.ParentId = f.id;
        fiM.Type = 'ContentPost';
    	fiM.ContentFileName = 'Metadata.xml';
    	FeedItem fiP = new FeedItem();
        fiP.ContentData = Blob.valueOf('preview data');
        fiP.ParentId = f.id;
        fiP.Type = 'ContentPost';
    	fiP.ContentFileName = 'Preview.pdf';
    	DML_CustomExtension.insertObj(new List<FeedItem>{fiD,fiM,fiP});
    	Id previewVersionId;
    	system.debug(LoggingLevel.ERROR,'***fi: '+[Select RelatedRecordId From FeedItem where id=:fiD.id or id=:fiM.id or id=:fiP.id]);
    	for(FeedItem fi:[Select id,RelatedRecordId From FeedItem where id=:fiD.id or id=:fiM.id or id=:fiP.id]){
    		if(fi.id==fiD.id)
    			f.Word_Document_Version_Id__c = fi.RelatedRecordId;
    		else if(fi.id==fiM.id)
    			f.Metadata_Version_Id__c = fi.RelatedRecordId;
    		else if(fi.id==fiP.id)
    			previewVersionId = fi.RelatedRecordId;
    	}
    	for(ContentVersion cv:[Select id,ContentDocumentId From ContentVersion where id=:f.Word_Document_Version_Id__c or id=:f.Metadata_Version_Id__c or id =:previewVersionId]){
    		if(cv.id==f.Word_Document_Version_Id__c){
	    		f.World_Document_FFX_ID__c = cv.ContentDocumentId;
	    		f.Current_Version__c = '1';
    		}
    		else if(cv.id==f.Metadata_Version_Id__c){
    			
	    		f.MetaData_Document_Id__c = cv.ContentDocumentId;
	    		f.Current_Metadata_Version__c = '1';
    		}
    		else if(cv.id==previewVersionId){
    			f.Preview_PDF_Document_ID__c = cv.ContentDocumentId;
    		}
    	}
    	update f;
	}
	
	public static void publish(Form__c form){
		form.status__c='Active';
		update form;
		form.status__c='Published';
		update form;
	}
	
	public static void republish(Form__c form){
		form.status__c='edit';
		update form;
		form.status__c='Active';
		update form;
		form.status__c='Published';
		update form;
	}

	public static Form__c createPublishedForm() {
		Form__c form = newFormWithDs();
        saveFromDesigner(form.Id);
        publish(form);

        form = [select Id, current_published_Version__c, Dataschema__c, Data_Schema_XML_ID__c,Word_Document_Version_Id__c,
        		Metadata_Version_Id__c, Preview_PDF_Document_ID__c, Current_Version__c
        		from Form__c where Id=:form.Id];

        return form;
	}
	
	public static Interactive_Fields_Collection__c createInteractiveCollection(){
		Interactive_Fields_Collection__c ic = new Interactive_Fields_Collection__c(Name='Test');
		insert ic;
		Interactive_Field__c ifi = new Interactive_Field__c();
		ifi.question__c ='test';
		ifi.type__c='text';
		ifi.Interactive_Fields_Collection__c = ic.id;
		insert ifi;
		return ic;
	}
	
	public static Form__c saveFromDesigner(Id formId){
		Form__c form = [select Id, name__c, form_id__c, World_Document_FFX_ID__c, current_version__c, MetaData_Document_Id__c, Preview_PDF_Document_ID__c from Form__c where Id =:formId];
		
		//word ffx
		if(form.World_Document_FFX_ID__c==null){
			Attachment att = new Attachment();
			att.parentId = form.id;
			att.Name='Document.ffx';
			att.Body = Blob.valueOf('blablabla');
			insert att;
			
			form.World_Document_FFX_ID__c = att.id;
			form.New_Dataschema__c = false;
			form.Word_Document_Version_Id__c = att.id;
		}
		
		//metadata
		if(form.MetaData_Document_Id__c==null){
			Attachment att = new Attachment();
			att.parentId = form.id;
			att.Name='Metadata.xml';
			att.body= Blob.valueOf('<FFHeader><FreeFormProperties><ID>'+form.form_id__c+'</ID><Name>'+form.Name__c+'</Name><Version>'+form.current_version__c+'</Version></FreeFormProperties></FFHeader>');
			insert att;
			
			form.MetaData_Document_Id__c = att.id;
			form.MetaData_Version_Id__c = att.id;
		}
		
		//PDF
		if(form.Preview_PDF_Document_ID__c==null){
			Attachment att = new Attachment();
			att.parentId = form.id;
			att.Name='preview.pdf';
			att.body = Blob.valueOf('56865ugyjhghjkyu9');
			insert att;
			
			form.Preview_PDF_Document_ID__c = att.id;
		}
		
		
		
		update form;
		return form;
	}
	
	public static Form__c saveFromDesignerOld(Id formId){
		Form__c form = [select Id, name__c, form_id__c, World_Document_FFX_ID__c, current_version__c, MetaData_Document_Id__c, Preview_PDF_Document_ID__c, Data_Schema_XML_ID__c from Form__c where Id =:formId];
		
		//word ffx
		if(form.World_Document_FFX_ID__c==null){
			FeedItem fi = new FeedItem();
			fi.parentId = form.id;
			fi.type='ContentPost';
			fi.ContentFileName='Document.ffx';
			fi.ContentData = Blob.valueOf('blablabla');
			insert fi;
			fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
			ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
			form.World_Document_FFX_ID__c = cv.ContentDocumentId;
			form.New_Dataschema__c = false;
			form.Word_Document_Version_Id__c = cv.id;
		}
		
		//metadata
		if(form.MetaData_Document_Id__c==null){
			FeedItem fi = new FeedItem();
			fi.parentId = form.id;
			fi.type='ContentPost';
			fi.ContentFileName='Metadata.xml';
			fi.ContentData = Blob.valueOf('<FFHeader><FreeFormProperties><ID>'+form.form_id__c+'</ID><Name>'+form.Name__c+'</Name><Version>'+form.current_version__c+'</Version></FreeFormProperties></FFHeader>');
			insert fi;
			fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
			ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
			form.MetaData_Document_Id__c = cv.ContentDocumentId;
			form.MetaData_Version_Id__c = cv.id;
		}
		
		//PDF
		if(form.Preview_PDF_Document_ID__c==null){
			FeedItem fi = new FeedItem();
			fi.parentId = form.id;
			fi.type='ContentPost';
			fi.ContentFileName='preview.pdf';
			fi.ContentData = Blob.valueOf('56865ugyjhghjkyu9');
			insert fi;
			fi = [select Id, RelatedRecordId From FeedItem Where id=:fi.id];
			ContentVersion cv = [select Id, ContentDocumentId From ContentVersion Where id=:fi.relatedRecordId];
			form.Preview_PDF_Document_ID__c = cv.ContentDocumentId;
		}
		
		
		
		update form;
		return form;
	}

	public static Account createAccount() {
		Account account = new Account(Name = 'test');
		insert account;
		return account;
	}
}