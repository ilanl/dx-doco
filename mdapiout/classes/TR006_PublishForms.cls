public with sharing class TR006_PublishForms {
    
    //to publish a form
    //Called by Trigger Form_After_Update
    public static void activate(List<Form__c> forms){
        List<FormData> fdLst = new List<FormData>();
        for(Form__c form:forms){
        	FormData fd = new FormData();
        	fd.FormID = (Integer)form.form_ID__c;
        	fd.FormName = form.Name__c;
			fd.SfVersionId = Integer.valueOf(form.current_version__c);
            fdLst.add(fd);
        }
        if(!fdLst.isEmpty())
            publish(JSON.serialize(fdLst), UserInfo.getSessionId(), isInteractive(forms)); 
    }

	private static Boolean isInteractive(List<form__c> forms){
		for (Form__c form :forms){
			if (form.Contains_Interactive_Collection__c || form.Contains_Interactive_Fields__c)
				return true;
		}
		return false;
	}

    @future(callout=true)
    private static void publish(string formsJson, String sessionId, Boolean isInteractive){
        Config__c myConf = Config__c.getInstance('Docomotion Default Settings');
        string epUrl = myConf.Server_URL__c+'/api/masspublish';
        PublishRequest publishRequest = new PublishRequest();
        publishRequest.Session_ID = sessionId;
        publishRequest.Org_Id = UserInfo.getOrganizationId();
        publishRequest.URL = URL.getSalesforceBaseUrl().toExternalForm() + '/services/Soap/u/35.0/';
        publishRequest.Forms = (List<FormData>)JSON.deserialize(formsJson, List<FormData>.class);
        publishRequest.User_Id = UserInfo.getUserId();
        publishRequest.IsInteractive = isInteractive;
        Http h = new Http();
        
        Blob headerValue = Blob.valueOf('SfFfDev:!AhbAjhv10#');
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(epUrl);
        req.setMethod('POST');
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setBody(JSON.serialize(publishRequest));
        req.setTimeout(120000);
        Utils.log('req',req);
        Utils.log('BODY',req.getBody());
        if (Test.isRunningTest())
            return;
        // Send the request, and return a response
		try{
	        HttpResponse res = h.send(req);
	        Utils.log('REPONSE',res.getBody());
			PublishResponse pResponse = (PublishResponse)JSON.deserialize(res.getBody(), PublishResponse.class);
			if (!pResponse.isSuccess){
				//Update forms with error
				List<Form__c> forms = new List<Form__c>();
				for (FormData fd:(List<FormData>)(JSON.deserialize(formsJson, List<FormData>.class))){
					forms.add(new Form__c(
						Form_Id__c=fd.FormID,
						Status__c='Failed',
						Publish_Error__c=pResponse.ErrorMessage,
						Internal_Error_Code__c = pResponse.InternalErrorCode
					));
                     
				}
				Database.Upsert(forms,Form__c.Form_Id__c,true);
			}
	    }catch (Exception e){}
    }
    
    public class PublishRequest
    {
        public string Session_Id
        { get; set; }
 
        public List<FormData> Forms
        { get; set; }
 
        public string Org_Id
        { get; set; }
 
        public string URL
        { get; set; }
        
        public string User_Id
        { get; set; }

		public boolean IsInteractive
        { get; set; }

        
    }

	public class PublishResponse{
		public string OrgID { get; set; }
		public boolean IsSuccess { get; set; }
		public string ErrorMessage { get; set; }
        public string InternalErrorCode {get; Set;}
		
	}
    
    public class FormData
    {
        public Integer FormID { get; set; }
        public string FormName { get; set; }
		public Integer SfVersionId {get; Set;}
    }
    
    //To save all the attachment (from the form preview)
    public static List<Attachment> formPreviewAtt = new List<Attachment>();
    
	//Need to rewrite this with the new publish flow
    public static void createVersionOnPublish(List<Form__c> forms){
		for (Form__c form : forms)
		{
			form.current_published_version__c = form.current_activated_version__c;
			form.current_version__c = ''+(Integer.valueOf(form.Current_Version__c)+1);
			//Update published_by__c and Published_Date__c
			form.Published_By__c = UserInfo.getUserId();
			form.Published_Date__c = Datetime.now();
			form.active__c=true;
		}
    }
    
	//To create Version on activation
	public static void createVersionOnActive(List<Form__c> forms){
		Map<Id,Dataschema__c> dsMap = new Map<Id, Dataschema__c>();
    	for(Form__c form: forms){
    		//activate the form:
    		if(!dsMap.containsKey(form.dataschema__c))
    			dsMap.put(form.dataschema__c, null);
    	}
    	dsMap = new Map<Id,Dataschema__c>([Select fields__c, edit_Fields__c, (Select Name, On__c, Path__c, Children_Name__c, lookup_field__c, sobject__c, fields__c From SOQL_Extensions__r) From Dataschema__c Where id in :dsMap.keySet()]);
        if(!formPreviewAtt.isEmpty())
            formPreviewAtt.clear();
        Map<Id, Map<string,Attachment>> attMap = createFormAttachmentsOnPublish(forms);
        createVersionOnActivate(forms, dsMap, attMap);
	}

	//To Immediatly publish a form after acrivating it (used in the designer)
	public static void immediatePublish(List<Form__c> forms){
		for(Form__c form : forms){
			form.Status__c = 'Published';
			form.Immediate_Publish__c = false;
		}
	}

    //To create the version object and update the current version in the form.
    //Called by trigger Form_Before_Update
    private static void createVersionOnActivate(List<Form__c> forms, Map<Id,Dataschema__c> dsMap, Map<Id, Map<string,Attachment>> attMap){
        Map<Id,Version__c> versions = new Map<Id,Version__c>();
        Set<Id> formIds = new Set<Id>();
        for(Form__c form:forms)
            formIds.add(form.id);
        List<Form__c> formsLst = [Select id, current_Version__c, Preview_PDF_Document_ID__c, Current_Activated_Version__c, Current_Metadata_Version__c, Current_Activated_Version__r.version__c From Form__c Where id in :formIds];
        for(Form__c form:formsLst){
            Utils.log('Current_Activated_Version__c',form.Current_Activated_Version__c);
            if(form.Current_Activated_Version__c != null && form.Current_Activated_Version__r.version__c == form.current_Version__c){
                versions.put(form.id, form.Current_Activated_Version__r);
            }
        }
        Utils.log('versions',versions);
        //to create the version (if there isn't any version with the same document version)
        for(Form__c form:forms){
            if(!versions.containsKey(form.id)){
            	if(form.main_object__c != null){
	            	Dataschema__c ds = dsMap.get(form.dataschema__c);
	                Version__c version = new Version__c(
	                    Name=form.form_id__c+' V'+form.current_Version__c,
	                    Form__c = form.id,
	                    Version__c = form.Current_Version__c,
	                    Fields__c = ds.fields__c,
	                    Edit_Fields__c = ds.Edit_Fields__c,
	                    children_relationships__c = buildChildrenRelationShips(ds.soql_extensions__r),
	                    contains_interactive_collection__c = form.Contains_Interactive_Collection__c,
	                    contains_interactive_fields__c = form.Contains_Interactive_Fields__c,
						total_version__c = form.total_Version__c
	                );
	                versions.put(form.id, version);
            	}
            }
            else{
				Dataschema__c ds = dsMap.get(form.dataschema__c);
                Version__c version = versions.get(form.id);
				version.Fields__c = ds.fields__c;
	            version.Edit_Fields__c = ds.Edit_Fields__c;
	            version.children_relationships__c = buildChildrenRelationShips(ds.soql_extensions__r);
	            version.contains_interactive_collection__c = form.Contains_Interactive_Collection__c;
	            version.contains_interactive_fields__c = form.Contains_Interactive_Fields__c;
				version.total_version__c = form.total_Version__c;
				Utils.log('current activated version',version);
            }
        }
        
        if(!versions.isEmpty()){
            //Insert the version
            DML_CustomExtension.upsertObj(versions.values());
            Set<Id> versionIds = new Set<Id>();
            for(Version__c v:versions.values()){
            	versionIds.add(v.id);
            }
            Map<Id,Version__c> versionsMap = new Map<Id,Version__c>([Select LastModifiedDate, LastModifiedById, Id, form__c, Preview_PDF_Id__c, XML_ID__c, Metadata_Id__c, Document_Id__c From Version__c Where id in :versionIds]);
            List<Attachment> attToInsert = new List<Attachment>();
            for(Id versionId: versionsMap.keySet()){//Check if this loop is relevent
            	Version__c version = versionsMap.get(versionId);
				if(attMap.containsKey(version.form__c)){
					for(string key: attMap.get(version.form__c).keySet()){
						Attachment att = attMap.get(version.form__c).remove(key);
						Attachment att2 = new Attachment(ContentType=att.contentType, Name=att.Name, Body=att.Body);
						att2.parentId = versionId;
						attToInsert.add(att2);
						if(version.Preview_PDF_Id__c != null && att2.Name ==  'Preview.pdf')
							att2.id = version.Preview_PDF_Id__c;
						else if(version.XML_ID__c != null && att2.Name == 'Dataschema.xml')
							att2.id = version.XML_ID__c;
						else if(version.Metadata_Id__c != null && att2.Name == 'Metadata.xml')
							att2.id = version.Metadata_Id__c;
						else if(version.Document_ID__c != null && att2.Name == 'Document.ffx')
							att2.id = version.Document_ID__c;	  		  		    
					}
				}
            }
            if(!attToInsert.isEmpty())
                DML_CustomExtension.upsertObj(attToInsert);
            for(Attachment att:attToInsert){
            	Version__c version = versionsMap.get(att.parentId);
            	if(att.Name=='Preview.pdf')
            		version.Preview_PDF_Id__c = att.id;
            	else if(att.Name=='Dataschema.xml')
            		version.XML_ID__c = att.id;
            	else if(att.Name=='Metadata.xml')
            		version.Metadata_Id__c = att.id;
            	else if(att.Name=='Document.ffx')
            		version.Document_Id__c = att.id;			
            }
            DML_CustomExtension.updateObj(versionsMap.values()); 
            //update the version lookup in the form
            for(Form__c form:forms){
				form.total_version__c = form.total_version__c + 1;
                form.Current_Activated_Version__c = versions.get(form.id).id;
            }
            
        }
    }
    
    private static Map<Id, Map<string,Attachment>> createFormAttachmentsOnPublish(List<Form__c> forms){
        Map<Id, Map<string,Attachment>> attMap = new Map<Id,Map<string,Attachment>>();//Map form Id => Attachments;
        Map<Id, Form__c> formMap = new Map<Id, Form__c>();
        Set<Id> attIds = new Set<Id>();
        for(Form__c form:forms){
            if(form.Preview_PDF_Document_ID__c != null){
                attIds.add(form.Preview_PDF_Document_ID__c);
            }
            attIds.add(form.metaData_Document_Id__c);
            attIds.add(form.World_Document_FFX_Id__c);
            attIds.add(form.Document_content_id__c);
            formMap.put(form.id, form);
        }
        
        //For the existing Map
        for(Attachment att:[Select id, body, parentId, Name, contentType From Attachment Where parentId in: formMap.keySet() and id in:attIds]){
            if(formMap.containsKey(att.parentId)){
            	Form__c form = formMap.get(att.parentId);
            	if(!attMap.containsKey(form.id))
        			attMap.put(form.Id, new Map<string,Attachment>());
        		attMap.get(form.Id).put(att.Name, att);
            }
        }
        return attMap;    
    }
    
    private static Map<Id,Attachment> getVersionAttachments(){
        Map<Id,Attachment> ret = new Map<Id,Attachment>();
        for(Attachment att:formPreviewAtt){
            ret.put(att.parentId,att.clone());
        }
        return ret;
    }
    
    private static string buildChildrenRelationShips(List<SOQL_Extension__c> soqlLst){
    	string ret;
		if(!soqlLst.isEmpty()){
			List<String> tab = new LIst<String>();
			for(SOQL_Extension__c s:soqlLst){
				Utils.Children children = new Utils.Children(s);
				tab.add(children.toFlatString());
			}
			ret = String.join(tab,'; ');
		}
    	return ret;
    }
    

}