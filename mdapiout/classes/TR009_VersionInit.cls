public with sharing class TR009_VersionInit {
	public static void addPsLookupToVersion(list<Version__c> triggerNew ,map<Id,Version__c> triggerOldmap){
		if(Trigger.isUpdate){
			set<id>vParentIdToDeletePsChild = new set<id>();
			for(Version__c v: triggerNew){
				if(v.Total_Version__c !=  triggerOldmap.get(v.id).Total_Version__c)
					vParentIdToDeletePsChild.add(v.id);	
			}
			list<Pictures_Setting__c> psToDelete = [ select id from Pictures_Setting__c where Version__c in:vParentIdToDeletePsChild];
			if(psToDelete.size()>0){
				delete psToDelete;
			}
		}
		list<id>formIds = new list<id>();
		for(Version__c v: triggerNew){
			formIds.add(v.Form__c);
		}
		if(formIds.size()==0 )return;
		list <Pictures_Setting__c> psList = [select Form__c,id,Extensions__c,Max_Pictures_Nb__c,Maximum_Size__c,Path__c from Pictures_Setting__c where Form__c in:formIds];
		list <Pictures_Setting__c> clonePsList = psList.deepClone(false);
		map<id,list<Pictures_Setting__c>>clonePsMap = new map<id,list<Pictures_Setting__c>>();
		for(Pictures_Setting__c p: clonePsList){
			if(clonePsMap.containsKey(p.Form__c))
				clonePsMap.get(p.Form__c).add(p);	
			else clonePsMap.put(p.Form__c ,new list<Pictures_Setting__c>{p});	
		}
		for(Version__c v: triggerNew){
			if(clonePsMap.containsKey(v.Form__c)){
				for(Pictures_Setting__c p:clonePsMap.get(v.Form__c)){
					p.Version__c = v.id;
					p.Form__c = null;
				}
				
			}
		}
		if(clonePsMap.size()>0){
			list<Pictures_Setting__c>psl = new list<Pictures_Setting__c>();
			for(list<Pictures_Setting__c> lp: clonePsMap.values()){
				psl.addall(lp);	
			}
				
		insert psl;
		}
		set<id>	vsForUpdate = new set<id>();
		for(Version__c v: triggerNew){
			if(clonePsMap.containsKey(v.Form__c)){
				vsForUpdate.add(v.id);
			}
		}
		if(!(system.isBatch() || system.isFuture()))
			updateVersions(vsForUpdate);
	}
	@future
	public static  void updateVersions(set<id> ids ){
		list<Version__c> vs = new list<Version__c>();
		for(Version__c v:[select  id from Version__c where id in :ids]){
			v.HasPictureSetting__c = true;
			vs.add(v);
		}
		update vs;
	}
	//To add the lookup to the publish version of the subform
	public static void addPublishedVersionLookup(List<Form_Subform_Association__c> associations){
		Map<Id, Form__c> subformMap = new Map<Id,Form__c>();
		for(Form_Subform_Association__c association: associations)
			subformMap.put(association.subform__c,null);
		subformMap	= new Map<Id,Form__c>([Select Current_Published_Version__c From Form__c Where id in :subformMap.keySet()]);
		for(Form_Subform_Association__c association: associations){
			association.Subform_Version__c = subformMap.get(association.subform__c).Current_Published_Version__c;
			association.Subform_Version_soql__c = subformMap.get(association.subform__c).Current_Published_Version__c;
		}
	}

}