public with sharing class TR011_DeleteFormFiles {

	public static void deleteFiles(List<Form__c> formLst){
		List<ContentDocument> toDelete = new List<ContentDocument>();
		for(Form__c f: formLst){
			if(f.Preview_PDF_Document_ID__c != null)
				toDelete.add(createContentDocumentFromId(f.Preview_PDF_Document_ID__c));
			if(f.World_Document_FFX_ID__c != null)
				toDelete.add(createContentDocumentFromId(f.World_Document_FFX_ID__c));
			if(f.MetaData_Document_Id__c != null)
				toDelete.add(createContentDocumentFromId(f.MetaData_Document_Id__c));
			if(f.Document_Content_Id__c != null)
				toDelete.add(createContentDocumentFromId(f.Document_Content_Id__c));		
		}
		if(!toDelete.isEmpty()){
			try{
				DML_CustomExtension.deleteObj(toDelete);
			}
			catch(Exception e){}
		}
	}
	
	private static ContentDocument createContentDocumentFromId(Id cdId){
		ContentDocument cd = new ContentDocument(Id=cdId);
		return cd;
	}
	
	public static boolean inactive = false; 

}