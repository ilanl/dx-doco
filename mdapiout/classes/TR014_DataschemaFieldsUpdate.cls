public with sharing class TR014_DataschemaFieldsUpdate {

	private static Set<Id> alreadyDone = new Set<Id>();

	public static void needToOpenInDesigner(List<Dataschema__c> dLst, Map<Id,Dataschema__c> oldMap){
		List<Dataschema__c> dWithRemovedFIeldsLst = new List<Dataschema__c>();
		for(Dataschema__c d:dLst){
			Dataschema ds = new Dataschema(d);
			Dataschema dsOld = new Dataschema(oldMap.get(d.id));
			if(ds.fieldRemoved(dsOld)){
				dWithRemovedFIeldsLst.add(d);
			}
		}
		if(!dWithRemovedFIeldsLst.isEmpty()){
			Set<Id> dsIds = new Set<Id>();
			for(Dataschema__c d: dWithRemovedFIeldsLst){
				dsIds.add(d.Id);
			}
			List<Form__c> formLst = [Select Id, dataschema__c From Form__c Where dataschema__c in :dsIds and new_Dataschema__c = false];
			for(Form__c f:formLst){
				f.new_Dataschema__c = true;
				alreadyDone.add(f.dataschema__c);
			}
			DML_CustomExtension.updateObj(formLst);
		}
	}
	
	public static void needToOpenInDesigner(List<SOQL_Extension__c> sLst, Map<Id,SOQL_Extension__c> oldMap){
		Set<Id> dsIds = new Set<Id>();
		for(SOQL_Extension__c se:sLst){
			if(!alreadyDone.contains(se.dataschema__c)){
				//1. need to add the new fields in a children structure
				Utils.Children cNew = new Utils.Children(se);
				//2. and in the old one too
				Utils.Children cOld = new Utils.Children(oldMap.get(se.id));
				//checking the fields
				if(cNew.fieldRemoved(cOld))
					dsIds.add(se.dataschema__c);	
			}
		}
		if(!dsIds.isEmpty()){
			List<Form__c> formLst = [Select Id, dataschema__c From Form__c Where dataschema__c in :dsIds and new_Dataschema__c = false];
			for(Form__c f:formLst){
				f.new_Dataschema__c = true;
				alreadyDone.add(f.dataschema__c);
			}
			DML_CustomExtension.updateObj(formLst);
		}
	}
	
	public static void needToOpenInDesigner(List<SOQL_Extension__c> sLst){
		Set<Id> dsIds = new Set<Id>();
		for(SOQL_Extension__c se:sLst){
			if(!alreadyDone.contains(se.dataschema__c))
				dsIds.add(se.dataschema__c);
		}
		if(!dsIds.isEmpty()){
			List<Form__c> formLst = [Select Id, dataschema__c From Form__c Where dataschema__c in :dsIds and new_Dataschema__c = false];
			for(Form__c f:formLst){
				f.new_Dataschema__c = true;
				alreadyDone.add(f.dataschema__c);
			}
			DML_CustomExtension.updateObj(formLst);
		}
	}
}