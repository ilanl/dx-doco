@isTest
private class Test_CTRL13_ViewInteractiveForm {

    static testMethod void myUnitTest() {
        TEST_Helper.insertSettings();
        Account a = new Account(Name='test');
        insert a;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(TEST_Helper.createInteractiveCollection());
        CTRL13_ViewInteractiveForm ctrl = new CTRL13_ViewInteractiveForm(sc);
        
        ApexPages.currentPage().getParameters().put('changeIndex', '0');
        ApexPages.currentPage().getParameters().put('status', 'Edit');
        ctrl.changeView();
        
        ApexPages.currentPage().getParameters().put('changeIndex', '0');
        ApexPages.currentPage().getParameters().put('status', 'Cancel');
        ctrl.changeView();
        
        ctrl.interactiveFieldMap.get(0).Question__c = 'what?';
        ctrl.interactiveFieldMap.get(1).Question__c = 'whats up?';
        ctrl.addLine();
        ctrl.saveFields();
        
        ctrl.interactiveFieldMap.put(2,new Interactive_Field__c());
        ctrl.interactiveFieldMap.get(1).Question__c = 'what?';
        ctrl.interactiveFieldMap.get(1).Type__c = 'Picklist';
        ctrl.addLine();
        //ctrl.interactiveFieldMap.get(1).Type__c = 'Text';
        //ctrl.addLine();
        
        
        
        ApexPages.currentPage().getParameters().put('deleteIndex', '0');
        ctrl.deleteLine();
        
        //ctrl.interactiveFieldMap.get(1).Question__c = '';
        ctrl.interactiveFieldMap.get(1).Question__c = 'whats up?';
        ctrl.saveFields();
        
    }
}