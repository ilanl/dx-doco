@isTest
private class Test_CTRL17_GoogleDriveBrowser {
	
	static testmethod void UnitTestConnection(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		CTRL17_GoogleDriveBroswer.isConnectionStatusOk();
	}

	static testmethod void UnitTestgetFileFromStandardDocument(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		CTRL17_GoogleDriveBroswer.getfilesListFromGSettingsFolder();
	}

	static testmethod void UnitTestgetFileList(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		CTRL17_GoogleDriveBroswer.getfilesList('123');
	}

	static testmethod void UnitTestMoveBack(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		CTRL17_GoogleDriveBroswer.moveBackByFolderId('1234');
	}

	static testmethod void UnitTestCreateFolder(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		CTRL17_GoogleDriveBroswer.createNewFolder('Test','Test');
	}

}