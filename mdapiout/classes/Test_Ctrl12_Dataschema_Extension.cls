/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Ctrl12_Dataschema_Extension {

    static testMethod void simpleSave() {
        Dataschema__c d = new Dataschema__c (name='test', main_object__c='Contact');
        CTRL12_Dataschema_Extensions ctrl = new CTRL12_Dataschema_Extensions(new ApexPages.Standardcontroller(d));
        List<SelectOption> allObjects = CTRL12_Dataschema_Extensions.getAllObjectsList();
        ctrl.saveDs();
    }
    
    static testMethod void simpleSaveAndDefineDataschema() {
        Dataschema__c d = new Dataschema__c (name='test', main_object__c='Contact');
        CTRL12_Dataschema_Extensions ctrl = new CTRL12_Dataschema_Extensions(new ApexPages.Standardcontroller(d));
        List<SelectOption> allObjects = CTRL12_Dataschema_Extensions.getAllObjectsList();
        ctrl.saveAndDefineDs();
    }
    
    static testMethod void saveDataschemaWithInteractiveCollection() {
        Dataschema__c d = new Dataschema__c (name='test', main_object__c='Contact', fields__c='Name');
        insert d;
        Interactive_Fields_Collection__c ic = Test_Helper.createInteractiveCollection();
        Interactive_Dataschema_Association__c ass = new Interactive_Dataschema_Association__c(Interactive_Form__c=ic.id,dataschema__c=d.id);
        insert ass;
        PageReference pageRef = Page.VF12_EditDataSchema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',d.id);
        ApexPages.currentPage().getParameters().put('clone','1');
        Dataschema__c d1 = new Dataschema__c (name='test2', main_object__c = d.Main_Object__c, fields__c = d.Fields__c);
        CTRL12_Dataschema_Extensions ctrl = new CTRL12_Dataschema_Extensions(new ApexPages.Standardcontroller(d1));
        List<SelectOption> allObjects = CTRL12_Dataschema_Extensions.getAllObjectsList();
        ctrl.saveDs();
    }
    
    static testMethod void cloneDataschemaWithChildren(){
    	Dataschema__c d = new Dataschema__c (name='test', main_object__c='Account', fields__c='Name, Opportunities.Name');
        insert d;
        SOQL_Extension__c s = new SOQL_Extension__c(on__c='Children', Path__c='Opportunities', Children_Name__c='OpportunityLineItems', dataschema__c=d.id, sobject__c='OpportunityLineItem', lookup_field__c='OpportunityId',fields__c='ListPrice');
        insert s;
        PageReference pageRef = Page.VF12_EditDataSchema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',d.id);
        ApexPages.currentPage().getParameters().put('clone','1');
        Dataschema__c d1 = new Dataschema__c (name='test2', main_object__c = d.Main_Object__c, fields__c = d.Fields__c);
        CTRL12_Dataschema_Extensions ctrl = new CTRL12_Dataschema_Extensions(new ApexPages.Standardcontroller(d1));
        List<SelectOption> allObjects = CTRL12_Dataschema_Extensions.getAllObjectsList();
        ctrl.saveDs();
    }
    
    static testMethod void saveFormWithError() {
        Dataschema__c d = new Dataschema__c (name='test', main_object__c='Contact', fields__c='Name');
        insert d;
        PageReference pageRef = Page.VF12_EditDataSchema;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',d.id);
        ApexPages.currentPage().getParameters().put('clone','1');
        Dataschema__c d1 = new Dataschema__c (name='test2');
        CTRL12_Dataschema_Extensions ctrl = new CTRL12_Dataschema_Extensions(new ApexPages.Standardcontroller(d1));
        List<SelectOption> allObjects = CTRL12_Dataschema_Extensions.getAllObjectsList();
        ctrl.saveDs();
        ctrl.saveAndDefineDs();
    }
}