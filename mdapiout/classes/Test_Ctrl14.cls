/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Ctrl14 {

    static testMethod void myUnitTest1() {
    	Document template = new Document();
    	template.Body = Blob.valueof('test');
        template.folderId = UserInfo.getUserId();
        template.name = 'test.html';
        insert template;
        
        Contact c = new Contact(FirstName='Ilan', LastName='Levi', email='dsfd@fdfd.cd');
        insert c;
        
    	PageReference pageRef = Page.VF14_EmailSending;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('template_id',template.id);
    	ApexPages.currentPage().getParameters().put('to_id',c.id);
    	ApexPages.currentPage().getParameters().put('retURL',c.id);
    	CTRL14_EmailSending ctrl = new CTRL14_EmailSending();
    	ctrl.cancel();
    	ctrl.cc='ass.sdd@dffd.cf';
    	ctrl.tos='sds@fdfd.cd';
    	ctrl.bcc='test@test.com';
    	ctrl.subject='subject';
    	ctrl.body='sdfdfd';
    	ctrl.send();
    }
    
    static testMethod void myUnitTest2() {
    	Document template = new Document();
    	template.Body = Blob.valueof('test');
        template.folderId = UserInfo.getUserId();
        template.name = 'test.html';
        insert template;
        
        Contact c = new Contact(FirstName='Ilan', LastName='Levi', email='dsfd@fdfd.cd');
        insert c;
        
    	PageReference pageRef = Page.VF14_EmailSending;
    	Test.setCurrentPage(pageRef);
    	ApexPages.currentPage().getParameters().put('template_id',template.id);
    	ApexPages.currentPage().getParameters().put('retURL',c.id);
    	CTRL14_EmailSending ctrl = new CTRL14_EmailSending();
    	ctrl.cancel();
    	ctrl.cc='ass.sdddffd.cf';
    	ctrl.tos='sdsfdfd.cd';
    	ctrl.bcc='testtest.com';
    	ctrl.subject='subject';
    	ctrl.body='sdfdfd';
    	ctrl.send();
    }
}