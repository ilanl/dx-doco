/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Ctrl15 {

    static testMethod void myUnitTest1() {
        TEST_Helper.insertSettings();
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a;
        Contact c = new Contact(LastName='sfddfd');
        insert c;
        Case c1 = new Case(AccountId=a.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};
        Opportunity t = new Opportunity(accountId=a.id, stageName='Closed Won', CloseDate=Date.today(), Name='opp');
        insert t;
        
        Case ca = new Case();
        insert ca;
        
        Form__c form = TEST_Helper.newFormWithDs();
       	Test_Helper.saveFromDesigner(form.id);
       	Test_Helper.publish(form);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        String fLstStr = Ctrl15_MobileGenerate.getForms(a.id, 'recent');
        fLstStr = Ctrl15_MobileGenerate.getForms(a.id, 'last_modified');
        fLstStr = Ctrl15_MobileGenerate.getForms(a.id, 'all');
        string lookup = Ctrl15_MobileGenerate.getLookups(a.id);
        string save = Ctrl15_MobileGenerate.saveGeneration(a.id, (Integer)(form.Form_Id__c), 'pdf', new List<String>{'attachment'}, null);
        save = Ctrl15_MobileGenerate.saveGeneration(a.id, (Integer)(form.Form_Id__c), 'html', new List<String>{'document'}, null);
        save = Ctrl15_MobileGenerate.saveGeneration(a.id, (Integer)(form.Form_Id__c), 'docx', new List<String>{'chatter'}, null);
        save = Ctrl15_MobileGenerate.saveGeneration(a.id, (Integer)(form.Form_Id__c), 'pdf', new List<String>{'download'}, null);
		string sobjectName = Ctrl15_MobileGenerate.getSobjectName(a.id);
    }

	static testmethod void nonStaticContext(){
		Ctrl15_MobileGenerate ctrl = new Ctrl15_MobileGenerate();
		string nsField = ctrl.nsField;
		string nsClass = ctrl.nsClass;
	}

    private static testmethod void test_isDesignerUser() {
        Ctrl15_MobileGenerate ctrl = new Ctrl15_MobileGenerate();
        Boolean isDesignerUser = ctrl.isDesignerUser;
    }

    

    private static testmethod void test_getSobjectName() {
        Account account = Test_Helper.createAccount();
        String sobjectName = Ctrl15_MobileGenerate.getSobjectName(account.Id);
        System.assertEquals(account.Name, sobjectName);
    }

    private static testmethod void test_getContactLookupFields() {
        Account account = Test_Helper.createAccount();
        String lookupFieldsJson = Ctrl15_MobileGenerate.getContactLookupFields(account.Id);

        List<Utils.LookupPickListValue> lookupFields = (List<Utils.LookupPickListValue>) Json.deserialize(lookupFieldsJson, List<Utils.LookupPickListValue>.class);
        System.assert(lookupFields.size() > 0);
        Utils.log('lookupFields:', lookupFields);
    }
}