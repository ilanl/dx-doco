@isTest
private class Test_ExceptionWS { 

	static testmethod void standardExceptionTest(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=true,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		ExceptionWS eWs = new ExceptionWS(1,'test error');
		eWs.setSessionId(UserInfo.getSessionId());
		eWs.send();
	}

	static testmethod void RenderExceptionExceptionTest(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=true,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		SchemaFactory.RenderException re = new SchemaFactory.RenderException('1', 'error Test', 1);
		ExceptionWS eWs = new ExceptionWS(re);
		eWs.setSessionId(UserInfo.getSessionId());
		eWs.send();
	}

	static testmethod void QueryExceptionTest(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Debug_mode__c=true,Delete_chatter_files__c=false, server_URL__c='https://api2.docomotioninc.com');
        insert myConf;
		try {	        
			List<Account> accLst = (List<Account>)Database.Query('Select non_existing_fields__c From Account');
		}
		catch (QueryException  qe)
		{
			Utils.DocomotionQueryFieldException dqe= new Utils.DocomotionQueryFieldException(qe);	
			ExceptionWS eWs = new ExceptionWS(dqe,1);
			eWs.setSessionId(UserInfo.getSessionId());
			eWs.send();
		}
	}
}