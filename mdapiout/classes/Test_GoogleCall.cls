@isTest
private class Test_GoogleCall {

	static testmethod void unitTestSaveCredentials(){
		GoogleCall gc = new GoogleCall(true);
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		gc.saveCredentials('123','https://tata.toto');
		gc.googleCredentials('toto', 'tata');
		gc.googleCredentials('toto');
		boolean b = gc.hasCredentials;
		boolean b2 = gc.isNew();
		gc.deleteSettings();
		gc.refreshAndSaveAccessToken();
		gc.isConnectionStatusOk ();
	}

	static testmethod void unitTestHaveFilesInFolder(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test.startTest();
		GoogleCall gc = new GoogleCall();
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		gc.haveFileInFolder('123', 'coucou');
		gc.getfilesList('123');
		gc.moveBackByFolderId('12347');
		gc.getExistingFolderId('123', 'coucou');
		gc.getFolderId('coucou');
		Test.stopTest();
	}

	static testmethod void unitTestCreateInitFolder(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test.startTest();
		GoogleCall gc = new GoogleCall();
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		gc.GoogleCreateInitFolder();
		gc.saveSettings();
		Test.stopTest();
	}

	static testmethod void unitTestCreateFile(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test.startTest();
		GoogleCall gc = new GoogleCall();
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		gc.createNewFile('Test', 'pdf', '123', Blob.valueOf('sdafgsdgdfg'));
		gc.createNewFile('Test2', 'docx', '123', Blob.valueOf('sdafgsdgdfg'));
		gc.createNewFile('Test2', 'html', '123', Blob.valueOf('sdafgsdgdfg'));
		gc.updateExistingFile('123', 'pdf', Blob.valueOf('asfgsdgdf'));
		Test.stopTest();
	}

	static testmethod void unitTestErrors(){
		Config__c myConf = new Config__c(Name='Docomotion Default Settings',Google_Drive__c=true,Google_Drive_Mode__c='Organization');
        insert myConf;
		GoogleAPI_Settings__c gSettings = new GoogleAPI_Settings__c(Access_Token__c='123', Docomotion_Folder_Id__c='123',Path__c='ilan',Refresh_Token__c='123');
		insert gSettings;
		Test.startTest();
		GoogleCall gc = new GoogleCall();
		Test_GoogleMock gMock = new Test_GoogleMock();
		Test.setMock(HttpCalloutMock.class, gMock);
		gMock.setError(401);
		gc.createNewFile('Test', 'pdf', '123', Blob.valueOf('sdafgsdgdfg'));
		Test.stopTest();
	}

}