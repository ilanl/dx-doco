@isTest
global class Test_GoogleMock implements HttpCalloutMock {

	public boolean isDirectory=true;
	public Integer currentHttpCode = 200;

	global HTTPResponse respond(HTTPRequest req){
    	HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setStatusCode(currentHttpCode);
		if (req.getEndpoint().containsIgnoreCase('/token')){
			res.setBody(JSON.serialize(oauthResults()));
		}
		else if(req.getEndpoint().endsWith('/revoke')){
			res.setBody('sdsfd');
		}
		else if(req.getMethod()=='GET' && req.getEndpoint().containsIgnoreCase('/files/')){
			string params = req.getEndpoint().subStringAfter('?');
			string fieldsParams = getParams(params).get('fields');
			res.setBody(JSON.serialize(returnFileDir(req.getEndpoint().substringAfter('/files/').substringBefore('?'),getFieldsParams(fieldsParams))));
		}
		else if(req.getMethod()=='GET' && req.getEndpoint().containsIgnoreCase('/files')){
			if (req.getEndpoint().containsIgnoreCase('application/vnd.google-apps.folder')){
				res.setBody(JSON.serialize(returnDirectories()));
			}
			else{
				res.setBody(JSON.serialize(returnFiles()));
			}
			
		}
		else if(req.getMethod()=='POST' && req.getEndpoint().containsIgnoreCase('/files')){
			System.debug(req.getBody());
			res.setBody(JSON.serialize(createDirectory()));
		}	
		return res;
    	
	}

	public void setError(Integer errorCode){
		this.currentHttpCode = errorCode;
	}

	private Map<String,Object> oauthResults(){
		Map<String,Object> ret = new Map<String,Object>();
		ret.put('access_token','123');
		ret.put('refresh_token','1234');
		return ret;
	}

	private Map<String, List<GoogleUtils.GoogleFile>> returnDirectories(){
		Map<String, List<GoogleUtils.GoogleFile>> ret = new Map<String, List<GoogleUtils.GoogleFile>>();
		List<GoogleUtils.GoogleFile> filesList = new List<GoogleUtils.GoogleFile>();
		GoogleUtils.GoogleFile dir = new GoogleUtils.GoogleFile();
		dir.name = 'Ilan';
		dir.mimeType = 'application/vnd.google-apps.folder';
		filesList.add(dir);
		ret.put('files',filesList);
		return ret;
	}

	private Map<String, List<GoogleUtils.GoogleFile>> returnFiles(){
		Map<String, List<GoogleUtils.GoogleFile>> ret = new Map<String, List<GoogleUtils.GoogleFile>>();
		List<GoogleUtils.GoogleFile> filesList = new List<GoogleUtils.GoogleFile>();
		GoogleUtils.GoogleFile dir = new GoogleUtils.GoogleFile();
		dir.name = 'Ilan';
		dir.mimeType = 'application/pdf';
		dir.parents = new List<String>{'123'};
		dir.id = '12345';
		filesList.add(dir);
		GoogleUtils.GoogleFile dir2 = new GoogleUtils.GoogleFile();
		dir2.name = 'coucou';
		dir2.mimeType = 'application/pdf';
		dir2.parents = new List<String>{'123'};
		dir2.id = '12346';
		filesList.add(dir2);
		GoogleUtils.GoogleFile dir3 = new GoogleUtils.GoogleFile();
		dir3.name = 'dir';
		dir3.mimeType = 'application/vnd.google-apps.folder';
		dir3.parents = new List<String>{'123'};
		dir3.id = '12347';
		filesList.add(dir3);
		ret.put('files',filesList);
		return ret;
	}

	private Map<String,Object> returnFileDir(string id, Set<String>toShow){
		GoogleUtils.GoogleFile file = new GoogleUtils.GoogleFile();
		file.name = 'dir';
		file.mimeType = 'application/vnd.google-apps.folder';
		file.parents = new List<String>{'123'};
		file.id = id;
		Map<String,Object> ret = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(file));
		if (toShow != null)
		{
			for (string key : ret.keySet()){
				if (!toShow.contains(key))
					ret.remove(key);
			}
		}
		return ret;
	}

	private Map<String,String> getParams(string params){
		Map<String,String> ret = new Map<String,String>();
		for (string s: params.split('&')){
			ret.put(s.substringBefore('='),s.substringAfter('='));
		}
		return ret;
	}

	private Set<String> getFieldsParams(string fieldstring){
		Set<String> ret;
		string s = EncodingUtil.urlDecode(fieldstring,'UTF-8');
		ret = new Set<String>(s.split(', '));
		return ret;
	}

	private Map<String,String> createDirectory(){
		Map<String, String> ret = new Map<String,String>();
		ret.put('id','1224354');
		return ret;
	}
}