@isTest()
global class Test_PublishMockup implements HttpCalloutMock{
	global HTTPResponse respond(HTTPRequest req){
		HttpResponse res = new HttpResponse();
        res.setBody('Ok');
        res.setStatusCode(200);
        return res;
	}

}