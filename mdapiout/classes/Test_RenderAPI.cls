/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_RenderAPI {

    static testMethod void myUnitTest() {
        TEST_Helper.insertSettings();
        Account a2 = new Account(Name='Parent Test',phone='23534646', Fax='2345346');
        insert a2;
        Account a = new Account(Name='Test', parentId=a2.id,phone='23534646', Fax='2345346');
        insert a;
        Contact c = new Contact(LastName='sfddfd');
        insert c;
        Case c1 = new Case(AccountId=a.id);
        Case c2 = new Case(AccountId=a2.id);
        insert new List<Case>{c1,c2};
        Opportunity t = new Opportunity(accountId=a.id, stageName='Closed Won', CloseDate=Date.today(), Name='opp');
        insert t;
        
        Case ca = new Case();
        insert ca;
        
        Form__c form = TEST_Helper.newFormWithDs();
       	Test_Helper.saveFromDesigner(form.id);
       	Test_Helper.publish(form);
        Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.ATTACHMENT, RenderAPI.Format.PDF);
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.ATTACHMENT, RenderAPI.Format.PDF,c.id);
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.ATTACHMENT, RenderAPI.Format.PDF,'test');
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.CHATTER, RenderAPI.Format.PDF);
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.DOCUMENT, RenderAPI.Format.PDF);
        RenderAPI.renderCreateFile((Integer)form.form_Id__c, a.id, RenderAPI.Mode.DOWNLOAD, RenderAPI.Format.PDF);
        RenderAPI.renderCreateFileAsync((Integer)form.form_Id__c, a.id, RenderAPI.Mode.ATTACHMENT, RenderAPI.Format.PDF,c.id);
        RenderAPI.renderCreateFileAsync((Integer)form.form_Id__c, a.id, RenderAPI.Mode.CHATTER, RenderAPI.Format.PDF,'test');
        RenderAPI.renderCreateFileAsync((Integer)form.form_Id__c, a.id, RenderAPI.Mode.DOCUMENT, RenderAPI.Format.PDF);
        RenderAPI.renderBlob((Integer)form.form_Id__c, a.id, RenderAPI.Format.PDF);
    }
    
    static testMethod void myUnitTestWithSubforms() {
    	TEST_Helper.insertSettings();
    	Account ap = new Account(Name='testparent');
        insert ap;
        Contact c = new Contact(FirstName='sfdfd',LastName='test');
        insert c;
        Account a = new Account(Name='test', parentId=ap.id);
        insert a;
        Opportunity t = new Opportunity(accountId=a.id, stageName='Closed Won', CloseDate=Date.today(), Name='opp');
        insert t;
        
        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
		insert newProd;

		PriceBookEntry pbEntry = new PriceBookEntry(UnitPrice = 300,PriceBook2Id = Test.getStandardPricebookId(),Product2Id = newProd.Id,IsActive = true);
		insert pbEntry ;
		
        OpportunityLineItem oli = new OpportunityLineItem(pricebookentryid=pbEntry.Id,TotalPrice=2000,Quantity = 2,OpportunityID = t.Id);
		insert oli;
		
		Form__c s = Test_Helper.newSubFormWithDs();
		Test_Helper.saveFromDesigner(s.Id);
		Test_Helper.publish(s);
		
		Form__c f = Test_Helper.newFormWithDs();
		Form_Subform_Association__c ass = new Form_Subform_Association__c(form__c = f.id, subform__c=s.id);
		insert ass;
		Test_Helper.saveFromDesigner(f.Id);
		Test_Helper.publish(f);
		Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
        RenderAPI.renderCreateFile((Integer)f.form_Id__c, a.id, RenderAPI.Mode.ATTACHMENT, RenderAPI.Format.PDF);
    }
}