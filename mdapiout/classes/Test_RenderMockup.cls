@isTest
global class Test_RenderMockup implements HttpCalloutMock{
	
	private boolean hasError;
	
	global Test_RenderMockup(boolean error){
		hasError = error;
	}
	
	global HTTPResponse respond(HTTPRequest req){
		HttpResponse res = new HttpResponse();
        res.setStatusCode(200);
        system.debug('***req: '+req);
		if (req.getEndpoint().containsIgnoreCase('google')){
			Test_GoogleMock gMock = new Test_GoogleMock();
			return gMock.respond(req);
		}
        if(req.getMethod()=='POST'){
	        res.setHeader('Content-Type', 'application/json');
	        if(!hasError)
	        	res.setBody('{"FileURL":"www.test.com\\test.pdf","ErrorID":0}');
	        else
	        	res.setBody('{"FileURL":"www.test.com\\test.pdf","ErrorID":1000,"MessageID":"pan pan"}');	
        }
        else{
        	res.setBody('test');
        }
        return res;
	}

}