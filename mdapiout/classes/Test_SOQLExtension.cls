/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_SOQLExtension {

    static testMethod void unitTestDelete() {
    	Form__c f = TEST_Helper.newFormWithDs();
    	Id dsId = f.dataschema__c;
    	//remove a field that is unique on the children
    	CTRL04_DataSchema.FieldSaveAction fsa9 = new CTRL04_DataSchema.FieldSaveAction();
        fsa9.action='none';
        fsa9.path='Assets.Cases.CaseNumber';
        fsa9.childrenPath = 'Assets.Cases';
        fsa9.onChild=true;
        fsa9.sobjectName='Case';
      	CTRL04_DataSchema.saveFieldsDelta(dsId, new List<CTRL04_DataSchema.FieldSaveAction>{fsa9});
    }
    
    static testMethod void unitTestUpdate(){
    	Form__c f = TEST_Helper.newFormWithDs();
    	Id dsId = f.dataschema__c;
    	//remove a field that is unique on the children
    	CTRL04_DataSchema.FieldSaveAction fsa9 = new CTRL04_DataSchema.FieldSaveAction();
        fsa9.action='none';
        fsa9.path='Assets.Cases.CaseNumber';
        fsa9.childrenPath = 'Assets.Cases';
        fsa9.onChild=true;
        fsa9.sobjectName='Case';
        CTRL04_DataSchema.FieldSaveAction fsa = new CTRL04_DataSchema.FieldSaveAction();
        fsa.action='select';
        fsa.path='Assets.Cases.CreatedById';
        fsa.childrenPath = 'Assets.Cases';
        fsa.onChild=true;
        fsa.sobjectName='Case';
      	CTRL04_DataSchema.saveFieldsDelta(dsId, new List<CTRL04_DataSchema.FieldSaveAction>{fsa9, fsa});
    }
    
    static testMethod void changeOnDataschema(){
    	Form__c f = TEST_Helper.newFormWithDs();
    	Id dsId = f.dataschema__c;
    	//remove a field that is unique on the children
    	CTRL04_DataSchema.FieldSaveAction fsa9 = new CTRL04_DataSchema.FieldSaveAction();
        fsa9.action='none';
        fsa9.path='Name';
      	CTRL04_DataSchema.saveFieldsDelta(dsId, new List<CTRL04_DataSchema.FieldSaveAction>{fsa9});
    }
}