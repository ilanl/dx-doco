@isTest
private class Test_Signature {
	static testMethod void SignatureOnInsert(){
		TEST_Helper.insertSettings();
		Contact c = new Contact(LastName='test', email='test@test.com');
		insert c;
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(true));
		Signature__c signature = new Signature__c(Signer__c=c.Id, form__c=form.Id, record_Id__c=c.Id, chatter__c=true);
		insert signature;
		Test.stopTest();
	}

	static testMethod void SignatureOnInsertF(){
		TEST_Helper.insertSettings();
		Contact c = new Contact(LastName='test', email='test@test.com');
		insert c;
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(false));
		Signature__c signature = new Signature__c(Signer__c=c.Id, form__c=form.Id, record_Id__c=c.Id);
		insert signature;
		Test.stopTest();
	}

	static testMethod void SignatureOndeleteFiles(){
		TEST_Helper.insertSettings();
		Contact c = new Contact(LastName='test', email='test@test.com');
		insert c;
		Form__c form = TEST_Helper.newFormWithDs();
        TEST_Helper.saveFromDesigner(form.Id);
        Test_Helper.publish(form);
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(false));
		Signature__c signature = new Signature__c(Signer__c=c.Id, form__c=form.Id, record_Id__c=c.Id);
		SignaturesRestCall.setIsTest(true);
		insert signature;
		SignaturesRestCall.setIsTest(false);
		Test.startTest();
		SignaturesRestCall.deleteFilesOnDocomotion(new Set<Id>{signature.Id});
		Test.stopTest();
	}

	
}