/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Utils {

    static testMethod void myUnitTest() {
       List<Object> objLst = new List<Object>();
       Form__c f = new Form__c(Name__c='test');
       insert f;
       objLst.add(f);
       objLst.add(true);
       objLst.add(f.id);
       objLst.add('test');
       objLst.add(Blob.valueOf('test'));
       objLst.add(Date.today());
       objLst.add(DateTime.now());
       objLst.add(Time.newInstance(10,10,10,10));
       objLst.add(5);
       long l = 5;
       objLst.add(l);
       decimal d = 5.6;
       objLst.add(d);
       double dl = 3.4;
       objLst.add(dl);
       objLst.add(new List<String>{'dd'});
       for(Object obj:objLst)
       		Utils_Type.getType(obj);
       Utils_Type.isId((String)f.id);
       Utils_Type.createObject('true','Boolean');
       Utils_Type.createObject('12/2/1983','Date');
       Utils_Type.createObject('1983-2-12','Date');
       //Utils_Type.createObject('12.2.1983','Date');
       Utils_Type.createObject(''+DateTime.now(),'DateTime');
       Utils_Type.createObject('1.2','Double');
       Utils_Type.createObject(f.id,'Id');
       Utils_Type.createObject('1','Integer');
    }
}