@isTest
public class Test_process_builder_extension {
	public static Form__c form;
	public static Account account;
	public static Contact con;
	public static void TestInit(){
		TEST_Helper.insertSettings();
		TEST_Helper.createWordTemplate();
		form =TEST_Helper.newFormOnContactWithDs();
		account = new Account(Name = 'test');
		insert account;
		con = new Contact(LastName='ContTest1', Email='test1contact@duptest.com', AccountId = account.Id, LeadSource='Inbound Call');
	    insert con;
	}
	static testMethod void pbe_Doc_test() {
		TestInit();
		Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
		ProcessBuilder_Extension_Doc.PbdRequest request = new ProcessBuilder_Extension_Doc.PbdRequest();
		request.formId = Integer.valueOf(form.Form_Id__c);
		request.sobjectId = con.id;
		request.mode = 'DOCUMENT';//{DOCUMENT,ATTACHMENT,CHATTER}
		request.format = 'PDF';
		test.startTest();
		ProcessBuilder_Extension_Doc.processBuilderDoc(new list<ProcessBuilder_Extension_Doc.PbdRequest>{request});
		request.mode = 'ATTACHMENT';
		ProcessBuilder_Extension_Doc.processBuilderDoc(new list<ProcessBuilder_Extension_Doc.PbdRequest>{request});
		request.mode = 'CHATTER';
		ProcessBuilder_Extension_Doc.processBuilderDoc(new list<ProcessBuilder_Extension_Doc.PbdRequest>{request});
		test.stopTest();
		
		
	}
	static testMethod void pbe_Link_test() {
		TestInit();
		Test.setMock(HttpCalloutMock.class, new Test_SignatureMock(true));
		ProcessBuilder_Extension_Link.PblRequest request = new ProcessBuilder_Extension_Link.PblRequest();
		request.formId = Integer.valueOf(form.Form_Id__c);
		request.sobjectId = con.id;
		request.whoId = con.id;
		test.startTest();
		ProcessBuilder_Extension_Link.processBuilderLink(new list<ProcessBuilder_Extension_Link.PblRequest>{request});
		test.stopTest();
	}
	static testMethod void pbe_Email_test() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
		System.runAs(thisUser){ 
			TestInit();
			Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
			ProcessBuilder_Extension_Email.PbeRequest request = new ProcessBuilder_Extension_Email.PbeRequest();
			request.formId = Integer.valueOf(form.Form_Id__c);
			request.sobjectId = con.id;
			request.whoId = con.id;
			request.format = 'PDF';
			request.isActivatedVersion = false;
			request.fileName = 'EmailTest';
			EmailTemplate et = new EmailTemplate();
			et.isActive = true;
			et.Name = 'name';
			et.DeveloperName = 'emailtestDevName';
			et.TemplateType = 'text';
			et.FolderId = UserInfo.getUserId();
			et.Subject = 'Your Subject Here';
			insert et;
			request.TemplateId = et.id;
			test.startTest();
			ProcessBuilder_Extension_Email.processBuilderEmail(new list<ProcessBuilder_Extension_Email.PbeRequest>{request});
			test.stopTest();    
		}
		
	}
	static testMethod void pbe_EmailBod_test() {
		User thisUser = [select Id from User where Id = :UserInfo.getUserId()]; 
		System.runAs(thisUser){ 
			TestInit();
			Test.setMock(HttpCalloutMock.class, new Test_RenderMockup(false));
			ProcessBuilder_Extension_EmailBody.PbeeRequest request = new ProcessBuilder_Extension_EmailBody.PbeeRequest();
			request.formId = Integer.valueOf(form.Form_Id__c);
			request.sobjectId = con.id;
			request.whoId = con.id;
			request.isActivatedVersion = false;
			test.startTest();
			ProcessBuilder_Extension_EmailBody.processBuilderEmailBody(new list<ProcessBuilder_Extension_EmailBody.PbeeRequest>{request});
			test.stopTest();
		}
	}
    
}