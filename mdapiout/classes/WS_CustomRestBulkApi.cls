@RestResource(urlMapping='/custom_bulk')
global with sharing class WS_CustomRestBulkApi {
	//For saving the object and do a treatment
	private static Map<string, Map<string, List<sobject>>> actionsMap = new Map<string, Map<string, List<sobject>>>();
	private static Integer savedRecords = 0;

	//For the order of execution
	private static ExecutionOrder execOrder = new ExecutionOrder();
	
	//The object we are receiving from the rest
	global class RestApiAction{
		public string sobjectName;
		public string action;
		public string jsonStr;
	}
	
	//Response returned
	global class RestApiResponse{
		public Boolean isSuccess;
		public Integer recordSaved;//To Implement
		public Map<string, Map<string, List<sobject>>> records = new Map<string, Map<string, List<sobject>>>();
		public String errorMessage;
	}
	
	//The REST PATCH entry point
	@HttpPatch
	global static void doPatch(List<RestApiAction> actions){
		RestContext.response.addHeader('Content-Type', 'application/json');
		RestApiResponse response = new RestApiResponse();
		SavePoint savePoint = Database.setSavepoint();
		try{
			//To populate the data
			for(RestApiAction action:actions){
				add(action.sobjectName, action.action, createSobject(action.sobjectName, action.jsonStr));
			}
			//To execture the data
			execute();
			//the response
			response.isSuccess = true;
			response.recordSaved = savedRecords;
			response.records = actionsMap;
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
		}
		catch(Exception e){
			Utils.log('Custom Rest Bulk API Exception',e.getMessage()+'\r\n\r'+e.getStackTraceString());
			Database.rollback(savePoint);
			response.isSuccess = false;
			response.recordSaved = 0;
			response.records = new Map<string, Map<string, List<sobject>>>();
			response.errorMessage = e.getMessage();
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(response));
		}
	}
	
	//for creating a sobject from its json
	private static sobject createSobject(string sobjectName, string jsonStr){
		Sobject s = (Sobject)JSON.deserialize(jsonStr, Type.forName(sobjectName));
		return s;
	}
	
	//to insert in the map
	private static void add(string sobjectName, string action, sobject sobj){
		if(!actionsMap.containsKey(sobjectName.toLowerCase()))
			actionsMap.put(sobjectName.toLowerCase(), new Map<string, List<sobject>>());
		if(!actionsMap.get(sobjectName.toLowerCase()).containsKey(action.toLowerCase()))
			actionsMap.get(sobjectName.toLowerCase()).put(action.toLowerCase(), new List<sobject>());
		actionsMap.get(sobjectName.toLowerCase()).get(action.toLowerCase()).add(sobj);
		execOrder.register(sobjectName.toLowerCase(), action.toLowerCase());
	}
	
	//execution
	private static void execute(){
		for(string action : execOrder.getOperationsList()){
			for(string sobjectName : execOrder.getObjectsForOperation(action)){
				//Insert mode
				if(action.toLowerCase()=='insert'){
					DML_CustomExtension.insertObj(actionsMap.get(sobjectName).get(action));
				}
				//update mode
				else if(action.toLowerCase()=='update'){
					DML_CustomExtension.updateObj(actionsMap.get(sobjectName).get(action));
				}
				//delete mode
				else if(action.toLowerCase()=='delete'){
					DML_CustomExtension.deleteObj(actionsMap.get(sobjectName).get(action));
				}
				//undelete mode
				else if(action.toLowerCase()=='undelete'){
					DML_CustomExtension.undeleteObj(actionsMap.get(sobjectName).get(action));
				}
				savedRecords += actionsMap.get(sobjectName).get(action).size();
			}
		}
	}

	//For the correct execution order
	class ExecutionOrder{
		//VARIABLES
		private List<String> operation = new List<String>{'insert', 'delete', 'undelete', 'update'};
		private List<String> order = new List<String>{
			Utils.getNameSpace()+'word_template__c',
			Utils.getNameSpace()+'dataschema__c',
			Utils.getNameSpace()+'soql_extensions__c',
			Utils.getNameSpace()+'interactive_dataschema_association__c',
			Utils.getNameSpace()+'interactive_fields_collection__c',
			Utils.getNameSpace()+'interactive_field__c',
			Utils.getNameSpace()+'form__c',
			Utils.getNameSpace()+'interactive_collections_association__c',
			Utils.getNameSpace()+'form_subform_association__c'
		};
		private Map<String,List<String>> operationMap = new Map<String, List<String>>();

		//METHODS
		public List<String> getOperationsList(){
			return operation;
		}

		public List<String> getObjectsForOperation(string opString){
			if(!operationMap.containsKey(opString))
				return new List<String>();
			else
				return operationMap.get(opString);
		}
		
		
		private void register(string objectName, string operation){
			if(!operationMap.containsKey(operation))
				operationMap.put(operation, new List<String>());
			Set<String> currentObjects = new Set<String>(operationMap.get(operation));
			currentObjects.add(objectName);
			operationMap.get(operation).clear();
			if(operation=='delete'){//When deleting
				for(Integer i = order.size() - 1; i >= 0; --i ){
					if(currentObjects.contains(order[i])){
						operationMap.get(operation).add(order[i]);
						currentObjects.remove(order[i]);
					}
				}
				if(!currentObjects.isEmpty()){
					operationMap.get(operation).addAll(currentObjects);
				}
			}
			else{//for other operations
				for(Integer i = 0; i < order.size(); ++i ){
					if(currentObjects.contains(order[i])){
						operationMap.get(operation).add(order[i]);
						currentObjects.remove(order[i]);
					}
				}
				if(!currentObjects.isEmpty()){
					operationMap.get(operation).addAll(currentObjects);
				}
			}
		}
	}
	
	
}