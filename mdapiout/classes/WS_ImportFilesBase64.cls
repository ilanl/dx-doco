@RestResource(urlMapping='/importFilesBase64')
global with sharing class WS_ImportFilesBase64 {
    
    @HttpPost
    global static void RequestJSONformat() {
        if(!(SObjectType.Attachment.isAccessible() && SObjectType.Attachment.fields.Body.isAccessible())|| Test_Ws_ImportFilesBase64.noPermission == true){
        	RestContext.response.statusCode = 401;
            RestContext.response.responseBody = Blob.valueof(JSON.serialize('{"errorMsg":"No access permissions"}'));
            return;
        }
        String jsonStr = RestContext.request.requestBody.toString();
        RequestJesonFormat  rjf;
        try{
       		rjf = (RequestJesonFormat)System.JSON.deserialize(jsonStr, RequestJesonFormat.class);
        }catch(Exception E){
        	Utils.log('wrong json structure ',E.getMessage());
            RestContext.response.statusCode = 400;
            RestContext.response.responseBody = Blob.valueof(JSON.serialize('{"errorMsg":"wrong json structure"}'));
             return;
        }
        List<String> ids = (List<String>)rjf.Ids;
        if(ids.size()> 0){ 
            list<id>invalidIds = new list<id>();
            list<Base64>Base64List = new list<Base64>();
            list<id>attachmentIds = new list<id>();
            map<id,Attachment> alLattachmentsMap = new map<id,Attachment>([SELECT id, BodyLength FROM Attachment WHERE id in :ids ORDER BY BodyLength ASC]);
            
            Utils.log('alLattachmentsMap size : ',alLattachmentsMap.size());
            Integer usedbytes = Limits.getHeapSize();   	
            Integer HeapLimit = Limits.getLimitHeapSize()- usedbytes;
            set<id> verifiedAttIds = new set<id>();
            for(Attachment a : alLattachmentsMap.values()){ // get all bigItems 
                if(a.BodyLength> Limits.getLimitHeapSize()-10000){
                    invalidIds.add(a.id);
                    continue;
                }
                if((HeapLimit - (a.BodyLength) )>0){// get all fit ids we can return in cuurent transaction while not hit the heap limit 
                    verifiedAttIds.add(a.Id);
                    HeapLimit -= a.BodyLength;
                }
            }
           Utils.log('verifiedAttIds size : ', verifiedAttIds.size());
            alLattachmentsMap = null;
            RestContext.response.statusCode = 200;
            RestContext.response.addHeader('Content-Type', 'application/json');
            RestContext.response.responseBody = Blob.valueof(JSON.serialize(getRF(verifiedAttIds,invalidIds)));
        }
        else{
            Utils.log('no attachments found',null);
            RestContext.response.statusCode = 202;
            RestContext.response.responseBody = Blob.valueof(JSON.serialize('{"errorMsg":"no attachments found"}'));
            return;
        }
    }
    //build the respons body 
    public static ResponseFormat getRF(set<id>verifiedAttIds,list<id> invalidIds){
        list<Base64>Base64List = new list<Base64>();
        for(Attachment a: [SELECT id, Body, BodyLength FROM Attachment WHERE id in :verifiedAttIds ORDER BY BodyLength ASC ]){
            try{
            	Base64List.add(new Base64(a.Id,EncodingUtil.base64Encode(a.Body),false));
            	}catch(exception e){
            		Base64List.add(new Base64(a.Id,null,true));
            		Utils.log('error while rendering attachment body as bas64',e.getMessage());
            	}
            a = null;		
        }
        while((Limits.getLimitHeapSize() - Limits.getHeapSize()) <0){//just verifying
            Base64List.remove(Base64List.size()-1);	
        }
        return new ResponseFormat(Base64List,invalidIds);
    }
    //wrapper classes - use as a base template for the json serialize response  
    public class Base64{
        public Id id;
        public String  Base64;
        public Boolean  Error;
        public Base64(Id id, string base64,Boolean  error){
            this.Id = id;
            this.Base64 = base64;
            this.Error = error;
        }
    }
    public class BigItem{
        public Id Id;
        public BigItem(Id id){
            this.Id = id;
        }
    }
    global class ResponseFormat{
        public Base64[] Items;
        public BigItem[] BigItems;
        public ResponseFormat(Base64[] items,list<id> invalidIds){
            this.Items = items;
            this.BigItems = new list<BigItem>() ;
            for(Id id: invalidIds){
                BigItems.add(new BigItem(id));    
            }
            
            
        } 
    }
    //wrapper class - use as a base template for the json derialize request 
    public class RequestJesonFormat{
        public Id[] Ids;
    }
    
}