@RestResource(urlMapping='/pushinteractive')
global with sharing class WS_Interactive{
	
	@HttpPost
	global static string doPost(string xml){
		try{
			Utils.log('xml',xml);
			string formId;//The form Id
			Id whoId;//The id of the contact or lead
			Id whatId;//The id of the object on which the doc is generated
			Map<String,String> fieldsMap = new Map<String,String>();
			/*******************read the XML*******************************/
			//There is a field subtree with all the fields and a sfdc subtree with the whoid and the formid.
			DOM.Document doc = new Dom.Document();
			doc.load(xml);
			Dom.XmlNode root = doc.getRootElement();
			//1. the sfdc node
			Dom.XmlNode sfdcNode = root.getChildElement('sfdc',null);
			formId = sfdcNode.getChildElement('formid',null).getText();
			string who = sfdcNode.getChildElement('whoid',null).getText();
			whoId = (who=='')?null:(Id)who;
			whatId = sfdcNode.getChildElement('whatid',null).getText();

			//2. Cellosign updates
			UpdateCellosign cellosign;
			Dom.XmlNode celloNode = root.getChildElement('cello',null);
			if (celloNode != null){
				cellosign = new UpdateCellosign(celloNode);
				cellosign.exec();
			}
			
			//3. the fields node
			Dom.XmlNode fieldsNode = root.getChildElement('fields',null);
			for(Dom.XmlNode fieldNode:fieldsNode.getChildren()){
				List<Dom.XmlNode> lst = fieldNode.getChildElements();
				if(lst.isEmpty()){
					if(utils_Type.getType(fieldNode.getName())=='Id' || fieldNode.getName().split('\\.').size()==2){
						//to fix the empty radio box bug && multi picklist:
						List<String> t = fieldNode.getName().split('\\.');
						if(t.size() == 2 && t[1]=='value'){
							string key = (utils_Type.getType(t[0])=='Id')?t[0]:t[0].toLowerCase();
							if(!fieldsMap.containsKey(key))
								fieldsMap.put(key,fieldNode.getText());
							else
								fieldsMap.put(key,fieldsMap.get(key)+', '+fieldNode.getText());	
						}
						else if(t.size() != 2 || t[0]!=t[1]){
							string key = (utils_Type.getType(t[0])=='Id')?fieldNode.getName():fieldNode.getName().toLowerCase();
                            fieldsMap.put(key,fieldNode.getText());

						}
					}
					else
						fieldsMap.put(fieldNode.getName().toLowerCase(),fieldNode.getText());
						
				}
			}
			
			Id signatureId = (cellosign != null)?cellosign.getSignatureId():null;
			InteractiveUpdate iu = new InteractiveUpdate(formId, whoId, whatId,fieldsMap, signatureId);
		}
		catch(Exception e){
			Utils.log('Exception',e.getMessage()+'\r\n'+e.getStackTraceString());
			return '{"Status":"Error", "Message":"'+e.getMessage()+'"}';
		}
		
		return '{"Status":"OK"}';
	}
	
	class InteractiveUpdate{
		private Form__c form;//The main Form => for the main object
		private Map<Id,Interactive_Field__c> questionsMap;//for the questions
		private Map<string,String> UpdatefieldsMap;
		private Set<String> radioSet;//for the radio IF
		private Id whoId;
		private Id whatId;
		private Id signatureId;
		
		
		public InteractiveUpdate(string formId, Id whoId, id whatId, Map<string,string> fieldsMap, Id signatureId){
			try{
				form = [Select Id from Form__c Where form_id__c = :Integer.valueOf(formId)];
			}
			catch(Exception e){
				throw new Utils.CustomException('The form doesn\'t exist');
			}
			questionsMap = new Map<Id,Interactive_Field__c>();
			radioSet = new Set<String>();
			
			UpdatefieldsMap = new Map<String,String>();
			this.whoId = whoId;
			this.whatId = whatId;
			this.signatureId = signatureId;
			//1. loop all the fields in the Map
			for(string field:fieldsMap.keySet()){
				if(Utils_Type.isId(field)){//If id => it 's a Interactive field insert
					questionsMap.put(Id.valueOf(field),null);
				}
				else if(isRadio(field)){
					List<String> t = field.split('\\.');
					questionsMap.put(Id.valueOf(t.get(0)),null);
					radioSet.add(field);
				}
				else{//it's a field update
					if(fieldsMap.get(field) != '')
						UpdatefieldsMap.put(field,fieldsMap.get(field));
				}
			}
			
			//For the survey
			if(!questionsMap.isEmpty()){
				updateFieldsMap(fieldsMap);
				saveSurvey(fieldsMap);
			}
			
			//For the fields update	
			if(!updatefieldsMap.isEmpty()){
				updateFields();
			}
		}


		//To create the new surveys
		public void saveSurvey(Map<string,string> fieldsMap){
			Map<Id,Interactive_Submission__c> submissionMap = new Map<Id,Interactive_Submission__c>();//Map collection Id => submission
			List<Interactive_Response__c> intResponseLst = new List<Interactive_Response__c>();
			//we create the map of all the questions and we save their collections, and save the submission.
			if(!questionsMap.isEmpty() /*&& whoId != null*/){
				questionsMap = new Map<Id,Interactive_Field__c>([Select Id, Interactive_Fields_Collection__c From Interactive_Field__c Where id in :questionsMap.keySet()]);
				for(Interactive_Field__c intField: questionsMap.values()){
					if(!submissionMap.containsKey(intField.Interactive_Fields_Collection__c)){
						Interactive_Submission__c submission = new Interactive_Submission__c(
							Interactive_Fields_Collection__c=intField.Interactive_Fields_Collection__c,
							form__c = form.id
						);
						if(whoId != null){
							if(whoIsContact())
								submission.contact__c = whoId;
							else
								submission.lead__c = whoId;
						}
						system.debug('***signature ID: '+signatureId);
						if(signatureId != null)
							submission.signature__c =  signatureId;
						submissionMap.put(intField.Interactive_Fields_Collection__c, submission);		
					}
				}
				DML_CustomExtension.insertObj(submissionMap.values());
				if(!submissionMap.isEmpty()){
					for(Interactive_Field__c question: questionsMap.values()){
						Interactive_Submission__c submission = submissionMap.get(question.Interactive_Fields_Collection__c);
						Interactive_Response__c response = new Interactive_Response__c();
						response.Interactive_Field__c = question.id;
						response.Interactive_Submission__c = submission.id;
						response.response__c = fieldsMap.get(question.id);
						intResponseLst.add(response);
					}
					if(!intResponseLst.isEmpty())
						DML_CustomExtension.insertObj(intResponseLst);
				}
			}
		}
		
		public void updateFields(){
			SobjectType mainObj = whatid.getsobjectType();
			SobjectHierarchy h = new SobjectHierarchy(mainObj);
			h.addPath(UpdatefieldsMap.keySet());
			Map<string, List<Sobject>> toUpdateMap = h.buildSobj(updatefieldsMap, whatId);
			if(toUpdateMap != null){
				Utils.log('toUpdate',toUpdateMap);
				for(List<Sobject> sobLst:toUpdateMap.values()){
					try{
						DML_CustomExtension.updateObj(sobLst);
					}
					catch(Exception e){
						Utils.log('DML Exception', e);
					}
				}
			}
		}
		
		private void updateFieldsMap(Map<string,string> fieldsMap){
			if(!radioSet.isEmpty()){
				Set<Id> iFieldsIds = new Set<Id>();
				for(String str: radioSet){
					List<String> t = str.split('\\.');
					iFieldsIds.add(t.get(0));
				}
				Map<Id,Interactive_Field__c> iMap = new Map<Id,Interactive_Field__c>([select id, Choices2__c From Interactive_Field__c Where id in :iFieldsIds]);
				for(String str: radioSet){
					List<String> t = str.split('\\.');
					Interactive_Field__c myIf = iMap.get(t.get(0));
					system.debug('*********************************************'+myIf);
					List<String> choices = myIf.choices2__c.split('\r*\n');
					Integer index = Integer.valueOf(t.get(1).replace('a',''));
					string value = choices.get(index);
					fieldsMap.put(myIf.id,value);
					fieldsMap.remove(str);
				}
			}
		}
		
		private boolean isRadio(String f){
			List<String> t = f.split('\\.');
			return (t.size()==2 && Utils_Type.isId(t.get(0)));
		}
		
		private boolean whoIsContact(){
			if(whoId != null){
				return (Schema.Contact.SObjectType == whoId.getSobjectType());
			}
			return false;
		}
		
		
	}
	
	class SobjectHierarchy{
		private Schema.sObjectType mainObjectType;//The main object
		private Set<string> fieldsSet;//Set of all the fields of the object
		private Map<String,SobjectHierarchy> lookupsNextLevel;//map of the lookup with their associated hierarchy
		private Sobject sobj;
		private String sobjectPath;
		
		public SobjectHierarchy(Schema.sObjectType sobjType, string prePath){
			sobjectPath = prePath;
			mainObjectType = sobjType;
			fieldsSet = new Set<string>();
			lookupsNextLevel = new Map<String,SobjectHierarchy>();
		}
		
		public SobjectHierarchy(Schema.sObjectType sobjType){
			this(sobjType,'');
		}
		
		public void addPath(Set<string> pathLst){
			Map<String,Schema.sObjectType> lookups = new Map<String,Schema.sObjectType>();
			Map<string,Set<String>> nextPath = new Map<string,Set<string>>();
			for(String path:pathLst){
				string[] pathTab = path.split('\\.');
				if(pathTab.size()==1){
					fieldsSet.add(path);
				}
				else{
					lookups.put(pathTab[0],null);
					//the new path
					string newPath='';
					for(Integer i=1; i<pathTab.size();++i){
						newPath += pathTab[i];
						if(i < pathTab.size()-1)
							newPath += '.';
					}
					if(!nextPath.containsKey(pathTab[0]))
						nextPath.put(pathTab[0],new Set<String>());
					nextPath.get(pathTab[0]).add(newPath);	
				}
			}
			if(!lookups.isEmpty()){
				buildLookupsMap(lookups);
				for(String lookup:nextPath.keySet()){
					
					//add the lookup in the hierarchy map
					if(lookups.containsKey(lookup)){
						string newSobjectPath = (sobjectPath=='')?lookup:sobjectPath+'.'+lookup;
						SobjectHierarchy h = new SobjectHierarchy(lookups.get(lookup),newSobjectPath);
						h.addPath(nextPath.get(lookup));
						lookupsNextLevel.put(lookup,h);
					}
				}
			}

		}
		
		
		//to build the sobj
		public Map<string,List<Sobject>> buildSobj(Map<string,string> fieldsValMap, Id id){
			Sobject idObj = IdSoqlExecute(id);
			if(idObj != null)
				return buildSobj(fieldsValMap, idObj);
			else
				return null;	
		}
		
		public Map<string,List<Sobject>> buildSobj(Map<string,string> fieldsValMap, Sobject idsObj){
			Map<string,List<Sobject>> ret = new Map<string,List<Sobject>>();
			sobj = mainObjectType.newSobject();
			Sobject tmpObject = idsObj;
			if(sobjectPath != ''){
				List<String> pathTab = sobjectPath.split('\\.');
				for(Integer i=0; i<pathTab.size(); ++i ){
					if(i == pathTab.size()-1){
						sobj.put('id',tmpObject.get(utils.getFieldName(pathTab.get(i))));
					}
					else{
						tmpObject = tmpObject.getSObject(pathTab.get(i));
					}		
				}
			}
			else{
				sobj.put('id',idsObj.get('id'));
			}
			
			Schema.Describesobjectresult oD = mainObjectType.getDescribe();
			Map<String, Schema.SObjectField> fMap = oD.fields.getMap();
			for(String field:fieldsSet){
				string newPath = (sobjectPath=='')?field:sobjectPath+'.'+field;
				if(fieldsValMap.containsKey(newPath)){
					string objType = ''+fMap.get(field).getDescribe().getType();
					try{
						sobj.put(field,Utils_Type.createObject(fieldsValMap.get(newPath),objType));
					}
					catch(Exception e){
					}
				}
			}
			for(SobjectHierarchy h:lookupsNextLevel.values()){
				Map<String, List<Sobject>> recMap = h.buildSobj(fieldsValMap, idsObj);
				for(String recObj: recMap.keySet()){
					if(!ret.containsKey(recObj))
						ret.put(recObj,new List<Sobject>());
					ret.get(recObj).addAll(recMap.get(recObj));	
				}
			}
			if(!ret.containsKey(''+mainObjectType))
				ret.put(''+mainObjectType,new List<sobject>());
			if(sobj.get('id') != null)	
				ret.get(''+mainObjectType).add(sobj);	
			return ret;
		}
		
		//to build the map of lookups
		private void buildLookupsMap(Map<String,Schema.sObjectType> lookups){
			Schema.DescribeSObjectResult oD = mainObjectType.getDescribe();
			Map<String, Schema.SObjectField> fMap = oD.fields.getMap();
			for(string fieldName:lookups.keySet()){
				string SFfieldName = Utils.getFieldName(fieldName);
				if(fMap.containsKey(SFFieldName)){
					Schema.Describefieldresult fD = fMap.get(SFFieldName).getDescribe();
					lookups.put(fieldName,fD.getReferenceTo().get(0));
				}
			}
		}
		
		private Sobject IdSoqlExecute(Id whatId){
			string soql = buildIdSoqlQuery(whatId);
			List<Sobject> sobjLst = Database.query(soql);
			if(!sobjLst.isEmpty()){
				return sobjLst.get(0);
			}
			return null;
		}
		
		private string buildIdSoqlQuery(Id whatId){
			string soql = 'Select ';
			List<String> fieldTab =  buildIdSoql();
			for(Integer i=0; i< fieldTab.size(); ++i){
				soql += fieldTab[i];
				if(i < fieldTab.size()-1)
					soql += ', ';
			}
			soql += ' From '+mainObjectType;
			soql += ' Where id=\''+whatId+'\'';
			return soql;
		}
		
		private List<String> buildIDSoql(){
			List<String> tab = new List<String>();
			for(string lookup: lookupsNextLevel.keySet()){
				string lookupSF = Utils.getFieldName(lookup);
				tab.add(lookupSF);
				for(string str: lookupsNextLevel.get(lookup).buildIdSoql()){
					tab.add(lookup+'.'+str);
				}
			}
			for(string f:fieldsSet)
				tab.add(f);
			return tab;
		}
	}

	class UpdateCellosign{

		private Id signId;
		private string clientIp;
		private string browser;
		private string timeStr;
		private string pdfFileUrl;
		private Map<String, String> attachmentsUrl;

		public UpdateCellosign(Dom.XmlNode node){
			signId = (Id)(node.getChildElement('signatureId',null).getText());
			clientIp = node.getChildElement('ip',null).getText();
			browser = node.getChildElement('browser',null).getText();
			timeStr = node.getChildElement('time',null).getText();
			pdfFileUrl = node.getChildElement('file_url',null).getText();
			Dom.XmlNode attNode = node.getChildElement('attachments',null);
			attachmentsUrl = new Map<String, String>();
			if (attNode != null){
				for (Dom.XmlNode fNode : attNode.getChildElements()){
					string fileUrl = fNode.getChildElement('url',null).getText();
					string fileName = fNode.getChildElement('name',null).getText();
					attachmentsUrl.put(fileUrl, fileName);
				}
			}
		}

		public void exec(){
			Signature__c signature = new Signature__c(Id=signId);
			signature.Signature_Date_and_Time__c = Datetime.now();
			signature.client_IP__c = clientIp;
			signature.browser__c = browser;
			signature.time__c = timeStr;
			signature.signed__c = true;
			DML_CustomExtension.updateObj(signature);
			insertAttachments(signId, pdfFileUrl, attachmentsUrl);
		}

		public Id getSignatureId(){
			return signId;
		}
	}

	@future(Callout=true)
	private static void insertAttachments(Id signId, string pdfFileUrl, Map<String, String> attachmentsUrl){
		List<Attachment> attToInsert = new List<Attachment>();
		attToInsert.add(readFile(pdfFileUrl,signId,'Signed Document'));
		for (string attUrl : attachmentsUrl.keySet()){
			attToInsert.add(readFile(attUrl, signId, attachmentsUrl.get(attUrl)));
		}
		SignaturesRestCall.deleteFilesOnDocomotion(new Set<Id>{signId});
		DML_CustomExtension.insertObj(attToInsert);
	}

	private static Attachment readFile(string url, string parentId){
		return readFile(url, parentId,url.substringAfterLast('/'));
	}
	
	private static Attachment readFile(string url, string parentId, string fileName){
		Attachment att = new Attachment();
		if (fileName.countMatches('.') == 0){
			string extension = url.substringAfterLast('.');
			fileName = fileName+'.'+extension;
		}
		 
		att.Name = fileName;
		att.parentId = parentId;
		att.body = getDocument(url);
		return att;
	}

	private static Blob getDocument(string url){
		Http h = new Http();
		HttpRequest req = new HttpRequest();
		if(url==null)
			return null;
		string first = url.substringBeforeLast('/');
		string fileName = url.substringAfterLast('/');
		string urlDoc2 = first + '/'+EncodingUtil.urlEncode(fileName,'UTF-8');
		req.setEndpoint(urlDoc2);
		req.setMethod('GET');
		req.setTimeout(60000);
		try{
			HttpResponse res = h.send(req);
			system.debug(logginglevel.info,'@@@@@@@@@@@ attachment http res = '+res);
			if(res.getStatusCode() == 200)
				return res.getBodyAsBlob();
			else
				return null;
		}
		catch(Exception e){
        	Utils.log('***Ex: ',e.getMessage());
        	if (Test.isRunningTest()) 
            	return Blob.valueof('test');
        	return null;
    	}  
	}

}