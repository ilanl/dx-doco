@RestResource(urlMapping='/linkgeneration/*')
global with sharing class WS_LinkGeneration {
	
	@HttpGet
    global static void doGet(){
		RestRequest req = RestContext.request;
		RestContext.response.addHeader('Content-Type', 'application/json');

		Id SignatureId = (Id)(req.requestURI.substringAfterLast('/'));
		try{	        
			Signature__c s = [Select Id, signer__c, Signer_lead__c, Record_ID__c, Expired__c, Form__c, published__c, version__r.hasPictureSetting__c From Signature__c Where Id=:SignatureId limit 1];
			if (s.expired__c){
				RestContext.response.statusCode = 410;
				ErrorMessage error = new ErrorMessage('Expired Link');
				RestContext.response.ResponseBody = Blob.valueOf(error.toJson());
			}
			else{
				SchemaFactory.Form f = new SchemaFactory.Form(s.Record_ID__c);
				f.setForm(s.Form__c, !s.published__c);
				f.setFormat('HTML');
				if(s.signer__c != null)
					f.setWhoId(s.signer__c);
				else if(s.Signer_lead__c != null)
					f.setWhoId(s.signer_Lead__c);
				string ffiData = f.getFFIData();
				Utils.log('FFI',ffiData);
				string jsonStr = f.generateJson(ffiData);
				RestContext.response.statusCode = 202;
				RestContext.response.ResponseBody = Blob.valueOf(jsonStr);
			}
		}
		catch (Exception  e){
			ErrorMessage error = new ErrorMessage(e.getMessage());
			RestContext.response.ResponseBody = Blob.valueOf(error.toJson());
			RestContext.response.statusCode = 400;
		}
		
	}

	class ErrorMessage{
		string message;

		public ErrorMessage(string txt){
			message = txt;
		}

		public String toJson(){
			return JSON.serialize(this);
		}
	}
}