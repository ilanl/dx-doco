<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>MasrterRecord on Contact</label>
    <protected>false</protected>
    <values>
        <field>Fields_Name__c</field>
        <value xsi:type="xsd:string">MasterRecordId</value>
    </values>
    <values>
        <field>Sobject__c</field>
        <value xsi:type="xsd:string">Contact</value>
    </values>
</CustomMetadata>
