trigger Form_Before_Update on Form__c (before update) {
	List<Form__c> publishedForms = new List<Form__c>();
	List<Form__c> initVersionForms = new List<Form__c>();
	List<Form__c> activatedForms = new List<Form__c>();

    //to update the preview link
    for(Form__c form:Trigger.new){
        
		Form__c oldForm = trigger.oldMap.get(form.id);
        if(form.status__c=='Published' && form.status__c != oldForm.Status__c){
        	publishedForms.add(form);
        }

		if (form.status__c=='Activating' && form.status__c != oldForm.Status__c){
			form.Publish_Error__c = null;
		}

		if (form.status__c=='Active' && form.status__c != oldForm.Status__c)
		{
			activatedForms.add(form);
		}

        if(form.World_Document_FFX_ID__c!=null && oldForm.World_Document_FFX_ID__c == null){
        	initVersionForms.add(form);
        }
    }
    
	//To Create Version
	if(!activatedForms.isEmpty()){
		TR006_PublishForms.createVersionOnActive(activatedForms);
	}

    //For the edit fields.
    if(!publishedForms.isEmpty()){
    	TR006_PublishForms.createVersionOnPublish(publishedForms);
    }
    
    if(!initVersionForms.isEmpty()){
    	TR001_VersionUpdate.initVersions(initVersionForms);
    }

    
}