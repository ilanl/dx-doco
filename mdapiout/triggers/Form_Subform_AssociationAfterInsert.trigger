trigger Form_Subform_AssociationAfterInsert on Form_Subform_Association__c (after insert) {
	Map<Id,Set<Id>> formAMap = new Map<Id,Set<Id>>();
	for(Form_Subform_Association__c a: Trigger.new){
		if(!formAMap.containsKey(a.form__c))
			formAMap.put(a.form__c,new Set<Id>());
		formAMap.get(a.form__c).add(a.id);	
	}
	//To add the surveys
	TR004_InteractiveAssociations.createAssociations(trigger.new);
	
	//To update the XML
	TR013_RegenerateXML.regenerateFormXML(formAMap.keySet());	
	
}