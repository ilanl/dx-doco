trigger Form_Subform_AssociationAfterUpdate on Form_Subform_Association__c (after update) {
	//To update the dataschema
	Set<Id> formIds = new Set<Id>();
	for(Form_Subform_Association__c a: Trigger.new){
		formIds.add(a.form__c);
	}
	if(!formIds.isEmpty())
		TR013_RegenerateXML.regenerateFormXML(formIds);
}