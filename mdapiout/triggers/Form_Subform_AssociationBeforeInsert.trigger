trigger Form_Subform_AssociationBeforeInsert on Form_Subform_Association__c (before insert) {
	//When we insert an association, we have to fill the subform version lookup field
	TR009_VersionInit.addPublishedVersionLookup(Trigger.new);
}