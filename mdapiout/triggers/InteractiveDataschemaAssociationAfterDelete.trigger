trigger InteractiveDataschemaAssociationAfterDelete on Interactive_Dataschema_Association__c (after delete) {
	TR004_InteractiveAssociations.deleteAssociations(Trigger.old);
}