trigger InteractiveDataschemaAssociationAfterInsert on Interactive_Dataschema_Association__c (after insert) {
	TR004_InteractiveAssociations.createAssociations(Trigger.new);
}