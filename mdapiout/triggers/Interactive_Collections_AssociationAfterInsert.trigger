trigger Interactive_Collections_AssociationAfterInsert on Interactive_Collections_Association__c (after insert) {
	Set<Id> formsIds = new Set<Id>();
	for(Interactive_Collections_Association__c a:trigger.new){
		formsIds.add(a.form__c);
	}
	if(!formsIds.isEmpty()){
		TR013_RegenerateXML.regenerateFormXML(formsIds);
	}
}