trigger Interactive_Field_AfterDelete on Interactive_Field__c (after delete) {
	/**To syncronize the form that has been associated with the Interactive collection of the field**/
	TR010_InteractiveFieldsUpdate.regenerateDataschemaXML(Trigger.old, true);
}