trigger Interactive_Field_AfterInsert on Interactive_Field__c (after insert) {
	/**To syncronize the form that has been associated with the Interactive collection of the field**/
	TR010_InteractiveFieldsUpdate.regenerateDataschemaXML(Trigger.new, false);
}