trigger Mass_Generation_Execution on Mass_Generation_Execution__c (after update)  {

	if(Trigger.isAfter){
		if(Trigger.isUpdate){
			MassGenerationHandler.generateFiles(Trigger.new, Trigger.oldMap);
		}
	}

}