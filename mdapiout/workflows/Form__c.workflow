<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Status_Edit</fullName>
        <field>Status__c</field>
        <literalValue>Edit</literalValue>
        <name>Status Edit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>form_ID_From_Form_Number</fullName>
        <field>Form_Id__c</field>
        <formula>VALUE(Name)</formula>
        <name>form ID From Form Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>immediatePublished_false</fullName>
        <field>Immediate_Publish__c</field>
        <literalValue>0</literalValue>
        <name>immediatePublished false</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>status_equals_published</fullName>
        <field>Status__c</field>
        <literalValue>Published</literalValue>
        <name>status equals published</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Form On Insert</fullName>
        <actions>
            <name>form_ID_From_Form_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Immediate Publish</fullName>
        <actions>
            <name>immediatePublished_false</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>status_equals_published</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(   ISPICKVAL(Status__c, &apos;Active&apos;),  Immediate_Publish__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Version Upgrade</fullName>
        <actions>
            <name>Status_Edit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>When we upgrade the version the status become &quot;Edit&quot;</description>
        <formula>AND(  ISCHANGED( Current_Version__c ),  NOT(ISCHANGED(Status__c)),  ISPICKVAL( Status__c , &apos;Published&apos;) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
