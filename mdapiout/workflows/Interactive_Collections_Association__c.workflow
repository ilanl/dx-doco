<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>unique_Constraint</fullName>
        <field>unique__c</field>
        <formula>Form__c +&apos;|&apos;+ collection__c</formula>
        <name>unique Constraint</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Interactive Related Form Unique</fullName>
        <actions>
            <name>unique_Constraint</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
