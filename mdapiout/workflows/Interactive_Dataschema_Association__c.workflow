<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Dataschema_Interective_unique</fullName>
        <field>unique__c</field>
        <formula>Dataschema__c +&apos;|&apos;+ Interactive_Form__c</formula>
        <name>Dataschema Interective unique</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Interactive Related Data schema unique</fullName>
        <actions>
            <name>Dataschema_Interective_unique</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
