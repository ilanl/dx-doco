<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Generation_Asinc_Report_Message</fullName>
        <description>Generation Asinc Report Message</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Docomotion_Emails/Async_Generation_Template</template>
    </alerts>
    <alerts>
        <fullName>Generation_Report_Message</fullName>
        <description>Generation Report Message</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Docomotion_Emails/Mass_Generation_Template</template>
    </alerts>
    <rules>
        <fullName>Generation Asynchonous on Complete</fullName>
        <actions>
            <name>Generation_Asinc_Report_Message</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mass_Generation_Execution__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Generation Asynchonous</value>
        </criteriaItems>
        <criteriaItems>
            <field>Mass_Generation_Execution__c.Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Mass Generation on Complete</fullName>
        <actions>
            <name>Generation_Report_Message</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Mass_Generation_Execution__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Mass Generation</value>
        </criteriaItems>
        <criteriaItems>
            <field>Mass_Generation_Execution__c.Completed__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
